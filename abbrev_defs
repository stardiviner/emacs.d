;;-*-coding: utf-8;-*-
(define-abbrev-table 'global-abbrev-table
  '(
    ("M1" "Apple Silicon M1 ARM CPU" nil :count 2)
    ("myemail" "numbchild@gmail.com" nil :count 0)
    ("mygmail" "numbchild@gmail.com" nil :count 0)
    ("mygmailformat" "numbchild[@]{gmail}.com" nil :count 0)
    ("myhomepage" "http://stardiviner.github.io" nil :count 0)
    ("mynick" "stardiviner" nil :count 0)
    ("myqq" "348284894" nil :count 0)
    ("mytwitter" "@numbchild" nil :count 0)
    ("opensuse" "openSUSE" nil :count 1)
    ("youtube" "YouTube" nil :count 1)
   ))

