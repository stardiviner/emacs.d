;;; init.el --- --- user init file  -*- lexical-binding: t; -*-

;;; Commentary:


;;; Code:

;;; load ahead

;;; increase GC at Emacs startup to speedup.
(defvar emacs-start-time (float-time))
(setq gc-cons-threshold 8000000)
(add-hook
 'after-init-hook
 (lambda ()
   (setq gc-cons-threshold (car (get 'gc-cons-threshold 'standard-value)))))


;;; Emacs splash
(setq fancy-splash-image ; for `fancy-splash-head'
      (expand-file-name "resources/logos/my-emacs-logo.png" user-emacs-directory))
;; (setq fancy-startup-text)

;;; Emacs initial message
(setq inhibit-startup-echo-area-message "Hacking happy! stardiviner.")
(setq-default initial-scratch-message
              (concat ";; Happy Hacking " (or user-login-name "") "!\n\n"))

;;; [ server (daemon) mode for command "emacsclient" ]

;; Same effect as command "$ emacs --daemon", it is used for command "emacsclient".

(require 'server)
;; (setq server-use-tcp t) ; allow for running multiple Emacs daemons.
(unless (server-running-p)
  (server-start))
;; detect SSH keys
(if (getenv "SSH_AUTH_SOCK")
    (message "Emacs server.el detected SSH keys from $SSH_AUTH_SOCK: %s" (getenv "SSH_AUTH_SOCK"))
  (user-error "Emacs server.el CAN'T detected SSH keys from $SSH_AUTH_SOCK"))


;;; [ Emacs init load-path ]

(add-to-list 'load-path "/usr/share/emacs/site-lisp/") ; for compiled version Emacs load system installed Emacs related packages.

(let* ((my-init-path (expand-file-name "init" user-emacs-directory))
       (default-directory my-init-path))
  (add-to-list 'load-path my-init-path)
  ;; recursively load init files.
  (normal-top-level-add-subdirs-to-load-path))

(setq load-prefer-newer t)

;;; [ Emacs package manager ]

(load "init-package")

;;; debug, profiling etc

(load "init-emacs-debug")
(load "init-emacs-profiler")
(load "init-emacs-benchmark")

;;; [ pinentry ] --- GnuPG Pinentry server implementation

(use-package pinentry
  :ensure t
  :if (executable-find "pinentry")
  :init (pinentry-start))

;;; my custom functions

(use-package dash
  :ensure t
  :config (with-eval-after-load "dash"
            (dash-enable-font-lock)))

(load "init-library")
(load "init-functions")

;;; [ Systems ]

(cl-case system-type
  (gnu/linux
   (load "init-linux"))
  (darwin
   (load "init-macOS")
   (load "init-linux"))
  (windows-nt
   (load "init-microsoft-windows")))

;;; [ System Package Manager & Configuration Tools ]

(load "init-package-manager")

;;; [ High Performance Computing (HPC) ]

;; (load "init-HPC")

;;; [ Emacs Settings ]
(load "init-emacs-environment")
(load "init-emacs-settings")
(load "init-emacs-encrypt")
(load "init-emacs-performance")
(load "init-emacs-security")
(load "init-emacs-encoding")
(load "init-emacs-font")
(load "init-emacs-face")
(load "init-emacs-theme")
(load "init-emacs-appearance")
(load "init-emacs-prettify")
(load "init-emacs-emoji")
(load "init-emacs-mode-line")
(load "init-emacs-popup")
(load "init-emacs-completion")
(load "init-emacs-text-complete")
(load "init-emacs-notify")
(load "init-emacs-help")
(load "init-emacs-minibuffer")
(load "init-emacs-header-line")
(load "init-emacs-buffer")
(load "init-emacs-backup")
(load "init-emacs-frame")
(load "init-emacs-window")
(load "init-emacs-tab-bar")
(load "init-emacs-idle")
(load "init-emacs-edit")
(load "init-emacs-region")
(load "init-emacs-clipboard")
(load "init-emacs-navigation")
(load "init-emacs-bookmark")
(load "init-emacs-annotate")
(load "init-emacs-keybinding")
(load "init-emacs-outline")
(load "init-emacs-macro")
;; (load "init-emacs-input-method")
(load "init-emacs-file")
(load "init-emacs-color")
(load "init-emacs-image")
(load "init-emacs-pdf")
(load "init-emacs-djvu")
(load "init-emacs-ebook")
(load "init-dired")
(load "init-emacs-modes")
(load "init-emacs-abbrev")
(load "init-emacs-search")
(load "init-emacs-highlight")
(load "init-emacs-regex")
(load "init-emacs-overlay")
(load "init-emacs-terminal")
(load "init-emacs-shell")
(load "init-emacs-comint")
(load "init-emacs-subprocess")
(load "init-emacs-rpc")
(load "init-emacs-network")
;; (load "init-emacs-xwidget")
(load "init-emacs-accessibility")
(when (eq system-type 'gnu/linux)
  (load "init-eaf"))

;;; [ hypertextual information management system ]

(load "init-org-mode")

;;; [ Vim ]

;; (load "init-vim")

;;; [ Natural Languages ]

(load "init-languages")
(load "init-language-english")
(load "init-language-chinese")
;; (load "init-language-japanese")

;;; [ Tools ]
(unless (boundp 'tools-prefix)
  (define-prefix-command 'tools-prefix))
(global-set-key (kbd "C-x t") 'tools-prefix)

(load "init-tool-calendar")
(load "init-tool-dictionary")
(load "init-tool-translate")
(load "init-tool-ocr")
(load "init-tool-clock")
;; (load "init-tool-pomodoro")
;; (load "init-tool-speak")
(load "init-tool-calculator")
(load "init-tool-keyboard")
(load "init-tool-remote")
(load "init-tool-tmux")
(load "init-tool-hex")
(load "init-tool-diagram")
(load "init-tool-ascii")
(load "init-tool-painting")
(load "init-tool-network")
(load "init-tool-browser")
(load "init-tool-downloader")
(load "init-tool-sync")
(load "init-tool-NAS")
(load "init-tool-reading")
(load "init-tool-email")
(load "init-tool-rss")
(load "init-tool-podcast")
(load "init-tool-read-it-later")
(load "init-tool-contacts")
(load "init-tool-password-manager")
(load "init-tool-accounting")
(load "init-tool-paste")
(load "init-tool-irc")
;; (load "init-communication")
(load "init-tool-music")
(load "init-tool-lyrics")
;; (load "init-tool-subtitle")
(load "init-tool-audio")
(load "init-tool-video")
;; (load "init-tool-screenshot")
;; (load "init-tool-screencast")
;; (load "init-stack-exchange")
;; (load "init-tool-social-network")
;; (load "init-tool-weather")
(load "init-tool-utilities")
;; (load "init-leetcode")
(load "init-tool-uncensorship")
(load "init-tool-log")
(load "init-tool-AI")

;;; Programming
(load "init-prog-programming")
(load "init-prog-license")
(load "init-prog-code")
(load "init-prog-comment")
(load "init-prog-indent")
(load "init-prog-folding")
(load "init-prog-sense")
;; (load "init-prog-parser")
;;; fix issue which `company-rtags' backend is before `company-irony'.
(load "init-prog-snippet")
(load "init-prog-template")
(load "init-prog-sidebar")
(load "init-prog-document")
(load "init-prog-eval")
(load "init-prog-compile")
(load "init-prog-build-system")
(load "init-prog-task-management")
(load "init-prog-lint")
(unless (boundp 'debug-prefix)
  (define-prefix-command 'debug-prefix))
(global-set-key (kbd "C-c d") 'debug-prefix)
(load "init-prog-debugger")
(load "init-prog-profiler")
(load "init-prog-test")
;; (load "init-prog-test-coverage")
(load "init-prog-reformat")
(load "init-prog-refactor")
(load "init-prog-project")
(load "init-prog-vcs")
;; (load "init-prog-bug-track-system")

;;; Programming Languages
(load "init-prog-lsp")

(load "init-prog-lang-lisp")
(load "init-prog-lang-emacs-lisp")
(load "init-prog-lang-common-lisp")
(load "init-prog-lang-scheme")
;; (load "init-prog-lang-racket")
;; (load "init-prog-lang-newLisp")
;; (load "init-prog-lang-shen")
(load "init-prog-lang-clojure")
(load "init-prog-lang-python")
(load "init-prog-lang-ruby")
;; (load "init-prog-lang-perl")
(load "init-prog-lang-shell")
(load "init-prog-lang-C-common")
;; (load "init-prog-lang-dotnet")
;; (load "init-prog-lang-D")
;; (load "init-prog-lang-go")
(load "init-prog-lang-rust")
;; (load "init-prog-lang-nim")
(load "init-prog-lang-lua")
(when (eq system-type 'darwin)
  (load "init-prog-lang-swift"))
(load "init-prog-lang-java")
;; (load "init-prog-lang-kotlin")
;; (load "init-prog-lang-jvm-groovy")
;; (load "init-prog-lang-php")
(load "init-prog-lang-html")
(load "init-prog-lang-css")
(load "init-prog-lang-javascript")
;; (load "init-prog-lang-coffeescript")
;; (load "init-prog-lang-sibilant")
;; (load "init-prog-lang-dart")
;; (load "init-prog-lang-sdlang")
;; (load "init-prog-lang-haskell")
;; (load "init-prog-lang-ML")
;; (load "init-prog-lang-scala")
;; (load "init-prog-lang-elixir")
;; (load "init-prog-lang-erlang")
(load "init-prog-lang-R")
(load "init-prog-lang-julia")
;; (load "init-prog-lang-gnuplot")
;; (load "init-prog-lang-octave")
(load "init-prog-lang-matlab")
;; (load "init-prog-lang-wolfram")
;; (load "init-prog-lang-prolog")
;; (load "init-prog-lang-ocaml")
;; (load "init-prog-lang-fortran")
;; (load "init-prog-lang-verilog")
;; (load "init-prog-lang-assembly")
;; (load "init-prog-lang-forth")
;; (load "init-prog-lang-HDL")
;; (load "init-prog-lang-solidity")

;;; [ Markup-based Typesetting System ]

(load "init-markup")
(load "init-markup-TeX")
;; (load "init-bibliography")
(load "init-markup-Markdown")
;; (load "init-markup-reStructuredText")
;; (load "init-markup-AsciiDoc")

;;; [ Data Formats ]

;; (load "init-xml")
(load "init-json")
(load "init-csv")
;; (load "init-rdf")
;; (load "init-yaml")
;; (load "init-toml")
(load "init-data-query")

;;; [ Query Languages ]

(load "init-SQL")
;; (load "init-NewSQL")
(load "init-NoSQL")
;; (load "init-CQL")
(load "init-GraphQL")

;;; [ Programming Tools ]

(unless (boundp 'prog-tools-prefix)
  (define-prefix-command 'prog-tools-prefix))
(global-set-key (kbd "C-c t") 'prog-tools-prefix)

(load "init-DevOps")
;; (load "init-elasticsearch")

;;; [ Frameworks ]

(load "init-prog-framework-web")
;; (load "init-prog-web-browser")
(load "init-HTTP-RESTful")
;; (load "init-prog-framework-ruby-on-rails")
;; (load "init-prog-framework-android")
(load "init-prog-framework-IoT")
;; (load "init-serial-programming")
;; (load "init-prog-framework-qt")
;; (load "init-prog-framework-ethereum")

;;; [ Data Science ]

(load "init-data-science")

;;; [ Science ]

;; (load "init-academic")
;; (load "init-math")
;; (load "init-physics")
;; (load "init-chemistry")
;; (load "init-biology")
;; (load "init-musician")

;;; [ Engineering ]

;; (load "init-electronic")
;; (load "init-electric-music")

;;; [ Hack ]

;; (load "init-reverse-engineering")

;;; [ Authoring & Writing ]

(load "init-text-checker")
(load "init-authoring")

;;; [ Chef & Cooking ]

(load "init-chef")

;;; [ Games ]

;; (load "init-games")



(load "init-emacs-workspace")

;;; detect external system has Emacs process running?
;;; If yes, like `bug-hunter' is running. Then don't load session.
;; (progn
;;   (defun how-many-emacs ()
;;     (cl-case system-type
;;       (gnu/linux
;;        (let* ((ps-output (shell-command-to-string "ps -C emacs -o pid="))
;;               (ps-list (split-string ps-output))
;;               (ps-proc-num (length ps-list)))
;;          ps-proc-num))
;;       (darwin
;;        (let* ((ps-output (shell-command-to-string "ps aux | grep Emacs.app | grep -v grep"))
;;               (ps-list (delq nil
;;                              (mapcar (lambda (line)
;;                                        (if (string-empty-p line) nil line))
;;                                      (split-string ps-output "\n"))))
;;               (ps-proc-num (length ps-list)))
;;          ps-proc-num))))
;;   (let ((emacs-processes (how-many-emacs)))
;;     (when (or (null emacs-processes) (<= emacs-processes 1))
;;       (load "init-emacs-session"))))

;;; [ playground ] -- Manage sandboxes for alternative Emacs configurations.

;; (use-package playground
;;   :ensure t
;;   :defer t
;;   :commands (playground-checkout playground-checkout-with-options))

;;; [ splash ]

;; (defun stardiviner-splash-animation ()
;;   "Show ASCII animation."
;;   (animate-sequence '("Fuck this shit world!"
;;                       "Author: stardiviner"
;;                       "Date: 2011/10/0 (yes, day 0!)") 0)
;;   (kill-buffer "*Animation*"))
;; (add-hook 'after-init-hook #'stardiviner-splash-animation)


;; (defun my/fancy-startup-screen ()
;;   "My custom `fancy-startup-screen'."
;;   (interactive)
;;   (let ((splash-buffer (get-buffer-create "*GNU Emacs*")))
;;     (with-current-buffer splash-buffer
;;       (let ((inhibit-read-only t))
;;         (erase-buffer)
;;         (setq default-directory command-line-default-directory)
;;         (make-local-variable 'startup-screen-inhibit-startup-screen)
;;         (if pure-space-overflow
;;             (insert pure-space-overflow-message))
;;         (fancy-splash-head)
;;         (dolist (text fancy-startup-text)
;;           (apply #'fancy-splash-insert text)
;;           (insert "\n"))
;;         (skip-chars-backward "\n")
;;         (delete-region (point) (point-max))
;;         (insert "\n"))
;;       (setq tab-width 22
;;             buffer-read-only t)
;;       (set-buffer-modified-p nil)
;;       (if (and view-read-only (not view-mode))
;;           (view-mode-enter nil 'kill-buffer))
;;       (goto-char (point-min))
;;       (forward-line))
;;     (progn
;;       (split-window-below)
;;       (switch-to-buffer splash-buffer))))
;;
;; (add-hook 'after-init-hook #'my/fancy-startup-screen)

;; Use a hook so the message doesn't get clobbered by other messages.
(add-hook 'emacs-startup-hook
          (lambda ()
            (message "Emacs ready in %s with %d garbage collections."
                     (format "%.2f seconds"
                             (float-time (time-subtract after-init-time before-init-time)))
                     gcs-done)))

;;; init.el ends here
(put 'list-timers 'disabled nil)
