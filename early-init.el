;;; This file is introduced in Emacs 27.1

;;; Emacs can now be configured using an early init file.
;;;
;;; The file is called 'early-init.el', in 'user-emacs-directory'.  It is
;;; loaded very early in the startup process: before graphical elements
;;; such as the tool bar are initialized, and before the package manager
;;; is initialized.  The primary purpose is to allow customizing how the
;;; package system is initialized given that initialization now happens
;;; before loading the regular init file (see below).

;;; [ Debug ]
;; for Emacs startup freeze debug.
;; (setq debug-on-quit t)
;; (add-hook 'after-init-hook #'(lambda () (setq debug-on-quit nil)))
(setq debug-on-error t)
(add-hook 'after-init-hook #'(lambda () (setq debug-on-error nil)))

;; (setq package-user-dir
;;       (let ((elpa-dir-name (format "elpa_%s" emacs-major-version))) ;default = "elpa"
;;         (file-name-as-directory (expand-file-name elpa-dir-name user-emacs-directory))))

(setq package-install-upgrade-built-in t)

;;; [ frame appearance ]

;; (setq default-frame-alist
;;       `((top . 0) (left . 0)
;;         (min-height . 1) (min-width . 1)
;;         ;; don't use full screen size (which have not considered KDE panel space)
;;         ;; (height . ,(display-pixel-height))
;;         ;; (width . ,(display-pixel-width))
;;         (left-fringe . 0) (right-fringe . 0)
;;         (ns-transparent-titlebar . t)      ; transparent title bar
;;         ;; (ns-appearance . light)            ; default light appearance
;;         (alpha-background . 85)            ; true alpha transparent
;;         ;; (undecorated . t)                  ; no title bar
;;         ;; (undecorated-round . t)            ; round corners
;;         (tool-bar-lines . 0)               ; no tool bar
;;         (menu-bar-lines . 0)               ; no menu bar
;;         (scroll-bar . nil)                 ; no scroll bar
;;         (vertical-scroll-bars . nil)       ; no vertical scroll bar
;;         ;; (internal-border-width . 8)        ; internal border width
;;         (internal-border-color . "orange") ; internal border color
;;         (right-divider-width . 1)          ; window divider line width
;;         ))

;; (modify-all-frames-parameters default-frame-alist)

;;; Emacs GUI
(require 'cl-lib)
(cl-case system-type
  (darwin
   ;; (toggle-menu-bar-mode-from-frame)
   ;; (setq-default ns-auto-hide-menu-bar nil) ; auto hide macOS menubar, but appears when mouse is hover on hide area.
   (when (fboundp 'menu-bar-mode) (menu-bar-mode -1)))
  (gnu/linux
   (when (fboundp 'menu-bar-mode) (menu-bar-mode -1))))
(when (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(when (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

;; frame title
(setq-default ns-use-proxy-icon nil)
(setq-default icon-title-format nil)
;; (setq-default frame-title-format nil)
;; (setq frame-resize-pixelwise nil)

;; Let other frames inherit from `default-frame-alist'.
(setq initial-frame-alist default-frame-alist)
(setq window-system-default-frame-alist default-frame-alist)
(setq minibuffer-frame-alist default-frame-alist)

;; Every time a window is started, make sure it get maximized
;; (add-to-list 'default-frame-alist '(fullscreen . maximized))

(provide 'early-init)
