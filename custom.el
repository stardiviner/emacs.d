
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-show-quick-access nil nil nil "Customized with use-package company")
 '(connection-local-criteria-alist
   '(((:application tramp :protocol "kubernetes")
      tramp-kubernetes-connection-local-default-profile)
     ((:application eshell) eshell-connection-default-profile)
     ((:application tramp :protocol "flatpak")
      tramp-container-connection-local-default-flatpak-profile
      tramp-flatpak-connection-local-default-profile)
     ((:application tramp :machine "localhost")
      tramp-connection-local-darwin-ps-profile)
     ((:application tramp :machine "Mac-mini.local")
      tramp-connection-local-darwin-ps-profile)
     ((:application tramp) tramp-connection-local-default-system-profile
      tramp-connection-local-default-shell-profile)))
 '(connection-local-profile-alist
   '((tramp-flatpak-connection-local-default-profile
      (tramp-remote-path "/app/bin" tramp-default-remote-path "/bin" "/usr/bin"
                         "/sbin" "/usr/sbin" "/usr/local/bin" "/usr/local/sbin"
                         "/local/bin" "/local/freeware/bin" "/local/gnu/bin"
                         "/usr/freeware/bin" "/usr/pkg/bin" "/usr/contrib/bin"
                         "/opt/bin" "/opt/sbin" "/opt/local/bin"))
     (tramp-kubernetes-connection-local-default-profile
      (tramp-config-check . tramp-kubernetes--current-context-data)
      (tramp-extra-expand-args 97
                               (tramp-kubernetes--container
                                (car tramp-current-connection))
                               104
                               (tramp-kubernetes--pod
                                (car tramp-current-connection))
                               120
                               (tramp-kubernetes--context-namespace
                                (car tramp-current-connection))))
     (eshell-connection-default-profile (eshell-path-env-list))
     (tramp-container-connection-local-default-flatpak-profile
      (tramp-remote-path "/app/bin" tramp-default-remote-path "/bin" "/usr/bin"
                         "/sbin" "/usr/sbin" "/usr/local/bin" "/usr/local/sbin"
                         "/local/bin" "/local/freeware/bin" "/local/gnu/bin"
                         "/usr/freeware/bin" "/usr/pkg/bin" "/usr/contrib/bin"
                         "/opt/bin" "/opt/sbin" "/opt/local/bin"))
     (tramp-connection-local-darwin-ps-profile
      (tramp-process-attributes-ps-args "-acxww" "-o"
                                        "pid,uid,user,gid,comm=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                        "-o" "state=abcde" "-o"
                                        "ppid,pgid,sess,tty,tpgid,minflt,majflt,time,pri,nice,vsz,rss,etime,pcpu,pmem,args")
      (tramp-process-attributes-ps-format (pid . number) (euid . number)
                                          (user . string) (egid . number)
                                          (comm . 52) (state . 5)
                                          (ppid . number) (pgrp . number)
                                          (sess . number) (ttname . string)
                                          (tpgid . number) (minflt . number)
                                          (majflt . number)
                                          (time . tramp-ps-time) (pri . number)
                                          (nice . number) (vsize . number)
                                          (rss . number) (etime . tramp-ps-time)
                                          (pcpu . number) (pmem . number) (args)))
     (tramp-connection-local-busybox-ps-profile
      (tramp-process-attributes-ps-args "-o"
                                        "pid,user,group,comm=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                        "-o" "stat=abcde" "-o"
                                        "ppid,pgid,tty,time,nice,etime,args")
      (tramp-process-attributes-ps-format (pid . number) (user . string)
                                          (group . string) (comm . 52)
                                          (state . 5) (ppid . number)
                                          (pgrp . number) (ttname . string)
                                          (time . tramp-ps-time) (nice . number)
                                          (etime . tramp-ps-time) (args)))
     (tramp-connection-local-bsd-ps-profile
      (tramp-process-attributes-ps-args "-acxww" "-o"
                                        "pid,euid,user,egid,egroup,comm=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                        "-o"
                                        "state,ppid,pgid,sid,tty,tpgid,minflt,majflt,time,pri,nice,vsz,rss,etimes,pcpu,pmem,args")
      (tramp-process-attributes-ps-format (pid . number) (euid . number)
                                          (user . string) (egid . number)
                                          (group . string) (comm . 52)
                                          (state . string) (ppid . number)
                                          (pgrp . number) (sess . number)
                                          (ttname . string) (tpgid . number)
                                          (minflt . number) (majflt . number)
                                          (time . tramp-ps-time) (pri . number)
                                          (nice . number) (vsize . number)
                                          (rss . number) (etime . number)
                                          (pcpu . number) (pmem . number) (args)))
     (tramp-connection-local-default-shell-profile (shell-file-name . "/bin/sh")
                                                   (shell-command-switch . "-c"))
     (tramp-connection-local-default-system-profile (path-separator . ":")
                                                    (null-device . "/dev/null"))))
 '(custom-safe-themes
   '("9df07329bb7ba73ae5851c21ddab709a0da884b8eba3cabea0c0974ce5e06f79"
     "d0bbfb9fc5b0f0c05982b8acfeaecd86097f2476c428048231df951854944492"
     "1f47b09e8d9905b69142a36f500d2c0277aea433f8a9e81438ec42b9fc460e1b"
     "10ae98f1bc52fd227fea56f0b149ea34928cb67ff5c489b90e47261646c2d5c7"
     "4dea7ab78d706a3b42ad98c7378953e7e1f35adacda06d2cc2b4c772779f87ea"
     "97bbe3ccae29399432940e10ac751a50359a75811a65ac8f6a7ddf8448e7e035"
     "64d17c018897282af7fcb9b88e586b0f8a334bb9952dc9e1f9532c212f55f255"
     "ad38a6e5ff60d34a6b608995d594a1297b661a3df19bb309b368a49730993b15"
     "990e24b406787568c592db2b853aa65ecc2dcd08146c0d22293259d400174e37"
     "54cf3f8314ce89c4d7e20ae52f7ff0739efb458f4326a2ca075bf34bc0b4f499"
     "74ba9ed7161a26bfe04580279b8cad163c00b802f54c574bfa5d924b99daa4b9"
     "0dd2666921bd4c651c7f8a724b3416e95228a13fca1aa27dc0022f4e023bf197"
     "b73a23e836b3122637563ad37ae8c7533121c2ac2c8f7c87b381dd7322714cd0"
     "6dc922f9335b44aa6203a7c58e25b34cc08995c720574e5c5717520102739489"
     "c4aa8fbfb87d000325392643c900c6ce4c37bc5213769c276c2b3f6b94319bfa"
     "cdfda25b6a008fe4c3eacbe95fb457422c38e86fbfacaceb31a6872f7352bb9a"
     "603a831e0f2e466480cdc633ba37a0b1ae3c3e9a4e90183833bc4def3421a961"
     "a3c45956260bde84d3a53447d4ac0f5453fce3b29543d58295029b6b5d47662f"
     "73b6fb50100174334d220498186ab5ca3ade90052f5a08e8262e5d7820f0a149"
     "6c39407218102c48de9617b5ae865e2058ef99410955b255cc9a63b591a46797"
     "06751bf6cad9ef8ee0e43e15ee6150556040a50a54e95d0a1eb86a352e94464d"
     "a06ac7f32de9715e0899a9a54d9df473f5c429af2ea68194ebd740e8b83c987d"
     "bf06fb33cc811cd05e21e11f6b6cde92c96e34c6bd2b39fcc17c5bda71aa67f8"
     "8a81f47cd347dc84828b42abb558898d4d6482d9e464990d88fe0e5e36287d80"
     "1434552ec945ed44b6efdaf745c73fe0e71f40b170b64f175149e8092796e1b5"
     "a9c822e83fdf9dc7127a5a87d8e2483943d31fa9f6107e645aa91af1aca308fa"
     "c60082bf91b4725c51432bbfa4f43d241ece70da7ff1b2ee3a4713ff1a7a7583"
     "a7b33ff1b21048e2c1f187be6708362110ba02be0be0f0d155bea39b11f1597f"
     "7ca0b8b8122af3d737a16d681d1ef37492e21a87040a260264583da6bfb2596d"
     "80b00f3bf7cdbdca6c80aadfbbb03145f3d0aacf6bf2a559301e61109954e30a"
     "4e0d634f5409e6de1e2e0273c47a8cdb46f1c03e436884e433a865c045dd21ce"
     "1949d1d612f7331f559a98216cd8bda0fba5fbaf7449297b0db2283cfc40d3e8"
     "0569b773f4f7b94b58d844debb644599769b20f3f9ecb3c86ba5226b34ef6087"
     "1925b028d855d5517cd8eb97497cf4fbb0cac466c122d6dfc4bac9b843e6869d"
     "97ef2fe48a437ea2e734556d5acf4c08c74647c497a952c1ae8571a71369f7a7"
     "88f7ee5594021c60a4a6a1c275614103de8c1435d6d08cc58882f920e0cec65e"
     "5f128efd37c6a87cd4ad8e8b7f2afaba425425524a68133ac0efd87291d05874"
     "77ebe4ddd6c11ed554dc46b287660a26398bcb66a32b56e85aa8288061feb45a"
     "ea7b97f6286c2637aa412467c2c1f40c6c04214b9b27dc14c7404a9ca12a817b"
     "ca70827910547eb99368db50ac94556bbd194b7e8311cfbdbdcad8da65e803be"
     "c086fe46209696a2d01752c0216ed72fd6faeabaaaa40db9fc1518abebaf700d"
     "9506141cceda0a866a056505497455254d8c34d6f2ff8b4b64ac6edf33127f77"
     "1781e8bccbd8869472c09b744899ff4174d23e4f7517b8a6c721100288311fa5"
     "de8f2d8b64627535871495d6fe65b7d0070c4a1eb51550ce258cd240ff9394b0"
     "e7820b899036ae7e966dcaaec29fd6b87aef253748b7de09e74fdc54407a7a02"
     "9031991af43f3a15e2b1fab62b5a0a985c46f24e596385f05bbf87770347d8ed"
     "7605a1e28ee0e86ae83730b5339f9d87bfb5052fac3475c37af786c43b552166"
     "a1b3cb714307bcf00b7ab40bbc5e404f9a690c2fff5485e4f7421a6e87502d40"
     "2a038eacdebb8cf0d2acb8349ce37a45ad971b331018d04552d5e80b7a99d04b"
     "371cb93a42723a8f147d34093c27475edfae40f2844ecaf6876506519db1b36b"
     "aaa4c36ce00e572784d424554dcc9641c82d1155370770e231e10c649b59a074"
     "05f24f8a60ae4f3c9bc5a69e7ee8d7215cfde011f8aa789baecf62f527b1952b"
     "71e32bfe1743dc081203e2542e06cf1811d6b6a16eeefeb8a284f1b87750b6d9"
     "5ff1ef0dca26934025f55f5940d0004e4adfcbfc351598c937edcb6c460dd14c"
     "15d0b838b7f70f49822e51c050907c2e52c5920d046f065d2915aa793c4280e6"
     "fce3524887a0994f8b9b047aef9cc4cc017c5a93a5fb1f84d300391fba313743"
     "0f8472aadf848c12f64d77ead3e6283618ab951e5ac6d80f5afef1a25d414a74"
     "e6ff132edb1bfa0645e2ba032c44ce94a3bd3c15e3929cdf6c049802cf059a2a"
     "c0346f24bc2ee22c35b1790ada0a34f8436e97901eb8ec2c324209859f745c69"
     "8af13d4d122a9edbace0f2017aa21fc05c6d3e8be5e79eb0642f395385602293"
     "51d91bbff9684535af6f55f25de10791e64ce99d6a03d988498f4ab88a0d6040"
     "279ded78b7a46d585d8697f63db71a012b8e53c49266755818046fe1a6d66806"
     "e65903c2a04a33e2b3da9b8738c0798ba14b98105bb4c7ae0c766b0843fd2cbc"
     "ac8f67ca99ca392b28a2a90f879267e3fc8ed022c697efde752489af6288749f"
     "4e70984dd92b10db2ac1dcfa022363226a2b82c50dfcd75fa890da53e8ba6893"
     "df7d4db1e226fc0b4354f7d4e52604876c5cb25a77ccd7dd1d56153f45f28228"
     "167cabd10b7d281693881f3fa0d3a2d53b452757938a0b1c9ed14ec1cf8cac31"
     "e9a19d653b9887be721197bd31bb322f3f30c1d1bc84cd5589d39d91d14e5676"
     "07c27d0438dad9334c4e37abf945d55c0f075cddebd9259a9d978c30eaf0e646"
     "d8474f1ac208f779675ba808806034048d80b2d7f1558f5eea4b1000c1f7072b"
     "6045cc9f07b217bcb7f0981a4f603db447164b52a54a35e55f7b619a16a43224"
     "7bdd0b0a1798c147ebb670f2025cf21b2611259605f36bc5aadbe10b4b161565"
     "3615478dbb3faaa1d4cf8eb891b2715c32347516bacc83e493428a2a5af9e0d0"
     "ed3041249b86e8d3b0fc136bdfee89777266d1898c1be6af627d7bfc31ac2902"
     "06cff54c4f5a7fde4f7b6552893d62bd5f488c575fe8306493f02cdc68307dc4"
     "b87975453605d679c2b4dceac72860d0a3876dcce0d81db82973f9d0fe2b2d86"
     "1175c99f916933a77c56e3c00ae49b603f3bb592d0d8635440feb4961fd10a8d"
     "aa731f1f3e2a2d0d8ec4520f8fd6c82fc4dd53a5dc185d819ef4058c77839d9f"
     "7e287c58cc609725240eb1b195859c19c7de91aeec329b31dfd12ac50f5a2aa5"
     "fadaf75a0b8f78a02dd9dd8dcbfd47dd059267fa07955e2810659f33a6845ab8"
     "9033296a3abcdacedb9c15cf121c40ef82f55486b317d3f5827782ebcafd2ada"
     "6c8221ad2738a07b539714e885dd4b33dccb36af7676a5ed7c7d2d6c157d726b"
     "913da224e628af6ca2817c16f1b6e98a46773d3472120d4659fe9e8fa567e7fd"
     "7f932380fde2083a2240ac12485e4745e3e54f6827ac6ec96d10d394a3da615e"
     "a5ca38d61e868af1c18e4bdd598070dfcc355e0b9b4877503eb4759ed811c612"
     "a0eada1b654eec9cd1d0e5a0ac6d17b7e4253726c5d40416ba8cc7228e3c21f6"
     "330f5adb1bc438635a0df3d32cb7d50ba750b9967b0351def617a1f7515a3c98"
     "a5ec7c4f001e1f89376c29625fee8ad1a4ec6b12fcf785fca839190fefa2b88e"
     "8840aa5df49a92ec49d120a1fd3fd5c1f4895be46085ea37d9e6bab49aef9fa1"
     "ae9456a67a30fa0f1d1298072c6300a747a9e7abd257f9216cfb6c07052501dd"
     "28197a581115edf9adb5cc2564b7955b8edd466761525286e1a882b03ea78d96"
     "f9414709c78752e4fce65ee8edec4af7f7e38d22bbbea90275b7ea5204b02aa9"
     "870086c209999c308f7eb2d0d0221aaf356813718cb33843c5f803d96ff4f3af"
     "76cbf847220b2b3167bd7ba8cb9c0e0e6b91c466510cf18fd5793024bfecef75"
     "f9b2cf6cdf948e4cfa7191030c568d30ea912b0321a186c1b67f75a16e556a96"
     "bd0f7f4e8e16131570af822de53626427055579e49cf6dc5419bac9360bbc57c"
     "67e285b44540910a25799702c0a4e119132d2cec9396ce6326415163b8088e91"
     "7cc5df15f7352cf3f6c3df03ec0df06ebbd8d6497bd4925bce0d510c3c981780"
     "7cb73b30a2ad21b2e09108dbd9ccf17c1c4dd44fe4f80f9d9b887f217e21a7b7"
     "f8d0b4d321d4e482210ec2da0dae13c9506589321258cfba58487631fb06d408"
     "f0bd06b24df680a111310d5970372c63bd078c18de995aac30c97f5cecdebfd3"
     "ce23c849af968dc977c391ce374e2e8d6a61e8e5fb3f96647ff3870c55301e6c"
     "cba54c3302d7bed9b0ae611ceda52d9808a5d599a57c39b8d217a06009de4603"
     "9085c408b75175f91c86734671b69ecafea1e79feb03ee40dcce712b3e0d6199"
     "3e0ca76de92e44b18b940915bc00746d0370eeefb6736e164bac23f5b93bcb59"
     "abad6d3abcc3d46a0e3beefea41ced07523b45e82a6be4d07eb80151d5d32ede"
     "437da1cdf1f82d97c42cd48e4b7b2e501186a7a714368b35d310839854ef9a7b"
     "21e9259e25720f102b17063212f25f7e734b3f6b9176a8d92512105935277c01"
     "00b1380cc539f4ca4401f9f9ebb4f8fd20ac4f8dda5dbbe9ccdc0fea1cd010ee"
     "a90ade880307fba7f35edf00f24034dc7aedc4f4b38c66b3bebb993ab3621754"
     "3f19c35017c98e180baba2976856c49d772b968d3263dec47562949c3db137b9"
     "9105efa94167480144ab07c40e385ad0064bc41ae43b6128547d4cceb58fc8ea"
     "cb5e61c619aab4875a295b18661e8f75187ad51c11ea905a46b4909bdacaa69f"
     "450a77ae3c6a6b0ce42d88f904279b5f01010f70159cf37af5f25c9d1e4ed3ff"
     "ebc581dee87efc234ba2b3e87e83ee00872a5a17e0300941b38ba8468804b3a6"
     "ad14b622b48136e22f0cd786f4225a6e6c07c5016e4146dbcc7dbd1dea0e5c8b"
     "cd0290a384bb75df10368cbc61a57f9e4a9abf0a746f1d2a6f305f036ea2fd0a"
     "6e3e8698ad5e304639fcd858d7cf104d4d79533aa721e0890864b1875a1f7141"
     "89d95a98aa6efdee3686893f240272487b6cba9a0a151beee94f26a18cdf2487"
     "aaf09a583a079538d6fe1272a19cd7accd15d7ed3751d45bfb2ad9dd3554daac"
     "af7e691ec61174f8d633045c67fa04461a0591aa21826fd54f88609b44278364"
     "e27556a94bd02099248b888555a6458d897e8a7919fd64278d1f1e8784448941"
     "77113617a0642d74767295c4408e17da3bfd9aa80aaa2b4eeb34680f6172d71a"
     "bbffe8bcbf04a80bab7b47dfdb1920e2b0d70e8734fdbe45bbe5c558104dbdd2" default))
 '(elfeed-webkit-auto-enable-tags '("webkit") t nil "Customized with use-package elfeed-webkit")
 '(icsql-connections
   '(("sqlite-test-db" sqlite "" "" "~/test.db" "" "" nil)
     ("postgresql-postgres" postgres "localhost" "5432" "postgres" "postgres"
      "324324" nil)))
 '(mouse-wheel-progressive-speed nil)
 '(org-agenda-files
   '("~/Org/Tasks/Tasks.org" "~/Org/Tasks/Daily.org"
     "~/Org/Tasks/Repeated Tasks.org" "~/Org/Tasks/Agenda Statistics.org"
     "~/Org/Tasks/Computer Tasks.org" "~/Org/Tasks/Read It Later.org"
     "~/Org/Tasks/Read Books.org" "~/Org/Tasks/Plans.org"
     "~/Org/Tasks/Entertainment Tasks.org"
     "~/Org/Tasks/Life Tasks/Life Tasks.org"
     "~/Org/Tasks/Financial Tasks/Financial Tasks.org"
     "~/Org/Tasks/Learning Tasks/Learn Programming Tasks.org"
     "~/Org/Tasks/Learning Tasks/Learning Tasks.org"
     "~/Org/Tasks/Work Tasks/Work Tasks.org"
     "~/Org/Tasks/Work Tasks/我的自媒体/我的自媒体.org"
     "~/Org/Tasks/Family Tasks/Family Tasks.org"
     "~/Org/Tasks/House Tasks/House Tasks.org"
     "~/Org/Tasks/Travel/Travel Plan.org" "~/Org/Projects/Projects.org"
     "~/Org/Projects/Ideas HowTo.org" "~/Org/Projects/Gantt/Gantt.org"
     "~/Org/Projects/Human Projects/Human Projects.org"
     "~/Org/Projects/Hardware Projects/Hardware Projects.org"
     "~/Org/Projects/World Projects/World Projects.org"
     "~/Org/Projects/Programming Projects/Code.org"
     "~/Org/Projects/Programming Projects/Programming Projects.org"
     "~/Org/Projects/Programming Projects/Interpersonal Network/Interpersonal Network.org"
     "~/Org/Projects/Female Projects/Female Projects.org"
     "~/Org/Projects/We Media Projects/We Media Projects.org"
     "~/Org/Projects/Writing Projects/Writing Projects.org"
     "~/Org/Projects/Woodworking Projects/Woodworking Projects.org"
     "~/Org/Projects/Technology Projects/Technology Projects.org"
     "~/Org/Projects/Society Projects/Society Projects.org"
     "~/Org/Projects/Sex Projects/Sex Projects.org"
     "~/Org/Projects/Product Projects/Product Projects.org"
     "~/Org/Projects/Organization Projects/Organization Projects.org"
     "~/Org/Projects/Male Projects/Male Projects.org"
     "~/Org/Projects/Immortality Projects/Immortality Projects.org"
     "~/Org/Projects/Finance Projects/Finance Projects.org"
     "~/Org/Projects/Culture Projects/Culture Projects.org"
     "~/Org/Projects/Clothes Projects/Clothes Projects.org"
     "~/Org/Projects/Business Projects/Business Projects.org"
     "~/Org/Projects/Building Projects/Building Projects.org"
     "~/Org/Projects/Agriculture Projects/Agriculture Projects.org"
     "~/Org/Accounts/local accounts.org" "~/Org/Contacts/Contacts.org"
     "~/Org/Calendars/Anniversary.org" "~/Org/Myself/Myself.org"
     "~/Org/Myself/My Health Status Management/My Health Status Management.org"
     "~/Org/Wiki/Things/Things.org"
     "~/Org/Tasks/Work Tasks/淘宝店铺经营/Taobao Shop Management.org"
     "~/Org/Tasks/Work Tasks/淘宝店铺经营/Taobao Order List/Taobao Order List.org"
     "~/Org/Tasks/Work Tasks/京东店铺经营/京东店铺经营.org"
     "~/Org/Wiki/Countries/China/Chinese Government/manuals/My Chinese Government Administration Manual/My Chinese Government Administration Manual.org"
     "~/Org/dotfiles/macOS/macOS dotfiles.org"
     "~/Org/dotfiles/Home/Home dotfiles.org"
     "~/Org/dotfiles/NAS/QNAP/My QNAP NAS dotfiles.org"
     "~/Org/dotfiles/NAS/Synology/My Synology NAS dotfiles.org"))
 '(package-selected-packages
   '(0x0 a ac-octave ace-jump-mode ace-pinyin activities aggressive-indent
         ample-regexps amread-mode anki anki-helper ansible ansible-doc anzu
         apples-mode applescript-mode arduino-cli-mode aria2 atomic-chrome
         auth-source-kwallet auth-source-xoauth2 babashka banner-comment
         benchmark-init binder binky blimp blockdiag-mode bluetooth bm boxes
         boxquote bpr breadcrumb bug-hunter build-helper cal-china-x calfw
         calfw-org calibre cape carbon-now-sh cargo cargo-mode cargo-transient
         casual cc-isearch-menu ccls cdlatex chocolate-theme circadian
         circe-notifications citar cl-generic clj-deps-new cljsbuild-mode
         cloak-mode cmake-font-lock cmake-ide cmake-project code-cells
         colorful-mode comint-mime company-ansible company-arduino
         company-auctex company-ghci company-irony-c-headers company-ledger
         company-lua company-math company-nginx company-quickhelp
         company-restclient company-sourcekit company-spell company-tabnine
         company-terraform company-web compile-multi-all-the-icons
         consult-compile-multi consult-dash consult-dir consult-eglot-embark
         consult-flycheck consult-lsp consult-mu consult-org-roam
         consult-projectile consult-tex consult-todo consult-yasnippet
         corfu-prescient counsel-css counsel-fd counsel-jq counsel-org-clock
         counsel-osx-app counsel-projectile counsel-tramp csproj-mode css-eldoc
         csv-mode daemons dape dash-at-point db-pg delight detached devdocs
         diff-hl diffed diffview dired-duplicates dired-efap dired-git-info
         dired-narrow dired-quick-sort dired-rsync-transient dired-sidebar
         dired-toggle-sudo diredfl direx djvu docker docker-api
         docker-compose-mode dockerfile-mode docsim doom-themes dotnet
         download-region dracula-theme dwim-shell-command edebug-inline-result
         edebug-x edit-as-format edit-server-htmlize ednc ein ejc-sql
         eldoc-cmake eldoc-overlay elein elf-mode elfeed-org elfeed-web
         elfeed-webkit elgantt elisp-demos elisp-depmap ellama emacsql-sqlite
         emamux emaps emmet-mode emms empv emr eradio erc eredis eros
         ert-results eshell-bookmark eshell-prompt-extras eshell-toggle
         ess-R-data-view ess-view essgd esup eval-in-repl eww-lnum
         exec-path-from-shell expand-region external-completion external-dict
         faceup fancy-narrow fd-dired ffmpeg-utils file-info find-dupes-dired
         firefox-javascript-repl flycheck-clj-kondo flycheck-inline
         flycheck-joker flycheck-julia flycheck-kotlin flycheck-ledger
         flycheck-package flycheck-rust flycheck-swift
         flymake-diagnostic-at-point flymake-margin flymake-sqlfluff flymd
         flyspell-correct focus forge form-feed-st format-table frame-local
         ftable geiser-guile geiser-mit geiser-racket germanium ghub+
         git-messenger git-modes gitignore-snippets gnu-elpa-keyring-update
         gnuplot gnuplot-mode golden-ratio grab-mac-link gradle-mode graphql
         graphviz-dot-mode halloweenie-theme helm-dash helm-lib-babel
         helm-org-ql helm-org-rifle helm-posframe helm-rage helm-system-packages
         helm-systemd helpful hide-mode-line highlight-blocks html2org httpcode
         httprepl ialign icsql igist imenu-list immersive-translate
         impatient-mode importmagic indent-guide indium inf-clojure inf-mongo
         info-colors info-rename-buffer ini-mode inspector intel-hex-mode
         irony-eldoc ivy-emoji ivy-hydra ivy-prescient jarchive javap-mode
         journalctl-mode jq-format jq-mode js-comint json-navigator
         json-reformat jsonian jupyter k8s-mode key-quiz kind-icon kiwix
         kotlin-mode kubel kubernetes launchctl lazy-load ledger-mode ligature
         linkode listen live-py-mode lognav-mode logview lsp-haskell lsp-java
         lsp-pyright lsp-sourcekit lsp-ui magic-latex-buffer magit-commit-mark
         magit-delta magit-gitflow magit-lfs magit-org-todos
         magit-patch-changelog magit-todos magrant makefile-executor marginalia
         matlab-mode media-progress-dired meson-mode micromamba mingus
         modern-cpp-font-lock mongo moonshot mpdired mu4e-marker-icons
         mysql-to-org n4js names nano-theme nerd-icons-completion
         nerd-icons-corfu nerd-icons-dired nerd-icons-ibuffer nginx-mode
         nhexl-mode nix-mode no-clown-fiesta-theme nov npm-mode ntlm oauth2
         ob-applescript ob-base64 ob-blockdiag ob-chatgpt-shell ob-clojurescript
         ob-csharp ob-cypher ob-dall-e-shell ob-deno ob-dsq ob-ess-julia
         ob-fsharp ob-graphql ob-html-chrome ob-http ob-kotlin ob-mathematica
         ob-mermaid ob-mongo ob-powershell ob-racket ob-redis ob-restclient
         ob-rust ob-sql-mode ob-swift ob-swiftui ob-tmux ob-translate
         ob-typescript olivetti one-themes orderless org-analyzer org-anki
         org-assistant org-bars org-board org-bookmarks org-bookmarks-extractor
         org-buffer-todos org-chef org-contacts org-download org-drill
         org-drill-table org-edit-latex org-edna org-excalidraw
         org-extra-link-types org-gantt org-html-themify org-if org-inline-anim
         org-kindle org-link-beautify org-mac-link org-media-note org-mime
         org-modern org-noter-pdftools org-notify org-pandoc-import
         org-password-manager org-pomodoro org-present org-rich-yank
         org-roam-bibtex org-roam-timestamps org-roam-ui org-tag-eldoc
         org-transclusion org-transform-tree-table org-translate
         org-tree-slide-pauses org-upcoming-modeline org-volume org-wc
         org-web-tools orgit orgtbl-aggregate orgtbl-ascii-plot orgtbl-join
         origami osa osx-lib ox-epub ox-gfm ox-gist ox-reveal page-break-lines
         pair-tree pamparam pandoc pandoc-mode password-menu password-mode
         pcap-mode pcmpl-git pdf-view-restore pdfgrep pet pgdevenv pinentry
         pinyin-isearch pinyin-search pkgbuild-mode plantuml-mode
         platformio-mode pocket-lib podcaster poke poke-mode pomidor pomm poporg
         powershell presentation pretty-symbols prodigy project-rootfile
         project-tasks projectile-variable projection-multi
         projection-multi-embark protobuf-mode proxy-mode ptemplate-templates
         ptree pumpkin-spice-theme puni puppet-mode pyim-basedict python
         python-insert-docstring qml-mode quack quelpa quickrun racer
         racket-mode rainbow-delimiters rainbow-fart rainbow-identifiers
         rainbow-mode raycast-mode realgud-ipdb realgud-jdb realgud-lldb
         realgud-node-debug realgud-node-inspect realgud-pry redacted redis
         regex-tool restclient-test rg rime robe ron-mode rsync-mode ruby-tools
         run-command-recipes saveplace-pdf-view scheme-complete
         sclang-extensions sdcv separedit sharper shell-command-x shrface
         shrink-path shut-up sideline-flycheck siri-shortcuts skeletor
         skewer-mode sly-asdf sly-macrostep sly-quicklisp sly-repl-ansi-color
         smartparens so-long soap-client spacious-padding spray sql-indent
         sql-sqlline sqlite-mode-extras sqlup-mode ssh-agency ssh-config-mode
         ssh-tunnels stem-english stem-reading-mode stripes sudo-edit surround
         svg symbol-overlay syncthing sysctl system-packages systemd tabnine
         talonscript-mode tempel-collection tide tmr tmux-mode toml-mode tongbu
         tramp-auto-auth tramp-nspawn translate-mode transwin tree-inspector
         tree-sitter-langs treesit-auto tsuki-theme typescript-mode vagrant
         vagrant-tramp valign vc-msg verb verilog-mode vertico-prescient
         visual-regexp-steroids vlf vue-mode vundo walkman waveform web-beautify
         web-mode-edit-element web-narrow-mode which-key wolfram wolfram-mode
         writeroom-mode x-path-walker xcode-mode xcode-project xmind-org
         xml-format yagist yaml-imenu yari yasnippet-capf))
 '(package-vc-selected-packages
   '((modern-tab-bar :url "git@github.com:aaronjensen/emacs-modern-tab-bar.git")
     (consult-web :url "git@github.com:armindarvish/consult-web.git")
     (colorful-mode :url "git@github.com:DevelopmentCool2449/colorful-mode.git")
     (tsuki-theme :url "git@github.com:vekatze/tsuki-theme.git")
     (ob-html-chrome :url "git@github.com:nikclayton/ob-html-chrome.git")
     (waveform :url "git@github.com:sachac/waveform-el.git")
     (org-media-note :url "git@github.com:yuchen-lea/org-media-note.git")
     (org-volume :url "git@github.com:akirak/org-volume.git")
     (ob-mathematica :url "git@github.com:tririver/ob-mathematica.git")
     (consult-mu :url "https://github.com/armindarvish/consult-mu")
     (sqlite-mode-extras :vc-backend Git :url
                         #("https://github.com/xenodium/sqlite-mode-extras" 0 45
                           (display
                            #("[https://github.com/xenodium/sqlite-mode-extras]⌈⌋"
                              0 1 (face org-link-beautify-link-decorator-face) 1
                              47 (face org-link-beautify-link-description-face)
                              47 48 (face org-link-beautify-link-decorator-face)
                              48 49 (face org-link-beautify-link-decorator-face)
                              49 50
                              (rear-nonsticky t display (raise 0.0)
                                              font-lock-face
                                              (:family "Symbols Nerd Font Mono"
                                                       :height 1.0)
                                              face
                                              (:inherit
                                               org-link-beautify-link-icon-face
                                               :underline nil))
                              50 51 (face org-link-beautify-link-decorator-face))
                            font-lock-multiline t face
                            (org-link vertico-current) type org-link-beautify
                            htmlize-link
                            (:uri
                             "https://github.com/xenodium/sqlite-mode-extras"))
                           45 46
                           (display
                            #("[https://github.com/xenodium/sqlite-mode-extras]⌈⌋"
                              0 1 (face org-link-beautify-link-decorator-face) 1
                              47 (face org-link-beautify-link-description-face)
                              47 48 (face org-link-beautify-link-decorator-face)
                              48 49 (face org-link-beautify-link-decorator-face)
                              49 50
                              (rear-nonsticky t display (raise 0.0)
                                              font-lock-face
                                              (:family "Symbols Nerd Font Mono"
                                                       :height 1.0)
                                              face
                                              (:inherit
                                               org-link-beautify-link-icon-face
                                               :underline nil))
                              50 51 (face org-link-beautify-link-decorator-face))
                            font-lock-multiline t face
                            (org-link vertico-current) type org-link-beautify
                            htmlize-link
                            (:uri
                             "https://github.com/xenodium/sqlite-mode-extras")
                            rear-nonsticky t)))
     (anki-helper :vc-backend Git :url
                  #("https://github.com/Elilif/emacs-anki-helper" 0 43
                    (face vertico-current)))))
 '(safe-local-variable-values
   '((org-clock-display-default-range quote untilnow) (eval org-clock-display)
     (org-image-actual-width . 200)
     (eval and buffer-file-name (not (eq major-mode 'package-recipe-mode))
           (or (require 'package-recipe-mode nil t)
               (let ((load-path (cons "../package-build" load-path)))
                 (require 'package-recipe-mode nil t)))
           (package-recipe-mode))
     (valign-mode) (org-num-mode) (org-src-fontify-natively)
     (org-image-actual-width . 250) (calendar-location-name . "Shaoxing Town")
     (calendar-longitude . 120.37954302845002)
     (calendar-latitude . 29.90256956936341) (calendar-time-zone . 480)
     (org-image-actual-width . 300)
     (elisp-lint-indent-specs (if-let* . 2) (when-let* . 1) (let* . defun)
                              (nrepl-dbind-response . 2) (cider-save-marker . 1)
                              (cider-propertize-region . 1)
                              (cider-map-repls . 1) (cider--jack-in . 1)
                              (cider--make-result-overlay . 1)
                              (insert-label . defun)
                              (insert-align-label . defun) (insert-rect . defun)
                              (cl-defun . 2) (with-parsed-tramp-file-name . 2)
                              (thread-first . 0) (thread-last . 0)
                              (transient-define-prefix . defmacro)
                              (transient-define-suffix . defmacro))
     (org-crypt-disable-auto-save . t) (firestarter . value)
     (org-src-preserve-indentation) (org-hide-macro-markers . t)
     (ispell-buffer-session-localwords "docopt" "testcases")
     (eval progn (require 'projectile)
           (puthash (projectile-project-root) "make v=vvv test-buttercup"
                    projectile-test-cmd-map))
     (org-make-toc-link-type-fn . org-make-toc--link-entry-org)
     (encoding . utf-8-unix) (org-image-actual-width . 400)
     (org-image-actual-width . 350)
     (org-global-properties quote ("女优" "发行商" "导演" "系列"))
     (org-custom-properties quote ("女优" "发行商" "导演" "系列"))
     (org-startup-with-latex-preview) (org-cycle-inline-images-display . t)
     (mangle-whitespace . t) (org-cycle-inline-images-display)
     (system-time-locale . "C")
     (elisp-lint-indent-specs (if-let* . 2) (when-let* . 1) (let* . defun)
                              (nrepl-dbind-response . 2) (cider-save-marker . 1)
                              (cider-propertize-region . 1)
                              (cider-map-repls . 1) (cider--jack-in . 1)
                              (cider--make-result-overlay . 1)
                              (insert-label . defun)
                              (insert-align-label . defun) (insert-rect . defun)
                              (cl-defun . 2) (with-parsed-tramp-file-name . 2)
                              (thread-first . 0) (thread-last . 0))
     (eval when
           (and (buffer-file-name) (not (file-directory-p (buffer-file-name)))
                (string-match-p "^[^.]" (buffer-file-name)))
           (unless (require 'package-recipe-mode nil t)
             (let ((load-path (cons "../package-build" load-path)))
               (require 'package-recipe-mode)))
           (unless (derived-mode-p 'emacs-lisp-mode) (emacs-lisp-mode))
           (package-build-minor-mode) (setq-local flycheck-checkers nil)
           (set (make-local-variable 'package-build-working-dir)
                (expand-file-name "../working/"))
           (set (make-local-variable 'package-build-archive-dir)
                (expand-file-name "../packages/"))
           (set (make-local-variable 'package-build-recipes-dir)
                default-directory))
     (org-blank-before-new-entry (heading . auto) (plain-list-item . auto))
     (org-list-description-max-indent . 5)
     (org-list-two-spaces-after-bullet-regexp) (major-mode . mail-mode)
     (flycheck-mode t) (aggressive-indent-mode)
     (elisp-lint-indent-specs (if-let* . 2) (when-let* . 1) (let* . defun)
                              (nrepl-dbind-response . 2) (cider-save-marker . 1)
                              (cider-propertize-region . 1)
                              (cider-map-repls . 1) (cider--jack-in . 1)
                              (cider--make-result-overlay . 1)
                              (insert-label . defun)
                              (insert-align-label . defun) (insert-rect . defun)
                              (cl-defun . 2) (with-parsed-tramp-file-name . 2)
                              (thread-first . 1) (thread-last . 1))
     (checkdoc-package-keywords-flag)
     (eval when (fboundp 'rainbow-mode) (rainbow-mode 1))
     (eval when
           (and (buffer-file-name) (not (file-directory-p (buffer-file-name)))
                (string-match-p "^[^.]" (buffer-file-name)))
           (unless (featurep 'package-build)
             (let ((load-path (cons "../package-build" load-path)))
               (require 'package-build)))
           (unless (derived-mode-p 'emacs-lisp-mode) (emacs-lisp-mode))
           (package-build-minor-mode) (setq-local flycheck-checkers nil)
           (set (make-local-variable 'package-build-working-dir)
                (expand-file-name "../working/"))
           (set (make-local-variable 'package-build-archive-dir)
                (expand-file-name "../packages/"))
           (set (make-local-variable 'package-build-recipes-dir)
                default-directory))
     (eval org-latex-export-chinese-conf t) (eval org-latex-exp-conf-mode t)
     (flycheck-mode . t) (org-startup-with-latex-preview . t)
     (major-mode . org-mode) (org-startup-with-inline-images)))
 '(url-proxy-services nil)
 '(vertico-posframe-mode t)
 '(warning-suppress-log-types '(discard-info undo))
 '(warning-suppress-types
   '(org-element-parser org-element-cache org-element discard-info undo)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(aw-leading-char-face ((t (:inherit nil :height 250 :foreground "red"))))
 '(fixed-pitch ((t (:family "DejaVu Sans Mono"))))
 '(fixed-pitch-serif ((t (:family "DejaVu Serif"))))
 '(variable-pitch ((t (:family "Fira Code")))))
