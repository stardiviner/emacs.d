;;; init-prog-parser.el --- init for Programming Parser

;;; Commentary:



;;; Code:

;;; [ tree-sitter ] -- the base framework of the Emacs binding for Tree-sitter, an incremental parsing system.

(use-package tree-sitter
  :if (treesit-available-p)
  :commands (tree-sitter-mode tree-sitter-hl-mode)
  :hook (tree-sitter . tree-sitter-hl-mode)
  :init (add-to-list 'display-buffer-alist
                     '("^\\*tree-sitter-tree: .*\\*" .
                       ((display-buffer-reuse-window display-buffer-in-side-window) .
                        ((reusable-frames . visible)
                         (side . right) (slot . 1)
                         (window-sides-vertical . nil)))))
  :config
  ;; Auto enable <lang>-ts-mode as major-mode.
  (when (treesit-available-p)
    (push '(sh-mode         . bash-ts-mode) major-mode-remap-alist)
    (push '(c-mode          . c-ts-mode) major-mode-remap-alist)
    (push '(c++-mode        . c++-ts-mode) major-mode-remap-alist)
    (push '(c-or-c++-mode   . c-or-c++-ts-mode) major-mode-remap-alist)
    (push '(css-mode        . css-ts-mode) major-mode-remap-alist)
    (push '(js-mode         . js-ts-mode) major-mode-remap-alist)
    (push '(javascript-mode . js-ts-mode) major-mode-remap-alist)
    (push '(typescript-mode . typescript-ts-mode) major-mode-remap-alist)
    (push '(js-json-mode    . json-ts-mode) major-mode-remap-alist)
    (push '(graphql-mode    . graphql-ts-mode) major-mode-remap-alist)
    (push '(python-mode     . python-ts-mode) major-mode-remap-alist)
    (push '(ruby-mode       . ruby-ts-mode) major-mode-remap-alist)
    (push '(yaml-mode       . yaml-ts-mode) major-mode-remap-alist)
    (push '(conf-toml-mode  . toml-ts-mode) major-mode-remap-alist)
    (push '(java-mode       . java-ts-mode) major-mode-remap-alist)
    (push '(clojure-mode    . clojure-ts-mode) major-mode-remap-alist)
    (push '(kotlin-mode     . kotlin-ts-mode) major-mode-remap-alist)
    (push '(csharp-mode     . csharp-ts-mode) major-mode-remap-alist)
    (push '(go-mode         . go-ts-mode) major-mode-remap-alist)
    (push '(rust-mode       . rust-ts-mode) major-mode-remap-alist)
    (push '(lua-mode        . lua-ts-mode) major-mode-remap-alist)
    (push '(ocaml-mode      . ocaml-ts-mode) major-mode-remap-alist)
    (push '(julia-mode      . julia-ts-mode) major-mode-remap-alist)
    (push '(elixir-mode     . elixir-ts-mode) major-mode-remap-alist)
    (push '(cmake-mode      . cmake-ts-mode) major-mode-remap-alist)
    (push '(html-mode       . html-ts-mode) major-mode-remap-alist)
    (push '(jq-mode         . jq-ts-mode) major-mode-remap-alist)
    (push '(mermaid-mode    . mermaid-ts-mode) major-mode-remap-alist))

  (add-to-list 'auto-mode-alist `("\\go.mod\\'" . go-mod-ts-mode))

  ;; For command `treesit-install-language-grammar' to install & compile language grammar.
  (setq treesit-language-source-alist
        '((bash       . ("https://github.com/tree-sitter/tree-sitter-bash.git"))
          (c          . ("https://github.com/tree-sitter/tree-sitter-c.git"))
          (cmake      . ("https://github.com/uyha/tree-sitter-cmake.git"))
          (cpp        . ("https://github.com/tree-sitter/tree-sitter-cpp.git"))
          (csharp     . ("https://github.com/tree-sitter/tree-sitter-c-sharp.git"))
          (css        . ("https://github.com/tree-sitter/tree-sitter-css.git"))
          (dockerfile . ("https://github.com/camdencheek/tree-sitter-dockerfile.git"))
          (go         . ("https://github.com/tree-sitter/tree-sitter-go.git"))
          (gomod      . ("https://github.com/camdencheek/tree-sitter-go-mod.git"))
          ;; (gomod      . ("https://github.com/camdencheek/tree-sitter-gomod.git"))
          (html       . ("https://github.com/tree-sitter/tree-sitter-html.git"))
          (java       . ("https://github.com/tree-sitter/tree-sitter-java.git"))
          (javascript . ("https://github.com/tree-sitter/tree-sitter-javascript.git"))
          (json       . ("https://github.com/tree-sitter/tree-sitter-json.git"))
          (python     . ("https://github.com/tree-sitter/tree-sitter-python.git"))
          (ruby       . ("https://github.com/tree-sitter/tree-sitter-ruby.git"))
          (rust       . ("https://github.com/tree-sitter/tree-sitter-rust.git"))
          (toml       . ("https://github.com/tree-sitter/tree-sitter-toml.git"))
          (tsx        . ("https://github.com/tree-sitter/tree-sitter-typescript.git" nil "tsx/src"))
          (typescript . ("https://github.com/tree-sitter/tree-sitter-typescript.git" nil "typescript/src"))
          (yaml       . ("https://github.com/ikatyang/tree-sitter-yaml.git"))))
  )

;; [ tree-sitter-langs ] -- Grammar bundle for tree-sitter

;; This is a convenient language bundle for the Emacs package `tree-sitter'. It
;; serves as an interim distribution mechanism, until `tree-sitter' is
;; widespread enough for language-specific major modes to incorporate its
;; functionalities.

(use-package tree-sitter-langs
  :ensure t
  :demand t
  :commands (tree-sitter-langs-install-grammars))

;;; [ treesit ] -- tree-sitter utilities

(require 'treesit)

;;; [ treesit-auto ] -- Automatically use tree-sitter enhanced major modes.

(use-package treesit-auto
  :ensure t
  :commands (global-treesit-auto-mode
             treesit-auto-install-all treesit-install-language-grammar)
  :custom (treesit-auto-install 'prompt)
  :config
  (treesit-auto-add-to-auto-mode-alist 'all)
  (global-treesit-auto-mode))

;;; [ Lexical Parsing ]

(load "init-prog-lexical-parsing")

;;; [ Grammar Parsing ]

(load "init-prog-grammar-parsing")


(provide 'init-prog-parser)

;;; init-prog-parser.el ends here
