;;; init-prog-task-management.el --- init for programming task management -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Christopher M. Miles, all rights reserved.

;;; Commentary:



;;; Code:

;;; [ project-tasks ] -- Efficient task management for your project.

(use-package project-tasks
  :ensure t
  :commands (project-tasks project-tasks-capture project-tasks-jump)
  :bind (:map project-prefix-map
              ("P" . project-tasks)
              ("o" . project-tasks-capture)
              ("O" . project-tasks-jump))
  :config
  ;; Show project-tasks when switching projects
  (add-to-list 'project-switch-commands '(project-tasks "tasks") t)
  ;; Add action to embark-file map
  (with-eval-after-load 'embark
    (define-key embark-file-map (kbd "P") #'project-tasks-in-dir)))



(provide 'init-prog-task-management)

;;; init-prog-task-management.el ends here
