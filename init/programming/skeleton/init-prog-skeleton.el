;;; init-prog-skeleton.el --- project skeleton -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Christopher M. Miles, all rights reserved.

;;; Commentary:



;;; Code:

;;; [ skeletor ] -- Powerful project skeletons for Emacs.

(use-package skeletor
  :ensure t
  :commands (skeletor-create-project-at skeletor-create-project))



(provide 'init-prog-skeleton)

;;; init-prog-skeleton.el ends here
