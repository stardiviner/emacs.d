;;; init-prog-tags.el -- init tags file config for Emacs.

;;; Commentary:

;; like tags: function, variable, class, scope etc lookup.

;;; Code:


(unless (boundp 'tags-prefix)
  (define-prefix-command 'tags-prefix))
(global-set-key (kbd "M-g t") 'tags-prefix)

;;; [ tags settings ]

(use-package etags
  :defer t
  :custom ((tags-add-tables t)          ; always add new tags to tables
           (tags-revert-without-query t)
           (tags-apropos-verbose t)))

;;; [ xref ]

(use-package xref
  ;; disable the following elements to avoid jump to other window when xref.
  :custom ((xref-prompt-for-identifier '(not xref-find-definitions
                                             ;; xref-find-definitions-other-window
                                             ;; xref-find-definitions-other-frame
                                             ))
           (xref-history-storage #'xref-window-local-history) ; original value `xref-global-history'.
           )
  :init (add-to-list 'display-buffer-alist '("^\\*xref\\*$" . (display-buffer-below-selected))))

;; NOTE: use etags & gtags, because company-mode support.
;; (load "init-etags")
;; (load "init-gtags")
;; (load "init-ctags")
;; (load "init-cscope") ; <----- prefer "cscope"
;; (load "init-rtags")


(provide 'init-prog-tags)

;;; init-prog-tags.el ends here
