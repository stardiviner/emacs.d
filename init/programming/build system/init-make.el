;;; init-make.el --- init for Make utility.
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ make-mode ]

(use-package make-mode
  :ensure t
  :defer t)


;;; [ ob-makefile ]

(use-package ob-makefile
  :defer t
  :commands (org-babel-execute:makefile)
  :config
  (add-to-list 'org-babel-load-languages '(makefile . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages))


;;; [ makefile-executor ] -- Emacs helpers to run things from makefiles.

(use-package makefile-executor
  :ensure t
  :defer t
  :commands (makefile-executor-mode)
  :bind (:map build-system-prefix ("<f6>" . makefile-executor-execute-project-target))
  :init (add-hook 'makefile-mode-hook 'makefile-executor-mode))

;;; support company-mode
(use-package company
  :ensure t
  :ensure make-mode
  :after make-mode
  :config
  (defun makefile-mode-setup ()
    (cond
     ((featurep 'corfu)
      (corfu-mode 1))
     ((featurep 'company)
      (setq-local company-dabbrev-ignore-case t)
      ;; (setq-local company-dabbrev-other-buffers nil)
      (setq-local company-dabbrev-code-ignore-case t)
      (setq-local company-backends '((company-keywords
                                      company-yasnippet
                                      company-dabbrev-code
                                      company-dabbrev)))
      (company-mode 1)))
    (setf (alist-get major-mode company-keywords-alist)
          (-uniq
           (append makefile-statements
                   makefile-special-targets-list
                   (->> (pcase major-mode
                          ('makefile-automake-mode makefile-automake-statements)
                          ('makefile-gmake-mode makefile-gmake-statements)
                          ('makefile-makepp-mode makefile-makepp-statements)
                          ('makefile-bsdmake-mode makefile-bsdmake-statements)
                          ('makefile-imake-mode makefile-imake-s))
                        (-filter #'stringp)
                        (-map #'split-string)
                        (-flatten))))))
  
  (require 'make-mode)
  (dolist (hook '(makefile-gmake-mode-hook
                  makefile-makepp-mode-hook
                  makefile-bsdmake-mode-hook
                  makefile-imake-mode-hook))
    (add-hook hook #'makefile-mode-setup)))


(provide 'init-make)

;;; init-make.el ends here
