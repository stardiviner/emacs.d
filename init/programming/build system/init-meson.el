;;; init-meson.el --- init for Meson -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Christopher M. Miles, all rights reserved.

;;; Commentary:



;;; Code:

;;; [ meson-mode ] -- Major mode for the Meson build system files.

(use-package meson-mode
  :ensure t
  :ensure company
  :commands (meson-lookup-doc meson-lookup-doc-at-point))



(provide 'init-meson)

;;; init-meson.el ends here
