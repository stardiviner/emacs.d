;;; init-SQL.el --- init for SQL
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ sql.el ] -- specialized comint.el for SQL interpreters.

(use-package sql
  :init (add-to-list 'display-buffer-alist '("^\\*SQL:.*\\*" . (display-buffer-below-selected)))
  :hook ((sql-mode . sql-indent-enable)
         (sql-mode . electric-pair-local-mode))
  :commands (sql-connect)
  :config
  ;; setup `sql-mode'
  (defun my/sql-mode-setup ()
    "Setup `sql-mode' with my custom config."
    (setq-local corfu-auto-prefix 1))
  (add-hook 'sql-mode-hook #'my/sql-mode-setup)

  ;; add "postgres" as alias of "postgresql".
  ;; Basically copy the `postgres' entry in `sql-product-alist' and add it as `postgresql'.
  (let ((pg (cdr (assq 'postgres sql-product-alist))))  ; Make a copy of the postgres plist
    (add-to-list 'sql-product-alist                     ; Add as a new entry under postgresql
                 (cons 'postgresql (plist-put pg :name "PostgreSQL"))))
  
  ;; fix MySQL/MariaDB prompt incompatible.
  (sql-set-product-feature 'mysql :prompt-regexp "^\\(MariaDB\\|MySQL\\) \\[[_a-zA-Z]*\\]> ")
  
  ;; for command `sql-connect'.
  (setq sql-connection-alist
        `((PostgreSQL/postgres@test.Linux
           (sql-product 'postgres)
           (sql-server "localhost")
           (sql-port 5432)
           (sql-user "postgres")
           (sql-password "324324")
           (sql-database "test"))
          (,(intern (format "PostgreSQL/%s@postgres.macOS" user-login-name))
           (sql-product 'postgres)
           (sql-server "localhost")
           (sql-port 5432)
           (sql-user user-login-name)
           (sql-password "324324")
           (sql-database "postgres"))
          (MySQL/root@test.Linux
           (sql-product 'mysql)
           (sql-server "localhost")
           (sql-port 3306)
           (sql-user "root")
           (sql-password "324324")
           (sql-database "test")))))

;;; [ sql-sqlline ] -- Adds SQLLine support to SQLi mode.

(use-package sql-sqlline
  :ensure t
  :commands (sql-sqlline))

;;; [ sql-indent] -- Support for indenting code in SQL files.

(use-package sql-indent
  :ensure t
  :commands (sqlind-minor-mode sqlind-show-syntax-of-line)
  :init (dolist (hook '(sql-mode-hook edbi:sql-mode-hook))
          (add-hook hook #'sqlind-minor-mode)))


;;; [ sqlup-mode ] -- An Emacs minor mode to upcase SQL keyword and functions.

(use-package sqlup-mode
  :ensure t
  :defer t
  :delight sqlup-mode
  :init (dolist (hook '(sql-mode-hook
                        sql-interactive-mode-hook
                        edbi:sql-mode-hook))
          (add-hook hook #'sqlup-mode))
  :config
  (defun my-sqlup-backward ()
    "Capitalization the word backward."
    (interactive)
    (backward-word) ; `backward-sexp'
    (upcase-word 1)
    (forward-char 1))

  (define-key sql-mode-map (kbd "C-c C-u") 'my-sqlup-backward)
  (define-key sql-mode-map (kbd "C-c u") 'sqlup-capitalize-keywords-in-region))

;;; [ sqlformat ] -- Reformat SQL using sqlformat or pgformatter.

;; (use-package sqlformat
;;   :ensure t
;;   :init (add-hook 'sql-mode-hook 'sqlformat-mode)
;;   (setq sqlformat-mode-format-on-save t))

;;; [ ob-sql ] -- Babel Functions for SQL.

(use-package ob-sql
  :defer t
  :after org
  :init
  (add-to-list 'org-babel-load-languages '(sql . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  (add-to-list 'org-babel-tangle-lang-exts '("sql" . "sql"))
  (add-to-list 'org-src-lang-modes (cons "SQL" 'sql))
  :config
  (add-to-list 'org-babel-default-header-args:sql '(:results . "table")))

;;; [ ob-sql-mode ] -- SQL code blocks evaluated by sql-mode.

(use-package ob-sql-mode
  :ensure t
  :defer t
  :custom (org-babel-sql-mode-template-selector "S") ; for "<S" expanding `org-structure-template-alist'.
  :commands (org-babel-execute:sql-mode)
  :init (setq org-confirm-babel-evaluate (lambda (lang body) (string= lang "sql-mode"))) ; security guard
  ;; Make [C-c '] `org-edit-special' editing `ob-sql-mode' source block with `sql-mode'.
  ;; This can fix the error: progn: No such language mode: sql-mode-mode.
  (add-to-list 'org-src-lang-modes '("sql-mode" . sql)) ; open sql-mode source block with `sql-mode'.
  (add-to-list 'org-babel-tangle-lang-exts '("sql-mode" . "sql"))
  :config
  (add-to-list 'org-babel-load-languages '(sql-mode . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)

  ;; Specify default :product and :session.
  ;; (add-to-list 'org-babel-default-header-args:sql-mode '(:product . "sqlite"))
  ;; (add-to-list 'org-babel-default-header-args:sql-mode '(:session . "sqlite-test.db")) ; from `sql-connection-alist'
  ;; (add-to-list 'org-babel-header-args:sql-mode '(:product . "sqlite"))

  ;; Auto set sql-mode buffer SQL product engine when editing source block with
  ;; header argument `:engine' or `:product' specified database product
  ;; corresponding sql-mode product/engine on [C-c '] `org-edit-special' ->
  ;; `org-edit-src-code' -> `org-babel-edit-prep:sql'.
  (defun org-babel-edit-prep:sql-mode (info)
    "Set `sql-product' in Org edit source block buffer.
 Set `sql-product' in Org edit buffer according to the
corresponding :product source block header argument."
    (let ((product (cdr (or (assq :engine (nth 2 info))
                            (assq :product (nth 2 info))))))
      (condition-case nil
          (sql-set-product product)
        (user-error "Cannot set `sql-product' in Org Src edit buffer"))))
  )

;;; [ ejc-sql ] -- Emacs SQL client uses Clojure JDBC.

(use-package ejc-sql
  :ensure t
  :custom ((ejc-completion-system 'standard)
           (ejc-complete-on-dot t)
           (ejc-show-result-bottom t)
	       (ejc-show-result-button t))
  :commands (ejc-connect
             ejc-connect-existing-repl ejc-connect-interactive ejc-sql-mode
             ejc-get-temp-editor-buffer
             ejc-direx:pop-to-buffer ejc-direx:switch-to-buffer)
  :hook ((sql-mode . ejc-sql-mode)
         (ejc-sql-mode . ejc-eldoc-setup))
  :init (add-to-list 'display-buffer-alist '("^\\*ejc-sql-output\\*" . (display-buffer-below-selected)))
  (defun ejc-connect-sqlite (sqlite-db-file)
    "A helper command to dynamically create SQLite Driver connection for SQLITE-DB-FILE."
    (interactive "fSelect SQLite .db file: ")
    (let ((connection-name (format "SQLite> %s" sqlite-db-file)))
      (ejc-create-connection
       connection-name
       :dependencies [[org.xerial/sqlite-jdbc "3.36.0.3"]]
       :subprotocol "sqlite"
       :subname (file-truename sqlite-db-file))
      (ejc-connect connection-name)))
  :config
  ;; setup ejc-sql
  (defun my/ejc-sql-setup ()
    (flyspell-mode -1)
    (flycheck-mode -1)
    (electric-pair-local-mode -1))
  (add-hook 'ejc-sql-mode-hook #'my/ejc-sql-setup)
  ;; ejc-sql complete
  (defun my/ejc-sql-complete-setup ()
    (cond
     ((featurep 'corfu)
      (require 'ejc-company)
      (setq-local company-minimum-prefix-length 3)
      (setq-local company-idle-delay 0.2)
      (setq-local company-tooltip-idle-delay 0.4)
      (setq-local completion-at-point-functions
                  (list (cape-company-to-capf #'ejc-company-backend)
                        (if (featurep 'yasnippet-capf)
                            #'yasnippet-capf
                          (cape-company-to-capf 'company-yasnippet))
                        #'cape-keyword #'cape-abbrev #'cape-dabbrev)))
     ((featurep 'company)
      (require 'ejc-company)
      (setq-local company-minimum-prefix-length 3)
      (setq-local company-idle-delay 0.2)
      (setq-local company-tooltip-idle-delay 0.4)
      (make-local-variable 'company-backends)
      (add-to-list 'company-backends '(ejc-company-backend :separate company-yasnippet))
      (company-mode 1))
     ((featurep 'auto-complete)
      (ejc-ac-setup)
      (auto-complete-mode 1))))
  (add-hook 'ejc-sql-mode-hook #'my/ejc-sql-complete-setup)

  ;; enable `ejc-sql' in `sql-interactive-mode'.
  (defun ejc-sql-interactive-mode-setup ()
    "Setup ejc-sql completion in `sql-interactive-mode'."
    (interactive)
    (call-interactively 'ejc-connect)
    (ejc-sql-mode))
  (add-hook 'sql-interactive-mode-hook #'ejc-sql-interactive-mode-setup)

  ;; result table
  (defun my/ejc-sql-customize-output ()
    (ejc-set-column-width-limit nil) ; don't limit column width and shrink.
    ;; (ejc-set-max-rows 50) ; set table max rows.
    ;; (ejc-set-use-unicode t)
    )
  (add-hook 'ejc-sql-connected-hook #'my/ejc-sql-customize-output)

  ;; database connection definitions
  (ejc-create-connection
   "PostgreSQL> postgres@localhost/test (Linux)"
   :dependencies [[org.postgresql/postgresql "42.4.0"]]
   :classname "org.postgresql.Driver"
   :connection-uri "jdbc:postgresql://localhost:5432/test"
   :user "postgres"
   :password (my/json-read-value my/account-file 'ejc-sql-postgresql))

  (ejc-create-connection
   (format "PostgreSQL> %s@localhost/test (macOS)" user-login-name)
   :dependencies [[org.postgresql/postgresql "42.4.0"]]
   :classname "org.postgresql.Driver"
   :connection-uri "jdbc:postgresql://localhost:5432/postgres"
   :user user-login-name
   :password (my/json-read-value my/account-file 'ejc-sql-postgresql))

  (ejc-create-connection
   "MariaDB> root@localhost/test"
   :dependencies [[org.mariadb.jdbc/mariadb-java-client "3.0.6"]]
   :classname "org.mariadb.jdbc.Driver"
   :connection-uri "jdbc:mariadb://localhost:3306/test"
   :user "root"
   :password (my/json-read-value my/account-file 'ejc-sql-mysql))

  (ejc-create-connection
   "MySQL> mysql@localhost/test"
   :dependencies [[mysql/mysql-connector-java "8.0.29"]]
   ;; FIXME :classname "mysql"
   :dbtype "mysql"
   :host "localhost"
   :port "3306"
   :user "root"
   :password (my/json-read-value my/account-file 'ejc-sql-mysql)
   :dbname "test")

  (ejc-create-connection
   "SQLite> test.db"
   :dependencies [[org.xerial/sqlite-jdbc "3.36.0.3"]]
   :subprotocol "sqlite"
   :subname (expand-file-name "~/Documents/Programming/SQL/SQLite/test.db"))

  ;; press [q] to close ejc-sql result window.
  (add-hook 'ejc-result-mode-hook (lambda () (local-set-key (kbd "q") 'delete-window))))

;;; [ icsql ] -- Interface to ciSQL for interacting with relational database managements systems (RDMBs).

(use-package icsql
  :ensure t
  :commands (icsql)
  :preface (when (file-exists-p "/Library/Java/JavaVirtualMachines/openjdk.jdk/Contents/Home")
             (setenv "JAVA_HOME" "/Library/Java/JavaVirtualMachines/openjdk.jdk/Contents/Home"))
  :custom ((icsql-path "/Users/stardiviner/.config/emacs/icsql")
           (icsql-java-home (or
                             "/Users/stardiviner/.sdkman/candidates/java/8.0.372-librca" ; SDKMAN install JDK 1.8. $ sdk use java 8.0.372-librca
                             "/Users/stardiviner/.sdkman/candidates/java/current/" ; $ sdk current
                             (getenv "JAVA_HOME")
                             "/Library/Java/JavaVirtualMachines/openjdk.jdk/Contents/Home/")))
  :init (add-to-list 'display-buffer-alist '("^\\*SQL\\*" . (display-buffer-below-selected)))
  :config
  ;; `sql-connection-alist'
  (add-to-list 'icsql-connections `("SQLite/test.db" sqlite "" "" ,(expand-file-name "~/Documents/Programming/SQL/SQLite/test.db") "" "" nil))
  (cl-case system-type
    (gnu/linux
     (add-to-list 'icsql-connections
                  '("PostgreSQL/stardiviner@postgres.macOS" postgres "localhost" "5432" "postgres" "postgres" "324324" nil)))
    (darwin
     (add-to-list 'icsql-connections
                  `("PostgreSQL/stardiviner@postgres.macOS" postgres "localhost" "5432" "postgres" ,user-login-name "324324" nil))))
  )

;;; [ edbi ]

;; (use-package edbi
;;   :ensure t
;;   :defer t
;;   :commands (edbi:open-db-viewer)
;;   :config
;;   (setq edbi:completion-tool 'auto-complete) ; none
;;
;;   (add-hook 'edbi:sql-mode-hook
;;             (lambda ()
;;               (define-key edbi:sql-mode-map (kbd "C-c C-q")
;;                 'edbi:dbview-query-editor-quit-command)
;;               (sqlup-mode 1)
;;               ;; for `edbi:completion-tool'
;;               (add-hook 'completion-at-point-functions
;;                         'edbi:completion-at-point-function nil t)
;;               (company-mode 1)
;;               ))
;;
;;   ;; [ edbi-minor-mode ] -- use edbi with regular SQL files.
;;   (use-package edbi-minor-mode
;;     :ensure t
;;     :init (add-hook 'sql-mode-hook (lambda () (edbi-minor-mode))))
;;
;;   ;; [ edbi-sqlite ] -- edbi helper application
;;   (use-package edbi-sqlite
;;     :ensure t
;;     :commands (edbi-sqlite))
;;
;;   ;; [ edbi-database-url ] -- run edbi with database url.
;;   (use-package edbi-database-url
;;     :ensure t
;;     :commands (edbi-database-url))
;;
;;   ;; [ company-edbi ]
;;   (use-package company-edbi
;;     :ensure t
;;     :init
;;     (defun my/company-edbi-setup ()
;;       (my-company-add-backend-locally 'company-edbi))
;;     (dolist (hook '(sql-mode-hook
;;                     sql-interactive-mode-hook
;;                     edbi:sql-mode-hook))
;;       (add-hook hook #'my/company-edbi-setup)))
;;   )

;;; [ EmacSQL ] -- high-level SQL database front-end.

;; (use-package emacsql
;;   :ensure t
;;   :defer t
;;   :init
;;   (use-package emacsql-sqlite-builtin
;;     :ensure t
;;     :defer t)
;;   )

;;; [ db-sql ] -- Connect to SQL server using tramp syntax.


;;; [ emacs-db ] -- very simple database for emacslisp, can also wrap other databases.

;; (use-package db
;;   :ensure t
;;   :defer t)

;;; [ lsp-sqls ] -- lsp-mode support for SQL clients.

(use-package lsp-mode
  :ensure t
  :defer t
  :requires (lsp-sqls)
  :custom ((lsp-sqls-connections
            `(((driver . "postgresql")
               (dataSourceName . (format "alias=%s host=%s port=%s user=%s password=%s dbname=%s sslmode=disable"
                                         "PostgreSQL-macOS/stardiviner@localhost:postgres"
                                         "127.0.0.1"
                                         "5432"
                                         ,(cl-case system-type
                                            (darwin "stardiviner")
                                            (gnu/linux "postgres"))
                                         ,(my/json-read-value my/account-file 'ejc-sql-postgresql)
                                         "postgres")))
              )))
  :commands (lsp
             lsp-sql-show-connections lsp-sql-switch-connection
             lsp-sql-show-databases lsp-sql-switch-database
             lsp-sql-show-schemas
             lsp-sql-execute-query lsp-sql-execute-paragraph)
  :hook ((sql-mode . lsp)
         (sql-mode . lsp-completion-mode))
  :init (add-to-list 'display-buffer-alist '("^\\*sqls results\\*" . (display-buffer-below-selected)))
  :config
  (define-key lsp-mode-map (kbd "C-c C-c") 'lsp-sql-execute-paragraph)
  (define-key lsp-mode-map (kbd "C-c C-q") 'lsp-sql-execute-query))

;;; [ format-table ] -- Parse and reformat tabular data in Emacs.

(use-package format-table
  :ensure t
  :defer t
  :commands (format-table))

;;; [ flymake-sqlfluff ] -- SQL linter and auto-formatter for Humans.

(use-package flymake-sqlfluff
  :ensure t
  :commands (flymake-sqlfluff-change-dialect)
  :init (flymake-sqlfluff-load))

(load "init-SQLite")
(load "init-MySQL")
(load "init-PostgreSQL")


(provide 'init-SQL)

;;; init-SQL.el ends here
