;;; init-NoSQL.el --- init for NoSQL
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

(load "init-Redis")
(load "init-MongoDB")
;; (load "init-CouchDB")

;;; Graph Databases
(load "init-Neo4j")


(provide 'init-NoSQL)

;;; init-NoSQL.el ends here
