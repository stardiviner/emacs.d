;;; init-SQLite.el --- init SQLite

;;; Commentary:

;;; Code:

;;; [ sqlite ] -- Emacs Lisp Functions for interacting with sqlite3 databases.

(use-package sqlite) ; Provides macro `with-sqlite-transaction'.

;;; [ sqlite-mode ] -- Mode for examining sqlite3 database files.

(use-package sqlite-mode
  :commands (sqlite-mode-open-file
             sqlite-mode-list-tables sqlite-mode-list-columns sqlite-mode-list-data))

;;; [ sqlite-mode-extras ] -- Extend Emacs's `sqlite-mode'.

(use-package sqlite-mode-extras
  :vc (:url "https://github.com/xenodium/sqlite-mode-extras")
  :bind (:map sqlite-mode-map
              ("n" . next-line)
              ("p" . previous-line)
              ("b" . sqlite-mode-extras-backtab-dwim)
              ("f" . sqlite-mode-extras-tab-dwim)
              ("+" . sqlite-mode-extras-add-row)
              ("D" . sqlite-mode-extras-delete-row-dwim)
              ("C" . sqlite-mode-extras-compose-and-execute)
              ("E" . sqlite-mode-extras-execute)
              ("S" . sqlite-mode-extras-execute-and-display-select-query)
              ("DEL" . sqlite-mode-extras-delete-row-dwim)
              ("g" . sqlite-mode-extras-refresh)
              ("<backtab>" . sqlite-mode-extras-backtab-dwim)
              ("<tab>" . sqlite-mode-extras-tab-dwim)
              ("RET" . sqlite-mode-extras-ret-dwim)))

;;; [ ob-sqlite ] -- Babel Functions for SQLite Databases.

(use-package ob-sqlite
  :defer t
  :after org
  :init
  (add-to-list 'org-babel-load-languages '(sqlite . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  (add-to-list 'org-babel-tangle-lang-exts '("sqlite" . "sqlite"))
  :config
  (setq org-babel-default-header-args:sqlite
        '((:column . t)
          ;; (:line . "yes")
          (:header . "yes")
          ;; (:echo . "yes")
          ;; (:bail . "yes")
          ;; (:separator . ",")
          ;; (:csv . t)
          ;; (:html . t)
          (:results . "verbatim")
          ;; (:wrap . "example")
          (:nullvalue . "Null")
          (:db . "data/code/temp.db") ; this is essential necessary for every src block.
          )))

;;; [ esqlite ] -- sqlite file manipulate utilities from Emacs.

;; (use-package esqlite
;;   :ensure t)


(provide 'init-SQLite)

;;; init-SQLite.el ends here
