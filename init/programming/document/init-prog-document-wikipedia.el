;;; init-prog-document-wikipedia.el --- init for Offline Wikipedia
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ mediawiki ] -- mediawiki frontend.

;; (use-package mediawiki
;;   :ensure t
;;   :defer t)


;;; [ ox-mediawiki ] -- Mediawiki Back-End for Org Export Engine.

;; Open a .org document and run `org-mw-export-as-mediawiki'.

;; (use-package ox-mediawiki
;;   :ensure t
;;   :defer t)

;;; [ oddmuse ] --

;; (defun my/oddmuse-setup ()
;;   (require 'company-oddmuse)
;;   (my-company-add-backend-locally 'company-oddmuse))
;; (add-hook 'mediawiki-mode-hook #'my/oddmuse-setup)

;;; [ kiwix ] -- Kiwix client for Emacs.

(use-package kiwix
  :ensure t
  :after org
  :commands (kiwix-launch-server kiwix-at-point kiwix-search-at-library kiwix-search-full-context)
  :bind (:map document-prefix ("w" . kiwix-at-point))
  :custom ((kiwix-server-type 'docker-remote)
           (kiwix-server-api-version "v2")
           (kiwix-server-url "http://192.168.31.251")
           (kiwix-server-port 8580))
  :hook (org-load . org-kiwix-setup-link)
  :init (require 'org-kiwix) (org-kiwix-setup-link))


(provide 'init-prog-document-wikipedia)

;;; init-prog-document-wikipedia.el ends here
