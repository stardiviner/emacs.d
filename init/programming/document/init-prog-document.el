;;; init-prog-document.el --- init for Programming Document Look Up.

;;; Commentary:

;;; Code:


(unless (boundp 'document-prefix)
  (define-prefix-command 'document-prefix))
(global-set-key (kbd "C-c h") 'document-prefix)

(load "init-prog-document-eldoc")
(load "init-prog-document-man")
;; (load "init-prog-document-rfc")
(load "init-prog-document-api")
(load "init-prog-document-assistant")
(load "init-prog-document-wikipedia")


(provide 'init-prog-document)

;;; init-prog-document.el ends here
