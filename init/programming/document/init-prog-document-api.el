;;; init-prog-document-api.el --- init for API
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; helper functions or commands

(defun my/dash-docs-common-docsets ()
  "A helper function to setting `dash-docs-common-docsets' based on `dash-docs-docsets-path' on macOS."
  (with-eval-after-load 'dash-docs
    (let ((docset-macos-path (expand-file-name "~/Library/Application Support/Dash/DocSets"))
          (docset-home-path (expand-file-name "~/.docsets"))
          (docset-config-path (expand-file-name "~/.config/dash/docsets")))
      (cond
       ;; macOS Dash.app
       ((and (file-exists-p docset-macos-path)
             (string-equal dash-docs-docsets-path docset-macos-path))
        (setq-default dash-docs-common-docsets '("Clojure"
                                                 "Python_3"
                                                 "JavaScript" "NodeJS"
                                                 "SQLite" "PostgreSQL" "Redis" "MongoDB"
                                                 "Java_SE19"
                                                 "Apple_API_Reference")))
       ;; dash-docs.el
       ((and (file-exists-p docset-config-path)
             (string-equal dash-docs-docsets-path docset-config-path))
        (setq-default dash-docs-common-docsets '("Clojure" "ClojureDocs" "ClojureScript"
                                                 "Python_3"
                                                 "JavaScript" "NodeJS" "TypeScript"
                                                 "SQLite" "PostgreSQL" "Redis" "MongoDB" "neo4j"
                                                 "Java")))
       ;; dash-docs.el
       ((and (file-exists-p docset-home-path)
             (string-equal dash-docs-docsets-path docset-home-path))
        (setq-default dash-docs-common-docsets '("Clojure" "ClojureDocs" "ClojureScript"
                                                 "Python_3"
                                                 "JavaScript" "NodeJS" "TypeScript"
                                                 "SQLite" "PostgreSQL" "Redis" "MongoDB" "neo4j"
                                                 "Java")))))))

(my/dash-docs-common-docsets)

;;; [ dash-docs ] -- Offline documentation browser using Dash docsets.

(use-package dash-docs
  :ensure t
  :defer t
  :custom ((dash-docs-docsets-path (if (file-exists-p "~/.config/dash/docsets")
                                       (expand-file-name "~/.config/dash/docsets")))
           (dash-docs-browser-func 'browse-url)
           (dash-docs-candidate-format "%d  %n  (%t)")
           (dash-docs-enable-debugging nil))
  :config (my/dash-docs-common-docsets)
  
  ;; buffer local docsets
  (defun my/dash-docs-local-docsets (docsets-list &optional append-to)
    (make-local-variable 'dash-docs-common-docsets)
    ;; filter non-exist docsets
    (require 'dash-docs)
    (setq docsets-list (seq-filter 'dash-docs-docset-path docsets-list))
    (if append-to
        (setq dash-docs-common-docsets
              (append dash-docs-common-docsets docsets-list))
      (setq-local dash-docs-common-docsets nil)
      (setq-local dash-docs-common-docsets docsets-list))
    (mapc 'dash-docs-activate-docset dash-docs-common-docsets))
  
  ;; Bash
  (defun dash-docs-buffer-local-shell-docsets ()
    (my/dash-docs-local-docsets '("Bash")))
  (add-hook 'sh-mode-hook 'dash-docs-buffer-local-shell-docsets)
  (add-hook 'shell-mode-hook 'dash-docs-buffer-local-shell-docsets)
  ;; Ruby
  (defun dash-docs-buffer-local-ruby-docsets ()
    (my/dash-docs-local-docsets '("Ruby")))
  (add-hook 'enh-ruby-mode-hook 'dash-docs-buffer-local-ruby-docsets)
  ;; Ruby on Rails
  (defun dash-docs-buffer-local-rails-docsets ()
    (my/dash-docs-local-docsets '("Ruby on Rails" "Ruby"))
    (my/dash-docs-local-docsets '("HTML" "JavaScript" "CSS" "jQuery") t))
  (add-hook 'projectile-rails-mode-hook 'dash-docs-buffer-local-rails-docsets)
  ;; Python
  (defun dash-docs-buffer-local-python-docsets ()
    (my/dash-docs-local-docsets '("Python_3"))
    (my/dash-docs-local-docsets '("Matplotlib" "SciPy") t)
    (my/dash-docs-local-docsets '("Qt_5" "Qt_for_Python" "pyside2") t)
    (my/dash-docs-local-docsets '("TensorFlow 2" "PyTorch") t))
  (add-hook 'python-mode-hook 'dash-docs-buffer-local-python-docsets)
  (add-hook 'inferior-python-mode-hook 'dash-docs-buffer-local-python-docsets)
  ;; Web
  (with-eval-after-load 'web-mode
    (defun dash-docs-buffer-local-web-docsets ()
      (my/dash-docs-local-docsets '("HTML" "CSS" "JavaScript"))
      (my/dash-docs-local-docsets '("jQuery" "React") t))
    (add-hook 'web-mode-hook 'dash-docs-buffer-local-web-docsets))
  ;; JavaScript
  (defun dash-docs-buffer-local-javascript-docsets ()
    (my/dash-docs-local-docsets '("JavaScript" "NodeJS" "jQuery"))
    (my/dash-docs-local-docsets '("React" "VueJS" "Angular" "BackboneJS" "Express" "Bootstrap_5" "D3JS") t)
    (my/dash-docs-local-docsets '("electron") t)
    (my/dash-docs-local-docsets '("Chrome_Extensions_API") t))
  (add-hook 'js-mode-hook 'dash-docs-buffer-local-javascript-docsets)
  ;; HTTP
  (defun dash-docs-buffer-local-http-docsets ()
    (my/dash-docs-local-docsets '("HTTP")))
  (add-hook 'restclient-mode-hook 'dash-docs-buffer-local-http-docsets)
  ;; HTML
  (defun dash-docs-buffer-local-html-docsets ()
    (my/dash-docs-local-docsets '("HTML" "JavaScript" "jQuery"))
    (my/dash-docs-local-docsets '("React" "D3JS") t))
  (add-hook 'html-mode-hook 'dash-docs-buffer-local-html-docsets)
  ;; CSS
  (defun dash-docs-buffer-local-css-docsets ()
    (my/dash-docs-local-docsets '("CSS"))
    (my/dash-docs-local-docsets '("Bootstrap_5") t))
  (add-hook 'css-mode-hook 'dash-docs-buffer-local-css-docsets)
  ;; Common Lisp
  (defun dash-docs-buffer-local-common-lisp-docsets ()
    (my/dash-docs-local-docsets '("Common Lisp")))
  (add-hook 'common-lisp-mode-hook 'dash-docs-buffer-local-common-lisp-docsets)
  (add-hook 'lisp-interaction-mode-hook 'dash-docs-buffer-local-common-lisp-docsets)
  ;; Clojure
  (defun dash-docs-buffer-local-clojure-docsets ()
    (my/dash-docs-local-docsets '("ClojureDocs" "Clojure" "Java"))
    (my/dash-docs-local-docsets '("RxJava") t))
  (add-hook 'clojure-mode-hook 'dash-docs-buffer-local-clojure-docsets)
  (add-hook 'cider-repl-mode-hook 'dash-docs-buffer-local-clojure-docsets)
  ;; ClojureScript
  (defun dash-docs-buffer-local-clojurescript-docsets ()
    (my/dash-docs-local-docsets '("ClojureDocs" "Clojure" "ClojureScript" "JavaScript")))
  (add-hook 'clojurescript-mode-hook 'dash-docs-buffer-local-clojurescript-docsets)
  (add-hook 'cider-repl-mode-hook 'dash-docs-buffer-local-clojurescript-docsets)
  ;; CIDER REPL
  (defun dash-docs-buffer-local-cider-docsets ()
    (if (equal cider-repl-type "clj")
        (my/dash-docs-local-docsets '("ClojureDocs" "Clojure" "Java"))
      (my/dash-docs-local-docsets '("ClojureDocs" "Clojure" "ClojureScript" "JavaScript"))))
  (add-hook 'cider-repl-mode-hook 'dash-docs-buffer-local-cider-docsets)
  ;; Kotlin
  (defun dash-docs-buffer-local-kotlin-docsets ()
    (my/dash-docs-local-docsets '("Kotlin")))
  (add-hook 'kotlin-mode-hook 'dash-docs-buffer-local-kotlin-docsets)
  ;; C
  (defun dash-docs-buffer-local-C-docsets ()
    (my/dash-docs-local-docsets '("C"))
    ;; TODO download LLVM, CMake, Clang, docset
    ;; (my/dash-docs-local-docsets '("GNU Make" "CMake" "LLVM" "Clang" "GLib") t)
    )
  (add-hook 'c-mode-hook 'dash-docs-buffer-local-C-docsets)
  ;; C++
  (defun dash-docs-buffer-local-C++-docsets ()
    (my/dash-docs-local-docsets '("C++"))
    ;; TODO download LLVM, Clang, docset
    ;; (my/dash-docs-local-docsets '("LLVM" "Clang" "GLib") t)
    (my/dash-docs-local-docsets '("Qt_5") t))
  (add-hook 'c++-mode-hook 'dash-docs-buffer-local-C++-docsets)
  ;; GNU Make
  (defun dash-docs-buffer-local-make-docsets ()
    (my/dash-docs-local-docsets '("GNU Make")))
  (add-hook 'make-mode-hook 'dash-docs-buffer-local-make-docsets)
  ;; CMake
  (defun dash-docs-buffer-local-cmake-docsets ()
    (my/dash-docs-local-docsets '("CMake")))
  (add-hook 'cmake-mode-hook 'dash-docs-buffer-local-cmake-docsets)
  ;; Go
  (defun dash-docs-buffer-local-go-docsets ()
    (my/dash-docs-local-docsets '("Go")))
  (add-hook 'go-mode-hook 'dash-docs-buffer-local-go-docsets)
  ;; Java
  (defun dash-docs-buffer-local-java-docsets ()
    (my/dash-docs-local-docsets '("Java"))
    (my/dash-docs-local-docsets '("RxJava" "JavaFx") t))
  (add-hook 'java-mode-hook 'dash-docs-buffer-local-java-docsets)
  ;; SQL
  (defun dash-docs-buffer-local-sql-docsets ()
    (cl-case sql-product
      (ansi (my/dash-docs-local-docsets '("SQLite" "PostgreSQL" "MySQL")))
      (sqlite (my/dash-docs-local-docsets '("SQLite")))
      (postgresql (my/dash-docs-local-docsets '("PostgreSQL")))
      (postgres (my/dash-docs-local-docsets '("PostgreSQL")))
      (mariadb (my/dash-docs-local-docsets '("MySQL")))
      (mysql (my/dash-docs-local-docsets '("MySQL")))))
  (add-hook 'sql-mode-hook 'dash-docs-buffer-local-sql-docsets)
  (add-hook 'sql-interactive-mode-hook 'dash-docs-buffer-local-sql-docsets)
  (add-hook 'ejc-sql-mode-hook 'dash-docs-buffer-local-sql-docsets)
  (add-hook 'ejc-sql-minor-mode-hook 'dash-docs-buffer-local-sql-docsets)
  ;; MongoDB
  (defun dash-docs-buffer-local-mongodb-docsets ()
    (my/dash-docs-local-docsets '("MongoDB")))
  (add-hook 'inf-mongo-mode-hook 'dash-docs-buffer-local-mongodb-docsets)
  ;; Redis
  (defun dash-docs-buffer-local-redis-docsets ()
    (my/dash-docs-local-docsets '("Redis")))
  (add-hook 'redis-mode-hook 'dash-docs-buffer-local-redis-docsets)
  ;; Neo4j
  (defun dash-docs-buffer-local-neo4j-docsets ()
    (my/dash-docs-local-docsets '("Neo4j")))
  (add-hook 'cypher-mode-hook 'dash-docs-buffer-local-neo4j-docsets)
  ;; LaTeX
  (defun dash-docs-buffer-local-latex-docsets ()
    (my/dash-docs-local-docsets '("LaTeX")))
  (add-hook 'latex-mode-hook 'dash-docs-buffer-local-latex-docsets)
  (add-hook 'LaTeX-mode-hook 'dash-docs-buffer-local-latex-docsets)
  ;; PlantUML
  (defun dash-docs-buffer-local-PlantUML-docsets ()
    (my/dash-docs-local-docsets '("PlantUML")))
  (add-hook 'plantuml-mode-hook 'dash-docs-buffer-local-PlantUML-docsets)
  ;; R
  (defun dash-docs-buffer-local-R-docsets ()
    (my/dash-docs-local-docsets '("R")))
  (add-hook 'ess-r-mode-hook 'dash-docs-buffer-local-R-docsets)
  (add-hook 'inferior-ess-r-mode-hook 'dash-docs-buffer-local-R-docsets)
  ;; Julia
  (defun dash-docs-buffer-local-julia-docsets ()
    (my/dash-docs-local-docsets '("Julia")))
  (add-hook 'ess-julia-mode-hook 'dash-docs-buffer-local-julia-docsets)
  (add-hook 'julia-mode-hook 'dash-docs-buffer-local-julia-docsets)
  ;; Docker
  (with-eval-after-load 'dockerfile-mode
    (defun dash-docs-buffer-local-docker-docsets ()
      (my/dash-docs-local-docsets '("Docker")))
    (add-hook 'dockerfile-mode-hook 'dash-docs-buffer-local-docker-docsets))
  ;; Kubernetes
  (with-eval-after-load 'k8s-mode
    (defun dash-docs-buffer-local-kubernetes-docsets ()
      (my/dash-docs-local-docsets '("kubernetes")))
    (add-hook 'k8s-mode-hook 'dash-docs-buffer-local-kubernetes-docsets))
  ;; Vagrant
  ;; GraphQL
  (with-eval-after-load 'graphql-mode
    (defun dash-docs-buffer-local-GraphQL-docsets ()
      (my/dash-docs-local-docsets '("GraphQL Specification")))
    (add-hook 'graphql-mode-hook 'dash-docs-buffer-local-GraphQL-docsets))
  ;; Swift
  (with-eval-after-load 'swift-mode
    (defun dash-docs-buffer-local-swift-docsets ()
      (my/dash-docs-local-docsets '("Swift"))
      (my/dash-docs-local-docsets '("iOS" "OS_X") t))
    (add-hook 'swift-mode-hook 'dash-docs-buffer-local-swift-docsets))
  ;; Android
  (with-eval-after-load 'android-mode
    (defun dash-docs-buffer-local-android-docsets ()
      (my/dash-docs-local-docsets '("Android" "Java")))
    (add-hook 'android-mode-hook 'dash-docs-buffer-local-android-docsets))
  ;; Arduino
  (with-eval-after-load 'arduino-mode
    (defun dash-docs-buffer-local-arduino-docsets ()
      (my/dash-docs-local-docsets '("Arduino"))
      (my/dash-docs-local-docsets '("C") t))
    (add-hook 'arduino-mode-hook 'dash-docs-buffer-local-arduino-docsets))
  ;; Linux
  (with-eval-after-load 'nginx-mode
    (defun dash-docs-buffer-local-nginx-docsets ()
      (my/dash-docs-local-docsets '("Nginx")))
    (add-hook 'nginx-mode-hook 'dash-docs-buffer-local-nginx-docsets))
  (with-eval-after-load 'apache-mode
    (defun dash-docs-buffer-local-nginx-docsets ()
      (my/dash-docs-local-docsets '("Apache_HTTP_Server")))
    (add-hook 'apache-mode-hook 'dash-docs-buffer-local-apache-docsets)))


(cond
 ;; [ consult-dash ] -- Consult front-end for dash-docs.
 ((featurep 'consult)
  (use-package consult-dash
    :ensure t
    :commands (consult-dash)
    :bind (("M-s d" . consult-dash)
           :map document-prefix ("C-d" . consult-dash))
    ;; Use the symbol at point as initial search term
    :init (consult-customize consult-dash :initial (thing-at-point 'symbol))
    (when (not (featurep 'dash-alfred)) (my/dash-docs-common-docsets))
    :config
    ;; Buffer-local values of docsets to search can be set by customizing the buffer-local variable `consult-dash-docsets' in a mode hook.
    ;; (add-hook 'python-mode-hook (lambda () (setq-local consult-dash-docsets '("Python_3" "NumPy" "Pandas"))))
    ))

 ;; [ helm-dash ] -- Offline documentation browser for +150 APIs using Dash docsets.
 ((featurep 'helm)
  (use-package helm-dash
    :ensure t
    :bind (("M-s d" . helm-dash-at-point)
           :map document-prefix ("C-d" . helm-dash-at-point) ("M-d" . helm-dash))
    :init
    (defun helm-dash--around (orig-func &rest args)
      "Make `helm-dash' ignore case. Useful for SQL docsets."
      (let ((case-fold-search t)
            (helm-case-fold-search t))
        (apply orig-func args)))
    (advice-add 'helm-dash :around #'helm-dash--around))))

;;; [ dash-at-point ] -- Search the word at point with Dash.

(use-package dash-at-point
  :if (and (eq system-type 'darwin) (file-exists-p "/Applications/Dash.app"))
  :ensure t
  :commands (dash-at-point dash-at-point-with-docset)
  :bind (:map document-prefix ("p" . dash-at-point) ("M-p" . dash-at-point-with-docset))
  :config
  (add-to-list 'dash-at-point-mode-alist '(clojure-mode . "clojure,java"))
  (add-to-list 'dash-at-point-mode-alist '(clojurescript-mode . "clojurescript,javascript,nodejs"))
  (add-to-list 'dash-at-point-mode-alist '(web-mode . "html,css,javascript,jquery")))

;;; [ dash-alfred ] -- Search Dash on-the-fly in Emacs.

(use-package dash-alfred
  :if (and (eq system-type 'darwin) (file-exists-p "/Applications/Dash.app"))
  :load-path "~/Code/Emacs/dash-alfred.el"
  :defer t
  :commands (dash-alfred-ivy dash-alfred-helm)
  :bind (:map document-prefix ("d" . dash-alfred-helm))
  :config (my/dash-docs-common-docsets))

;;; [ zeal-at-point ]

;; (use-package zeal-at-point
;;   :if (and (eq system-type 'gnu/linux) (executable-find "zeal"))
;;   :ensure t
;;   :defer t
;;   :bind (:map document-prefix
;;               ("C-d" . zeal-at-point))
;;   :init
;;   (setq zeal-at-point-zeal-version "0.3.0")
;;   :config
;;   ;; multiple docsets search
;;   (add-to-list 'zeal-at-point-mode-alist
;;                '(clojurescript-mode . ("clojure" "clojurescript")))
;;   (add-to-list 'zeal-at-point-mode-alist
;;                '(enh-ruby-mode . ("ruby" "rails")))
;;   (add-to-list 'zeal-at-point-mode-alist
;;                '(python-mode . ("python" "django")))
;;   (add-hook 'web-mode-hook
;;             (lambda ()
;;               (setq-local zeal-at-point-docset '("javascript" "html" "css"))))
;;   (add-hook 'projectile-rails-mode-hook
;;             (lambda ()
;;               (setq zeal-at-point-docset '("rails" "javascript" "html" "css")))))

;;; [ devdocs ] -- Emacs viewer for DevDocs.

(use-package devdocs
  :ensure t
  ;; :vc t :load-path "~/Code/Emacs/devdocs.el/"
  :defer t
  :custom ((devdocs-data-dir (expand-file-name "devdocs" user-emacs-directory))
           (devdocs-window-select t))
  :commands (devdocs-lookup devdocs-search devdocs-peruse devdocs-install devdocs-delete devdocs-update-all)
  :bind (:map document-prefix ("h" . devdocs-lookup) ("M-h" . devdocs-search))
  :init (add-to-list 'display-buffer-alist '("^\\*devdocs\\*" . (display-buffer-below-selected)))
  (defvar devdocs-current-docs-mode-alist
    '((emacs-lisp-mode . ("elisp"))
      ((TeX-mode LaTeX-mode) . ("latex"))
      (markdown-mode . ("markdown"))
      (python-mode . ("python~3.10" "matplotlib~3.7"
                      "numpy~1.23" "pandas~1" "pytorch~2" "tensorflow~2.9" "scikit_learn" "scikit_image"
                      "django~5.0" "django_rest_framework"))
      (ruby-mode . ("ruby~3.3"))
      (web-mode . ("html" "css" "javascript" "node" "deno" "npm" "dom" "http" "browser_support_tables"))
      (html-mode . ("html"))
      (css-mode . ("css" "bootstrap~5" "tailwindcss"))
      (js-mode . ("javascript" "node" "deno" "npm" "dom" "http" "web_extensions"))
      (typescript-mode . ("typescript" "node" "deno" "npm" "dom" "http"))
      (clojure-mode . ("clojure~1.10" "openjdk~8" "openjdk~21"))
      (redis-mode . ("redis"))
      (c-mode . ("c" "gcc~13"))
      (c++-mode . ("cpp" "gcc~13_cpp"))
      (go-mode . ("go"))
      (rust-mode . ("rust"))
      (java-mode . ("openjdk~21" "openjdk~8"))
      (lua-mode . ("lua~5.4" "hammerspoon"))
      (dart-mode . ("dart~2"))
      (kotlin-mode . ("kotlin~1.9"))
      (gnuplot-mode . ("gnuplot"))
      (nginx-mode . ("nginx"))
      (make-mode . ("gnu_make"))
      (cmake-mode . ("cmake~3.26"))
      (ansible . ("ansible"))
      (yaml-mode . ("ansible"))
      (dockerfile-mode . ("docker"))
      ((magit-mode magit-status-mode magit-log-mode magit-blob-mode magit-diff-mode) . ("git")))
    "An alist of `major-mode' locally associated devdocs docs for locally setting `devdocs-current-docs'.")

  (defun my/devdocs-smart-docs ()
    "Smartly set variable `devdocs-current-docs' based on current `major-mode'.
This variable is normally set by the `devdocs-lookup' command."
    (require 'devdocs)
    (make-local-variable 'devdocs-current-docs)
    (if (member major-mode (flatten-list (mapcar 'car devdocs-current-docs-mode-alist)))
        (setq-local devdocs-current-docs
                    (cdr (assoc major-mode devdocs-current-docs-mode-alist)))
      (pcase major-mode
        ('sh-mode
         (cond
          ((eq sh-shell "bash")
           (setq-local devdocs-current-docs '("bash")))
          ;; ((eq sh-shell "zsh")
          ;;  (setq-local devdocs-current-docs '("")))
          ))
        ('sql-mode
         (cond
          ((eq sql-product 'sqlite)
           (setq-local devdocs-current-docs '("sqlite")))
          ((eq sql-product 'postgresql)
           (setq-local devdocs-current-docs '("postgresql~16")))
          ((or (eq sql-product 'mysql) (eq sql-product 'mariadb))
           (setq-local devdocs-current-docs '("mariadb")))))))
    ;; Add common docs to list.
    (when (bound-and-true-p devdocs-current-docs)
      (add-to-list 'devdocs-current-docs "vagrant")))

  (add-hook 'prog-mode-hook #'my/devdocs-smart-docs))


(provide 'init-prog-document-api)

;;; init-prog-document-api.el ends here
