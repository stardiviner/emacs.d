;;; init-prog-document-rfc.el --- init for RFC
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ irfc ] -- Interface for IETF RFC document.

;; (use-package irfc
;;   :if (file-exists-p (expand-file-name "documentations/RFC/" user-emacs-directory))
;;   :ensure t
;;   :custom ((irfc-directory (expand-file-name "documentations/RFC" user-emacs-directory))
;;            (irfc-assoc-mode t)          ; RFC documents are associated with `irfc-mode'.
;;            (irfc-highlight-requirement-keywords t)
;;            (irfc-requirement-keywords '("MUST" "MUST NOT" "REQUIRED"
;;                                         "SHALL" "SHALL NOT" "SHOULD" "SHOULD NOT"
;;                                         "RECOMMENDED" "NOT RECOMMENDED"
;;                                         "MAY" "OPTIONAL" "NOT"))
;;            (irfc-requirement-keyword-overlay nil)
;;            (irfc-highlight-references t))
;;   :commands (irfc-visit)
;;   :bind (:map document-prefix ("r" . irfc-visit)))

;;; [ rfc-mode ] -- RFC document browser and viewer.

(use-package rfc-mode
  :if (file-exists-p (expand-file-name "documentations/RFC/" user-emacs-directory))
  :ensure t
  :defer t
  :custom (rfc-mode-directory (expand-file-name "documentations/RFC/" user-emacs-directory))
  :commands (rfc-mode-browse rfc-mode-read)
  :bind (:map document-prefix ("r" . rfc-mode-browse)))


(provide 'init-prog-document-rfc)

;;; init-prog-document-rfc.el ends here
