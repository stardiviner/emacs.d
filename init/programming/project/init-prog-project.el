;;; init-prog-project.el --- init Project settings for Emacs

;;; Commentary:


;;; Code:


(unless (boundp 'project-prefix)
  (define-prefix-command 'project-prefix))
(global-set-key (kbd "C-c p") 'project-prefix)

;;; [ project ] -- Operations on the current project.

(use-package project
  :defer t
  :bind-keymap ("C-x p" . project-prefix-map)
  :commands (project-switch-project
             project-remember-projects-under
             project-forget-project project-forget-zombie-projects project-forget-projects-under)
  :config
  ;; make `xref' use `ripgrep'.
  (setq xref-search-program 'ripgrep)
  ;; (when (fboundp 'consult-ripgrep)
  ;;   (advice-remove 'project-find-regexp #'consult-ripgrep))

  ;; `project-ignores': Return the list of glob patterns to ignore inside DIR.
  (setq project-vc-ignores
        (delete-dups
         (append
          project-vc-ignores
          '(".vscode/" ".idea/" ".vs/"
            ".cache/"
            "node_modules"
            "build/" "target/" "bin/" "obj/"
            "*.elc" "*.eln" "*.eld"))))
  
  ;; Add new command to `project-prefix-map'.
  (defun project-open-with-vscode ()
    "Open this project with Visual Studio Code."
    (interactive)
    (if (executable-find "code")
        (shell-command (format "code %s" (project-root (project-current t))))))

  (define-key project-prefix-map (kbd "M-v") 'project-open-with-vscode)

  (defun project-open-with-xcode ()
    "Open this project with Xcode."
    (interactive)
    (let ((xcode-bin "/Applications/Xcode.app/Contents/MacOS/Xcode"))
      (when (file-exists-p xcode-bin)
        (shell-command (format "%s %s" xcode-bin (project-root (project-current t)))))))

  (define-key project-prefix-map (kbd "M-c") 'project-open-with-xcode)
  )

;;; [ project-rootfile ] -- Extension of project.el to detect project root with root file (e.g. Gemfile).

(use-package project-rootfile
  :ensure t
  :after project
  :init
  ;; Same behavior as `projectile', a VCS root over a root file for project detection:
  (add-to-list 'project-find-functions #'project-rootfile-try-detect t)
  ;; Otherwise, if you prefer a root file, add the following:
  ;; (add-to-list 'project-find-functions #'project-rootfile-try-detect)
  :config
  ;; let `project-rootfile' detect non-rootfile project detect manually configured project with ".project" empty file.
  (add-to-list 'project-rootfile-list ".project")
  (add-to-list 'project-rootfile-list ".projectile")
  (add-to-list 'project-rootfile-list "compile_commands.json")
  (add-to-list 'project-rootfile-list "compile_flags.txt")
  (add-to-list 'project-rootfile-list "shadow-cljs.edn"))

;;; [ projectile ] -- minor mode to assist project management and navigation.

(use-package projectile
  :ensure t
  :defer t
  ;; :delight '(:eval (concat " [" (projectile-project-name) "]"))
  ;; :delight projectile-mode
  :delight ""
  :bind-keymap ("C-c p" . projectile-command-map)
  :custom ((projectile-switch-project-action #'projectile-commander)
           (projectile-use-git-grep t)
           (projectile-create-missing-test-files t))
  :init (add-to-list 'display-buffer-alist '("^\\*Projectile Commander Help\\*" . (display-buffer-below-selected)))
  :hook (after-init . projectile-mode)
  :config
  ;; `project-projectile' detecting project root is not right. The
  ;; "~/Org/code/project_demo/src/main.clj" project root will be detected as
  ;; "~/Org" instead of "~/Org/code/project_demo/".
  (remove-hook 'project-find-functions #'project-projectile)

  ;; ignore files & directories
  ;; (add-to-list 'projectile-globally-ignored-files "TAGS")
  (dolist (pattern '("^\\.python-environments$"))
    (add-to-list 'projectile-globally-ignored-directories pattern)
    (cl-return projectile-globally-ignored-directories))
  
  (defun my/projectile-remove-compilation-find-file-advice ()
    "Fix `compilation-mode' inherited modes caused suspend on opening `compile-goto-error', `next-error' etc jumped targets."
    (advice-remove 'compilation-find-file #'compilation-find-file-projectile-find-compilation-buffer))
  (add-hook 'projectile-mode-hook #'my/projectile-remove-compilation-find-file-advice))

;; [ consult-projectile ] -- Consult integration for projectile.

(use-package consult-projectile
  :if (featurep 'consult)
  :ensure t
  :bind (:map projectile-command-map
              ([remap projectile-find-file] . consult-projectile)
              ([remap projectile-switch-to-buffer] . consult-projectile)))

;; [ counsel-projectile ] -- Ivy UI for Projectile.

(use-package counsel-projectile
  :if counsel-mode
  :ensure t
  :bind (:map projectile-command-map
              ([remap projectile-find-file] . consult-projectile)
              ([remap projectile-switch-to-buffer] . consult-projectile)))

;;; [ projectile-variable ] -- store project local variables.

(use-package projectile-variable
  :ensure t
  :after projectile
  :defer t
  :commands (projectile-variable-put projectile-variable-get projectile-variable-alist))

;;; [ project-shells ] -- manage the shell buffers for each project.

;; (use-package project-shells
;;   :ensure t
;;   :defer t
;;   :preface (setq project-shells-keymap-prefix "C-c p M-!")
;;   :init (global-project-shells-mode 1))

;;; [ projection ] -- projectile like project management library built on Emacs project.el

(use-package projection
  :ensure t
  :after project
  ;; WARNING: `global-projection-hook-mode' caused Emacs suspend performance issue.
  ;; :hook (after-init . global-projection-hook-mode)
  :hook (prog-mode . projection-hook-mode)
  :bind-keymap ("C-x P" . projection-map)
  :init
  (use-package projection-multi
    :ensure t
    :bind (:map projection-map ("RET" . projection-multi-compile)))
  (use-package projection-multi-embark
    :ensure t
    :after embark
    :after projection-multi
    :config (projection-multi-embark-setup-command-map)))

;;; [ ptemplate ] -- Powerful project (directory) templates for Emacs.

(use-package ptemplate
  :ensure t
  :ensure ptemplate-templates
  :commands (ptemplate-new-project)
  :config (add-to-list 'ptemplate-template-dirs (expand-file-name "ptemplate" user-emacs-directory))
  (eval-after-load 'ptemplate (ptemplate-templates-mode 1)))


(provide 'init-prog-project)

;;; init-prog-project.el ends here
