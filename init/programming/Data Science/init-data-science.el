;;; init-data-science.el --- init Emacs for Data Science

;;; Commentary:



;;; Code:
;;; ----------------------------------------------------------------------------
;;; [ jupyter ] -- An interface to communicate with Jupyter kernels in Emacs.

(use-package jupyter
  :ensure t
  :demand t
  :commands (jupyter-run-repl jupyter-connect-repl jupyter-repl-associate-buffer jupyter-server-list-kernels)
  :preface
  (defun jupyter-kernelspec-list ()
    "Return a list of available jupyter kernels and their corresponding languages.
The elements of the list have the form (\"language\" \"kernel\")."
    (let ((kernelspecs (cdar (json-read-from-string
                              (shell-command-to-string
                               (concat jupyter-executable " kernelspec list --json 2>/dev/null"))))))
      (-map (lambda (spec)
              (cons (->> (cdr spec)
                         (assoc 'spec)
                         cdr
                         (assoc 'language)
                         cdr)
                    (symbol-name (car spec))))
            kernelspecs)))
  :init
  (use-package ob-jupyter
    :demand t
    :commands (org-babel-execute:jupyter-python)
    :custom (jupyter-long-timeout 25)   ; for Clojure Clojupyter long time startup.
    :init (add-to-list 'display-buffer-alist '("^\\*jupyter-repl.*\\*" . (display-buffer-below-selected)))
    :config
    (add-to-list 'org-babel-load-languages '(jupyter . t) 'append)
    (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
    ;; Map "jupyter-<language>" with programming language major modes for
    ;; `org-edit-special' [C-c '] and source block syntax highlighting.
    (add-to-list 'org-src-lang-modes '("jupyter-python" . python))
    (add-to-list 'org-src-lang-modes '("jupyter-ruby" . ruby))
    (add-to-list 'org-src-lang-modes '("jupyter-R" . R))
    (add-to-list 'org-src-lang-modes '("jupyter-julia" . julia))
    (add-to-list 'org-src-lang-modes '("jupyter-clojure" . clojure))
    ;; (org-babel-make-language-alias "jupyter-python" "python")
    (org-babel-make-language-alias "jupyter-ruby" "ruby")
    (org-babel-make-language-alias "jupyter-R" "R")
    (org-babel-make-language-alias "jupyter-julia" "julia")
    (org-babel-make-language-alias "jupyter-clojure" "clojure")
    ;; add ":kernel" header argument as safe for `org-lint'.
    (add-to-list 'org-babel-header-arg-names 'kernel)
    (add-to-list 'org-babel-header-arg-names 'session)
    ;; set default header arguments for Jupyter Python kernel.
    (defvar org-babel-default-header-args:jupyter-python nil) ; avoid Emacs loading error void symbol.
    (add-to-list 'org-babel-default-header-args:jupyter-python '(:kernel . "python3"))
    (add-to-list 'org-babel-default-header-args:jupyter-python '(:session . "python3")))

  (use-package jupyter-python)
  (use-package jupyter-R)
  (use-package jupyter-julia)
  (use-package jupyter-javascript)
  
  (use-package jupyter-org-extensions
    :defer t
    ;; :custom (jupyter-org-interaction-mode-map nil)
    :commands (jupyter-org-interaction-mode
               jupyter-org-hydra/body jupyter-org-insert-src-block)
    ;; Hydra commands to manipulate `ob-jupyter' source blocks.
    :bind (:map org-babel-map ("M-j" . jupyter-org-hydra/body)) ; `jupyter-org-interaction-mode-map' [C-c h]
    :init (remove-hook 'org-mode-hook #'jupyter-org-interaction-mode)))

;;; [ Emacs IPython Notebook (EIN) ] -- IPython notebook client in Emacs

;;; NOTE: `ein' depend on package `polymode' which has advice `polymode-with-current-base-buffer' on buffer saing.
(use-package ein
  :ensure t
  :defer t
  :mode ("\\.ipynb\\'" . ein:ipynb-mode)
  :commands (ein:notebook-open ein:run ein:login ein:stop)
  :config
  (defun my/jupyter-open-notebook-file (app &optional file)
    "A function to open Jupyter notebook file in Emacs."
    (when (bound-and-true-p file)
      (async-shell-command
       (format "%s %s %s"
               (executable-find "jupyter") "notebook" (shell-quote-argument file))
       " *jupyter-open-notebook*" " *jupyter-open-notebook*")))
  (add-to-list 'org-file-apps '("\\.ipynb\\'" . my/jupyter-open-notebook-file)))

;;; [ code-cells ] -- Emacs utilities for code split into cells, including Jupyter notebooks.

(use-package code-cells
  :ensure t
  ;; :custom (code-cells-convert-ipynb-style)
  :commands (code-cells-mode
             code-cells-mode-maybe
             code-cells-convert-ipynb code-cells-write-ipynb
             code-cells-eval))


(provide 'init-data-science)

;;; init-data-science.el ends here
