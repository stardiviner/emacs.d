;;; init-prog-web-server.el --- init for Web Server
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:
;;; [ elnode ] -- Asynchronous (non-blocking evented IO) HttpServer framework written in Emacs Lisp bind to Node.js.

;;; Usage:
;;
;; 1. [M-x elnode-make-webserver]
;; 2. Serve files from: [enter directory]
;; 3. TCP Port: (try something over 8000): 8009
;;
;; - `elnode-make-webserver'
;; - `elnode-start' & `elnode-stop'
;; - `list-elnode-servers' & `elnode-server-list'

(use-package elnode
  :ensure t
  :defer t
  :custom ((elnode-init-host "localhost")
           (elnode-init-port "8000"))
  :commands (elnode-make-webserver elnode-server-list elnode-start elnode-stop))

;;; [ web-server ]

(use-package web-server
  :ensure t
  :defer t)

;;; [ websocket ] -- Emacs WebSocket Client and Server

(use-package websocket
  :ensure t
  :defer t)


(provide 'init-prog-web-server)

;;; init-prog-web-server.el ends here
