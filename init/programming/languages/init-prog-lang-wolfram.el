;;; init-prog-lang-wolfram.el --- init for Wolfram
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ wolfram-mode ] -- Mathematica editing and inferior mode.

(use-package wolfram-mode
  :ensure t
  :defer t
  :mode (("\\.wl$" . wolfram-mode)
         ("\\.m$" . wolfram-mode))
  :commands (run-wolfram))

;;; [ ob-wolfram ] -- An org-babel integration for wolfram language.

(use-package ob-wolfram
  :after org
  :config
  (add-to-list 'org-babel-load-languages '(wolfram . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  (add-to-list 'org-babel-tangle-lang-exts '("wolfram" . "wls"))
  ;; add ":wolfram" header argument as safe for `org-lint'.
  (add-to-list 'org-babel-header-arg-names 'wolfram)
  (add-to-list 'org-babel-default-header-args:wolfram '(:eval . "yes"))
  (add-to-list 'org-babel-default-header-args:wolfram '(:noweb . "yes"))
  (add-to-list 'org-babel-default-header-args:wolfram '(:results . "output")))

;;; [ ob-mathematica ] -- Org Babel source block support for Wolfram Mathematica.

(use-package ob-mathematica
  :vc (:url "git@github.com:tririver/ob-mathematica.git")
  :custom (org-babel-mathematica-command
           (pcase system-type
             (darwin "MathematicaScript -script")
             (gnu/linux "mathematica -script")))
  :config
  (add-to-list 'org-babel-load-languages '(mathematica . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  (add-to-list 'org-src-lang-modes '("mathematica" . "mathematica"))
  ;; add ":wolfram" header argument as safe for `org-lint'.
  (add-to-list 'org-babel-header-arg-names 'mathematica)
  (add-to-list 'org-babel-default-header-args:mathematica '(:eval . "yes")))

;;; [ wolfram.el ] -- Wolfram Alpha integration by showing result with Org-mode.

(use-package wolfram
  :ensure t
  :defer t
  :custom (wolfram-program "wolframscript") ; "math"
  :commands (walfram-alpha)
  :bind (:map document-prefix ("A" . wolfram-alpha))
  :init (add-to-list 'display-buffer-alist '("^\\*wolfram\\*" . (display-buffer-below-selected)))
  (add-to-list 'display-buffer-alist '("^\\*WolframAlpha\\*" . (display-buffer-below-selected)))
  :custom (wolfram-alpha-app-id "YX2WUR-2J7GPTXY44"))


(provide 'init-prog-lang-wolfram)

;;; init-prog-lang-wolfram.el ends here
