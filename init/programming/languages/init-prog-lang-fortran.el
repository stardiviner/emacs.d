;;; init-prog-lang-fortran.el --- init for Fortran -*- lexical-binding: t; -*-

;;; Commentary:



;;; Code:

;;; [ fortran ] -- Built-in Fortran mode for GNU Emacs.

(use-package fortran
  :defer t
  :mode (("\\.f\\'" . fortran-mode)
         ("\\.f90\\'" . fortran-mode)
         ("\\.f95\\'" . fortran-mode)))

;;; [ ob-fortran ] -- Babel Functions for Fortran.

(use-package ob-fortran
  :defer t
  :after org
  :init
  (add-to-list 'org-babel-load-languages '(fortran . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  (add-to-list 'org-babel-tangle-lang-exts '("fortran" . "f90"))
  :config
  (add-to-list 'org-babel-default-header-args:fortran '(:eval . "yes"))
  (add-to-list 'org-babel-default-header-args:fortran '(:noweb . "yes"))
  (add-to-list 'org-babel-default-header-args:fortran '(:results . "output")))



(provide 'init-prog-lang-fortran)

;;; init-prog-lang-fortran.el ends here
