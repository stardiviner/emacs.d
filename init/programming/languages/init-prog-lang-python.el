;;; init-prog-lang-python.el --- init Python Programming Language Environment
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ python ] -- (built-in) Python's flying circus support for Emacs.

(use-package python
  :ensure t
  :defer t
  :commands (run-python python-mode)
  :bind (:map python-mode-map ("C-c C-s" . run-python))
  :custom ((python-shell-interpreter "python")
           (python-shell-dedicated t)
           (python-shell-completion-native-enable nil))
  :init
  (add-to-list 'display-buffer-alist '("^\\*Python\\*" . (display-buffer-below-selected)))
  (add-to-list 'display-buffer-alist '("^\\*Python Doc\\*" . (display-buffer-below-selected)))
  :config
  (cl-case python-shell-interpreter
    ("python"
     (setq python-shell-interpreter-args "-i"))
    ("ipython"
     (setq python-shell-interpreter-args "--simple-prompt --pprint")
     (setenv "IPY_TEST_SIMPLE_PROMPT" "1")))
  (add-hook 'python-mode-hook #'electric-pair-local-mode)
  (add-hook 'python-mode-hook #'flymake-mode-off 'append))

;;; [ ob-python ] -- Babel Functions for Python.

(use-package ob-python
  :defer t
  :after org
  :init
  (add-to-list 'org-babel-load-languages '(python . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  (add-to-list 'org-babel-tangle-lang-exts '("python" . "py"))
  ;; add ":python" header argument as safe for `org-lint'.
  (add-to-list 'org-babel-header-arg-names 'python)
  :config
  (add-to-list 'org-babel-default-header-args:python '(:eval . "yes"))
  (add-to-list 'org-babel-default-header-args:python '(:noweb . "yes"))
  (add-to-list 'org-babel-default-header-args:python '(:results . "output")))

(with-eval-after-load 'org
  (add-to-list 'org-default-properties "PyPI"))

;;; [ lsp-pyright ] -- Python LSP client using Pyright.

(use-package lsp-mode
  :ensure t
  :ensure lsp-pyright
  :defer t
  :hook (python-mode . lsp)
  :config (require 'lsp-pyright) (require 'dap-python))

;;; [ pyvenv ] -- Python virtual environment interface for Emacs.

;; (use-package pyvenv
;;   :ensure t
;;   :init (pyvenv-workon (if-let (virtual_env (getenv "VIRTUAL_ENV"))
;;                            (car (last (split-string virtual_env "/")))
;;                          "3.8"))
;;   :hook (python-mode . pyvenv-mode))

;;; [ conda ] -- Work with your conda environments.

;; (use-package conda
;;   :ensure t
;;   :mode ("\\.condarc\\'" . yaml-mode)
;;   :custom (conda-anaconda-home "/opt/homebrew/Caskroom/miniforge/base/") ;  "/usr/local/Caskroom/miniconda/base/"
;;   :init
;;   ;; if you want interactive shell [M-x shell] support, include:
;;   (conda-env-initialize-interactive-shells)
;;   ;; if you want Eshell [M-x eshell] support, include:
;;   (conda-env-initialize-eshell)
;;   ;; if you want auto-activation (see below for details), include:
;;   ;; (conda-env-autoactivate-mode t)
;;   ;; if you want to automatically activate a conda environment on the opening of a file:
;;   ;; (add-to-hook 'find-file-hook
;;   ;;              (lambda () (when (bound-and-true-p conda-project-env-path)
;;   ;;                      (conda-env-activate-for-buffer))))
;;   )

;;; [ micromamba ] -- Emacs package for working with micromamba environments.

(use-package micromamba
  :ensure t
  :custom (micromamba-executable (or (executable-find "micromamba") (executable-find "mamba")))
  :commands (micromamba-activate micromamba-deactivate))

;;; [ pet ] -- Executable and virtualenv tracker for `python-mode'.

;;; NOTE: disable pet to fix Org babel python source block editing suspend.
;; (use-package pet
;;   :ensure t
;;   :hook ((python-mode . pet-mode)
;;          (python-mode . pet-flycheck-setup))
;;   :init
;;   ;; set up python-mode to use the correct interpreter.
;;   (defun my/pet-setup-python-env ()
;;     (setq-local python-shell-interpreter (pet-executable-find "python")
;;                 python-shell-virtualenv-root (pet-virtualenv-root)))
;;   (add-hook 'python-mode-hook #'my/pet-setup-python-env))

;;; [ poetry ] -- Python dependency management and packaging in Emacs.

;; (use-package poetry
;;   :ensure t
;;   :defer t
;;   :commands (poetry)
;;   :hook (python-mode . poetry-tracking-mode))

;;; [ live-py-mode ] -- Python Live Coding in Emacs.

(use-package live-py-mode
  :ensure t
  :defer t
  :commands (live-py-mode))

;;; [ pdb ] -- Emacs built-in Python Debugger support for "pdb".

(use-package gud
  :defer t
  :commands (pdb))

;;; [ realgud ]

(use-package realgud
  :ensure t
  :defer t
  :commands (realgud:pdb))

;;; [ realgud-ipdb ] -- Realgud front-end to ipdb (IPython enabled pdb).

(use-package realgud-ipdb
  :ensure t
  :defer t
  :commands (realgud:ipdb))

;;; [ pygen ] -- Python code generation in Emacs with Elpy and python-mode.

;; (use-package pygen
;;   :ensure t
;;   :init
;;   (add-hook 'python-mode-hook 'pygen-mode))

;;; [ elpygen ] -- Generate a Python function/method using a symbol under point.

;;; [ cinspect ] -- Use cinspect to look at the CPython source of builtins and other C objects!

;; (use-package cinspect
;;   :ensure t
;;   :defer t
;;   :init (add-hook 'python-mode-hook 'cinspect))

;;; [ importmagic ] -- Fix Python imports using importmagic.

;; (use-package importmagic
;;   :ensure t
;;   :hook (python-mode . importmagic-mode))

;;; [ python-insert-docstring ] -- Automatic insertion of Google style function docstrings in Python.

(use-package python-insert-docstring
  :ensure t
  :bind (:map python-mode-map ("C-c i" . python-insert-docstring-with-google-style-at-point)))


(provide 'init-prog-lang-python)

;;; init-prog-lang-python.el ends here
