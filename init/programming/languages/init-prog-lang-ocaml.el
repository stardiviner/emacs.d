;;; init-prog-lang-ocaml.el --- init for OCaml
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ tuareg-mode ] -- major mode for OCaml.

(use-package tuareg
  :ensure t
  :defer t
  :commands (run-ocaml) ; Run an OCaml REPL process.  I/O via buffer *OCaml*.
  :init
  (autoload 'utop "utop" "Toplevel for OCaml" t)
  (autoload 'utop-minor-mode "utop" "Minor mode for utop" t)
  (add-to-list 'display-buffer-alist '("^\\*OCaml\\*" . (display-buffer-below-selected)))
  :config
  ;; setup environment variables using opam
  ;; (dolist (var (car
  ;;               (read-from-string (shell-command-to-string
  ;;                                  "opam config env --sexp"))))
  ;;   (setenv (car var) (cadr var)))
  
  ;; utop top level
  (add-hook 'tuareg-mode-hook 'utop-minor-mode))

;;; [ ob-ocaml ]

(use-package ob-ocaml
  :config
  (add-to-list 'org-babel-load-languages '(ocaml . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  (add-to-list 'org-babel-tangle-lang-exts '("ocaml" . "ml"))
  (add-to-list 'org-babel-default-header-args:ocaml
               '(:eval . "yes"))
  (add-to-list 'org-babel-default-header-args:ocaml
               '(:noweb . "yes"))
  (add-to-list 'org-babel-default-header-args:ocaml
               '(:results . "output")))

;;; [ opam ] -- OCaml Package Manger

;; Base configuration for OPAM

(defun opam-shell-command-to-string (command)
  "Similar to shell-command-to-string, but returns nil unless the process
  returned 0, and ignores stderr (shell-command-to-string ignores return value)"
  (let* ((return-value 0)
         (return-string
          (with-output-to-string
            (setq return-value
                  (with-current-buffer standard-output
                    (process-file shell-file-name nil '(t nil) nil
                                  shell-command-switch command))))))
    (if (= return-value 0) return-string nil)))

(defun opam-update-env (switch)
  "Update the environment to follow current OPAM switch configuration"
  (interactive
   (list
    (let ((default
           (car (split-string (opam-shell-command-to-string "opam switch show --safe")))))
      (completing-read
       (concat "opam switch (" default "): ")
       (split-string (opam-shell-command-to-string "opam switch list -s --safe") "\n")
       nil t nil nil default))))
  (let* ((switch-arg (if (= 0 (length switch)) "" (concat "--switch " switch)))
         (command (concat "opam config env --safe --sexp " switch-arg))
         (env (opam-shell-command-to-string command)))
    (when (and env (not (string= env "")))
      (dolist (var (car (read-from-string env)))
        (setenv (car var) (cadr var))
        (when (string= (car var) "PATH")
          (setq exec-path (split-string (cadr var) path-separator)))))))

(opam-update-env nil)

(defvar opam-share
  (let ((reply (opam-shell-command-to-string "opam config var share --safe")))
    (when reply (substring reply 0 -1))))

(add-to-list 'load-path (concat opam-share "/emacs/site-lisp"))
;; OPAM-installed tools automated detection and initialisation

(defun opam-setup-tuareg ()
  (add-to-list 'load-path (concat opam-share "/tuareg") t)
  (load "tuareg-site-file"))

(defun opam-setup-add-ocaml-hook (h)
  (add-hook 'tuareg-mode-hook h t)
  (add-hook 'caml-mode-hook h t))

(defun opam-setup-ocp-indent ()
  (autoload 'ocp-setup-indent "ocp-indent" "Improved indentation for Tuareg mode")
  (autoload 'ocp-indent-caml-mode-setup "ocp-indent" "Improved indentation for Caml mode")
  (add-hook 'tuareg-mode-hook 'ocp-setup-indent t)
  (add-hook 'caml-mode-hook 'ocp-indent-caml-mode-setup  t))

(defun opam-setup-ocp-index ()
  (autoload 'ocp-index-mode "ocp-index" "OCaml code browsing, documentation and completion based on build artefacts")
  (opam-setup-add-ocaml-hook 'ocp-index-mode))

(defun opam-setup-merlin ()
  (require 'merlin)
  (opam-setup-add-ocaml-hook 'merlin-mode)

  (defcustom ocp-index-use-auto-complete nil
    "Use auto-complete with ocp-index (disabled by default by opam-user-setup because merlin is in use)"
    :group 'ocp_index)
  (defcustom merlin-ac-setup 'easy
    "Use auto-complete with merlin (enabled by default by opam-user-setup)"
    :group 'merlin-ac)

  ;; So you can do it on a mac, where `C-<up>` and `C-<down>` are used
  ;; by spaces.
  (define-key merlin-mode-map
              (kbd "C-c <up>") 'merlin-type-enclosing-go-up)
  (define-key merlin-mode-map
              (kbd "C-c <down>") 'merlin-type-enclosing-go-down)
  (set-face-background 'merlin-type-face "skyblue"))

(defun opam-setup-utop ()
  (autoload 'utop "utop" "Toplevel for OCaml" t)
  (autoload 'utop-minor-mode "utop" "Minor mode for utop" t)
  (add-hook 'tuareg-mode-hook 'utop-minor-mode))

(defvar opam-tools
  '(("tuareg" . opam-setup-tuareg)
    ("ocp-indent" . opam-setup-ocp-indent)
    ("ocp-index" . opam-setup-ocp-index)
    ("merlin" . opam-setup-merlin)
    ("utop" . opam-setup-utop)))

(defun opam-detect-installed-tools ()
  (let*
      ((command "opam list --installed --short --safe --color=never")
       (names (mapcar 'car opam-tools))
       (command-string (mapconcat 'identity (cons command names) " "))
       (reply (opam-shell-command-to-string command-string)))
    (when reply (split-string reply))))

(defvar opam-tools-installed (opam-detect-installed-tools))

(defun opam-auto-tools-setup ()
  (interactive)
  (dolist (tool opam-tools)
    (when (member (car tool) opam-tools-installed)
      (funcall (symbol-function (cdr tool))))))

(opam-auto-tools-setup)

;;; [ merlin ] -- context-sensitive completion for OCaml in Vim and Emacs.

;;; add opam installed "merlin"
(let ((opam-share (ignore-errors (car (process-lines "opam" "var" "share")))))
  (when (and opam-share (file-directory-p opam-share))
    ;; Register Merlin
    (add-to-list 'load-path (expand-file-name "emacs/site-lisp" opam-share))
    (autoload 'merlin-mode "merlin" nil t nil)
    ;; Automatically start it in OCaml buffers
    (add-hook 'tuareg-mode-hook 'merlin-mode t)
    (add-hook 'caml-mode-hook 'merlin-mode t)
    ;; Use opam switch to lookup ocamlmerlin binary
    (setq merlin-command 'opam)))

(use-package merlin
  :ensure t
  :ensure merlin-eldoc
  :ensure merlin-company
  :defer t
  :init
  (defun my/merlin-mode-setup ()
    (merlin-mode 1)
    (my-company-add-backend-locally 'merlin-company-backend)
    (define-key tuareg-mode-map (kbd "C-c C-s") 'tuareg-run-metaocaml))
  (dolist (hook '(tuareg-mode-hook
                  caml-mode-hook))
    (add-hook hook #'my/merlin-mode-setup))
  
  :config
  ;; Use opam switch to lookup ocamlmerlin binary
  ;; (setq merlin-command 'opam)
  ;; (setq merlin-command "ocamlmerlin")
  ;; (setq tuareg-interactive-program "/usr/local/bin/opam config -- exec metaocaml")

  ;;(setq merlin-error-after-save nil)

  ;; complete
  (setq merlin-completion-dwim t
        merlin-completion-types t
        merlin-completion-arg-type t
        merlin-completion-with-doc t)

  (defun tuareg-run-metaocaml ()
    "Run an OCaml toplevel process.  I/O via buffer `*ocaml-toplevel*'."
    (interactive)
    (tuareg-run-process-if-needed
     "/usr/bin/opam config exec -- metaocaml")
    (display-buffer tuareg-interactive-buffer-name)))

;;; [ lsp-mode ] -- ocaml-lsp-server.

(use-package lsp-mode
  :ensure t
  :defer t
  :hook (tuareg-mode . lsp))

;;; [ utop ] -- universal toplevel for OCaml.

(use-package utop
  :ensure t
  :defer t)


(provide 'init-prog-lang-ocaml)

;;; init-prog-lang-ocaml.el ends here
