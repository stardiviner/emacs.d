;;; init-prog-lang-emacs-lisp.el --- init Emacs Lisp for Emacs
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

(use-package elisp-mode
  :config
  (defun my/emacs-lisp-mode-setup ()
    "Setting up \\=`emacs-lisp-mode'."
    (cond
     (corfu-mode
      (setq-local completion-at-point-functions
				  (list #'cape-file
                        #'elisp-completion-at-point
                        #'cape-elisp-symbol
                        (if (featurep 'yasnippet-capf)
                            #'yasnippet-capf
                          (cape-company-to-capf 'company-yasnippet))
                        #'cape-abbrev
				        #'cape-dabbrev))
      (setq-local cape-dabbrev-min-length 2)
      (setq-local cape-dabbrev-check-other-buffers t))
     (company-mode
      )))

  (add-hook 'emacs-lisp-mode-hook #'my/emacs-lisp-mode-setup))

;; Recompile your elc when saving an elisp file.
;; (add-hook 'after-save-hook
;;           #'(lambda ()
;;               (when (file-exists-p (byte-compile-dest-file buffer-file-name))
;;                 (emacs-lisp-byte-compile)))
;;           'append 'local)

;;; [ ob-emacs-lisp ]

(use-package ob-emacs-lisp
  :defer t
  :commands (org-babel-execute:emacs-lisp)
  :config
  (add-to-list 'org-babel-load-languages '(emacs-lisp . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  (add-to-list 'org-babel-tangle-lang-exts '("emacs-lisp" . "el"))
  ;; (add-to-list 'org-babel-default-header-args:emacs-lisp '(:lexical . "yes"))
  (add-to-list 'org-babel-default-header-args:emacs-lisp '(:noweb . "yes"))
  (org-babel-make-language-alias "elisp" "emacs-lisp"))

;;; [ IELM (ELISP interactive) ] -- an REPL for emacs. (Read-Eval-Print-Loop)

(use-package ielm
  :ensure t
  :defer t
  :commands (ielm)
  :custom ((ielm-dynamic-return t)
           (ielm-dynamic-multiline-inputs t))
  :init (add-to-list 'display-buffer-alist '("^\\*ielm\\*" . (display-buffer-below-selected)))
  :hook ((ielm-mode . paredit-mode)
         (ielm-mode . eldoc-mode))
  :config
  (defun my/ielm-company-setup ()
    (my-company-add-backend-locally 'company-elisp))
  (add-hook 'ielm-mode-hook #'my/ielm-company-setup))

;;; [ eros ] -- Evaluation Result OverlayS for Emacs Lisp.

(use-package eros
  :ensure t
  :init (eros-mode t))

;;; [ flymake-elisp-config ] -- Setup `load-path' for flymake on Emacs Lisp mode.

(use-package flymake-elisp-config
  :ensure t
  :after flymake
  :init
  ;; Make `load-path' for flymake customizable through `flymake-elisp-config-load-path-getter'.
  (flymake-elisp-config-global-mode)
  ;; Automatically set `load-path' for flymake.
  (flymake-elisp-config-auto-mode))

;;; [ macrostep ] -- interactive macro-expander for Emacs.

(use-package macrostep
  :ensure t
  :defer t
  :commands (macrostep-expand)
  :custom ((macrostep-expand-in-separate-buffer nil)
           (macrostep-expand-compiler-macros t)))

;;; [ elmacro ] -- display keyboard macros or latest interactive commands as emacs lisp.

;; (use-package elmacro
;;   :ensure t
;;   :defer t
;;   :init (elmacro-mode 1))

;;; [ elp ] -- Emacs Lisp profiler.

(use-package elp
  :defer t
  :commands (elp-instrument-package elp-instrument-function elp-instrument-list elp-results)
  :init (add-to-list 'display-buffer-alist '("\\*ELP Profiling Results\\*" . (display-buffer-below-selected))))

;;; [ trace ] -- tracing facility for Emacs Lisp functions.

(use-package trace
  :commands (trace-function trace-function-foreground trace-function-background)
  :init (add-to-list 'display-buffer-alist '("\\*trace-output\\*" . (display-buffer-below-selected))))

(defmacro +measure-time (&rest body)
  "Measure the time it takes to evaluate BODY."
  `(let ((time (current-time)))
     ,@body
     (let ((measured-time (format "%.06fs" (float-time (time-since time)))))
       (message measured-time)
       measured-time)))

;;; [ ERT ] -- Emacs Lisp Regression Testing.

(use-package ert
  :commands (ert ert-run-tests-interactively)
  :init (add-to-list 'display-buffer-alist '("^\\*ert\\*" . (display-buffer-below-selected))))

(use-package ert-x
  :commands (ert-kill-all-test-buffers))

;;; [ ert-results ] -- Filter ERT test results display, Add {f} filter and {t} toggle display to Emacs `ert-results-mode'.

(use-package ert-results
  :ensure t
  :after ert)

;;; [ xtest ] -- Simple Testing with Emacs & ERT

;;; [ faceup ] -- Regression test system for font-lock

;;; [ test-simple ] -- Simple Unit Test Framework for Emacs Lisp

;;; [ buttercup ] -- Behavior-Driven Emacs Lisp Testing

;;; [ dash.el ] -- A modern list library for Emacs.

(use-package dash
  :ensure t
  :defer t
  ;; syntax highlighting of dash functions.
  :init (eval-after-load 'dash '(dash-enable-font-lock)))

;;; [ elisp-depmap ] -- A library to generate a dependency map for elisp projects.

(use-package elisp-depmap
  :ensure t
  :commands (elisp-depmap-graphviz-digraph elisp-depmap-graphviz elisp-depmap-makesummarytable))

;;; [ pair-tree ] -- An Emacs Lisp cons cell explorer as a learning tool for visualizing Emacs Lisp lists.

(use-package pair-tree
  :ensure t
  :defer t
  :commands (pair-tree)
  :init (add-to-list 'display-buffer-alist '("^\\*pair-tree\\*\\'" . (display-buffer-below-selected)))
  (add-to-list 'display-buffer-alist '("^\\*sexp\\*\\'" . (display-buffer-below-selected))))


(provide 'init-prog-lang-emacs-lisp)

;;; init-prog-lang-emacs-lisp.el ends here
