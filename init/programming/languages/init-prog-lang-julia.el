;;; init-prog-lang-julia.el --- init for Julia
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ julia-mode ] -- Major mode for editing Julia source code.

(use-package julia-mode
  :ensure t
  :defer t
  :commands (run-julia inferior-julia)
  :config
  ;; add `julia-mode' to `prog-mode''.
  (add-hook 'julia-mode-hook
            (lambda () (unless (derived-mode-p 'prog-mode) (run-hooks 'prog-mode-hook)))))

;;; [ ESS - Julia ] -- Julia support with ESS.

(use-package ess
  :ensure t
  :defer t
  :requires (ess-julia)
  :commands (run-ess-julia julia))

;;; [ ob-julia ] -- Org babel support for Julia language.

(use-package ob-ess-julia
  :ensure t
  :ensure ess
  :defer t
  :commands (org-babel-execute:ess-julia)
  :init (defalias 'org-babel-execute:julia #'org-babel-execute:ess-julia)
  :config
  (add-to-list 'org-babel-load-languages '(ess-julia . t))
  (add-to-list 'org-babel-load-languages '(julia . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  ;; Link this language to ess-julia-mode (although it should be done by default):
  (add-to-list 'org-src-lang-modes '("ess-julia" . ess-julia)))

;;; [ flycheck-julia ] -- Add a julia syntax checker to flycheck using Lint.jl

;; (use-package flycheck-julia
;;   :ensure t
;;   :defer t
;;   :init (flycheck-julia-setup)
;;   (when global-flycheck-mode
;;     (add-to-list 'flycheck-global-modes 'julia-mode)
;;     (add-to-list 'flycheck-global-modes 'ess-julia-mode)))


(provide 'init-prog-lang-julia)

;;; init-prog-lang-julia.el ends here
