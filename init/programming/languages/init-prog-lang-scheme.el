;;; init-prog-lang-scheme.el --- init for Scheme
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ Scheme Mode ]

(use-package scheme
  :defer t
  :custom (scheme-program-name "guile")
  :commands (run-scheme)
  :hook (scheme-mode . paredit-mode)
  :init (add-to-list 'display-buffer-alist '("\\* Guile REPL \\*" . (display-buffer-below-selected)))
  :config
  (define-key scheme-mode-map (kbd "C-c C-s") 'run-scheme)
  
  ;; auto run `run-scheme' for scheme buffer.
  (defun run-scheme-auto-create ()
    "Auto run `run-scheme' when not running."
    (unless (and scheme-buffer
                 (get-buffer scheme-buffer)
                 (comint-check-proc scheme-buffer))
      (save-window-excursion
        (run-scheme scheme-program-name))
      ;; (switch-to-buffer scheme-buffer)
      ))
  ;; (add-hook 'scheme-mode-hook 'run-scheme-auto-create)
  )

;;; [ geiser ] -- Scheme completion.

(use-package geiser
  :ensure t
  :ensure geiser-guile
  :ensure geiser-mit
  :defer t
  :commands (run-geiser run-guile)
  :custom ((geiser-default-implementation 'guile)) ; 'guile, 'racket, 'chicken
  :hook (scheme-mode . geiser-mode)
  :config
  ;; company-backend
  (defun my/geiser-company-setup ()
    (my-company-add-backend-locally 'geiser-company-backend))
  (add-hook 'scheme-mode-hook #'my/geiser-company-setup)
  ;; auto start geiser inferior buffer process `run-geiser'.
  (with-eval-after-load 'scheme
    (defun my-run-geiser-auto ()
      (interactive)
      (let ((geiser-guile-buffer "* Guile REPL *")
            (geiser-racket-buffer "* Racket REPL *")
            (geiser-chicken-buffer "* Chicken REPL *"))
        (unless (get-buffer geiser-guile-buffer)
          (save-window-excursion
            (run-geiser geiser-default-implementation)))))
    (define-key scheme-mode-map (kbd "C-c C-s") 'my-run-geiser-auto)))

;;; [ Quack ] -- enhanced Emacs Support for Editing and Running Scheme/Racket Code.

(use-package quack
  :ensure t
  :defer t)

;;; [ ob-scheme ] -- Babel Functions for Scheme.

(use-package ob-scheme
  :defer t
  :after org
  :init
  (add-to-list 'org-babel-load-languages '(scheme . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  (add-to-list 'org-babel-tangle-lang-exts '("scheme" . "scm"))
  ;; FIXME: ob-scheme does not use already opened existing REPL session.
  (with-eval-after-load 'scheme
    (pcase scheme-program-name
      ("guile"
       (add-to-list 'org-babel-default-header-args:scheme '(:session . "* Guile REPL *")))
      ("racket"
       (add-to-list 'org-babel-default-header-args:scheme '(:session . "* Racket REPL *"))))))

;;; [ scheme-complete ] -- Scheme-complete provides a single function, scheme-smart-complete, which you can use for intelligent, context-sensitive completion for any Scheme implementation.

(use-package scheme-complete
  :ensure t
  :config
  (defun my/scheme-complete-setup ()
    "Setup scheme-complete for `completion-at-point-functions' with `scheme-smart-complete'."
    ;; complete
    ;; (make-local-variable 'completion-at-point-functions)
    ;; (add-to-list 'completion-at-point-functions 'scheme-smart-complete)
    (define-key scheme-mode-map (kbd "TAB") 'scheme-smart-complete)
    ;; indent
    (setq-local lisp-indent-function 'scheme-smart-indent-function)
    ;; eldoc
    (if (boundp 'eldoc-documentation-functions)
        (add-hook 'eldoc-documentation-functions 'scheme-get-current-symbol-info nil t)
      (set (make-local-variable 'eldoc-documentation-function) 'scheme-get-current-symbol-info))
    (eldoc-mode 1))
  (add-hook 'scheme-mode-hook 'my/scheme-complete-setup)
  (when (featurep 'geiser)
    (add-hook 'geiser-mode-hook 'my/scheme-complete-setup)))


(provide 'init-prog-lang-scheme)

;;; init-prog-lang-scheme.el ends here
