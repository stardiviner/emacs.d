;;; init-prog-lang-swift.el --- init for Swift
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ swift-mode ]

(use-package swift-mode
  :ensure t
  :defer t
  :commands (run-swift)
  :init (add-to-list 'display-buffer-alist '("^\\*Swift REPL .*\\*" . (display-buffer-below-selected)))
  :config
  (cl-case system-type
    (darwin
     (setq swift-mode:repl-executable
           (concat (when (executable-find "xcrun") "xcrun ") "swift")))
    (gnu/linux
     (setq swift-mode:repl-executable "docker run --rm --privileged -it swift swift")
     (defun swift-mode-docker-attach-repl (cmd &optional dont-switch keep-default)
       (docker-container-attach "swift" nil)
       (rename-buffer "*swift*"))
     (advice-add 'run-swift :after #'swift-mode-docker-attach-repl))))

;; [ ob-swift ] -- Org Babel functions for swift evaluation.

(use-package ob-swift
  :vc t :load-path "~/Code/Emacs/ob-swift"
  :defer t
  :commands (org-babel-execute:swift)
  :config
  (add-to-list 'org-babel-load-languages '(swift . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  (add-to-list 'org-babel-tangle-lang-exts '("swift" . "swift")))

;;; [ ob-swiftui ] -- Org Babel functions for SwiftUI evaluation.

(use-package ob-swiftui
  :ensure t
  :defer t
  :commands (org-babel-execute:swiftui)
  :config
  (add-to-list 'org-babel-load-languages '(swiftui . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  (add-to-list 'org-src-lang-modes '("swiftui" . swift))
  (add-to-list 'org-babel-tangle-lang-exts '("swiftui" . "swift")))

;;; [ lsp-sourcekit ] -- sourcekit-lsp client for lsp-mode.

(use-package lsp-sourcekit
  :ensure t
  :defer t
  :hook (swift-mode . lsp)
  ;; :custom (lsp-sourcekit-executable (executable-find "sourcekit-lsp"))
  :custom (lsp-sourcekit-executable "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/sourcekit-lsp")
  :config
  ;; lsp-sourcekit can't get complete candidates
  ;; https://github.com/emacs-lsp/lsp-sourcekit/issues/18
  (defun sourcekit-lsp-completion-fix (method &rest args)
    "Quick'n dirty fix for https://github.com/emacs-lsp/lsp-sourcekit/issues/18"
    (when (and (eq major-mode 'swift-mode) (string-equal method "textDocument/completion"))
      (message "%s" method)
      (lsp-completion--clear-cache)))
  (advice-add 'lsp-request :before 'sourcekit-lsp-completion-fix))

;;; [ company-sourcekit ] -- company-mode completion backend for SourceKit.

(use-package company-sourcekit
  :ensure t
  :defer t
  :init
  (defun my/company-sourcekit-setup ()
    (my-company-add-backend-locally 'company-sourcekit))
  (add-hook 'swift-mode-hook #'my/company-sourcekit-setup))

;;; [ flycheck-swift ] -- Flycheck extension for Apple's Swift.

(use-package flycheck-swift
  :ensure t
  :defer t
  :hook (swift-mode . flycheck-swift-setup))


(provide 'init-prog-lang-swift)

;;; init-prog-lang-swift.el ends here
