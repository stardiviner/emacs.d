;;; init-prog-lang-lisp-newLisp.el --- init for newLisp
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ newlisp-mode ]

(use-package newlisp-mode
  :ensure t
  :defer t
  :preface (define-derived-mode newlisp-mode prog-mode "NewLisp")
  :mode "\\.lsp$"
  :interpreter "newlisp"
  :config
  (add-hook 'newlisp-mode-hook
            #'(lambda ()
                (add-hook 'completion-at-point-functions
                          'newlisp-completion-at-point nil t)))
  
  ;; setup `*newlisp*' buffer (`comint-mode' of newLisp)
  (defun newlisp-repl-inferior-buffer-setup ()
    (if (equal (buffer-name) "*newlisp*")
        (progn
          (if (fboundp 'paredit-mode)
              (paredit-mode t)
            (when (fboundp 'smartparens-strict-mode)
              (smartparens-strict-mode 1)))
          ;; for `company-mode' backend `company-capf'
          (add-hook 'completion-at-point-functions
                    'newlisp-completion-at-point nil t))))

  (add-hook 'comint-mode-hook 'newlisp-repl-inferior-buffer-setup))


(provide 'init-prog-lang-lisp-newLisp)

;;; init-prog-lang-lisp-newLisp.el ends here
