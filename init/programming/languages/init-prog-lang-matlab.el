;;; init-prog-lang-matlab.el --- init for Matlab

;;; Commentary:



;;; Code:


;;; [ matlab-mode ]

(use-package matlab-mode
  :if (executable-find "matlab")
  :ensure t
  :mode ("\\.m\\'" . matlab-mode)
  ;; Modules:
  ;; `matlab-shell' - Run MATLAB in an inferior process.
  ;; `matlab-netshell' - Control MATLAB from a network port.
  ;; `matlab-complete' - Simple completion tool for matlab-mode.
  ;; `matlab-cgen' - In buffer code generation features (templates, etc). [C-c C-c] keymap
  ;; `matlab-scan' - Tools for contextually scanning a MATLAB buffer.
  ;; `matlab-topic' - Help browsing via Emacs buffers.
  ;; `matlab-publish' - Utilities for editing MATLAB files for publishing.
  :custom (matlab-shell-command-switches '("-nodesktop" "-nosplash"))
  :commands (matlab-shell
             matlab-shell-describe-command
             matlab-netshell-server-start
             matlab-complete-symbol
             matlab-generate-latex
             matlab-publish-region)
  :init (add-to-list 'display-buffer-alist '("^\\*MATLAB\\*" . (display-buffer-below-selected))))

;;; [ ob-matlab ] -- Babel support for Matlab.

(use-package ob-matlab
  :if (executable-find "matlab")
  :defer t
  :commands (org-babel-execute:matlab)
  :config
  (add-to-list 'org-babel-load-languages '(matlab . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  (add-to-list 'org-babel-tangle-lang-exts '("matlab" . "m"))
  (add-to-list 'org-babel-default-header-args:matlab '(:session . "*MATLAB*"))
  (add-to-list 'org-babel-default-header-args:matlab '(:noweb . "yes"))
  (add-to-list 'org-babel-default-header-args:matlab '(:results . "output")))



(provide 'init-prog-lang-matlab)

;;; init-prog-lang-matlab.el ends here
