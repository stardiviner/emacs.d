;;; init-prog-lang-javascript.el --- init JavaScript for Emacs
;;; -*- coding: utf-8 -*-

;;; Commentary:


;;; Code:

;;; [ JavaScript ]

(use-package js
  :mode (("\\.js\\'" . js-mode)
         ("\\.jsx\\'" . js-jsx-mode))
  :config
  ;; fix `js-find-symbol' [M-.] overriding other packages' keybinding.
  (substitute-key-definition 'js-find-symbol 'xref-find-definitions js-mode-map))

;;; [ js-comint ] -- JavaScript interpreter in window.

(use-package js-comint
  :ensure t
  :defer t
  :commands (run-js)
  :init (add-to-list 'display-buffer-alist '("^\\*JavaScript REPL\\*$" . (display-buffer-below-selected))))

;;; [ ob-js ] -- Babel Functions for Javascript.

(use-package ob-js
  :defer t
  :after org
  :init
  (add-to-list 'org-babel-load-languages '(js . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  (add-to-list 'org-babel-tangle-lang-exts '("js" . "js"))

  (org-babel-make-language-alias "javascript" "js")
  
  (defun org-babel-insert-header-arg:js ()
    "Insert ob-js `SESSION' header argument.
- `js-comint'
- `skewer-mode'
- `Indium'
"
    (interactive)
    ;; detect current source block LANGUAGE.
    (if (or (string-equal "js" (org-element-property :language (org-element-context)))
            (string-equal "javascript" (org-element-property :language (org-element-context))))
        (let ((session (completing-read "ob-js session: "
                                        `(,(when (featurep 'js-comint) "js-comint")
                                          ,(when (featurep 'skewer-mode) "skewer-mode")
                                          ,(when (featurep 'indium) "indium")))))
          (org-babel-insert-header-arg
           "session"
           (pcase session
             ("js-comint" "\"*Javascript REPL*\"")
             ("skewer-mode" "\"*skewer-repl*\"")
             ("indium" "\"*JS REPL*\""))))
      (user-error "This command ONLY works on JavaScript source block.")))

  (define-key org-babel-map (kbd "J") 'org-babel-insert-header-arg:js)
  
  :config
  (add-to-list 'org-babel-default-header-args:js '(:results . "output")))

;;; [ ob-deno ] -- Babel Functions for Javascript/TypeScript with Deno.

(use-package ob-deno
  :ensure t
  :defer t
  :after org
  :init
  (add-to-list 'org-babel-load-languages '(deno . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  (add-to-list 'org-babel-tangle-lang-exts '("deno" . "js"))
  :config
  (add-to-list 'org-babel-default-header-args:deno '(:results . "output"))
  ;; optional (require the typescript.el)
  (add-to-list 'org-src-lang-modes '("deno" . typescript)))

;;; [ nvm ] -- Manage Node versions within Emacs.

;; (use-package nvm
;;   :ensure t
;;   :init (nvm-use "v13.5.0"))

;;; [ lsp-mode ] -- Javascript and Typescript support for lsp-mode using javascript-typescript-langserver.

(use-package lsp-mode
  :ensure t
  :defer t
  :init (with-eval-after-load 'lsp-mode
          (add-to-list 'lsp-disabled-clients '(js-mode . (json-ls graphql-ls))))
  :hook ((js-mode rjsx-mode) . lsp))

(with-eval-after-load 'org
  (add-to-list 'org-default-properties "npm"))

;;; [ npm-mode ] -- minor mode for working with npm projects.

;; `npm-mode-*' commands prefix, [C-c n] keybinding prefix.
(use-package npm-mode
  :ensure t
  :defer t
  :hook (js-mode . npm-mode))

;;; [ typescript-mode ] -- Major mode for editing typescript.

(use-package typescript-mode
  :ensure t
  :defer t)

;;; [ ob-typescript ] -- org-babel functions for typescript evaluation.

(use-package ob-typescript
  :ensure t
  :defer t
  :after org
  :init
  (add-to-list 'org-babel-load-languages '(typescript . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  (add-to-list 'org-babel-tangle-lang-exts '("typescript" . "ts"))
  :config
  (add-to-list 'org-babel-default-header-args:typescript '(:results . "output")))

;;; [ tide ] -- Typescript Interactive Development Environment.

(use-package tide
  :ensure t
  :after typescript-mode
  :commands (tide-mode)
  :hook ((typescript-mode . tide-setup)
         (typescript-mode . tide-hl-identifier-mode)))

;;; [ lsp-mode ] -- Javascript and Typescript support for lsp-mode using javascript-typescript-langserver.

(use-package lsp-mode
  :ensure t
  :hook (typescript-mode . lsp))

;;; [ osa ] -- OSA (JavaScript / AppleScript) bridge.

(use-package osa
  :ensure t
  :defer t
  :commands (osa-unpack osa-eval osa-eval-file))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Vue.js                                                                           ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package vue-mode
  :ensure t
  :ensure vue-html-mode
  :defer t)


(provide 'init-prog-lang-javascript)

;;; init-prog-lang-javascript.el ends here
