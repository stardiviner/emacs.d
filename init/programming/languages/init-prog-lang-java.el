;;; init-prog-lang-java.el --- init Java
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ sdkman ] -- The Software Development Kit Manager

(when (file-exists-p "/Users/stardiviner/.sdkman/candidates/java/current/bin/")
  (add-to-list 'exec-path "/Users/stardiviner/.sdkman/candidates/java/current/bin/"))

;; pom  files treated as xml file.
(add-to-list 'auto-mode-alist '("\\.pom\\'" . nxml-mode))
;; jmod files treated as archive file.
(add-to-list 'auto-mode-alist '("\\.jmod\\'" . archive-mode))

(defun my/java-mode-setup ()
  "My setup for `java-mode'."
  ;; set tab with to 4.
  (setq-local c-basic-offset 4)
  (setq-local tab-width 4)
  (setq-local standard-indent 4)
  (setq-local completion-ignore-case t))
(add-hook 'java-mode-hook #'my/java-mode-setup)

(add-hook 'java-mode-hook #'electric-pair-local-mode)

(defun java-mode-electric-layout-setting ()
  "auto insert newline after specific characters."
  (setq-local electric-layout-rules '((?\; . after)))
  (add-to-list 'electric-layout-rules '( ?\{ .  after))
  (add-to-list 'electric-layout-rules '( ?\} .  before)))
(add-hook 'java-mode-hook #'electric-layout-local-mode)
(add-hook 'java-mode-hook #'java-mode-electric-layout-setting)

;;; [ ob-java ] -- org-babel functions for java evaluation.

(use-package ob-java
  :defer t
  :after org
  :init
  (add-to-list 'org-babel-load-languages '(java . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  (add-to-list 'org-babel-tangle-lang-exts '("java" . "java"))
  (add-to-list 'org-babel-header-arg-names 'classname)
  :config
  (add-to-list 'org-babel-default-header-args:java '(:results . "output")))

;;; [ eglot-java ] -- Java extension for the eglot LSP client.

;; (use-package eglot-java
;;   :ensure t
;;   :hook (java-mode . eglot-ensure))

;;; [ lsp-java ] -- Java support for lsp-mode.

(use-package lsp-java
  :ensure t
  :hook (java-mode . lsp)
  ;; `aggressive-indent-mode' caused long suspend with lots of requests.
  :init (with-eval-after-load 'aggressive-indent
          (add-to-list 'aggressive-indent-excluded-modes 'java-mode))
  :config (require 'dap-java))

;;; [ gradle-mode ] -- Gradle integration with Emacs' `compile'.

(use-package gradle-mode
  :ensure t
  :delight gradle-mode)

;;; [ sdkman ] -- Software Development Kit

;;; add Gradle & Groovy bins to TRAMP path.
;; (add-to-list 'tramp-remote-path
;;              (concat (getenv "HOME")
;;                      "/.sdkman/candidates/gradle/current/bin"))
;; (add-to-list 'tramp-remote-path
;;              (concat (getenv "HOME")
;;                      "/.sdkman/candidates/groovy/current/bin"))

;;; [ jarchive ] -- Open project dependencies inside jar archives, pairs well with eglot!

;; Opening a jar URI via Emacs functions like `find-file' will
;; automatically extract the contents of a file contained in the jar
;; and open them in a buffer.

(use-package jarchive
  :ensure t
  :custom (eglot-extend-to-xref t)
  :commands (jarchive-mode jarchive-patch-eglot)
  :mode ("\\.jar\\'" . jarchive-mode))

;;; [ javap-mode ] -- show the ouput of javap when opening a jvm class file in Emacs.

(use-package javap-mode
  :ensure t
  :commands (javap-buffer javap-mode))


(provide 'init-prog-lang-java)

;;; init-prog-lang-java.el.el ends here
