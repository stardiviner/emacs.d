;;; init-prog-lang-racket.el --- init for Racket

;;; Commentary:



;;; Code:

;;; [ racket-mode ] -- Major mode for Racket language.

(use-package racket-mode
  :ensure t
  :mode (("\\.rkt\\'" . racket-mode))
  :commands (racket-documentation-search))

;;; [ ob-racket ]

(use-package ob-racket
  :vc (:url "https://github.com/DEADB17/ob-racket")
  :config
  (add-to-list 'org-babel-load-languages '(racket . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  (add-to-list 'org-babel-tangle-lang-exts '("racket" . "rkt")))

;;; [ geiser ] -- GNU Emacs and Scheme talk to each other.

(use-package geiser
  :ensure t
  :ensure geiser-racket
  :defer t
  :commands (run-geiser run-racket geiser-racket-connect)
  :init (add-to-list 'display-buffer-alist '("\\* Racket REPL \\*" . (display-buffer-below-selected))))

;;; [ Quack ] -- enhanced Emacs Support for Editing and Running Scheme/Racket Code.

(use-package quack
  :ensure t
  :defer t)



(provide 'init-prog-lang-racket)

;;; init-prog-lang-racket.el ends here
