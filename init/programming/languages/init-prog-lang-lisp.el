;;; init-prog-lang-lisp.el --- Lisp dialects init
;;
;;; Commentary:

;;; Code:

;;; [ Lisp ]

;;; Common Settings for All Lisp dialects

;; for command `run-lisp'.
(setq inferior-lisp-program "sbcl")

;;; [ lisp-mode ]

;;; [ lisp-data-mode ] -- A major-mode for Lisp s-expressions like ".dir-locals.el", but not for executable program.

;;; [ lisp-extra-font-lock ] -- Highlight bound variables and quoted expressions in Lisp.

;; (use-package lisp-extra-font-lock
;;   :ensure t
;;   :config (lisp-extra-font-lock-global-mode 1))

;;; [ Par Edit (paredit) ] -- performing structured editing of S-expression data.

(use-package paredit
  :ensure t
  :defer t
  :delight paredit-mode
  :hook ((emacs-lisp-mode . paredit-mode)
         (lisp-mode . paredit-mode)
         (lisp-interactive-mode . paredit-mode)
         (common-lisp-mode . paredit-mode)
         (scheme-mode . paredit-mode)
         (clojure-mode . paredit-mode)
         (clojurescript-mode . paredit-mode))
  :config
  ;; remove following keybindings
  (dolist (binding (list (kbd "C-<left>") (kbd "C-<right>")
                         (kbd "C-M-<left>") (kbd "C-M-<right>")
                         ;; avoid `paredit-mode' bind key [RET] for command `paredit-RET'.
                         (kbd "RET")))
    (define-key paredit-mode-map binding nil))
  ;; disable kill-sentence, which is easily confused with the kill-sexp binding,
  ;; but doesn't preserve sexp structure
  (define-key paredit-mode-map [remap kill-sentence] nil)
  (define-key paredit-mode-map [remap backward-kill-sentence] nil)
  ;; don't override [M-s] keybinding prefix.
  (define-key paredit-mode-map (kbd "M-s") nil)

  ;; patches

  ;; fix paredit auto add space before ( in circular list refs. #1= (a b c . #1#)
  (add-to-list 'paredit-space-for-delimiter-predicates 'no-space-for-list-refs)

  (defun no-space-for-list-refs (endp delimiter)
    (or endp
        (save-excursion
          (not (and (eq delimiter ?\()
                    (re-search-backward (rx "#" (+ (any "0-9")) "=")
                                        (line-beginning-position) t))))))

  ;; fix paredit auto add space before ( in list splicing. ",@()".
  (add-to-list 'paredit-space-for-delimiter-predicates 'no-space-for-list-splicing)

  (defun no-space-for-list-splicing (endp delimiter)
    (or endp
        (save-excursion
          (not (and (eq delimiter ?\()
                    (re-search-backward (rx ",@")
                                        (line-beginning-position) t)))))))

;;; [ rainbow-delimiters ] -- rainbow color parenthesis

(use-package rainbow-delimiters
  :ensure t
  :defer t
  :delight rainbow-delimiters-mode
  :hook ((emacs-lisp-mode . rainbow-delimiters-mode)
         (lisp-mode . rainbow-delimiters-mode)
         (lisp-interaction-mode . rainbow-delimiters-mode)
         (common-lisp-mode . rainbow-delimiters-mode)
         (scheme-mode . rainbow-delimiters-mode)
         (clojure-mode . rainbow-delimiters-mode)
         (clojurescript-mode . rainbow-delimiters-mode)))

;;; [ highlight-blocks ] -- Highlight the blocks point is in with different colors for lisp s-exp blocks.

(use-package highlight-blocks
  :ensure t
  :hook (((lisp-mode emacs-lisp-mode common-lisp-mode clojure-mode) . highlight-blocks-mode))
  :config
  ;;========================================= colorful faces ========================================
  ;; auto update `highlight-blocks' face colors.
  (defvar highlight-blocks--rainbow-colors nil)

  (defun highlight-blocks--rainbow-colors--follow-appearance (&optional theme)
    "Set highlight-blocks background colors list based on color theme light or dark."
    (let (;; light theme colors
          (leuven-colors     '("#BAFFFF" "#FFCACA" "#FFFFBA" "#CACAFF" "#CAFFCA" "#FFBAFF" "#FFE7BA" "#E6E6FA" "#87CEFF" "#EEE9E9"))
          (light-blue-colors '("#0091EA" "#00AFFE" "#40C4FE" "#80D7FE" "#01569B" "#0277BC" "#0287D1" "#039BE4" "#03A9F4" "#29B6F6"))
          ;; dark theme colors
          (gradiant-gray-colors '("#253138" "#37464F" "#455A63" "#536D79" "#5F7C8A" "#78909C" "#90A3AD" "#AFBDC5" "#CFD7DC" "#EBEEF0"))
          (palenight-blue-colors '("#292D3E" "#434966" "#50587A" "#67719D" "#7B87BB" "#8E9CD8" "#2F3E9F" "#5C6BC0" "#3C5AFD" "#3E51B5"))
          (indiago-blue-colors   '("#1A227D" "#283592" "#2F3E9F" "#3948AB" "#3E51B5" "#5C6BC0" "#7985CA" "#9FA8DA" "#C5C9E9" "#E8EAF6")))
      (cl-case (frame-parameter nil 'background-mode)
        (light
         (cl-case theme
           (leuven (setq highlight-blocks--rainbow-colors leuven-colors))
           (t (setq highlight-blocks--rainbow-colors leuven-colors))))
        (dark
         (cl-case theme
           (doom-palenight (setq highlight-blocks--rainbow-colors palenight-blue-colors))
           (doom-one (setq highlight-blocks--rainbow-colors gradiant-gray-colors))
           (doom-nord (setq highlight-blocks--rainbow-colors gradiant-gray-colors))
           (t (setq highlight-blocks--rainbow-colors gradiant-gray-colors)))))))

  (defun highlight-blocks--define-rainbow-colors (colors)
    (dotimes (i (length colors))
      (face-spec-set
       (intern (format "highlight-blocks-depth-%d-face" (1+ i)))
       `((((class color) (background dark))  :background ,(nth i colors) :extend t)
         (((class color) (background light)) :background ,(nth i colors) :extend t))
       'face-defface-spec)))

  (defun highlight-blocks--rainbow-colors--reset (&optional theme)
    (highlight-blocks--rainbow-colors--follow-appearance theme)
    (highlight-blocks--define-rainbow-colors highlight-blocks--rainbow-colors))

  (add-hook 'load-theme-after-hook #'highlight-blocks--rainbow-colors--reset 90)
  (add-hook 'after-init-hook #'highlight-blocks--rainbow-colors--reset 90))

;;; [ eval-sexp-fu ] -- highlighting the sexps during evaluation in action.

;; (use-package eval-sexp-fu
;;   :ensure t
;;   :defer t
;;   :custom ((eval-sexp-fu-flash-duration 0.5)
;;            (eval-sexp-fu-flash-error-duration 1.5)
;;            ;; (eval-sexp-fu-flash-function)
;;            ;; (eval-sexp-fu-flash-doit-function)
;;            )
;;   :init (eval-sexp-fu-flash-mode))

;;; [ parinfer-mode ] -- Parinfer on Emacs with oakmac's parinfer-elisp.

;; (use-package parinfer
;;   :ensure t
;;   :defer t
;;   :bind (;; Use this to toggle Indent/Paren Mode.
;;          ("C-," . parinfer-toggle-mode)
;;          ;; Some other commands you may want.
;;          ("M-r" . parinfer-raise-sexp)
;;          ("M-m" . mark-sexp)
;;          ("M-j" . parinfer-transpose-sexps)
;;          ("M-k" . parinfer-reverse-transpose-sexps))
;;   :hook ((emacs-lisp-mode . parinfer-mode)
;;          (clojure-mode . parinfer-mode)))


(provide 'init-prog-lang-lisp)

;;; init-prog-lang-lisp.el ends here
