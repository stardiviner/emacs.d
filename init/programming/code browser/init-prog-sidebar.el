;;; init-prog-sidebar.el --- init Emacs sidebar for Programming.
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;; [ dired-sidebar ] -- Sidebar for Emacs leveraging Dired.

(use-package dired-sidebar
  :ensure t
  :ensure hide-mode-line
  :defer t
  :commands (dired-sidebar-toggle-sidebar dired-sidebar-toggle-with-current-directory)
  :bind ("<f8>" . dired-sidebar-toggle-sidebar)
  :custom ((dired-sidebar-no-delete-other-windows t)
           (dired-sidebar-theme 'nerd))
  :config
  (setq dired-sidebar-use-custom-font t)
  (set-face-attribute 'dired-sidebar-face nil
                      :family "FiraCode Nerd Font" :weight 'medium)

  ;; disable `zoom-mode' before toggle `dired-sidebar'.
  (when (featurep 'zoom)
    (defvar dired-sidebar--zoom-mode-status nil)
    (advice-add 'dired-sidebar-show-sidebar :before
                (lambda (&optional b)
                  (setq dired-sidebar--zoom-mode-status zoom-mode)
                  (zoom--off)))
    (advice-add 'dired-sidebar-hide-sidebar :after #'zoom--on)))

;;; [ treemacs ] -- A tree style file explorer package.

;; (use-package treemacs
;;   :ensure t
;;   :ensure treemacs-nerd-icons
;;   :ensure treemacs-tab-bar ; Integration of tab-bar-mode into treemacs' buffer scoping framework.
;;   :ensure hide-mode-line
;;   :defer t
;;   :commands (treemacs)
;;   ;; hide mode-line in dired-sidebar buffer.
;;   :hook (treemacs-mode . hide-mode-line-mode)
;;   :config
;;   ;; `treemacs-nerd-icons'
;;   (treemacs-load-theme "nerd-icons")
;;   ;; `treemacs-tab-bar'
;;   (treemacs-set-scope-type 'Tabs))



(provide 'init-prog-sidebar)

;;; init-prog-sidebar.el ends here
