;;; init-prog-debugger.el --- init for Debugger
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ GDB ]

(use-package gdb-mi
  :ensure t
  :defer t
  :commands (gdb gdb-many-windows)
  :custom ((gdb-many-windows t)
           (gdb-show-main t)))

;;; [ realgud ] -- A modular GNU Emacs front-end for interacting with external debuggers.

(use-package realgud
  :ensure t
  :defer t
  :custom ((realgud:gdb-command-name (cl-case system-type (darwin "lldb") (gnu/linux "gdb"))))
  :commands (realgud:gdb                               ; C/C++
             realgud:gdb-pid realgud:gdb-pid-associate ; debug process ID and associate with file.
             realgud:bashdb realgud:zshdb              ; Shell: Bash, Zsh
             realgud:pdb realgud:ipdb                  ; Python
             realgud:rdebug realgud:trepan             ; Ruby
             )
  :init (add-to-list 'display-buffer-alist '("^\\*gdb .* shell\\*" . (display-buffer-below-selected))))

(use-package realgud-lldb               ; Realgud front-end to LLDB.
  :ensure t
  :commands (realgud--lldb)
  :init (defalias 'realgud:lldb 'realgud--lldb))

(use-package realgud-jdb                ; Realgud front-end to Java's jdb debugger"
  :ensure t
  :commands (realgud--jdb)
  :init (defalias 'realgud:jdb 'realgud--jdb))

(use-package realgud-node-debug         ; Realgud front-end to older "node debug"
  :ensure t
  :commands (realgud:node-debug))

(use-package realgud-node-inspect       ; Realgud front-end to newer "node inspect"
  :ensure t
  :commands (realgud:node-inspect))

(use-package realgud-pry                ; Realgud front-end to the Ruby pry debugger.
  :ensure t
  :commands (realgud:pry))

;;; [ moonshot ] -- Run executable file, debug and build commands on project +projectile, compilation-mode, ivy, realgud.

(use-package moonshot
  :ensure t
  :defer t
  :bind (("C-c x x" . moonshot-run-executable)
         ("C-c x d" . moonshot-run-debugger)
         ("C-c x c" . moonshot-run-runner)))

;;; [ dape ] -- Debug Adapter Protocol for Emacs. Dape is an debug adapter client for Emacs.

;; Use `dape-configs' to set up your debug adapter configurations.
;; To initiate debugging sessions, use the command `dape'.

(use-package dape
  :ensure t
  :commands (dape))


(provide 'init-prog-debugger)

;;; init-prog-debugger.el ends here
