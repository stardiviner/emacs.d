;;; init-prog-folding.el --- init for Programming Folding
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

(unless (boundp 'prog-fold-prefix)
  (define-prefix-command 'prog-fold-prefix))
(global-set-key (kbd "C-c SPC") 'prog-fold-prefix)

;;; [ origami ] -- A folding minor mode for Emacs.

(use-package origami
  :ensure t
  :defer t
  :custom ((origami-show-fold-header t)
           (origami-fold-replacement "..."))
  :bind (:map prog-fold-prefix
              ("SPC" .  origami-toggle-node)
              ("TAB". origami-toggle-all-nodes)
              ("n" . origami-next-fold)
              ("p" . origami-previous-fold)
              ("c" . origami-close-node)
              ("C" . origami-close-all-nodes)
              ("o" . origami-open-node)
              ("O" . origami-open-all-nodes)
              ("T" . origami-recursively-toggle-node)
              (">" . origami-open-node-recursively)
              ("<" . origami-close-node-recursively)
              ("O" . origami-show-only-node)
              ("u" . origami-undo)
              ("r" . origami-redo)
              ("!" . origami-reset))
  :hook (prog-mode . origami-mode)
  :config
  (require 'color) ; for `color-darken-name', `color-lighten-name'.
  (set-face-attribute 'origami-fold-header-face nil
                      :box nil
                      :overline (cl-case (frame-parameter nil 'background-mode)
                                  (light (color-darken-name (face-background 'default) 20))
                                  (dark (color-lighten-name (face-background 'default) 10)))
                      :background (cl-case (frame-parameter nil 'background-mode)
                                    (light (color-darken-name (face-background 'default) 10))
                                    (dark (color-lighten-name (face-background 'default) 5)))))


(provide 'init-prog-folding)

;;; init-prog-folding.el ends here
