;;; init-prog-programming.el --- init for common Programming
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ prog-mode ]

(defun prog-mode-hook-ensure ()
  (unless (derived-mode-p 'prog-mode)
    (run-hooks 'prog-mode-hook)))

(dolist (hook '(c-mode-hook
                c++-mode-hook
                python-mode-hook
                ruby-mode-hook
                html-mode-hook
                css-mode-hook))
  (add-hook hook #'prog-mode-hook-ensure))


(provide 'init-prog-programming)

;;; init-prog-programming.el ends here
