;;; init-prog-compile.el --- Summary
;;
;;; Commentary:

;;; Code:

;;; [ compile ]

(use-package compile
  :defer t
  :commands (compile)
  :bind ("<f5>" . compile)
  :custom ((compilation-ask-about-save t) ; save without asking.
           (compilation-skip-threshold 2) ; don't stop on info or warnings.
           (compilation-window-height 7)
           (compilation-scroll-output 'first-error) ; 'first-error ; stop on first error.
           (compilation-auto-jump-to-first-error nil) ; jump to error file position.
           (compilation-auto-jump-to-next nil))
  :init
  (add-to-list 'display-buffer-alist '("^\\*compilation\\*" . (display-buffer-below-selected)))
  (add-to-list 'display-buffer-alist '("^\\*Compile-Log\\*" . (display-buffer-below-selected)))
  ;; make `compile-goto-error' open result target in current window.
  (add-to-list 'display-buffer-alist
               '((lambda (&rest _) (eq this-command 'compile-goto-error)) .
                 ((display-buffer-same-window) . ((inhibit-same-window . nil)))))
  ;; smart `compile' command detection.
  (add-hook 'c-mode-common-hook #'compile-command-smart-detect)
  (defun compile-command-smart-detect ()
    "Smartly detect current file and project Makefile to decide `compile-command'."
    (if (or (file-exists-p "makefile")
		    (file-exists-p "Makefile"))
        (setq-local compile-command
		            (concat "make -k "
			                (if buffer-file-name
			                    (shell-quote-argument
			                     (file-name-sans-extension buffer-file-name)))))
      (cl-case major-mode
        (c-mode
         (setq-local compile-command
                     (concat "gcc -g "
                             buffer-file-name
                             " -o "
                             (if buffer-file-name
			                     (shell-quote-argument
			                      (file-name-sans-extension buffer-file-name))))))
        (c++-mode
         (setq-local compile-command
                     (concat "g++ -g "
                             buffer-file-name
                             " -o "
                             (if buffer-file-name
			                     (shell-quote-argument
			                      (file-name-sans-extension buffer-file-name))))))))))

;;; [ fancy-compilation ] -- Emacs compilation-mode enhancements.

;; (use-package fancy-compilation
;;   :ensure t
;;   :init (with-eval-after-load 'compile (fancy-compilation-mode)))

;;; [ compile-multi ] -- A multi target interface to compile.

(use-package compile-multi
  :ensure t
  :commands (compile-multi)
  :bind ("<f5>" . compile-multi)
  :config
  ;; Emacs Lisp
  (defun byte-compile-this-file+ ()
    (byte-compile-file (buffer-file-name)))
  (push `(emacs-lisp-mode
          ("emacs:bytecompile" . ,#'byte-compile-this-file+))
        compile-multi-config)
  ;; GNU Make
  (push '((file-exists-p "Makefile")
          ("make:build" . "make build")
          ("make:test" . "make test")
          ("make:all" . "make all"))
        compile-multi-config)
  ;; Python
  (push `(python-mode
          ("python:pylint" "python" "-m" "pylint" (buffer-file-name)))
        compile-multi-config)
  (push `(python-mode
          ("python3:pylint" "python3" "-m" "pylint" (buffer-file-name)))
        compile-multi-config)
  ;; HTML
  (push `((member major-mode '(html-mode web-mode))
          ("html:tidy" "tidy" "-output" "output.html" (buffer-file-name)))
        compile-multi-config)
  (push `((member major-mode '(html-mode web-mode))
          ("Web Browser:Google Chrome"
           ,(cl-case system-type
              (darwin "/Applications/Google\\ Chrome.app/Contents/MacOS/Google\\ Chrome")
              (gnu/linux "google-chrome-stable"))
           (buffer-file-name)))
        compile-multi-config)
  (push `((member major-mode '(html-mode web-mode))
          ("Web Browser:Firefox"
           ,(cl-case system-type
              (darwin "/Applications/Firefox.app/Contents/MacOS/firefox")
              (gnu/linux "firefox"))
           (buffer-file-name)))
        compile-multi-config)
  ;; Clojure
  ;; babashka
  :init
  (use-package consult-compile-multi ; Consulting read support for `compile-multi'.
    :ensure t
    :after compile-multi
    :config (consult-compile-multi-mode 1))
  (use-package compile-multi-all-the-icons ; Affixate `compile-multi' with icons.
    :if (featurep 'all-the-icons)
    :ensure t
    :after all-the-icons-completion
    :after compile-multi
    :config (all-the-icons-completion-mode 1))
  (use-package compile-multi-embark ; Integration for `compile-multi' and `embark'.
    :ensure t
    :after embark
    :after compile-multi
    :config (compile-multi-embark-mode 1)))


(provide 'init-prog-compile)

;;; init-prog-compile.el ends here
