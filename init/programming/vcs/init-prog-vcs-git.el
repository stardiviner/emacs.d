;;; init-prog-vcs-git.el --- init Git for Emacs
;; -*- lexical-binding: t -*-
;;
;;; Commentary:

;;; Code:


(unless (boundp 'prog-vcs-git-prefix)
  (define-prefix-command 'prog-vcs-git-prefix))
(define-key 'prog-vcs-prefix (kbd "g") 'prog-vcs-git-prefix)

;;; [ vc-git ]

(use-package vc-git
  :defer t)

;;; [ pcmpl-git ] -- Complete both git commands and their options and arguments.

(use-package pcmpl-git
  :ensure t)

;;; [ git-modes ] -- Major modes for editing Git configuration files.

(use-package git-modes
  :ensure t)

;;; [ gitignore-snippets ] -- The `gitignore-mode' templates from Gitignore.io with Yasnippet.

(use-package gitignore-snippets
  :ensure t
  :after yasnippet
  ;; Make sure to call `gitignore-snippets-init' after yasnippet loaded.
  :init (gitignore-snippets-init))

;;; [ git-commit ] -- This package assists the user in writing good Git commit messages.

(use-package git-commit
  :ensure t
  :defer t
  :config
  ;; Enable `company-dabbrev' in git commit buffer.
  (defun my/company-dabbrev-ignore-except-magit-diff (buffer)
    (let ((name (buffer-name buffer)))
      (and (string-match-p (or (default-value 'company-dabbrev-ignore-buffers) "\\`[ *]") name)
           (not (string-match-p "\\*magit-diff:" name)))))

  (defun my/git-commit-setup ()
    (setq-local fill-column 72)
    (auto-fill-mode t)
    (display-fill-column-indicator-mode 1)
    (electric-pair-local-mode t)
    (cond
     ((featurep 'jit-spell) (jit-spell-mode 1))
     ((featurep 'flyspell) (flyspell-mode 1)))
    (cond
     ((featurep 'corfu)
      (corfu-mode 1)
      (setq-local completion-at-point-functions
                  (list #'cape-file
                        (if (featurep 'yasnippet-capf)
                            #'yasnippet-capf
                          (cape-company-to-capf 'company-yasnippet))
                        #'cape-sgml
                        #'cape-tex
                        #'cape-emoji
                        #'cape-dabbrev
                        #'cape-dict)))
     ((featurep 'company)
      (company-mode-on)
      (setq-local company-dabbrev-code-modes '(text-mode magit-diff-mode))
      (setq-local company-dabbrev-ignore-buffers #'my/company-dabbrev-ignore-except-magit-diff)
      (setq-local company-dabbrev-code-other-buffers 'all)
      (setq-local company-backends '((company-dabbrev-code :with company-abbrev :separate company-spell))))))
  
  (add-hook 'git-commit-setup-hook #'my/git-commit-setup))

;;; [ Magit ] -- A Git porcelain inside Emacs.

(use-package magit
  :ensure t
  :defer t
  :preface (defalias 'magit-log-region 'magit-log-buffer-file)
  :custom ((magit-git-executable (or (executable-find "/usr/bin/git") (executable-find "git")))
           (magit-clone-default-directory (expand-file-name "~/Code"))
           (magit-repository-directories `((,user-emacs-directory . 0)
                                           ("~/Code/" . 3)
                                           ("~/Org/Website" . 1)))
           ;; let magit status buffer display in current window.
           (magit-display-buffer-function 'display-buffer)
           ;; use toggled arguments. For example, "-c" (--color) for log.
           (magit-prefix-use-buffer-arguments 'always)
           (magit-diff-refine-hunk t)   ; `magit-diff-toggle-refine-hunk'
           ;; improve magit performance
           (auto-revert-buffer-list-filter 'magit-auto-revert-repository-buffer-p)
           (magit-section-initial-visibility-alist '((stashes . show)
                                                     (untracked . show)
                                                     (unpulled . show)
                                                     ;; (unstaged . show)
                                                     ;; (unpushed . show)
                                                     )))
  :commands (magit-status magit-dispatch magit-file-dispatch) ; [C-x g], [C-x M-g], [C-c M-g]
  :bind ("C-x g" . magit-status)
  :init
  ;; manage popup buffers.
  (add-to-list 'display-buffer-alist '("\\`magit:.*\\'" . (display-buffer-same-window)))
  (add-to-list 'display-buffer-alist '("^magit-diff.*" . (display-buffer-below-selected)))
  (add-to-list 'display-buffer-alist '("^magit-revision.*" . (display-buffer-below-selected)))
  (add-to-list 'display-buffer-alist '("^magit-log.*" . (display-buffer-same-window)))
  (add-to-list 'display-buffer-alist '("^magit-process.*" . (display-buffer-same-window)))
  :config
  ;; NOTE: Magit runs git, no shell is involved, so shell quoting like '' should not be used.
  (transient-append-suffix 'magit-log "-S"
    '("-P" "Toggle Perl regexp matching" "--perl-regexp")))

;; (use-package magit
;;   :ensure t
;;   :delight magit-wip-mode
;;   :custom ((magit-wip-merge-branch t))
;;   :hook (magit-status-mode . magit-wip-mode)) ; NOTE: This will slow-down performance of Magit.

;;; [ magit-lfs ] -- Magit support for GLFS: Git Large File System. keybinding [:]

(use-package magit-lfs
  :ensure t
  :commands (magit-lfs))

;;; [ magit-gitflow ] -- Git Flow plugin for Magit. keybinding [C-f]

(use-package magit-gitflow
  :ensure t
  :defer t
  :after magit
  :hook (magit-status-mode . turn-on-magit-gitflow))

;;; [ magit-tbdiff ] -- Magit extension for range diffs.

;; (use-package magit-tbdiff
;;   :ensure t)

;;; [ magit-topgit ] -- TopGit extension for Magit.

;; (use-package magit-topgit
;;   :ensure t
;;   :defer t
;;   :init (add-hook 'magit-mode-hook 'magit-topgit-mode))

;;; [ magit-todos ] -- Show source file TODOs in Magit.

;; (use-package magit-todos
;;   :ensure t
;;   :defer t
;;   :custom ((magit-todos-update nil)
;;            (magit-todos-exclude-globs '(".git/"
;;                                         "elpa/" "quelpa/" "themes/" "site-lisp/" "extensions/" ; Emacs packages
;;                                         "eaf/" "resources/" "pyim/" "irony/" "elfeed/" "snippets/" "ptemplate/" "emojis/" ; Emacs external files
;;                                         "devdocs/" "documentations/" "persist/" "org-mode/" "org-page/" ; Emacs external files
;;                                         "eclipse.jdt.ls/" ; Emacs external tools
;;                                         "desktop-save/" "auto-save-list/" ".auto-save-list/" "eshell/" "var/" "url/" "request/" ; Emacs session files
;;                                         ".temp/" ".cache/" ".marks/" ".extensions/" "transient/" "org-persist/" ; Emacs temp directories
;;                                         )))
;;   :hook (magit-status-mode . magit-todos-mode))

;;; [ magit-org-todos ] -- Display file "todo.org" (in project root path) to your Magit status section.

(use-package magit-org-todos
  :ensure t
  :ensure projectile
  :defer t
  :hook (magit-status-mode . magit-org-todos-autoinsert)
  :commands (magit-org-todos--magit-visit-org-todo)
  :config
  (with-eval-after-load 'projectile
    (define-key projectile-command-map (kbd "C-o")  'magit-org-todos--magit-visit-org-todo)))

;;; [ forge ] -- Work with Git forges, such as Github and Gitlab, from the comfort of Magit and the rest of Emacs.

(use-package forge
  :ensure t
  :defer t
  :after magit
  :init (add-to-list 'display-buffer-alist '("\\*forge: .*\\*" . (display-buffer-below-selected))))

;;==============================================================================

(unless (boundp 'git-quick-prefix)
  (define-prefix-command 'git-quick-prefix))
(global-set-key (kbd "M-g g") 'git-quick-prefix)

;;; [ diff-hl ] -- highlighting uncommitted changes with continuous fringe vertical block.

(use-package diff-hl ; [C-x v] prefix
  :ensure t
  :defer t
  :custom ((vc-git-diff-switches '("--histogram"))
           (diff-hl-show-staged-changes nil))
  :hook ((prog-mode . diff-hl-mode)
         (dired-mode . diff-hl-dired-mode)
         (magit-post-refresh . diff-hl-magit-post-refresh))
  :commands (diff-hl-next-hunk diff-hl-previous-hunk diff-hl-diff-goto-hunk)
  :bind (:map git-quick-prefix
              ("n"   . diff-hl-next-hunk)
              ("p"   . diff-hl-previous-hunk)
              ("d"   . diff-hl-show-hunk)
              ("M-n" . diff-hl-show-hunk-next)
              ("M-p" . diff-hl-show-hunk-previous)
              ("="   . diff-hl-diff-goto-hunk)
              ("s"   . diff-hl-stage-current-hunk)
              ("r"   . diff-hl-revert-hunk)
              ("c"   . magit-commit-create)))

;;; [ vc-msg ] -- Show commit message of current line in Emacs.

(use-package  vc-msg
  :ensure t
  :defer t
  :custom (vc-msg-git-show-commit-function 'magit-show-commit)
  :commands (vc-msg-show)
  :bind (:map git-quick-prefix ("m" . vc-msg-show)))

;;; [ git-cliff ] -- Genarate and update git repository CHANGELOG.md with git-cliff.

(use-package git-cliff
  :ensure t
  :commands (git-cliff-menu)
  :init
  ;; Integrate to `magit-tag'
  (with-eval-after-load 'magit-tag
    (transient-append-suffix 'magit-tag ; [t c]
      '(1 0 -1)
      '("c" "changelog" git-cliff-menu))))



(provide 'init-prog-vcs-git)

;;; init-prog-vcs-git.el ends here
