;;; init-prog-lsp.el --- init for Language Server Protocol

;;; Commentary:



;;; Code:

;;; [ eglot ] -- Client for Language Server Protocol (LSP) servers.

;; (use-package eglot
;;   :commands (eglot eglot-list-connections)
;;   :custom ((eglot-extend-to-xref t))
;;   :hook (prog-mode . eglot-ensure)
;;   :init
;;   (use-package consult-eglot
;;     :ensure t
;;     :ensure consult-eglot-embark
;;     :commands (consult-eglot-symbols)
;;     :init (consult-eglot-embark-mode)))

;;; [ lsp-mode ] -- clients for servers using Language Server Protocol.

(use-package lsp-mode
  :ensure t
  :defer t
  :commands (lsp lsp-deferred lsp-describe-session lsp-org lsp-ui-imenu)
  ;; :hook (prog-mode . lsp-deferred)
  :bind (:map lsp-mode-map
              ("C-c C-d" . lsp-describe-thing-at-point)
              ("M-RET"   . lsp-execute-code-action))
  :custom (;; (lsp-auto-configure nil) ; disable auto prepend `company-capf' backend.
           (lsp-warn-no-matched-clients nil) ; don't warning no matched clients.
           ;; (lsp-server-install-dir (concat user-emacs-directory "lsp-servers"))
           ;; speed-up lsp-mode performance
           (lsp-use-plists t)
           (lsp-log-io nil)             ; for Debug
           (lsp-keep-workspace-alive nil) ; auto close workspace lsp daemon after closed all workspace files.
           ;; (lsp-auto-guess-root t)
           ;; (lsp-idle-delay 0.5)
           (lsp-enable-folding nil)
           (lsp-diagnostics-provider :none) ; no real-time syntax check
           ;; (lsp-enable-snippet nil) ; handle yasnippet by myself
           (lsp-enable-symbol-highlighting nil)
           (lsp-signature-function 'lsp-signature-posframe) ; use `posframe' to display function parameters docs.
           ;; (lsp-signature-doc-lines 5) ; limit signature doc lines.
           ;; (lsp-enable-links nil)
           (lsp-headerline-breadcrumb-enable-diagnostics nil))
  :init (add-to-list 'display-buffer-alist '("^\\*lsp.*\\*" . (display-buffer-below-selected)))
  ;; Emacs don't load some lsp-mode modules.
  (setq lsp-client-packages ; [M-x load-library RET lsp-* RET] to load LSP clients.
        '(ccls lsp-clangd lsp-cmake
               lsp-clojure ; lsp-lisp
               lsp-go lsp-java lsp-rust lsp-sourcekit lsp-csharp lsp-fsharp lsp-lua
               lsp-pyright ; lsp-pyls lsp-pylsp lsp-pyright lsp-python-ms
               ;; lsp-solargraph
               lsp-javascript lsp-eslint lsp-vetur lsp-svelte
               lsp-html lsp-css lsp-tailwindcss lsp-emmet
               lsp-bash
               ;; lsp-r
               lsp-sqls
               ;; lsp-qml
               lsp-docker lsp-dockerfile lsp-nginx lsp-vimscript
               lsp-xml lsp-yaml lsp-toml lsp-json lsp-graphql
               lsp-latex lsp-tex
               lsp-markdown))
  :hook (lsp-mode . lsp-enable-which-key-integration)
  :config
  ;; disable some lsp clients
  ;; (add-to-list 'lsp-disabled-clients 'ccls)
  ;; (add-to-list 'lsp-disabled-clients '(emacs-lisp-mode . nil))
  ;; (add-to-list 'lsp-disabled-clients '(web-mode . angular-ls))

  ;; [ LSP completion ]
  (require 'lsp-completion)
  ;; Fix `lsp-completion-mode' enabled `company-mode', conflict with my global `corfu-mode' in buffer.
  (defun my/lsp-completion-disable-company-mode ()
    "Turn off `company-mode' from `lsp-completion-mode' when conflict with `corfu-mode'."
    (when corfu-mode
      (company-mode -1)))
  (add-hook 'lsp-completion-mode-hook #'my/lsp-completion-disable-company-mode 100)

  ;; don't scan 3rd party javascript libraries
  (push "[/\\\\][^/\\\\]*\\.\\(json\\|html\\|jade\\)$" lsp-file-watch-ignored)

  ;; Fix ‘lsp-org’ error: flycheck-add-mode: lsp is not a valid syntax checker.
  (with-eval-after-load 'lsp-diagnostics
    (flycheck-define-generic-checker 'lsp
      "A syntax checker using the Language Server Protocol (LSP)
provided by lsp-mode.
See https://github.com/emacs-lsp/lsp-mode."
      :start #'lsp-diagnostics--flycheck-start
      :modes '(lsp-placeholder-mode) ;; placeholder
      :predicate (lambda () lsp-mode)
      :error-explainer (lambda (e)
                         (cond ((string-prefix-p "clang-tidy" (flycheck-error-message e))
                                (lsp-cpp-flycheck-clang-tidy-error-explainer e))
                               (t (flycheck-error-message e))))))

  ;; lsp-signature
  (setq lsp-signature-function 'lsp-signature-posframe)

  ;; Org mode Babel source block advice on [C-c '] (`org-edit-src-code') to inject lsp-mode workspace session info.
  (advice-add 'org-edit-src-code :around #'org-edit-src-code--lsp-workspace-inject)

  (defvar org-edit-src-code--lsp-workspace-enable-modes-list
    '("python" "ruby"
      "C" "C++" "cmake"
      "shell" "bash" "zsh"
      "javascript" "html" "css"
      "java" "rust" "go" "swift" "kotlin"
      "lua" "php" "haskell" "ocaml" "prolog"
      "R" "julia"
      "sql" "sqlite"
      "graphql" "xml" "json" "yaml" "toml"
      "markdown" "latex")
    "A list of Org Babel programming language to be enabled lsp-mode in opened source block buffer.")

  (defvar org-edit-src-code--lsp-workspace-disable-modes-list
    '("emacs-lisp" "lisp" "clojure" "clojurescript" "scheme"
      "org" "conf" "singularity")
    "A list of Org Babel programming language to be disabled lsp-mode in opened source block buffer.")

  (defun org-edit-src-code--lsp-workspace-inject (origin-func &rest args)
    "Auto set the temporary Org opened source block buffer to inject filename and lsp-mode workspace session.
This will enable lsp-mode features including: code completion."
    ;; detect exclude `default-directory' by identity file ".lsp-mode-disable".
    (if (file-exists-p ".lsp-mode-disable")
        (progn
          (apply origin-func args)
          (user-error "The lsp-mode is disabled in %s contains .lsp-mode-disable" default-directory))
      (let* ((info (org-babel-get-src-block-info))
             (params (nth 2 info))
             (lang (car info))
             (param-file (cdr (assq :file params))) ; :file header argument
             (param-dir (cdr (assq :dir params))) ; :dir header argument, use it as `(lsp-workspace-root)'
             (origin-org-buffer (current-buffer))
             (origin-org-filename (buffer-file-name origin-org-buffer))
             (enable-lsp-for-org-p))
        (cond
         ((member lang org-edit-src-code--lsp-workspace-enable-modes-list)
          (setq enable-lsp-for-org-p t))
         ((member lang org-edit-src-code--lsp-workspace-disable-modes-list)
          (setq enable-lsp-for-org-p nil))
         (t
          (setq enable-lsp-for-org-p (yes-or-no-p "Inject temp lsp-mode workspace session into the opened source block buffer? "))))
        (if enable-lsp-for-org-p
            (progn
              (apply origin-func args)
              (unless param-file
                ;; Org source block missing `:file' parameter, use original Org buffer filename.
                (require (intern (format "ob-%s" lang)) nil t)
                (cond
                 ((string-equal lang "cmake") (setq param-file "CMakeLists.txt"))
                 (t
                  (if-let ((ext (cdr (assoc lang org-babel-tangle-lang-exts))))
                      (setq param-file (format "%s.%s" (file-name-base origin-org-filename) ext))
                    (user-error "[Org mode Babel + lsp-mode] Have not found header argument `:file' for the source block.
Because can't find file extension for current source block in `org-babel-tangle-lang-exts'.")))))
              (when param-file
                ;; fake Org mode opened source block virtual buffer as real file buffer.
                (setq-local buffer-file-name (expand-file-name param-file))
                (setq-local buffer-file-truename (expand-file-name param-file))
                (rename-buffer param-file)
                ;; auto attach a temporary created lsp-mode workspace session.
                (let ((lsp-headerline-breadcrumb-enable nil)
                      (lsp-auto-guess-root nil)
                      (lsp-enable-file-watchers nil)
                      (lsp-file-watch-threshold 500))
                  (make-local-variable 'lsp-configure-hook)
                  (remove-hook 'lsp-configure-hook 'lsp-headerline-breadcrumb-mode)
                  (flyspell-mode-off)
                  (flycheck-mode -1)
                  (lsp-workspace-folders-add default-directory)
                  (when-let ((workspace (car lsp--buffer-workspaces))
                             (root-dir (or param-dir (lsp-workspace-root))))
                    (with-lsp-workspace workspace
                      (setq-local default-directory root-dir)
                      ;; setting the `lsp--workspace' structure.
                      (setf (lsp--workspace-root workspace) root-dir)))
                  (lsp)
                  (lsp-ui-mode -1)
                  (add-hook 'completion-at-point-functions #'lsp-completion-at-point nil t))
                (message (format "[Org mode Babel + lsp-mode] The source block buffer injected lsp-mode workspace %s."
                                 (file-name-directory buffer-file-name)))))
          (apply origin-func args)))))
  )

;; [ lsp-ui ] -- UI modules for lsp-mode.

(use-package lsp-ui
  :ensure t
  :after lsp
  :commands (lsp-ui-mode)
  ;; :hook (lsp-mode . lsp-ui-mode)
  :custom ((lsp-ui-doc-enable t)
           (lsp-ui-doc-show-with-cursor t)
           (lsp-ui-doc-header t)
           (lsp-ui-doc-include-signature t)
           ;; (lsp-ui-doc-position 'at-point) ; 'top, 'at-point, 'bottom
           (lsp-ui-doc-use-childframe t)
           (lsp-ui-doc-use-webkit nil)
           (lsp-ui-sideline-show-hover nil)
           (lsp-ui-sideline-enable nil)
           ;; set lsp-ui-doc border
           (lsp-ui-doc-border "dark slate blue"))
  :bind (:map lsp-ui-mode-map
              ([remap xref-find-definitions] . lsp-ui-peek-find-definitions) ; [M-.]
              ([remap xref-find-references] . lsp-ui-peek-find-references)   ; [M-?]
              ("C-c C-j" . lsp-ui-imenu))
  :preface (if (featurep 'xwidget-internal) (setq lsp-ui-doc-use-webkit t)))

;;; [ lsp-ivy ] -- LSP Ivy integration.

;; (use-package lsp-ivy
;;   :if (featurep 'ivy)
;;   :ensure t
;;   :commands (lsp-ivy-workspace-symbol lsp-ivy-global-workspace-symbol)
;;   :bind (:map lsp-command-map ("g G" . lsp-ivy-workspace-symbol)))

(use-package consult-lsp
  :if (featurep 'vertico)
  :ensure t
  :after lsp
  :commands (consult-lsp-diagnostics consult-lsp-symbols consult-lsp-file-symbols)
  :bind ((:map lsp-mode-map ([remap xref-find-apropos] . consult-lsp-symbols)))
  :config (setq vertico-multiform-commands (append vertico-multiform-commands '(consult-lsp-diagnostics posframe))))

;;; [ lsp-treemacs ] -- LSP treemacs

(use-package lsp-treemacs
  :ensure t
  :after lsp
  :custom ((treemacs-read-string-input 'from-minibuffer))
  ;; :hook (lsp-mode . lsp-treemacs-sync-mode)
  :commands (lsp-treemacs-errors-list
             lsp-treemacs-quick-fix
             lsp-treemacs-symbols-list
             lsp-treemacs-references
             lsp-treemacs-implementations
             lsp-treemacs-call-hierarchy
             lsp-treemacs-type-hierarchy
             lsp-treemacs-deps-list))

;;; [ dap-mode ] -- Debug Adapter Protocol mode for lsp-mode.

(use-package dap-mode
  :ensure t
  :after lsp
  :commands (dap-debug dap-hydra)
  :bind (:map dap-mode-map
              ("<f5>" . dap-debug)
              ("<f7>" . dap-step-in)
              ("<M-f7>" . dap-step-out)
              ("<f8>" . dap-next)
              ("<f9>" . dap-continue))
  ;; enable only some features
  ;; :custom (dap-auto-configure-features '(sessions locals controls tooltip))
  ;; :init
  ;; (dap-mode 1)                         ; "Global minor mode for DAP mode."
  ;; (dap-ui-mode 1)                      ; "Global minor mode for Displaying DAP visuals."
  ;; (dap-tooltip-mode 1)                 ; "Global minor mode for Toggle the display of GUD tooltips."
  ;; (dap-ui-controls-mode 1)             ; "Global minor mode for Displaying DAP visuals."
  :config
  (defun dap-mode-call-dap-hydra ()
    (call-interactively #'dap-hydra))
  (add-hook 'dap-stopped-hook #'dap-mode-call-dap-hydra))

;; (use-package dap-LANGUAGE) to load the dap adapter for your language

;;; [ lsp-docker ] -- lsp-mode uses lsp-docker to run language servers using in Docker containers.

(use-package lsp-docker
  :ensure t
  :defer t
  :commands (lsp-docker-start lsp-docker-register))


(provide 'init-prog-lsp)

;;; init-prog-lsp.el ends here
