;;; init-arduino.el --- init for Arduino Development -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Christopher M. Miles, all rights reserved.

;;; Commentary:



;;; Code:

;;; [ arduino-mode ] -- major mode for Arduino.

(use-package arduino-mode
  :ensure t
  :defer t
  ;; :requires (flycheck-arduino)
  :commands (flycheck-arduino-setup)
  :hook (arduino-mode . flycheck-arduino-setup)
  :config
  ;; ob-arduino: Org Mode Babel support for Arduino.
  (require 'ob-arduino)
  (add-to-list 'org-babel-load-languages '(arduino . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages))

;;; [ company-arduino ] -- code completion support for arduino-mode.

(use-package company-arduino
  :vc t :load-path "~/Code/Emacs/company-arduino"
  :ensure company-c-headers
  :defer t
  :after arduino-mode
  :preface
  (unless (getenv "ARDUINO_HOME")
    (setenv "ARDUINO_HOME" (expand-file-name "~/Arduino"))
    (setenv "ARDUINO_LIBS" (expand-file-name "~/Arduino/libraries"))
    (setq company-arduino-home (setenv "ARDUINO_HOME" (expand-file-name "~/Arduino"))))
  ;; Turn-on irony-mode on arduino-mode (on .ino file).
  (with-eval-after-load 'irony
    (add-to-list 'irony-supported-major-modes 'arduino-mode))
  :commands (company-arduino-turn-on)
  :hook ((arduino-mode . irony-mode)
         (arduino-mode . company-arduino-turn-on))
  :config
  ;; workaround for Arduino v17 new libraries location.
  (add-to-list 'irony-arduino-includes-options (format "-I%s" (expand-file-name "~/Arduino/libraries")))

  (defun my/company-arduino-setup ()
    (my-company-add-backend-locally 'company-irony))
  (add-hook 'arduino-mode-hook #'my/company-arduino-setup))

;;; [ arduino-cli-mode ] -- Emacs support for the arduino-cli.

(use-package arduino-cli-mode ; [C-c C-a]
  :ensure t
  :defer t
  :commands (arduino-cli-new-sketch arduino-cli-board-list)
  :hook (arduino-mode . arduino-cli-mode))



(provide 'init-arduino)

;;; init-arduino.el ends here
