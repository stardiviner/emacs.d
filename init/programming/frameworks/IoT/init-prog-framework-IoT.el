;;; init-prog-framework-IoT.el --- init Emacs for the Arduino language
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ platformio-mode ] -- PlatformIO integration for Emacs

(use-package platformio-mode
  :ensure t
  :defer t
  :hook (c++-mode . platformio-conditionally-enable))

;;; [ Raspberry Pi ]

;;; [ Arduino ]

(load "init-arduino")


(provide 'init-prog-framework-IoT)

;;; init-prog-framework-IoT.el ends here
