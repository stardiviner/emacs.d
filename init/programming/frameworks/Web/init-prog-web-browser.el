;;; init-prog-web-browser.el --- init for Web Browser
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ dom ] -- XML/HTML (etc.) DOM manipulation and searching functions.

;; (use-package dom
;;   :defer t)

;;; [ kite ] -- Emacs front end for the WebKit Inspector.

;; (use-package kite
;;   :ensure t)

;;; [ kite-mini ] -- Yet another Emacs package to interact with WebKit remote debugging API.

;; (use-package kite-mini
;;   :ensure t
;;   :defer t
;;   :config
;;   ;; Automatically Turn on the mode for your buffer of choice.
;;   (add-hook 'js-mode-hook (lambda () (kite-mini-mode t)))
;;   (add-hook 'css-mode-hook (lambda () (kite-mini-mode t)))
;;   )

;;; [ firefox-javascript-repl ] -- Jack into Firefox

(use-package firefox-javascript-repl
  :ensure t
  :preface (when (eq system-type 'darwin) (add-to-list 'exec-path "/Applications/Firefox.app/Contents/MacOS/"))
  :if (executable-find "firefox")
  :commands (firefox-javascript-repl)
  :init (add-to-list 'display-buffer-alist '("^\\*firefox-javascript-repl\\*" . (display-buffer-below-selected))))


(provide 'init-prog-web-browser)

;;; init-prog-web-browser.el ends here
