;;; init-data-query.el --- Data Query supporting. -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Christopher M. Miles, all rights reserved.

;;; Commentary:



;;; Code:

;;; [ ob-dsq ] -- dsq is a command-line tool for running SQL queries against JSON, CSV, Excel, Parquet, and more.

(use-package ob-dsq
  :ensure t)



(provide 'init-data-query)

;;; init-data-query.el ends here
