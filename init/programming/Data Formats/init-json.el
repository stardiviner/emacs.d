;;; init-json.el --- init for JSON
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ js-json-mode ] -- Major mode derived from `js-mode' by `define-derived-mode'.

;; (use-package js
;;   :commands (js-json-mode)
;;   :mode ("\\.json\\'" . js-json-mode))

;;; [ json-mode ] -- Extends the builtin js-mode to add better syntax highlighting for JSON.

(use-package json-mode
  :ensure t
  :defer t
  :mode ("\\.json\\'" . json-mode)
  :commands (json-mode-show-path json-mode-beautify))

;;; [ jsonian ] -- jsonian provides a fully featured major-mode to *view*, *navigate* and *edit* JSON files.

(use-package jsonian
  :ensure t)

;;; [ json-reformat ] -- Reformatting tool for JSON.

(use-package json-reformat
  :ensure t
  :commands (json-reformat-region))

;;; [ jq-mode ] -- An emacs major mode for editing jq-scripts.

(use-package jq-mode
  :ensure t
  :mode "\\.jq$"
  :commands (jq-mode jq-interactively)
  :bind (:map json-mode-map ("C-c C-j" . jq-interactively))
  :config
  (with-eval-after-load 'org
    (org-babel-do-load-languages 'org-babel-load-languages '((jq . t))))
  ;; (add-to-list 'org-babel-default-header-args:jq '(:compact . "yes"))
  )

;;; [ counsel-jq ] -- Live queries against JSON and YAML data.

(use-package counsel-jq
  :ensure t
  :defer t
  :commands (counsel-jq))

;;; [ jq-format ] -- Reformat JSON and JSONLines using "jq".

(use-package jq-format
  :ensure t
  :defer t
  :after json-mode
  :custom (jq-format-sort-keys nil)
  :commands (jq-format-json-on-save-mode
             jq-format-json-buffer jq-format-json-region
             jq-format-jsonlines-buffer jq-format-jsonlines-region)
  :hook (json-mode . jq-format-json-on-save-mode))

;;; [ json-snatcher ] -- Grabs the path to JSON values in a JSON file.

(use-package json-snatcher
  :ensure t
  :defer t
  :commands (jsons-print-path))

;;; [ json-navigator ] -- view and navigate JSON structures.

(use-package json-navigator
  :ensure t
  :defer t
  :commands (json-navigator-navigate-after-point json-navigator-navigate-region))



(provide 'init-json)

;;; init-json.el ends here
