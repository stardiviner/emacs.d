;;; init-rdf.el --- init for RDF


;;; Commentary:


;;; Code:

;;; [ rdf-prefix ] -- Prefix lookup for RDF.

(use-package rdf-prefix
  :ensure t)



(provide 'init-rdf)

;;; init-rdf.el ends here
