;;; init-toml.el --- init file for TOML

;;; Commentary:



;;; Code:

;;; [ toml-mode ] -- Major mode for editing TOML files.

(use-package toml-mode
  :ensure t)

;;; [ toml ] -- TOML (Tom's Obvious, Minimal Language) parser



(provide 'init-toml)

;;; init-toml.el ends here
