;;; init-csv.el --- init file for CSV -*- lexical-binding: t; -*-

;;; Commentary:



;;; Code:

;;; [ csv-mode ] -- major mode for editing comma/char separated values.

(use-package csv-mode
  :ensure t
  :defer t
  :hook ((csv-mode . csv-align-mode)
         (csv-mode . csv-header-line)))




(provide 'init-csv)

;;; init-csv.el ends here
