;;; init-yaml.el --- init for YAML
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ yaml-mode ]

(use-package yaml-mode
  :ensure t
  :defer t
  :mode (("\\.yaml$" . yaml-mode)
         ("\\.yml$" . yaml-mode))
  :hook (yaml-mode . lsp))

;;; [ yaml-imenu ] -- Enhancement of the imenu support in yaml-mode.

(use-package yaml-imenu
  :ensure t
  :defer t)


(provide 'init-yaml)

;;; init-yaml.el ends here
