;;; init-language-chinese.el --- init for Chinese
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ pangu-spacing ] -- Emacs minor-mode to add space between Chinese and English characters.

;; (use-package pangu-spacing
;;   :ensure t
;;   :defer t
;;   :init (global-pangu-spacing-mode 1)
;;   (add-hook 'org-mode-hook
;;             '(lambda () (set (make-local-variable 'pangu-spacing-real-insert-separtor) t))))

;;; [ pinyin-search ] -- Search Chinese by Pinyin

(use-package pinyin-search
  :ensure t
  :defer t
  :commands (pinyin-search pinyin-search-backward)
  :bind (:map language-search-prefix ("c" . pinyin-search) ("C" . pinyin-search-backward)))

;;; [ pinyin-isearch ] -- Pinyin mode for isearch

;; Replace key bindings for /functions/ `isearch-forward' and `isearch-backward'.
;; Allow with query {pinyin} to find {pīnyīn}.  [C-u C-s] used for *normal isearch*.
;; `pinyin-isearch-activate-submodes' Chinese characters search submode
;; -> C-s M-s p/h/s - to activate (p)inyin or (h) Chonese characters (s)trict

(use-package pinyin-isearch
  :ensure t
  :commands (pinyin-isearch-forward pinyin-isearch-backward)
  :init (pinyin-isearch-mode 1) (pinyin-isearch-activate-submodes))

;;; [ jieba.el ] -- 在Emacs中使用jieba中文分词.

;; (use-package jieba
;;   :vc (:url "https://github.com/cireu/jieba.el")
;;   :commands (jieba-mode)
;;   :init (jieba-mode))


(provide 'init-language-chinese)

;;; init-language-chinese.el ends here
