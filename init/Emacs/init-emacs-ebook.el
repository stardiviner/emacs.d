;;; init-emacs-ebook.el --- init for EBook
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ nov ] -- featureful EPUB reader mode.

(use-package nov
  :ensure t
  :mode ("\\.epub\\'" . nov-mode)
  :init (with-eval-after-load "org" ; open Org file: link type EPUB file with nov.el
          (add-to-list 'org-file-apps '("\\.epub\\'" . auto-mode)))
  :config (define-key nov-mode-map (kbd "q") 'kill-current-buffer)
  (org-link-set-parameters
   "epub"
   :follow 'nov-org-link-follow
   :store 'nov-org-link-store
   :complete 'org-link-complete-file))

;;; [ calibre ] -- Interact with Calibre libraries from Emacs.

(use-package calibre
  :ensure t
  :custom (calibre-libraries `(("Default" . ,(expand-file-name "~/Calibre Library"))))
  :init (require 'calibre-core) (require 'calibre-db) (require 'calibre-library)
  (add-to-list 'display-buffer-alist `("^\\*Library\\*" . (display-buffer-below-selected)))
  :commands (calibre-select-library
             calibre-library calibre-library-add-book calibre-dired-add))

;;; [ calibredb ] -- Yet Another calibre client for ebook management.

;; (use-package calibredb
;;   :ensure t
;;   :commands (calibredb)
;;   :custom ((calibredb-root-dir (expand-file-name "~/Calibre Library"))
;;            (calibredb-format-icons t)))

;;; [ fb2-reader ] -- Read FictionBook2 .fb2 and .fb2.zip documents.

(use-package fb2-reader
  :ensure t
  :mode (("\\.fb2\\(\\.zip\\)?\\'" . fb2-reader-mode))
  :commands (fb2-reader-continue))


(provide 'init-emacs-ebook)

;;; init-emacs-ebook.el ends here
