;;; init-emacs-benchmark.el --- init Benchmark settings for Emacs.
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ benchmark ] -- support for benchmarking Emacs code.

(use-package benchmark
  :defer t
  :commands (benchmark benchmark-call benchmark-run benchmark-progn))


(provide 'init-emacs-benchmark)

;;; init-emacs-benchmark.el ends here
