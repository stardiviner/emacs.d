;;; init-emacs-environment.el --- init Emacs environment variables

;;; Commentary:

;;; Code:

;;; [ User Information ]

(setq user-full-name "Christopher M. Miles")
(setq user-mail-address "numbchild@gmail.com")
;; (setq user-login-name "stardiviner")


;;; $PATH
;;; way 0:
;; (setenv "PATH" (concat (getenv "PATH") ":~/bin"))
;;; way 1:
(add-to-list 'exec-path (expand-file-name "~/bin"))
;;; way 2:
;; (defun eshell-mode-hook-func ()
;;   (setq eshell-path-env (concat "~/bin:" eshell-path-env))
;;   (setenv "PATH" (concat "/usr/local/bin:" (getenv "PATH"))))
;; (add-hook 'eshell-mode-hook 'eshell-mode-hook-func)


;;; time

(setq system-time-locale "C") ; make timestamps in org-mode appear in English.



(provide 'init-emacs-environment)

;;; init-emacs-environment.el ends here
