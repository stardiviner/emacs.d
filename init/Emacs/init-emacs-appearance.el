;;; init-emacs-appearance.el --- my Emacs apperance init

;;; Commentary:


;;; Code:

;;; [ maximum Emacs frame ]
;;
;; the t parameter apends to the hook, instead of prepending
;; this means it'd be run after other hooks that might fiddle
;; with the frame size
;; (add-hook 'window-setup-hook 'toggle-frame-maximized t)
(add-hook 'after-init-hook 'toggle-frame-maximized)

;;; [ frame-background-mode ]

(defun my/frame-background-mode-reset-based-on-background-color (&optional theme frame)
  "Setting the `frame-background-mode' to 'light or 'dark based on custom theme.
Check the result of (frame-parameter nil 'background-mode)"
  (interactive)
  (let* ((current-background-mode (frame-parameter frame 'background-mode))
         (background-color (frame-parameter frame 'background-color))
         (background-mode (cond
                           ;; (frame-terminal-default-bg-mode nil)
                           ((equal background-color "unspecified-fg") ; inverted colors
                            non-default-bg-mode)
                           ((not (color-values background-color frame))
                            default-bg-mode)
                           ((>= (apply '+ (color-values background-color frame))
                                ;; Just looking at the screen, colors whose
                                ;; values add up to .6 of the white total still
                                ;; look dark to me.
                                (* (apply '+ (color-values "white" frame)) .6))
                            'light)
                           (t 'dark)))
         (old-display-type (frame-parameter frame 'display-type))
         (display-type (cond ((null (window-system frame))
                              (if (tty-display-color-p frame) 'color 'mono))
                             ((display-color-p frame) 'color)
                             ((x-display-grayscale-p frame) 'grayscale)
                             (t 'mono))))
    (pcase background-mode
      ('light
       (let ((frame-background-mode 'light))
         (setq frame-background-mode 'light)
         (setq ns-system-appearance 'light)
         (if frame
             (frame-set-background-mode frame)
           (mapc 'frame-set-background-mode (frame-list)))))
      ('dark
       (let ((frame-background-mode 'dark))
         (setq frame-background-mode 'dark)
         (setq ns-system-appearance 'dark)
         (if frame
             (frame-set-background-mode frame)
           (mapc 'frame-set-background-mode (frame-list))))))
    (message "current custom theme frame parameter background-mode is [%s]"
             (frame-parameter nil 'background-mode))))

(add-hook 'load-theme-after-hook #'my/frame-background-mode-reset-based-on-background-color -100)

;;; Auto set `frame-background-mode' value when macOS appearance changed.
(defun my/frame-background-mode-reset-based-on-macOS-appearance (appearance)
  "Reset `frame-background-mode' value light or dark when macOS changing appearance."
  (pcase appearance
    ('light
     (setq frame-background-mode 'light)
     (mapc 'frame-set-background-mode (frame-list)))
    ('dark
     (setq frame-background-mode 'dark)
     (mapc 'frame-set-background-mode (frame-list)))))

(when (boundp 'ns-system-appearance-change-functions)
  (add-hook 'ns-system-appearance-change-functions #'my/frame-background-mode-reset-based-on-macOS-appearance)
  (remove-hook 'load-theme-after-hook #'my/frame-background-mode-reset-based-on-background-color))

;;; [ transwin ] -- Make window/frame transparent.

(use-package transwin
  :ensure t
  :init
  (defun my/transwin-set-transparency-for-theme-switch (theme)
    (cl-case (frame-parameter nil 'background-mode)
      (light (transwin-ask 100))
      (dark (transwin-ask 95))))
  (add-hook 'load-theme-after-hook #'my/transwin-set-transparency-for-theme-switch 94))

;;; [ Frame title ]

(setq-default frame-title-format "\n") ; empty frame title.

;; (setq frame-title-format
;;       '("" invocation-name " λ "
;;         (:eval
;;          (cond
;;           ;;; NOTE set title for KDE KWin manager matching PDF viewer frame.
;;           ((and (featurep 'eaf) (eq major-mode 'eaf-mode))
;;            (format "EAF:%s" eaf--buffer-app-name))
;;           ((buffer-file-name)
;;            (or (file-name-nondirectory (buffer-file-name)) "%b"))))))

;;; [ Emacs window border ]

(setq-default indicate-buffer-boundaries 'left)

;;; [[info:elisp#Layout Parameters][info:elisp#Layout Parameters]]

(defun my/set-frame-border-width ()
  "Set frame default fringe, margin, border width."
  (let* ((hidpi-p (screen-hidpi-p))
         (fringe-width (if hidpi-p 8 4))
         (margin-width (if hidpi-p 1 0))
         (border-width (if hidpi-p 2 1)))
    (set-fringe-style (cons fringe-width fringe-width))
    (setq-default left-fringe-width fringe-width
                  right-fringe-width fringe-width)
    (setq-default left-margin-width margin-width
                  right-margin-width margin-width)
    (set-frame-parameter nil 'internal-border-width border-width)
    (set-frame-parameter nil 'vertical-scroll-bars nil)
    (set-frame-parameter nil 'horizontal-scroll-bars nil)
    (set-frame-parameter nil 'scroll-bars-width nil)
    (set-frame-parameter nil 'left-fringe nil)
    (set-frame-parameter nil 'right-fringe nil)
    (set-frame-parameter nil 'right-divider-width 2)
    (set-frame-parameter nil 'bottom-divider-width 2)))

;; (my/set-frame-border-width)

;; (setq default-frame-alist '((internal-border-width . 15)))

;;; `fringe-indicator-alist'
(setq fringe-indicator-alist
      '((truncation left-arrow right-arrow)
        (continuation left-curly-arrow right-curly-arrow)
        (overlay-arrow . right-triangle)
        (up . up-arrow)
        (down . down-arrow)
        (top top-left-angle top-right-angle)
        (bottom bottom-left-angle bottom-right-angle top-right-angle top-left-angle)
        (top-bottom left-bracket right-bracket top-right-angle top-left-angle)
        (empty-line . empty-line)
        (unknown . question-mark)))

;;; HiDPI fringe bitmaps
(when (screen-hidpi-p)
  (define-fringe-bitmap 'left-curly-arrow
    (vector #b0011111110000000
            #b0011111110000000
            #b0011111110000000
            #b0011100000000000
            #b0011100000000000
            #b0011100000000000
            #b0011100000000000
            #b0011100001000000
            #b0011100001100000
            #b0011100001110000
            #b0011111111111000
            #b0011111111111100
            #b0011111111111000
            #b0000000001110000
            #b0000000001100000
            #b0000000001000000
            )
    16 16)

  (define-fringe-bitmap 'right-curly-arrow
    (vector #b0000000111111100
            #b0000000111111100
            #b0000000111111100
            #b0000000000011100
            #b0000000000011100
            #b0000000000011100
            #b0000000000011100
            #b0000001000011100
            #b0000011000011100
            #b0000111000011100
            #b0001111111111100
            #b0011111111111100
            #b0001111111111100
            #b0000111000000000
            #b0000011000000000
            #b0000001000000000
            )
    16 16)

  (define-fringe-bitmap 'left-arrow
    (vector #b0000000001000000
            #b0000000011000000
            #b0000000111000000
            #b0000001111000000
            #b0000011110000000
            #b0000111100000000
            #b0001111111111100
            #b0011111111111100
            #b0011111111111100
            #b0001111111111100
            #b0000111100000000
            #b0000011110000000
            #b0000001111000000
            #b0000000111000000
            #b0000000011000000
            #b0000000001000000
            )
    16 16)

  (define-fringe-bitmap 'right-arrow
    (vector #b0000001000000000
            #b0000001100000000
            #b0000001110000000
            #b0000001111000000
            #b0000000111100000
            #b0000000011110000
            #b0011111111111000
            #b0011111111111100
            #b0011111111111100
            #b0011111111111000
            #b0000000011110000
            #b0000000111100000
            #b0000001111000000
            #b0000001110000000
            #b0000001100000000
            #b0000001000000000
            )
    16 16)

  (define-fringe-bitmap 'right-triangle
    (vector #b0000000000000000
            #b0000000000000000
            #b0011000000000000
            #b0011110000000000
            #b0011111100000000
            #b0011111111000000
            #b0011111111110000
            #b0011111111111100
            #b0011111111111100
            #b0011111111110000
            #b0011111111000000
            #b0011111100000000
            #b0011110000000000
            #b0011000000000000
            #b0000000000000000
            #b0000000000000000
            )
    16 16)
  
  ;; Bitmap for breakpoint in fringe
  (define-fringe-bitmap 'breakpoint
    (vector #b0000000000000000
            #b0000000000000000
            #b0011000000000000
            #b0011110000000000
            #b0011111100000000
            #b0011111111000000
            #b0011111111110000
            #b0011111111111100
            #b0011111111111100
            #b0011111111110000
            #b0011111111000000
            #b0011111100000000
            #b0011110000000000
            #b0011000000000000
            #b0000000000000000
            #b0000000000000000
            )
    16 16)
  ;; Bitmap for gud-overlay-arrow in fringe
  (define-fringe-bitmap 'hollow-right-triangle
    (vector #b0000000000000000
            #b0000000000000000
            #b0011000000000000
            #b0011110000000000
            #b0011111100000000
            #b0011111111000000
            #b0011111111110000
            #b0011111111111100
            #b0011111111111100
            #b0011111111110000
            #b0011111111000000
            #b0011111100000000
            #b0011110000000000
            #b0011000000000000
            #b0000000000000000
            #b0000000000000000
            )
    16 16)

  (with-eval-after-load 'gdb
    (setq gdb-buffer-fringe-width (* gdb-buffer-fringe-width 2)))

  ;; for `realgud'
  (define-fringe-bitmap 'arrow-bitmap
    (vector #b0000000000000000
            #b0000000000000000
            #b0011000000000000
            #b0011110000000000
            #b0011111100000000
            #b0011111111000000
            #b0011111111110000
            #b0011111111111100
            #b0011111111111100
            #b0011111111110000
            #b0011111111000000
            #b0011111100000000
            #b0011110000000000
            #b0011000000000000
            #b0000000000000000
            #b0000000000000000
            )
    16 16)

  (define-fringe-bitmap 'realgud-bp-filled
    (vector #b0000001000000000
            #b0000001100000000
            #b0000001110000000
            #b0000001111000000
            #b0000000111100000
            #b0000000011110000
            #b0011111111111000
            #b0011111111111100
            #b0011111111111100
            #b0011111111111000
            #b0000000011110000
            #b0000000111100000
            #b0000001111000000
            #b0000001110000000
            #b0000001100000000
            #b0000001000000000
            )
    16 16)
  (define-fringe-bitmap 'realgud-bp-hollow
    (vector #b0000001000000000
            #b0000001100000000
            #b0000001110000000
            #b0000001111000000
            #b0000000111100000
            #b0000000011110000
            #b0011111111111000
            #b0011111111111100
            #b0011111111111100
            #b0011111111111000
            #b0000000011110000
            #b0000000111100000
            #b0000001111000000
            #b0000001110000000
            #b0000001100000000
            #b0000001000000000
            )
    16 16)
  )

;;; [ echo area ]

;;; remind user to use abbrev to simplify input in echo are.
(setq abbrev-suggest t)

;;; [ Widget ]

;;; [ line ]

;;; [ line space(spacing) / line height ]
;; - (info "(elisp) Line Height")
;; - (info "(elisp) Layout Parameters")
;; The total height of each display line consists of the height of the
;; contents of the line, plus optional additional vertical line spacing
;; above or below the display line.

;; additional space to put between lines.
;; (setq-default line-spacing 0.1)         ; nil, 0, 0.1, 1,

;;; [ line numbers ] -- Emacs native line number mode.

;; (setq display-line-numbers-width-start t)
;; (global-display-line-numbers-mode 1)

;;; [ current line & column ]

;; highlight current line
(use-package hl-line
  :ensure nil
  :defer t
  :hook (after-init . global-hl-line-mode))

;;; [ cursor point ]

;; auto move mouse away when cursor is at mouse position
(setq-default mouse-avoidance-mode 'animate)
(setq-default cursor-type 'box)
(setq cursor-in-non-selected-windows nil)
(defun my/set-cursor-color (&optional theme)
  (cl-case (alist-get 'background-mode (frame-parameters))
    (light (set-cursor-color "DodgerBlue"))
    (dark (set-cursor-color "firebrick1"))))
(add-hook 'load-theme-after-hook #'my/set-cursor-color 95)
;;; The `blink-cursor-mode' caused in-buffer picture blink.
(blink-cursor-mode -1)

;;; [ truncate continuous line & word wrap ]

;;; `toggle-truncate-lines'
;; (setq-default truncate-lines t)
(setq truncate-string-ellipsis "…")
;;
;;; `toggle-word-wrap'
;; (setq-default word-wrap t)
(setq word-wrap-by-category t) ; support better wrap on CJK characters.

;; (setq left-fringe-width 8
;;       right-fringe-width 8)

;; (define-fringe-bitmap 'left-curly-arrow
;;   [#b00011000
;;    #b00011000
;;    #b00000000
;;    #b00011000
;;    #b00011000
;;    #b00000000
;;    #b00011000
;;    #b00011000])
;; (define-fringe-bitmap 'right-curly-arrow
;;   [#b00011000
;;    #b00011000
;;    #b00000000
;;    #b00011000
;;    #b00011000
;;    #b00000000
;;    #b00011000
;;    #b00011000])

(define-fringe-bitmap 'right-curly-arrow
  [#b00000000
   #b00000000
   #b00000000
   #b11111100
   #b11111100
   #b00001100
   #b00001100
   #b00001100])

(define-fringe-bitmap 'left-curly-arrow
  [#b00000000
   #b00110000
   #b00110000
   #b00110000
   #b00111111
   #b00111111
   #b00000000
   #b00000000])

(setq visual-line-fringe-indicators '(left-curly-arrow right-curly-arrow))

;;; [ auto fill ] -- auto fill (break line) paragraphs like hitting [M-q].

(setq-default fill-column 100           ; set 100 line width for large screen.
              comment-fill-column 100)
;;; auto fill comments but not code in programming modes:
(add-hook 'prog-mode-hook
          #'(lambda () (setq-local comment-auto-fill-only-comments t)))

;;; [ fill column indicator ] -- Toggle display of `fill-column' indicator.

(use-package display-fill-column-indicator
  :custom ((display-fill-column-indicator-column t) ; t: means use `fill-column'.
           ;; The character can check out `display-fill-column-indicator' source code.
           ;; (display-fill-column-indicator-character ?\u2502)
           ;; (display-fill-column-indicator-character ?\u2503)
           )
  :hook (emacs-lisp-mode . display-fill-column-indicator-mode)
  :config (add-hook 'emacs-lisp-mode-hook #'(lambda () (setq-local fill-column 80))))

;;; [ Display ^L glyphs as horizontal lines (^L) ]

(use-package form-feed-st
  :ensure t
  :init (form-feed-st-mode 1))

;; - <C-x [/]> :: navigate.
;; "^\014",
;; (setq page-delimiter
;;       (rx bol ";;;" (not (any "#")) (* not-newline) "\n"
;;           (* (* blank) (opt ";" (* not-newline)) "\n")))
;; Expanded regexp:
;; "^;;;[^#].*\n\\(?:[[:blank:]]*\\(?:;.*\\)?\n\\)*"
;;
;; The regexp above is a bit special. We’re setting the page delimiter to be a
;; ;;; at the start of a line, plus any number of empty lines or comment lines
;; that follow it (that # part is to exclude ;;;###autoload cookies).


;;; Disable GUI dialog boxes

(setq use-dialog-box nil) ; use mini-buffer for everything' else..


;;; trailing whitespace

(use-package whitespace
  :hook ((markdown-mode conf-mode) . whitespace-mode)
  :config
  (setq whitespace-line-column 80) ; highlight beyond limit line length
  
  ;; Don't use different background for tabs.
  (face-spec-set 'whitespace-tab
                 '((t :background unspecified)))
  ;; Only use background and underline for long lines, so we can still have
  ;; syntax highlight.

  ;; For some reason use face-defface-spec as spec-type doesn't work.  My guess
  ;; is it's due to the variables with the same name as the faces in
  ;; whitespace.el.  Anyway, we have to manually set some attribute to
  ;; unspecified here.
  (face-spec-set 'whitespace-line
                 '((((background light))
                    :background "#d8d8d8" :foreground unspecified
                    :underline t :weight unspecified)
                   (t
                    :background "#404040" :foreground unspecified
                    :underline t :weight unspecified)))

  ;; Use softer visual cue for space before tabs.
  (face-spec-set 'whitespace-space-before-tab
                 '((((background light))
                    :background "#d8d8d8" :foreground "#de4da1")
                   (t
                    :inherit warning
                    :background "#404040" :foreground "#ee6aa7")))

  (setq whitespace-line-column nil
        whitespace-style '(face             ; visualize things below:
                           empty            ; empty lines at beginning/end of buffer
                           lines-tail       ; lines go beyond `fill-column'
                           space-before-tab ; spaces before tab
                           trailing         ; trailing blanks
                           tabs             ; tabs (show by face)
                           tab-mark         ; tabs (show by symbol)
                           )))

;;; [ long line ]

;;; `so-long-mode' deprecated, replaced by `long-line-threshold'.

;;; [ nerd-icons ] -- Emacs Nerd Font Icons Library

(use-package nerd-icons
  :ensure t
  :config
  ;; check out icons from `nerd-icons/*-alist'.
  ;; for CDisplay Archived Comic Book files.
  (add-to-list 'nerd-icons-extension-icon-alist '("cbr" nerd-icons-mdicon "nf-md-book_open_blank_variant" :face nerd-icons-lgreen))
  (add-to-list 'nerd-icons-extension-icon-alist '("cbz" nerd-icons-mdicon "nf-md-book_open_blank_variant" :face nerd-icons-lgreen))
  )

;;; [ nerd-icons-completion ] -- Add icons to completion candidates using nerd-icons.

(use-package nerd-icons-completion
  :ensure t
  :config (nerd-icons-completion-mode 1))

;;; [ pulse ] -- Pulsing Overlays.

;; (use-package pulse
;;   :defer t
;;   :commands (pulse-momentary-highlight-one-line
;;              pulse-momentary-highlight-region
;;              pulse-momentary-highlight-overlay)
;;   :config
;;   (setq pulse-command-advice-flag t)
;;   (add-hook 'org-babel-pre-tangle-hook #'pulse-line-hook-function)
;;   (add-hook 'org-babel-after-execute-hook #'pulse-line-hook-function))

;;; [ beacon ] -- Highlight the cursor whenever the window scrolls.

;; (use-package beacon
;;   :ensure t
;;   :custom ((beacon-blink-when-focused t)
;;            (beacon-color "VioletRed1"))
;;   :hook (after-init . beacon-mode))

;;; [ pulsar ] -- Pulse highlight on demand or after select functions.

;; (use-package pulsar
;;   :ensure t
;;   :commands (pulsar-setup pulsar-pulse-line pulsar-highlight-line)
;;   :hook (after-init . pulsar-setup)
;;   :config
;;   ;; integration with the `consult' package:
;;   (add-hook 'consult-after-jump-hook #'pulsar-recenter-top)
;;   (add-hook 'consult-after-jump-hook #'pulsar-reveal-entry)
;;   ;; integration with the built-in `imenu':
;;   (add-hook 'imenu-after-jump-hook #'pulsar-recenter-top)
;;   (add-hook 'imenu-after-jump-hook #'pulsar-reveal-entry))

;;; [ power-mode ] -- Imbue Emacs with power!

;; (use-package power-mode
;;   :if (eq system-type 'gnu/linux)
;;   :ensure t
;;   :hook (after-init . power-mode))



(provide 'init-emacs-appearance)

;;; init-emacs-appearance.el ends here
