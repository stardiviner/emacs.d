;;; init-emacs-window.el --- my Emacs window init

;;; Commentary:


;;; Code:

;;; [ display-buffer-alist ] ;; apply actions on `display-buffer'

;; Learn about display actions, see [[info:elisp#Display Action Functions]].
;; Check docstring of `display-buffer-alist'.
;;
;; Syntax:
;;
;;       (CONDITION . ACTION) -> (CONDITION . (FUNCTIONS . ALIST))

;; Functions:
;;
;; `display-buffer' actions, reference `display-buffer--action-function-custom-type'.
;;
;; - (display-buffer-below-selected)
;;
;; (display-buffer-below-selected
;;  '((window-height . 20) (fit-window-to-buffer . t)))
;;
;; - (display-buffer-same-window)
;; - (display-buffer-at-bottom)
;;
;;   (display-buffer-at-bottom
;;    (window-height . fit-window-to-buffer))
;;
;; - (display-buffer-in-side-window) :: like which-key popup window upon bottom minibuffer.
;;   - (display-buffer-in-side-window ((side . bottom) (window-height . 4)))
;;   - (display-buffer-in-side-window ((side . right)))
;;   - (display-buffer-in-side-window ((side . left)))
;; - (display-buffer-no-window)
;; - (display-buffer-in-child-frame) :: DON'T USE IT!!!
;; - (display-buffer-reuse-window (window-height . 0.3))
;; - (display-buffer-reuse-window display-buffer-same-window)

;; Examples:
;; (add-to-list 'display-buffer-alist
;;              '("\\*shell:.*?\\*.*"
;;                (display-buffer-in-side-window)
;;                (side . right)
;;                (window-width . 80)
;;                (dedicated . t)))
;;
;; (add-to-list 'display-buffer-alist
;;              '("^ \\*undo-tree\\*" .
;;                ((display-buffer-reuse-window display-buffer-in-side-window) .
;;                 ((reusable-frames . visible)
;;                  (side . right)
;;                  (slot . 1)
;;                  (window-sides-vertical . t)
;;                  (window-width . 0.5)
;;                  (window-height . 0.15)))))
;;
;; (add-to-list 'display-buffer-alist
;;              `(,(rx bos "*Flycheck errors*" eos) .
;;                ((display-buffer-reuse-window display-buffer-in-side-window) .
;;                 ((side            . bottom)
;;                  (reusable-frames . visible)
;;                  (window-height   . 0.15)))))
;;
;; (add-to-list 'display-buffer-alist
;;              '((lambda (&rest _)
;;                  (memq this-command '(compile-goto-error occur-mode-goto-occurrence)))
;;                ((display-buffer-reuse-window display-buffer-same-window) .
;;                 ((inhibit-same-window . nil)))))
;;
;; (add-to-list 'display-buffer-alist
;; 	         `(,(rx "*Org Help*")
;;                ((display-buffer-no-window) . ((allow-no-window . t)))))
;;
;; (add-to-list 'display-buffer-alist
;;              '((or (derived-mode . python-mode) (derived-mode . inferior-python-mode))
;;                (display-buffer-reuse-window display-buffer-reuse-mode-window)
;;                (inhibit-same-window . nil)))

(add-to-list 'display-buffer-alist '("^\\*Help\\*" . (display-buffer-below-selected)))
(add-to-list 'display-buffer-alist '("^\\*Error\\*" . (display-buffer-below-selected)))
(add-to-list 'display-buffer-alist '("^\\*Pp Eval Output\\*" . (display-buffer-below-selected)))
(add-to-list 'display-buffer-alist '("^\\*Output\\*" . (display-buffer-below-selected)))
(add-to-list 'display-buffer-alist '("^\\*Process List\\*" . (display-buffer-below-selected)))
(add-to-list 'display-buffer-alist '("^\\*Animation\\*" . (display-buffer-below-selected)))

;;; [ winner ] -- Restore old window configurations.

(use-package winner
  :ensure t
  :hook (after-init . winner-mode)
  :config
  (defun transient-winner-undo ()
    "Transient version of `winner-undo'."
    (interactive)
    (let ((echo-keystrokes nil))
      (winner-undo)
      (message "Winner: [u]ndo [r]edo")
      (set-transient-map
       (let ((map (make-sparse-keymap)))
         (define-key map [?u] #'winner-undo)
         (define-key map [?r] #'winner-redo)
         map)
       t))))

;;; [ ace-window ] -- Quickly switch windows in Emacs.

(use-package ace-window
  :ensure t
  :defer t
  :delight ace-window-mode
  :custom (aw-background nil)
  :custom-face (aw-leading-char-face ((t (:inherit nil :height 250 :foreground "red" :box t))))
  :commands (ace-window ace-window-posframe-mode)
  :bind (("C-x C-j" . ace-window))
  :config
  ;; refresh eldoc after ace-window selecting window.
  (with-eval-after-load 'eldoc (eldoc-add-command 'ace-window)))

;;; [ hydra ]

(use-package hydra
  :ensure t
  :ensure ace-window
  :defer t
  :config
  ;; `hydra-frame-window' is designed from `ace-window' and
  ;; matches aw-dispatch-alist with a few extra
  (defhydra hydra-frame-window (:color red :hint nil)
    "
^Frame^                 ^Window^      Window Size^^^^^^    ^Text Zoom^               (__)
_0_: delete             _t_oggle        ^ ^ _k_ ^ ^            _=_                   (oo)
_1_: delete others      _s_wap          _h_ ^+^ _l_            ^+^             /------\\/
_2_: new                _d_elete        ^ ^ _j_ ^ ^            _-_            / |    ||
_F_ullscreen            _f_rame         _b_alance^^^^          ^ ^        *  /\\---/\\  ~~  C-c w/C-x o w
"
    ("0" delete-frame :exit t)
    ("1" delete-other-frames :exit t)
    ("2" make-frame  :exit t)
    ("b" balance-windows)
    ("s" ace-swap-window)
    ("F" toggle-frame-fullscreen)
    ("t" toggle-window-split)
    ("d" delete-window :exit t)
    ("f" my:turn-current-window-into-new-frame :exit t)
    ("-" text-scale-decrease)
    ("=" text-scale-increase)
    ("h" shrink-window-horizontally)
    ("k" shrink-window)
    ("j" enlarge-window)
    ("l" enlarge-window-horizontally)
    ("q" nil "quit"))
  (global-set-key (kbd "C-x C-z") #'hydra-frame-window/body)
  (with-eval-after-load 'ace-window
    (add-to-list 'aw-dispatch-alist '(?w hydra-frame-window/body) t)))

;;; [ golden-ratio ] -- Automatic resizing of Emacs windows to the golden ratio.

(use-package golden-ratio
  :ensure t
  :delight golden-ratio-mode
  ;; :if (< (frame-width) 200) ; enable `golden-ratio' only in small screen.
  :custom ((golden-ratio-auto-scale nil)
           (golden-ratio-recenter t)
           ;; Exclude following pattern buffers.
           (golden-ratio-exclude-modes '(ediff-mode
                                         calendar-mode calc-mode dired-mode
                                         speedbar-mode project-explorer-mode
                                         gnus-summary-mode gnus-article-mode
                                         mu4e-headers-mode mu4e-compose-mode
                                         help-mode
                                         helm-major-mode helm-top-poll-mode
                                         restclient-mode))
           (golden-ratio-exclude-buffer-regexp '("\\`\\*.*?\\*\\'" ; "*...*" buffers
                                                 "\\` \\*.*?\\*\\'" ; " *...*" buffers
                                                 ))
           (golden-ratio-exclude-buffer-names '("*rg*"
                                                " *Org todo*" " *Org tags*" " *Org Attach*"
                                                "*helm*")))
  :init (golden-ratio-mode 1)
  :config
  ;; add extra commands to golden-ratio triggers list.
  ;; (add-to-list 'golden-ratio-inhibit-functions 'pop-to-buffer) ; support for `popwin'

  ;; for `which-key'
  ;; FIXME this make `golden-ratio' does not work.
  ;; (with-eval-after-load 'which-key
  ;;   (add-to-list 'golden-ratio-exclude-buffer-names which-key-buffer-name)
  ;;   (add-to-list 'golden-ratio-inhibit-functions 'which-key--start-timer)
  ;;   (add-to-list 'golden-ratio-inhibit-functions 'which-key--update))

  (add-to-list 'golden-ratio-extra-commands 'other-window) ; support [C-x o]
  (add-to-list 'golden-ratio-extra-commands 'window-number-select) ; support for `window-number'
  (add-to-list 'golden-ratio-extra-commands 'ace-window) ; support for `ace-window'
  (add-to-list 'golden-ratio-extra-commands 'org-noter-sync-current-page-or-chapter) ; support for `org-noter'
  (add-to-list 'golden-ratio-extra-commands 'org-noter-sync-current-note) ; support for `org-noter'

  ;; exclude calendar buffers
  (add-to-list 'golden-ratio-exclude-modes 'calendar-mode)
  (add-to-list 'golden-ratio-exclude-buffer-names calendar-buffer)
  (add-to-list 'golden-ratio-exclude-buffer-names holiday-buffer)
  (add-to-list 'golden-ratio-exclude-buffer-names diary-fancy-buffer)
  (add-to-list 'golden-ratio-exclude-buffer-names calendar-other-calendars-buffer)
  (add-to-list 'golden-ratio-exclude-buffer-names lunar-phases-buffer)
  (add-to-list 'golden-ratio-exclude-buffer-names solar-sunrises-buffer)

  ;; disable in ediff session.
  (add-hook 'ediff-before-setup-windows-hook #'(lambda () (golden-ratio-mode -1)))
  (add-hook 'ediff-quit-hook #'(lambda () (golden-ratio-mode 1)))

  ;; fix `dired-sidebar-toggle-sidebar' can't jump to sidebar window issue.
  (when (featurep 'dired-sidebar)
    (add-to-list 'golden-ratio-exclude-modes 'dired-sidebar-mode))
  )

;;; [ follow-mode ] -- [C-c .] same buffer different windows auto following in large screen.

(use-package follow
  :defer t
  :commands (follow-mode)
  :config
  (defun my/follow-mode-auto-split ()
    "Auto split when enabled `follow-mode', and close same buffer when disable mode."
    (if (= (length (get-buffer-window-list)) 2)
        (delete-window)
      (split-window-horizontally)))
  (add-hook 'follow-mode-hook #'my/follow-mode-auto-split))



(provide 'init-emacs-window)

;;; init-emacs-window.el ends here
