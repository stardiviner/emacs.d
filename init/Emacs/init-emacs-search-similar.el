;;; init-emacs-search-similar.el --- search engine for similar text file -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Christopher M. Miles, all rights reserved.

;;; Commentary:



;;; Code:

;;; [ docsim ] -- A fast, local search engine for your text files. Query your notes and get a ranked list of the best matches.

(use-package docsim
  :if (executable-find "docsim")
  :ensure t
  :commands (docsim-search docsim-search-buffer)
  :custom (docsim-search-paths `(,org-directory)))



(provide 'init-emacs-search-similar)

;;; init-emacs-search-similar.el ends here
