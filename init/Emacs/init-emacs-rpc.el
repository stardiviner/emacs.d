;;; init-emacs-rpc.el --- init Emacs for Remote Produrce Call (RPC)

;;; Commentary:



;;; Code:

;;; [ jsonrpc ] -- JSON-RPC library.

(use-package jsonrpc
  :ensure t
  :defer t)

;;; [ deno-bridge ] -- Build bridge between Emacs and Deno, execution of JavaScript and Typescript within Emacs.

(add-to-list 'load-path (expand-file-name "site-lisp/deno-bridge/" user-emacs-directory))
(require 'deno-bridge)



(provide 'init-emacs-rpc)

;;; init-emacs-rpc.el ends here
