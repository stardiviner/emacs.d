;;; init-emacs-search-ripgrep.el --- init for ripgrep
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

(unless (boundp 'rg-prefix)
  (define-prefix-command 'rg-prefix))
(define-key search-prefix (kbd "r") 'rg-prefix)

;;; [ rg ] -- Use ripgrep (grep and ag replacement) like rgrep.

(use-package rg
  :ensure t
  :defer t
  :preface (add-to-list 'exec-path (or (concat (getenv "CARGO_HOME") "/bin") (expand-file-name "~/.cargo/bin/")))
  :commands (rg-menu rg rg-literal rg-dwim rg-dwim-current-dir rg-dwim-project-dir)
  :bind (;; :map search-prefix ("s" . rg)
         :map rg-prefix
         ("r" . rg-menu)
         ("R" . rg)
         ("d" . rg-dwim)
         ("c" . rg-dwim-current-dir)
         ("p" . rg-dwim-project-dir)
         ("l" . rg-literal)
         ("C-s" . consult-ripgrep)
         :map project-prefix-map
         ("C-s" . rg-project)
         ("M-s" . projectile-ag))
  :custom (;; (rg-command-line-flags '("--debug"))
           (rg-keymap-prefix nil)
           (rg-group-result t)
           (rg-command-line-flags '("-j 4")))
  :init
  (rg-enable-default-bindings)
  (add-to-list 'display-buffer-alist '("^\\*rg\\*" . (display-buffer-below-selected)))
  :config
  (if (fboundp 'wgrep-rg-setup) (add-hook 'rg-mode-hook #'wgrep-rg-setup))

  ;; automatically "reveal context" when opening matches in org buffers.
  (defun rg-reveal-org ()
    "Call`org-reveal' if current buffer is an `org-mode' buffer."
    (when (derived-mode-p 'org-mode) (org-reveal)))
  (defun rg-next-error-reveal-org ()
    (add-hook 'next-error-hook #'rg-reveal-org nil 'local))
  (add-hook 'rg-mode-hook #'rg-next-error-reveal-org))


(provide 'init-emacs-search-ripgrep)

;;; init-emacs-search-ripgrep.el ends here
