;;; init-emacs-workspace.el --- init for Emacs workspace.

;;; Commentary:



;;; Code:

;;; [ persp-mode ] -- windows/buffers sets shared among frames + save/load.

;; (use-package persp-mode
;;   :ensure t
;;   :init (setq persp-keymap-prefix (kbd "C-x p"))
;;   :hook (after-init . persp-mode))

;;; [ spacebar ] -- Workspaces Tabbar.

;; (use-package spacebar
;;   :ensure t
;;   ;; :custom (spacebar-keymap-prefix (kbd "C-c w"))
;;   ;; :bind-keymap ("C-c w" . spacebar-command-map)
;;   :config (spacebar-mode))

;;; [ activities ] -- Save/restore sets of windows, tabs/frames, and their buffers inspired by KDE Activities.

(use-package activities
  :ensure t
  :custom ((activities-bookmark-warnings t)
           (activities-tabs-prefix nil)
           (activities-name-prefix nil))
  :init
  (activities-mode)
  (activities-tabs-mode)
  (add-to-list 'display-buffer-alist '("^\\*Activities\\*" . (display-buffer-below-selected)))
  :bind
  (;; C-prefix for activity change
   ("C-c w n"   . activities-new)       ; Save current state as a new activity with NAME.
   ("C-c w RET" . activities-resume)    ; Resume or switch ACTIVITY.
   ("C-c w s"   . activities-suspend)   ; Suspend ACTIVITY.
   ("C-c w k"   . activities-kill)      ; Kill ACTIVITY.
   ("C-c w d"   . activities-discard)   ; Delete ACTIVITY and its state permanently.
   ("C-c w r"   . activities-rename)    ; Rename ACTIVITY.
   ("C-c w C-s" . activities-save-all)  ; Save all active activities' last states.
   ;; Non C-prefix for activity no change
   ;; ("C-c w w"   . activities-switch)    ; Switch to ACTIVITY.
   ("C-c w w"   . activities-resume)    ; Switch or Resume ACTIVITY.
   ("C-c w b"   . activities-switch-buffer)  ; Switch to a buffer associated with ACTIVITY.
   ("C-c w <backspace>" . activities-revert) ; Reset ACTIVITY to its default state.
   ("C-c w l"   . activities-list))
  :config
  ;; Add activities activity name indicator on mode-line.
  (add-to-list 'mode-line-misc-info
               '(activities-mode
                 (:eval (when (activities-current)
                          (concat "[※ " (activities-activity-name (activities-current)) "]")))))
  )


(provide 'init-emacs-workspace)

;;; init-emacs-workspace.el ends here
