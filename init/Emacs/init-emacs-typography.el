;;; init-emacs-typography.el --- init for Typography.

;;; Commentary:



;;; Code:

;;; setence end require one/two spaces after a sentence periods.
;;; [[info:emacs#Sentences][info:emacs#Sentences]]
(setq-default sentence-end-double-space nil)



(provide 'init-emacs-typography)

;;; init-emacs-typography.el ends here
