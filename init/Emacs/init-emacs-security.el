;;; init-emacs-security.el --- init for Emacs Security.

;;; Commentary:



;;; Code:

;;; [ File Local Variables ]

(setq local-enable-local-variables t
      enable-local-eval 'maybe
      enable-local-variables t ; :all
      ;; safe-local-eval-forms
      )

;; (setq safe-local-variable-values)

;; (setq ignored-local-variable-values)

;;; [ password ] -- authentication sources for Gnus and Emacs.

(setq password-cache-expiry nil) ; (* 60 15), nil: don't expire password cache.

;;; [ password-mode ] -- hide sensitive information in buffers (passwords) using overlays.

(use-package password-mode
  :ensure t
  :defer t
  :delight password-mode
  :custom ((password-mode-password-prefix-regexs
            '("[Pp]assword:?[[:space:]]+"
              "[Pp]assword(.*):?[[:space:]]+" ; Org :PASSWORD(.*): property
              ))
           ;; (password-mode-password-regex "\\([[:graph:]]*\\)")
           )
  :config
  ;; only enable `password-mode' in encrypted `*.org.gpg' files.
  (defun org-mode--enable-password-mode ()
    (when (and (buffer-file-name)
               (string-equal (file-name-extension (buffer-file-name)) "gpg"))
      (password-mode 1)))
  (add-hook 'org-mode-hook #'org-mode--enable-password-mode))

;;; [ cloak-mode ] -- A minor mode to cloak sensitive values.

;; (use-package cloak-mode
;;   :ensure t
;;   :custom (cloak-mode-patterns '((envrc-file-mode . "[a-zA-Z0-9_]+[ \t]*=[ \t]*\\(.*+\\)$")))
;;   :init (global-cloak-mode))

;;; [ auth-source ] -- Emacs built-in authentication sources for Gnus and Emacs.

(use-package auth-source
  :ensure dash
  :demand
  :no-require t
  ;; :init (autoload '-filter "dash")
  :requires (dash)
  :custom ((auth-sources
            (append
             (-filter #'file-exists-p
                      `(,(concat user-emacs-directory "secrets/authinfo.gpg")
                        "~/.authinfo.gpg" "~/.authinfo" "~/.netrc"))
             '(macos-keychain-internet)
             '(macos-keychain-generic)))))

;;; [ password-menu ] -- a UI wrapper ("porcelain") for the Emacs built-in auth-source secrets library.

(use-package password-menu
  :ensure t
  :commands (password-menu-transient password-menu-completing-read))

;;; [ auth-source-xoauth2 ] -- Integrate auth-source with XOAUTH2

(use-package auth-source-xoauth2
  :ensure t
  :defer t
  :init (auth-source-xoauth2-enable))

;;; [ auth-source-kwallet ] -- KWallet integration for auth-source

(use-package auth-source-kwallet
  :if (eq system-type 'gnu/linux)
  :ensure t
  :config (auth-source-kwallet-enable))

;; [ oauth2 ] -- OAuth 2.0 Authorization Protocol

(use-package oauth2
  :ensure t
  :commands (oauth2-auth
             oauth2-refresh-access oauth2-auth-and-store
             oauth2-url-retrieve-synchronously oauth2-url-retrieve))

;;; [ Secrets ] -- presenting password entries retrieved by Security Service from freedesktop.org.

;; - Variable: `secrets-path'
;; - Command-Line Utility: `secret-tool'

;;; [ certificate ]

;; (require 'tls)

;; (setq tls-checktrust 'ask)

;;; Fix SSL certificate issue on `gnutls'.
(when (eq system-type 'gnu/linux)
  (setq tls-program
        '("openssl s_client -connect %h:%p -no_ssl2 -ign_eof"
          "gnutls-cli --x509cafile %t -p %p %h"
          "gnutls-cli -p %p %h")))

;; (require 'gnutls)
;;
;; (setq gnutls-verify-error t
;;       ;; gnutls-trustfiles
;;       )


(provide 'init-emacs-security)

;;; init-emacs-security.el ends here
