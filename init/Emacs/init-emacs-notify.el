;;; init-emacs-notify.el --- init for notification
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ notifications ]

;;; Usage:
;;
;; - (notifications-get-server-information)
;; - (notifications-notify :title "Title" :body "text")
;; - (notifications-notify :bus ...)

(require 'notifications)

;;; [ alert ] -- A Growl-like alerts notifier for Emacs.

(use-package alert
  :ensure t
  :defer t
  :custom ((alert-default-style (cl-case system-type
                                  (gnu/linux 'notifications) ; 'notifications, 'libnotify
                                  (darwin 'osx-notifier)))))

;;; [ ednc ] -- Emacs Desktop Notification Center.

(use-package ednc
  :ensure t
  :if (eq system-type 'gnu/linux)
  :hook (after-init . ednc-mode))


(provide 'init-emacs-notify)

;;; init-emacs-notify.el ends here
