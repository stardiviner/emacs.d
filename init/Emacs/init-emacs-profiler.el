;;; init-emacs-profiler.el --- init profilers for Emacs.

;;; Commentary:



;;; Code:

;;; [ profiler ] -- Emacs built-in profiler.

(use-package profiler
  :defer t
  :commands (profiler-start profiler-report profiler-stop))

;;; [ memory-report ] -- Generate a report of how Emacs is using memory.

(use-package memory-report
  :defer t
  :commands (memory-report)
  :init (add-to-list 'display-buffer-alist '("^\\*Memory Report\\*" . (display-buffer-below-selected))))

;;; [ benchmark-init.el ] -- Benchmark your Emacs initialization of require and load calls.

(use-package benchmark-init
  :ensure t
  :commands (benchmark-init/show-durations-tree benchmark-init/show-durations-tabulated)
  :init (benchmark-init/activate))

;;; [ esup ] -- the Emacs StartUp Profiler (ESUP)

(use-package esup
  :ensure t
  :defer t
  :commands (esup))



(provide 'init-emacs-profiler)

;;; init-emacs-profiler.el ends here
