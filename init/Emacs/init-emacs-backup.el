;;; init-emacs-backup.el --- init for Emacs backup

;;; Commentary:

;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Backup.html

;;; Usage:

;; - `recover-file' :: recover file from backup file suffix with "file~".


;;; Code:

;;; [ lock ]

;; .#foo.txt -- lock files, modify same file by different processes.
(setq create-lockfiles t)

;;; [ backup ]

;; foo.txt~ -- backup files, backup the latest recent file.
(setq make-backup-files t)

(setq backup-by-copying t
      backup-by-copying-when-mismatch t
      backup-by-copying-when-privileged-mismatch t
      backup-by-copying-when-linked t)

;; (setq backup-directory-alist
;;       '(("." . ".emacs_backups")) ; save backups in $(pwd)/.emacs_backups/filename.bak
;;       ;; `(("." . ,(expand-file-name (expand-file-name ".backups" user-emacs-directory))))
;;       ;; `((".*" . ,temporary-file-directory)) ; put all under directory /tmp.
;;       )

;; version control with versioned backup filename like "filename.ext.~1~".
;;
;; If a file's name is FOO, the names of its numbered backup versions are
;; FOO.~i~ for various integers i. A non-numbered backup file is called FOO~.
(setq version-control t                 ; Control use of version numbers for backup files.
      vc-make-backup-files nil          ; If nil (the default), files covered by version control don't get backups.
      kept-new-versions 2               ; number of new versions.
      kept-old-versions 2               ; number of old versions.
      delete-old-versions t             ; auto delete old versions.
      dired-kept-versions 2             ; When cleaning directory, number of versions to keep.
      )

;;; [ auto-save-mode ] -- toggle auto-saving in the current buffer.

;; Emacs periodically saves each file-visiting buffer in a separate \"auto-save
;; file\". This is a safety measure to prevent you from losing more than a
;; limited amount of work if the system crashes.
;;
;; `auto-save-list-file-prefix' "~/.config/emacs/auto-save-list/.saves-"

(setq auto-save-default t ; Non-nil says by default do auto-saving of every file-visiting buffer.
      auto-save-timeout (* 60 10)
      auto-save-interval (* 60 10)
      ;; delete-auto-save-files nil ; don't delete auto-save file when a buffer is saved or killed.
      auto-save-no-message t ; Non-nil means do not print any message when auto-saving.
      auto-save-include-big-deletions t)

;;  #foo.txt#
(auto-save-mode t) ; Toggle auto-saving in the current buffer (Auto Save mode).

(setq auto-save-visited-interval (* 60 5))

(auto-save-visited-mode t) ; Toggle automatic saving of file-visiting buffers to their files.


(provide 'init-emacs-backup)

;;; init-emacs-backup.el ends here
