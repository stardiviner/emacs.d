;;; init-emacs-buffer.el --- init Emacs buffer settings.

;;; Commentary:



;;; Code:

;;; [ auto-revert-mode ] -- auto-reload external file changes.

(use-package autorevert
  :defer t
  :delight auto-revert-mode
  :custom ((auto-revert-interval 30)
           ;; disable auto-revert for all files with TRAMP syntax.
           (auto-revert-remote-files nil))
  ;; :init (global-auto-revert-mode 1)
  )

;;; [ files ] -- file input and output commands for Emacs.

(use-package files
  :commands (revert-buffer ; [s-u]
             ))

;;; [ ibuffer ] -- operate on buffers like Dired.

(use-package ibuffer
  :defer t
  :custom (buffers-menu-max-size nil)
  :bind ("C-x C-b" . ibuffer))

(use-package nerd-icons-ibuffer
  :ensure t
  :delight 'nerd-icons-ibuffer-mode
  :hook (ibuffer-mode . nerd-icons-ibuffer-mode))

;;; [uniquify] -- meaningful names for buffers with the same name

(use-package uniquify
  :defer t
  :custom ((uniquify-buffer-name-style 'post-forward-angle-brackets)
           (uniquify-separator " • ")
           (uniquify-ignore-buffers-re "^\\*") ; don't muck with special buffers
           (uniquify-after-kill-buffer-p t)))

;;; [ recentf ] -- setup a menu of recently opened files.

(use-package recentf
  :defer t
  :custom ((recentf-save-file (expand-file-name ".temp/recentf" user-emacs-directory))
           ;; (recentf-auto-cleanup 'mode) ; 'mode, 'never.
           (recentf-exclude '("/tmp/" "/ssh:" "/docker:" "/podman:" "/kubernetes:" "/vagrant:")))
  :hook (after-init . recentf-mode))

;;; [ saveplace ] -- save visited files' point positions.

(use-package saveplace
  :custom (save-place-file "~/.config/emacs/places")
  :hook (after-init . save-place-mode))


(provide 'init-emacs-buffer)

;;; init-emacs-buffer.el ends here
