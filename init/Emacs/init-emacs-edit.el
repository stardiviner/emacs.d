;;; init-emacs-edit.el --- init Emacs editing

;;; Commentary:

;;; Code:
(unless (boundp 'editing-prefix)
  (define-prefix-command 'editing-prefix))
(global-set-key (kbd "C-x e") 'editing-prefix)

(load "init-emacs-kill-ring")
(load "init-emacs-register")
(load "init-emacs-region")
(load "init-emacs-typography")
(load "init-emacs-edit-electric")
(load "init-emacs-edit-rectangle")
(load "init-emacs-edit-narrow")
(load "init-emacs-edit-tabulate")
(load "init-emacs-edit-multiple-cursors")
(load "init-emacs-edit-indirect")
(load "init-emacs-edit-sudo")
(load "init-emacs-edit-server")
;; (load "init-emacs-edit-collaborate")

(provide 'init-emacs-edit)

;;; init-emacs-edit.el ends here
