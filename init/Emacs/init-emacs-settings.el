;;; init-emacs-settings.el --- init my Emacs settings

;;; Commentary:

;;; Code:

;; [ Customize ] -- tools for customizing Emacs and Lisp packages.

(use-package cus-edit
  :custom (custom-file (expand-file-name "custom.el" user-emacs-directory))
  :init
  ;; auto create custom file
  (if (file-exists-p custom-file)
      ;; auto load custom file
      (load custom-file :noerror)
    (write-region nil nil custom-file))
  ;; NOTE: This hook will prompt for saving useless customize variables like eshell, tramp connections etc.
  ;; :config (add-hook 'kill-emacs-query-functions 'custom-prompt-customize-unsaved-options)
  )

;;; [ yes-or-no ]

(setq use-short-answers t) ; use 'y' as yes, 'n' as no.
(setq confirm-kill-emacs 'yes-or-no-p)

;; [ Bell ]

(setq visible-bell nil)
;;; disable Emacs built-in bell when [C-g]
(setq ring-bell-function 'ignore)

;;; [ *Messages* log buffer ]

(setq message-log-max 5000)

;;; [ Warning Messages ]

;; disable the popping up of this undo discard warning info buffer.
(require 'warnings) ; fix variable `warning-suppress-types' is void.
(add-to-list 'warning-suppress-types 'undo)
(add-to-list 'warning-suppress-types 'discard-info)


(provide 'init-emacs-settings)

;;; init-emacs-settings.el ends here
