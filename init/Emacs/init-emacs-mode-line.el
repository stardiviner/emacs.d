;;; init-emacs-mode-line.el --- init modeline for Emacs
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ modify Emacs default mode-line ]

(defun mode-line-align (left right)
  "Return a string with LEFT and RIGHT at the edges of the
current window."
  (format (format "%%s %%%ds" (- (window-total-width) (length left) 2))
          left right))

;; (setq-default mode-line-format
;;               '(:eval
;;                 (mode-line-align
;;                  (format-mode-line
;;                   (list "%e" mode-line-front-space
;;                         " " mode-line-client mode-line-remote mode-line-mule-info mode-line-modified
;;                         " " mode-line-buffer-identification
;;                         " " mode-line-position
;;                         " " '(vc-mode vc-mode)
;;                         ;; " " mode-line-process
;;                         ))
;;                  (format-mode-line
;;                   (list " " mode-line-misc-info
;;                         " " mode-line-modes
;;                         ;; " " mode-name minor-mode-alist
;;                         " " mode-line-end-spaces)))))

;;; [ benchmarking mode-line rendering ]

;; (+measure-time
;;  (format-mode-line mode-line-format))
;;; => "0.000341s"

;;; [ hide-mode-line ] -- hides (or masks) the current buffer's mode-line.

(use-package hide-mode-line
  :ensure t
  :hook ((completion-list-mode . hide-mode-line-mode)
         (neotree-mode . hide-mode-line-mode)))

;;; [ doom-modeline ] -- A minimal and modern mode-line.

;; (use-package doom-modeline
;;   :ensure t
;;   :custom (;; (doom-modeline-buffer-file-name-style 'buffer-name)
;;            ;; (doom-modeline-icon nil) ; don't use icon will be faster
;;            ;; (doom-modeline-hud t) ; display approximately position percent in buffer.
;;            ;; (doom-modeline-enable-word-count nil) ; disable word count to avoid slow performance.
;;            ;; (doom-modeline-github t) ; display GitHub notifications
;;            ;; (doom-modeline-mu4e t)       ; display mu4e notifications.
;;            ;; (doom-modeline-irc t)
;;            ;; (doom-modeline-irc-buffers nil)
;;            ;; Fix the laggy issue, by don't compact font caches during GC.
;;            ;; (inhibit-compacting-font-caches t)
;;            )
;;   :hook (after-init . doom-modeline-mode)
;;   ;; :config
;;   ;; (setq doom-modeline-irc-stylize #'doom-modeline-irc-stylize-circe-buffer)
;;   ;; (defun doom-modeline-irc-stylize-circe-buffer (buffer-name) ; used in `doom-modeline--tracking-buffers'.
;;   ;;   (let* ((buffer (get-buffer buffer-name))
;;   ;;          (mode (with-current-buffer buffer major-mode)))
;;   ;;     (if (eq mode 'circe-channel-mode) ; "#emacs@Libera Chat"
;;   ;;         (when (string-match "\\(#[^@]*\\)@\\(.*\\)" buffer-name)
;;   ;;           (match-string 1 buffer-name)))))
;;   )


(provide 'init-emacs-mode-line)

;;; init-emacs-mode-line.el ends here
