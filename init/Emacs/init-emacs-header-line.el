;;; init-emacs-header-line.el --- init file for Emacs header line -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Christopher M. Miles, all rights reserved.

;;; Commentary:



;;; Code:

;;; [ breadcrumb ] -- Emacs header-line indication of where you are in a large project.

(use-package breadcrumb
  :ensure t
  :commands (breadcrumb-mode breadcrumb-local-mode)
  ;; WARNING: `breadcrumb--header-line' will invoke `project-current' which slow down Emacs performance.
  :hook ((markdown-mode . breadcrumb-local-mode)
         ;; (prog-mode . breadcrumb-local-mode) ; PERFORMANCE: disable in `prog-mode' for smoother input.
         ))




(provide 'init-emacs-header-line)

;;; init-emacs-header-line.el ends here
