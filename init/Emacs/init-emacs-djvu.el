;;; init-emacs-djvu.el --- init file for DjVu -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Christopher M. Miles, all rights reserved.

;;; Commentary:



;;; Code:

;;; [ djvu ] -- Edit and view Djvu files via djvused.

;;; This package is a front end for the command-line program djvused from DjVuLibre.

(use-package djvu
  :ensure t)



(provide 'init-emacs-djvu)

;;; init-emacs-djvu.el ends here
