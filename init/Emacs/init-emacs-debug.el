;;; init-emacs-debug.el --- init for Emacs debug

;;; Commentary:


;;; Code:

;;; [ debug ] -- Emacs built-in debugger.

(add-to-list 'display-buffer-alist '("^\\*Warnings\\*" . (display-buffer-below-selected)))
(add-to-list 'display-buffer-alist '("^\\*Backtrace\\*" . (display-buffer-below-selected)))

;;; [ Edebug ] -- Edebug is a source level debugger.

(use-package edebug
  ;; show full edebug value result instead of truncated.
  :custom ((edebug-print-length 500)
           (edebug-print-level  500))
  ;; :bind (:map emacs-lisp-mode-map ("C-c d e" . edebug-mode))
  :init (add-to-list 'display-buffer-alist '("^\\*edebug-trace\\*" . (display-buffer-below-selected))))

;;; [ edebug-x ] -- Extensions for Edebug.

(use-package edebug-x
  :ensure t
  :defer t
  :commands (edebug-x-mode)
  ;; :custom (edebug-x-stop-point-overlay t)
  :init (edebug-x-mode)
  (add-to-list 'display-buffer-alist '("^\\*Instrumented Functions\\*" . (display-buffer-below-selected)))
  (add-to-list 'display-buffer-alist '("^\\*Edebug Breakpoints\\*" . (display-buffer-below-selected))))

;;; [ edebug-inline-result ] -- Show Edebug result inline.

;; (use-package edebug-inline-result
;;   :ensure t
;;   :defer t
;;   :custom (edebug-inline-result-backend 'posframe)
;;   :hook (edebug-mode . edebug-inline-result-mode))

;;; [ eros ] -- Evaluation Result OverlayS for Emacs Lisp
;;; edebug show inline result with eros.

;;; NOTE: Disable this config because it's slow in edebug result displaying.
;; (use-package eros
;;   :ensure t
;;   :after edebug
;;   :config
;;   ;; remove string "Result:"
;;   (defun adviced:edebug-compute-previous-result (_ &rest r)
;;     "Adviced `edebug-compute-previous-result'."
;;     (let ((previous-value (nth 0 r)))
;;       (if edebug-unwrap-results
;;           (setq previous-value (edebug-unwrap* previous-value)))
;;       (setq edebug-previous-result (edebug-safe-prin1-to-string previous-value))))
;;   (advice-add #'edebug-compute-previous-result :around #'adviced:edebug-compute-previous-result)
;;   ;; display edebug previous result with eros inline overlay.
;;   (defun adviced:edebug-previous-result (_ &rest r)
;;     "Adviced `edebug-previous-result'."
;;     (eros--make-result-overlay edebug-previous-result
;;       :where (point)
;;       :duration eros-eval-result-duration))
;;   (advice-add #'edebug-previous-result :around #'adviced:edebug-previous-result)
;;   ;; setting eros overlay face.
;;   (defun eros-edebug-inline-result-face-reset (&optional theme)
;;     "Reset `eros-result-overlay-face' when theme loading."
;;     (set-face-attribute 'eros-result-overlay-face nil
;;                         :background (cl-case (frame-parameter nil 'background-mode)
;;                                       (light "yellow")
;;                                       (dark (color-darken-name (face-background 'default) 10)))
;;                         :foreground (cl-case (frame-parameter nil 'background-mode)
;;                                       (light "black")
;;                                       (dark "cyan"))))
;;   (add-hook 'load-theme-after-hook #'eros-edebug-inline-result-face-reset))

;;; [ bug-hunter ] -- Hunt down errors in elisp files.

(use-package bug-hunter
  :ensure t
  :defer t
  :commands (bug-hunter-file bug-hunter-init-file))

;;; [ inspector ] -- Inspector tool for Emacs Lisp objects. Similar to inspectors available for Smalltalk and Common Lisp, but for Emacs Lisp.

(use-package inspector
  :ensure t
  :ensure tree-inspector
  :ensure treeview
  :defer t
  :commands (inspector-inspect
             inspector-inspect-expression inspector-inspect-last-sexp
             inspector-inspect-defun inspector-inspect-region
             inspector-inspect-edebug-expression
             inspector-inspect-debugger-return-value
             inspector-pprint-inspected-object
             inspector-inspect-stack-frame inspector-inspect-in-stack-frame)
  :init (add-to-list 'display-buffer-alist '("^\\*inspector\\*" . (display-buffer-below-selected))))

;;; print and insert Emacs version info
(defun emacs-version-commit-insert ()
  "Insert version of Emacs and 7 characters of the commit hash."
  (interactive)
  (insert
   (format "GNU Emacs %s (commit %s)"
	       emacs-version
           (when emacs-repository-version
             (substring emacs-repository-version 0 7)))))


(provide 'init-emacs-debug)

;;; init-emacs-debug.el ends here
