;;; init-emacs-frame.el --- init Emacs frame
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:


;; (setq default-frame-alist '((user-size . t) (width . 80) (height . 25)))

;;=================================== undo or restore killed frame ==================================

(require 'frameset)
(require 'cl-lib)

(defvar my/killed-frame-ring-size 10)
(defvar my/killed-frames (make-ring my/killed-frame-ring-size))

(advice-add 'delete-frame :before #'my/remember-deleted-frame)

(defun my/remember-deleted-frame (&optional frame _force)
  (ring-insert my/killed-frames
               (frameset-save (list (or frame (selected-frame))))))

(defun my/undo-killed-frame (&optional n)
  (interactive "p")
  (let ((frames-before (frame-list)))
    (frameset-restore (ring-ref my/killed-frames (- (or n 1) 1)))
    (let ((restored (cl-set-difference (frame-list) frames-before)))
      (when (and restored (not (cdr restored)))
        (select-frame-set-input-focus (car restored))))))

(global-set-key (kbd "C-x 5 u") #'my/undo-killed-frame)

;;===================================================================================================
(global-set-key [remap toggle-frame-maximized] 'toggle-frame-fullscreen)

(put 'suspend-frame 'disabled  t)
(global-unset-key (kbd "C-z"))
;; (global-unset-key (kbd "C-x C-z"))

;;; frame commands from ctl-x-5-map
(use-package transient
  :ensure t
  :config
  (defun move-frame-right ()
    "Move the current frame right 200 pixels"
    (interactive)
    (let* ((p (frame-position))
           (x (car p))
           (y (cdr p)))
      (set-frame-position (selected-frame) (+ x 200) y)))

  (defun move-frame-left ()
    "Move the current frame left 200 pixels"
    (interactive)
    (let* ((p (frame-position))
           (x (car p))
           (y (cdr p)))
      (set-frame-position (selected-frame) (- x 200) y)))

  (defun select-previous-frame (&optional arg)
    "Select the -ARG'th visible frame on current display, and raise it.

All frames are arranged in a cyclic order.
This command selects the frame ARG steps previously in that order.
It is the reverse of `other-frame'."
    (interactive "p")
    (other-frame (* -1 arg)))

  (defun kill-buffer-and-frame ()
    "Kill the current buffer and the current frame."
    (interactive)
    ;; do this trick in case of dedicated window in special frame
    ;; in that case kill-buffer will delete-frame too
    ;; can't check after kill buffer since selected-frame will have changed
    (if (window-dedicated-p (selected-window))
        (kill-buffer (current-buffer))
      (kill-buffer (current-buffer))
      (delete-frame)))

  (transient-define-prefix my/frame-transient ()
    "Frame commands mirroring ctl-x-5-map"
    ["Configure Frames"
     ["Manage"
      ("2" "New" make-frame-command)
      ("0" "Delete" delete-frame)
      ;; FIXME
      ;; ("k" "Kill Frame & Buffer" kill-buffer-and-frame)
      ("1" "Delete others" delete-other-frames)
      ]
     ["Select"
      ("o" "Other" other-frame)
      ("n" "Next" other-frame)
      ;; ("p" "Previous" select-previous-frame)
      ]
     ["Display"
      ("-" "Fixed Width" variable-pitch-mode)
      ("l" "Lower" lower-frame)
      ("=" "Maximize" toggle-frame-maximized)
      ("i" "Iconify" iconify-frame)
      ]
     ["Move"
      ;; ("<" "Left" move-frame-left :transient t)
      ;; (">" "Right" move-frame-right :transient t)
      ]
     ]
    ["Open in other Frame"
     ["Files"
      ("b" "Buffer" switch-to-buffer-other-frame)
      ("C-o" "Buffer other frame" display-buffer-other-frame)
      ("C-f" "File" find-file-other-frame)
      ("f" "File" find-file-other-frame)
      ("r" "File Read-Only" find-file-read-only-other-frame)
      ]
     ["Apps"
      ("d" "Dired" dired-other-frame)
      ("." "Xref" xref-find-definitions-other-frame)
      ("m" "Compose Mail" compose-mail-other-frame)
      ]
     ["Help For"
      ("V" "Variable" find-variable-other-frame)
      ("F" "Function" find-function-other-frame)
      ("K" "Key" find-function-on-key-other-frame)
      ("L" "Library" find-library-other-frame)
      ]
     ]
    )

  (global-set-key (kbd "C-z") 'my/frame-transient))

;;; [ moom ] -- Commands to control frame position and size for multiple monitors.

;; (use-package moom
;;   ;; :ensure t
;;   :vc (:url "https://github.com/takaxp/moom" :branch "transient")
;;   :commands (moom-transient-dispatch moom-transient-config)
;;   :bind ("C-c z" . moom-transient-dispatch)
;;   :config
;;   (moom-mode 1)
;;   (require 'moom-transient)
;;   (with-eval-after-load "moom" ;; 'all
;;     (moom-recommended-keybindings '(move fit expand fill font reset undo))))


(provide 'init-emacs-frame)

;;; init-emacs-frame.el ends here
