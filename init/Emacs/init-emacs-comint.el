;;; init-emacs-comint.el --- init for Emacs comint
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ Comint ]

(setq comint-prompt-read-only t
      comint-eol-on-send t ; go to the end of the line before sending input.
      comint-move-point-for-output t)

;;; translate ANSI color sequences into text properties.
(autoload 'ansi-color-for-comint-mode-on "ansi-color" nil t)
(add-hook 'comint-mode-hook 'ansi-color-for-comint-mode-on)

;;; strip ANSI color escape sequences.
;; (autoload 'ansi-color-for-comint-mode-filter "ansi-color" nil t)
;; (add-hook 'comint-mode-hook 'ansi-color-for-comint-mode-filter)

(add-hook 'comint-output-filter-functions 'comint-strip-ctrl-m)

;; (add-hook 'comint-output-filter-functions 'comint-truncate-buffer)

;;; [ comint-mime ] -- Display graphics and other MIME attachments in Emacs `comint-mode'.

(use-package comint-mime
  :if (eq system-type 'gnu/linux)
  :ensure t
  :commands (comint-mime-setup)
  :init
  (add-hook 'shell-mode-hook 'comint-mime-setup)
  (add-hook 'inferior-python-mode-hook 'comint-mime-setup)
  ;; Note that for Python it is important to use the IPython interpreter.
  ;; (when (executable-find "ipython3")
  ;;   (setq python-shell-interpreter "ipython3"
  ;;         python-shell-interpreter-args "--simple-prompt --classic"))
  )


(provide 'init-emacs-comint)

;;; init-emacs-comint.el ends here
