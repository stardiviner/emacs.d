;;; init-emacs-outline.el --- init Emacs for outline.
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;; (unless (boundp 'outline-prefix)
;;   (define-prefix-command 'outline-prefix))
;; (global-set-key (kbd "C-C SPC") 'outline-prefix)


;;; [ outline ] -- Emacs built-in outline mode. [C-c @] prefix of `outline-minor-mode'.

;; (use-package outline
;;   :defer t
;;   :delight outline-minor-mode
;;   :hook (prog-mode . outline-minor-mode))

;;; [ allout ] -- [C-c @] prefix

(use-package allout
  :defer t
  :delight (allout-minor-mode allout-mode)
  :commands (allout-mode)
  :custom ((allout-auto-activation t)
           (allout-command-prefix (kbd "C-c @"))
           (allout-default-layout '(-2 : -1 *)) ; [buffer-local] allout-layout '(0 : -1 -1 0)
           (allout-use-mode-specific-leader nil)
           (allout-stylish-prefixes t)
           (allout-primary-bullet "*")  ; used by level-1
           (allout-header-prefix ".")
           (allout-distinctive-bullets-string "*+-=>()[{}&!?#%\"X@$~_\\:;^")
           (allout-plain-bullets-string "*+#>.") ; + -> #N -> > -> *
           (allout-plain-bullets-string-len 5))
  :init (defun my/allout-mode-enable () (unless (eq major-mode 'emacs-lisp-mode) (allout-mode 1)))
  :hook (prog-mode . my/allout-mode-enable))

;; (use-package allout-widgets
;;   :defer t
;;   :custom ((allout-widgets-auto-activation t))
;;   :hook (allout-mode . allout-widgets-mode))

;;; [ page-break-lines ] -- Display ^L page breaks as tidy horizontal lines.

(use-package page-break-lines
  :ensure t
  :hook (emacs-lisp-mode . page-break-lines-mode))



(provide 'init-emacs-outline)

;;; init-emacs-outline.el ends here
