;;; init-tramp.el --- init for tramp
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ Tramp ]

(use-package tramp
  :defer t
  ;; :preface (setq tramp-verbose 10) ; for verbose debug TRAMP
  :custom ((tramp-default-method "ssh")
           ;; speed-up tramp.
           (tramp-completion-reread-directory-timeout nil)
           (tramp-show-ad-hoc-proxies t))
  :config
  ;; use the settings in ~/.ssh/config instead of Tramp's
  ;; (setq tramp-use-ssh-controlmaster-options nil)
  
  ;; change SHELL environment variable to solve Tramp hangs issue.
  ;; (eval-after-load 'tramp '(setenv "SHELL" "/bin/bash"))
  ;; fix /bin/zsh no such file or directory issue on remote host.
  ;; (setq explicit-shell-file-name "/bin/bash")

  ;; Changing the default remote or local shell
  ;; [[info:tramp#Remote shell setup][tramp#Remote shell setup]]
  (add-to-list 'tramp-connection-properties
               (list (regexp-quote "/ssh::")
                     "remote-shell" "/bin/bash"))
  (add-to-list 'tramp-connection-properties
               (list (regexp-quote "/ssh:Mac-mini.local:")
                     "remote-shell" "/bin/bash"))
  (add-to-list 'tramp-connection-properties
               (list (regexp-quote "/ssh:stardiviner@localhost:")
                     "remote-shell" "/bin/bash"))
  (add-to-list 'tramp-connection-properties
               (list (regexp-quote "/ssh:raspberry_pi-wireless:")
                     "remote-shell" "/bin/bash"))
  (add-to-list 'tramp-connection-properties
               (list (regexp-quote "/ssh:pi@192.168.31.36:")
                     "remote-shell" "/bin/bash"))
  (add-to-list 'tramp-connection-properties
               (list (regexp-quote "/ssh:root@192.168.31.36:")
                     "remote-shell" "/bin/bash"))
  (add-to-list 'tramp-connection-properties
               (list (regexp-quote "/ssh:qnap_nas_stardiviner:")
                     "remote-shell" "/bin/bash"))

  (add-to-list 'tramp-remote-path "~/bin") ; search extra PATH on remote host.

  ;; don't generate backups for remote files opened as root (security hazzard)
  (setq backup-enable-predicate
        (lambda (name)
          (and (normal-backup-enable-predicate name)
               (not (let ((method (file-remote-p name 'method)))
                      (when (stringp method)
                        (member method '("su" "sudo"))))))))
  
  ;; `/sudoedit:' method
  (unless (version<= emacs-version "27")
    (require 'tramp-sudoedit)))

;;; [ tramp-auto-auth ] -- TRAMP automatic authentication library for ‘authsource’.

(use-package tramp-auto-auth
  :ensure t
  :after tramp
  :init (tramp-auto-auth-mode 1)
  :config
  (add-to-list 'tramp-auto-auth-alist '("root@localhost" . (:host "localhost" :user "root" :port "ssh")))
  (add-to-list 'tramp-auto-auth-alist '("root@dark" . (:host "dark" :user "root" :port "ssh")))
  (add-to-list 'tramp-auto-auth-alist '("pi@raspberry_pi-wireless" . (:host "raspberry_pi-wireless" :user "pi" :port "ssh")))
  (add-to-list 'tramp-auto-auth-alist '("root@raspberry_pi-wireless" . (:host "raspberry_pi-wireless" :user "root" :port "ssh")))
  (add-to-list 'tramp-auto-auth-alist '("pi@raspberry_pi-wire" . (:host "raspberry_pi-wire" :user "pi" :port "ssh")))
  (add-to-list 'tramp-auto-auth-alist '("root@raspberry_pi-wire" . (:host "raspberry_pi-wire" :user "root" :port "ssh")))
  (add-to-list 'tramp-auto-auth-alist '("pi@192.168.31.36" . (:host "192.168.31.36" :user "pi" :port "ssh")))
  (add-to-list 'tramp-auto-auth-alist '("pi@192.168.31.37" . (:host "192.168.31.37" :user "pi" :port "ssh"))))

;;; [ counsel-tramp ] -- Tramp with Ivy/counsel interface.

(use-package counsel-tramp
  :ensure t
  :defer t
  :commands (counsel-tramp)
  :init (defalias 'exit-tramp 'tramp-cleanup-all-buffers))

;;; [ tramp-container ] -- Tramp integration for Docker-like containers: Docker, Podman, Kubernetes.

(require 'tramp-container)



(provide 'init-tramp)

;;; init-tramp.el ends here
