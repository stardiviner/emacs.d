;;; init-emacs-theme.el --- Init for Themes
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ X Resources ]

(setq inhibit-x-resources t)

;;; [ font-core.el: font-lock ]

(global-font-lock-mode t)

(autoload 'color-lighten-name "color.el")
(autoload 'color-darken-name "color.el")

;;; [ custom theme ]

(let ((themes-dir (concat user-emacs-directory "themes")))
  (unless (file-exists-p themes-dir)
    (make-directory themes-dir t))
  (setq custom-theme-directory (concat user-emacs-directory "themes")))

(add-to-list 'custom-theme-load-path custom-theme-directory)


;;; `load-theme' hooks
(defcustom load-theme-before-hook nil
  "Functions to run before load theme."
  :type 'hook)

(defcustom load-theme-after-hook nil
  "Functions to run after load theme."
  :type 'hook)

(defun load-theme-hook-wrapper (origin-func theme &rest args)
  "A wrapper of hooks around `load-theme'."
  (mapc #'disable-theme custom-enabled-themes)
  ;; before load-theme-hook
  (run-hook-with-args 'load-theme-before-hook theme)
  ;; apply origin function
  (apply origin-func theme args)
  ;; after load-theme hook
  (run-hook-with-args 'load-theme-after-hook theme))

(advice-add 'load-theme :around #'load-theme-hook-wrapper)

;;; reset theme helper
(defun my/custom-theme-reset ()
  "Reset theme using current enabled custom-theme."
  (if-let ((current-theme (or (car custom-enabled-themes)
                              (when (custom-theme-p (car custom-known-themes))
                                (car custom-known-themes)))))
      (load-theme current-theme t)
    (message "[WARNING] Currently not enabled any themes!")))

;;; [ leuven-theme ] -- Awesome Emacs color theme on white background.

(use-package leuven-theme
  ;; :ensure t
  :load-path "~/Code/Emacs/leuven-theme"
  :preface (add-to-list 'custom-theme-load-path "~/Code/Emacs/leuven-theme")
  :pin manual
  :demand t
  :config (when (not (or (featurep 'circadian)
                         (bound-and-true-p ns-system-appearance-change-functions)))
            (load-theme 'leuven t)))

;;; [ one-themes ] -- A port of the Vim/Atom One colorscheme to Emacs.

;; (use-package one-themes
;;   :ensure t
;;   :config (when (not (featurep 'circadian)) (load-theme 'one-dark)))

;;; [ doom-themes ] -- an opinionated pack of modern color-themes.

(use-package doom-themes
  :load-path "~/Code/Emacs/doom-themes"
  :preface (add-to-list 'custom-theme-load-path "~/Code/Emacs/doom-themes/themes")
  :pin manual
  :custom (doom-palenight-padded-modeline 1) ; like CSS padding
  :config (when (not (or (featurep 'circadian)
                         (bound-and-true-p ns-system-appearance-change-functions)))
            (load-theme 'doom-palenight t)))

;;; [ tsuki-theme ] -- Tsuki Theme for Emacs

;; (use-package tsuki-theme
;;   :vc (:url "git@github.com:vekatze/tsuki-theme.git" :rev :newest)
;;   :init (load-theme 'tsuki t))

;;; [ chocolate-theme ] -- A dark chocolate theme.

;; (use-package chocolate-theme
;;   :ensure t
;;   :config (when (not (or (featurep 'circadian)
;;                          (bound-and-true-p ns-system-appearance-change-functions)))
;;             (load-theme 'chocolate t)))

;;; Auto change theme based on macOS appearance change hook.
(defun macOS-appearance-change (appearance)
  "Load theme, taking current system APPEARANCE into consideration."
  (mapc #'disable-theme custom-enabled-themes)
  (pcase appearance
    ('light (load-theme 'leuven t))
    ('dark (load-theme 'doom-palenight t))))

(when (boundp 'ns-system-appearance-change-functions)
  (add-hook 'ns-system-appearance-change-functions #'macOS-appearance-change))

;;; [ nano-theme ] -- GNU Emacs / N Λ N O Theme

;; (use-package nano-theme
;;   :ensure t
;;   :commands (nano-light nano-dark nano-mode)
;;   ;; :init (nano-mode)
;;   )

;;; [ circadian ] -- Theme-switching based on daytime.

(use-package circadian
  ;; don't enable on macOS system appearance change hook.
  :unless (bound-and-true-p ns-system-appearance-change-functions)
  :ensure t
  :custom ((calendar-location-name "Shaoxing Town")
           (calendar-time-zone +480)
           (calendar-latitude 29.72)
           (calendar-longitude 120.20)
           (circadian-themes '((:sunrise . leuven)
                               (:sunset . doom-palenight))))
  :hook (after-init . circadian-setup))

;;; Use large font for function name only in programming modes.
;;
;; (defun my-enlarge-function-name ()
;;   "Use large font for function name only in programming modes."
;;   (face-remap-add-relative 'font-lock-function-name-face
;;                            :overline t :height 120))
;;
;; (add-hook 'prog-mode-hook #'my-enlarge-function-name)


(provide 'init-emacs-theme)

;;; init-emacs-theme.el ends here
