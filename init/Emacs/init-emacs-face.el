;;; init-emacs-face.el --- init for Emacs faces.

;;; Commentary:

;;; Code:

;;; [ variable-pitch ] -- support for displaying proportional fonts.

(use-package faces
  :custom (face-font-family-alternatives
           '(("Fira Code" "DejaVu Sans Mono" "Hack" "Consolas" "Monaco" "Monospace")))
  :custom-face
  (variable-pitch ((t (:family "FiraCode Nerd Font"))))
  (fixed-pitch ((t (:family "DejaVu Sans Mono"))))
  (fixed-pitch-serif ((t (:family "DejaVu Serif"))))
  (default ((t (:family "FiraCode Nerd Font")))))

;;; [ face-remap ] -- Functions for managing `face-remapping-alist'.

(use-package face-remap
  :delight buffer-face-mode
  :commands (buffer-face-mode
             buffer-face-set buffer-face-toggle
             variable-pitch-mode))



(provide 'init-emacs-face)

;;; init-emacs-face.el ends here
