;;; init-emacs-shell.el --- init Shell in Emacs.
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ Shell ] -- [M-x shell]

(use-package shell
  :preface
  ;; [M-x shell] is a nice shell interface to use, let's make it colorful.
  (autoload 'ansi-color-for-comint-mode-on "ansi-color" nil t)
  (add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)
  ;; If you need a terminal emulator rather than just a shell, consider [M-x term]
  ;; instead.
  :defer t
  :mode (("\\.\\(xinitrc\\|xsessionrc\\|xsession\\|xprofile\\)\\'" . sh-mode))
  :custom (shell-file-name (executable-find "bash"))
  :commands (shell)
  :hook (shell-mode . corfu-mode)
  :init 
  (add-to-list 'display-buffer-alist '("^\\*shell\\*$" . (display-buffer-below-selected)))
  (add-to-list 'display-buffer-alist '("^\\*Shell Command Output\\*$" . (display-buffer-no-window)))
  (add-to-list 'display-buffer-alist '("^\\*Async Shell Command\\*" . (display-buffer-no-window)))
  :config
  ;; fix ignore case caused slow performance on shell commands completion by company-mode.
  ;; (add-hook 'sh-mode-hook #'(lambda () (setq-local completion-ignore-case nil)))
  
  ;; for auto nifty command substitution [!!] and ^a^b.
  (define-key shell-mode-map (kbd "SPC") 'comint-magic-space)
  
  ;; a helper function to run sudo Shell command.
  (defun sudo-shell-command (command)
    "The suggested way to run sudo Shell `COMMAND' with TRAMP's sudo method."
    (interactive "MAsync sudo command (root): ")
    (with-temp-buffer
      (cd "/sudo::/")
      (async-shell-command command))))

;;; [ Eshell ]

(load "init-eshell")

;;; [ sh-scripts ] -- Major mode for editing shell scripts.

(use-package sh-scripts
  :commands (sh-mode)
  :config
  (add-hook 'sh-mode-hook #'flyspell-mode-off))

;;; [ native-complete ] -- Completion in shell buffers using native mechanisms.

;; (use-package native-complete
;;   :ensure t
;;   :ensure company-native-complete
;;   :init (with-eval-after-load 'shell
;;           (native-complete-setup-bash)))

;;; [ shell-command-x ] -- Extensions for Emacs' shell commands.

(use-package shell-command-x
  :ensure t
  :init (shell-command-x-mode 1))

;;; [ dwim-shell-command ] -- Shell commands with DWIM behaviour.

(use-package dwim-shell-command
  :ensure t
  :init (add-to-list 'display-buffer-alist '("^.*\\*DWIM shell command\\* .*" . (display-buffer-below-selected)))
  (require 'dwim-shell-commands) ; provides `dwim-shell-commands-*' prefix commands.
  :commands (dwim-shell-command)
  ;; :bind (([remap shell-command] . dwim-shell-command)
  ;;        :map dired-mode-map
  ;;        ([remap dired-do-async-shell-command] . dwim-shell-command)
  ;;        ([remap dired-do-shell-command] . dwim-shell-command)
  ;;        ([remap dired-smart-shell-command] . dwim-shell-command))
  :config
  ;; use `dwim-shell-command-on-marked-files' macro to define Emacs commands.
  )

;;; [ run-command ] -- Efficient and ergonomic external command invocation for Emacs.

(use-package run-command
  :ensure t
  :commands (run-command)
  :init
  (use-package run-command-recipes
    :ensure t
    :config (run-command-recipes-use-all)))



(provide 'init-emacs-shell)

;;; init-emacs-shell.el ends here
