;;; init-emacs-idle.el --- init for when Emacs Idle
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ type-break ] -- encourage rests from typing at appropriate intervals.

;;; [M-x type-break]

;;; [ zone ] -- idle display hacks.

;; (use-package zone
;;   :config (zone-when-idle (* 60 5)))
;;
;; (require 'zone-words nil t)
;; (setq zone-programs (vconcat zone-programs '(zone-words)))


(provide 'init-emacs-idle)

;;; init-emacs-idle.el ends here
