;;; init-emacs-search-engine.el --- init for Search Engines
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ WebJump ] -- Emacs built-in programmable Web hotlist.

(use-package webjump
  :defer t
  :commands (webjump)
  :bind ("C-x /" . webjump)
  :config
  (setq webjump-sites
        '(;; Search Engines
          ("Magi (search engine)" . [simple-query "magi.com" "https://magi.com/search?q=" ""])
          ("Google (search engine)" . [simple-query "google.com" "https://www.google.com.tw/search?safe=off&q=" ""])
          ("DuckDuckGo (search engine)" . [simple-query "duckduckgo.com" "https://duckduckgo.com/?q=" ""])
          ("DogeDoge (search engine)" . [simple-query "dogedoge.com" "https://www.dogedoge.com/results?q=" ""])
          ("Blekko (search engine)" . [simple-query "blekko.com" "https://blekko.com/#?q=" ""])
          ("Bing (search engine)" . [simple-query "bing.com" "http://cn.bing.com/search?q=" ""])
          ("Brave (search engine)" . [simple-query "search.brave.com" "https://search.brave.com/search?q=" "&source=web"])
          ("Yahoo (search engine)" . [simple-query "www.yahoo.com" "search.yahoo.com/search?p=" ""])
          ("Baidu 百度 (search engine)" . [simple-query "baidu.com" "http://www.baidu.com/s?wd=" ""])
          ("neeva (search engine)" . [simple-query "neeva.com" "https://neeva.com/search?q=" ""])
          ("yep (search engine)" . [simple-query "yep.com" "https://yep.com/web?q=" ""])
          ("kagi (search engine)" . [simple-query "kagi.com" "https://kagi.com/search?q=" ""])
          ("You.com (search engine, AI, GPT)" . [simple-query "you.com" "https://you.com/search?q=" "&fromSearchBar=true&tbm=youchat&chatMode=default"])
          ("微信搜一搜 (wechat,search engine)" . [simple-query "weixin.sogou.com" "https://weixin.sogou.com/weixin?query=" "&type=2"])
          ;; wiki
          ("Wikipedia Search" . [simple-query "wikipedia.org" "https://en.wikipedia.org/wiki/" ""])
          ("维基百科 Wikipedia (zh-CN) Search" .
           [simple-query "wikipedia.org" "http://www.wikipedia.org/search-redirect.php?search=" "&language=zh&go=Go"])
          ("百度百科 Baidu Baike Search" . [simple-query "baike.baidu.com" "http://baike.baidu.com/search/none?word=" ""])
          ;; Q&A
          ("Stack Exchange Search" . [simple-query "stackexchange.com" "http://stackexchange.com/search?q=" ""])
          ("Stack Overflow Search" . [simple-query "stackoverflow.com" "https://stackoverflow.com/search?q=" ""])
          ("Quora Search" . [simple-query "quora.com" "https://www.quora.com/search?q=" ""])
          ("知乎 Zhihu Search" . [simple-query "zhihu.com" "http://www.zhihu.com/search?q=" ""])
          ;; Tools
          ("Wolfram Alpha Eval Input (Mathematica)" . [simple-query "wolframalpha.com" "http://www.wolframalpha.com/input/?i=" ""])
          ;; Translate
          ("Google Translate" . [simple-query "translate.google.com" "https://translate.google.com/?q=" "&sl=auto&tl=zh-CN&op=translate"])
          ("Google Translate URL" . [simple-query "translate.google.com" "https://translate.google.com/website?u=" "&sl=auto&tl=zh-CN"])
          ;; ("Google Translate URL" .
          ;;  [simple-query "translate.google.com" "https://translate.google.com.hk/translate?u=" "&hl=&sl=auto&tl=zh-CN&sandbox=1"])
          ("DeepL Translate" . [simple-query "deepl.com" "https://www.deepl.com/en/translator#auto/zh/" ""])
          ("百度翻译 Baidu (document,translate)" . [simple-query "fanyi.baidu.com" "https://fanyi.baidu.com/#en/zh/" ""])
          ("DocTranslator (document,translate)" .
           [simple-query "onlinedoctranslator.com" "https://www.onlinedoctranslator.com/zh-CN/%E5%B0%86%E8%8B%B1%E8%AF%AD%E7%BF%BB%E8%AF%91%E6%88%90%E4%B8%AD%E6%96%87(%E7%AE%80%E4%BD%93)_en_zh-CN" ""])
          ;; Languages
          ;; LaTeX
          ("TeX/LaTeX CTAN Search" . [simple-query "ctan.org" "https://www.ctan.org/search/?phrase=" ""])
          ;; Programming Docs: API
          ("APIs Search (api,search)" . [simple-query "apis.io" "http://apis.io/?search=" ""])
          ("DevDocs (api,search)"  . "https://devdocs.io/")
          ("Rosetta Code Search" . [simple-query "rosettacode.org" "https://www.rosettacode.org/mw/index.php?&search=" ""])
          ("Mozilla Developer (MDN Web Docs)" . [simple-query "developer.mozilla.org" "https://developer.mozilla.org/en-US/search?q=" ""])
          ("Mozilla Developer (MDN Plus AI Help)" . "https://developer.mozilla.org/en-US/plus/ai-help")
          ("RFC Search (rfc.fyi)" . [simple-query "rfc.fyi" "https://rfc.fyi/?search=" ""])
          ("RFC Search (ietf.org)" . [simple-query "ietf.org" "https://datatracker.ietf.org/doc/search/?name=" "&activedrafts=on&rfcs=on"])
          ;; FSF, not including Emacs-specific.
          ;; GNU FTP Mirror List from https://www.gnu.org/order/ftp.html
          ("GNU Project FTP Archive" . [mirrors "https://ftp.gnu.org/pub/gnu/" "https://ftpmirror.gnu.org"])
          ("GNU Project Home Page" . "www.gnu.org")
          ;; Emacs
          ("Emacs Home Page" . "www.gnu.org/software/emacs/emacs.html")
          ("Emacs Docs Search" . [simple-query "emacsdocs.org" "https://www.emacsdocs.org/search?q=" ""])
          ("Savannah Emacs page" . "savannah.gnu.org/projects/emacs")
          ("Emacs Wiki Search" . [simple-query "emacswiki.org" "www.emacswiki.org/emacs?search=" ""])
          ("Stack Exchange - Emacs" . [simple-query "emacs.stackexchange.com" "http://emacs.stackexchange.com/search?q=" ""])
          ("Emacs China Forum" . [simple-query "emacs-china.org" "https://emacs-china.org/search?q=" ""])
          ("Reddit Emacs [r/emacs]" . "https://www.reddit.com/r/emacs/")
          ("emacs-help mailing list" .
           [simple-query "lists.gnu.org"
                         "https://lists.gnu.org/archive/cgi-bin/namazu.cgi?submit=Search&idxname=help-gnu-emacs&query="
                         "&max=20&result=normal&sort=score"])
          ("emacs-devel mailing list" .
           [simple-query "lists.gnu.org"
                         "https://lists.gnu.org/archive/cgi-bin/namazu.cgi?submit=Search&idxname=emacs-devel&query="
                         "&max=20&result=normal&sort=score"])
          ;; Org Mode
          ("Org Mode (homepage)" . "https://orgmode.org/")
          ("Org Mode Community (Worg)" . "https://orgmode.org/worg/")
          ("Reddit Org-mode [r/orgmode]" . "https://www.reddit.com/r/orgmode/")
          ("Emacs Org mode mailing list (official)" . [simple-query "list.orgmode.org" "https://list.orgmode.org/?q=" ""])
          ("Emacs Org mode mailing list (GNU)" .
           [simple-query "lists.gnu.org"
                         "https://lists.gnu.org/archive/cgi-bin/namazu.cgi?submit=Search&idxname=emacs-orgmode&query="
                         "&max=20&result=normal&sort=score"])
          ;; LISP
          ("Lisp Doc (search)" . [simple-query "lispdoc.com" "http://lispdoc.com/?q=" ""])
          ;; Clojure
          ("clojure.org homepage" . "https://www.clojure.org/")
          ("cljdoc.org: CLJ/CLJS libraries docs (search)" . [simple-query "cljdoc.org" "https://cljdoc.org/search?q=require" ""])
          ("ClojureDocs (clojure,doc,search)" . [simple-query "clojuredocs.org" "http://clojuredocs.org/search?q=" ""])
          ("clojure-doc.org Clojure Documentation Guides" . "https://clojure-doc.org/")
          ("clojurians Slack Archive" . [simple-query "clojurians-log-v2.oxal.org" "https://clojurians-log-v2.oxal.org/search?q=" ""])
          ("clojars: search Clojure packages/libraries" . [simple-query "clojars.org" "https://clojars.org/search?q=" ""])
          ("Clojure Toolbox: search Clojure packages/libraries" . "https://www.clojure-toolbox.com/")
          ("ClojureWerkz: A growing collection of open source Clojure libraries" . "http://clojurewerkz.org/")
          ("GitHub Clojure Trending" . "https://github.com/trending/clojure?since=daily&spoken_language_code=")
          ("libs.garden: libraries and apps rating of Clojure" . "https://libs.garden/clojure")
          ("ask.clojure.org: Ask Clojure Q&A" . "https://ask.clojure.org/")
          ("clojureverse.org: Clojureverse" . "https://clojureverse.org/")
          ("Reddit Clojure [r/clojure]" . "https://www.reddit.com/r/Clojure/")
          ("Stack Overflow [clojure] tags" . "https://stackoverflow.com/questions/tagged/clojure")
          ("Clojure Slack Archives (search)" . [simple-query "clojurians-log-v2.oxal.org" "https://clojurians-log-v2.oxal.org/search?q=" ""])
          ("Clojurians Zulip web-public streams" . "https://clojurians.zulipchat.com/")
          ("Clojurians Zulip web-public streams (search)" . [simple-query "clojurians.zulipchat.com" "https://clojurians.zulipchat.com/#narrow/search/" ""])
          ("Clojure Planet" . "http://planet.clojure.in/")
          ("The REPL: A Clojure Podcast" . "https://www.therepl.net/")
          ("Clojure China forum" . "https://clojure-china.org/")
          ;; ClojureScript
          ;; Racket
          ("Racket Language Search Manuals" . [simple-query "docs.racket-lang.org" "https://docs.racket-lang.org/search/index.html?q=" ""])
          ("Racket Language Packages Search" . [simple-query "pkgd.racket-lang.org" "https://pkgd.racket-lang.org/pkgn/search?q=" ""])
          ;; Java
          ("Java Docs Search" . [simple-query "docs.oracle.com" "https://docs.oracle.com/apps/search/search.jsp?category=java&q=" ""])
          ("Java Maven Search" . [simple-query "search.maven.org" "https://search.maven.org/search?q=" ""])
          ("Reddit Java [r/java]" . "https://www.reddit.com/r/java/")
          ("Reddit Learn Java [r/learnjava]" . "https://www.reddit.com/r/learnjava/")
          ("Reddit Java help [r/javahelp]" . "https://www.reddit.com/r/javahelp/")
          ;; Library Documentation Hosting for Common Lisp
          ("Common Lisp Quickdocs Search" . [simple-query "quickdocs.org" "http://quickdocs.org/search?q=" ""])
          ;; .NET (C#, F#)
          (".NET API Docs Search" . [simple-query "docs.microsoft.com" "https://docs.microsoft.com/en-us/dotnet/api/?term=" ""])
          ("NuGet search for C# packages" . "https://nuget.info/packages")
          ;; Ruby
          ("Ruby Docs Search" . [simple-query "ruby-doc.com" "http://ruby-doc.com/search.html?q=" ""])
          ("RubyGems.org: Ruby Packages" . [simple-query "rubygems.org" "https://rubygems.org/search?query=" ""])
          ;; Python
          ("Python 3 Docs Search" . [simple-query "docs.python.org" "http://docs.python.org/3/search.html?q=" ""])
          ("Python 3 Docs Search (Chinese,中文)" . [simple-query "docs.python.org" "https://docs.python.org/zh-cn/3/search.html?q=" ""])
          ("PyPi (python,pip,package,search)" . [simple-query "pypi.org" "https://pypi.org/search/?q=" ""])
          ("Python Code Examples (search)" . [simple-query "programcreek.com" "https://www.programcreek.com/python/?submit=Search&ClassName=" ""])
          ;; JavaScript
          ("npm: JavaScript package search" . [simple-query "npmjs.com" "https://www.npmjs.com/search?q=" ""])
          ;; Perl
          ("meta::cpan (Perl,package,CPAN,search engine)" . [simple-query "search.cpan.org" "http://search.cpan.org/search?query=" "&mode=all"])
          ;; PHP
          ("PHP Doc Search" . [simple-query "cn2.php.net" "http://cn2.php.net/results.php?q=" ""])
          ;; R
          ("RDocumentation (R,document,search)" . [simple-query "rdocumentation.org" "https://www.rdocumentation.org/search?q=" ""])
          ("CRAN Package Archive (R,package,search)" . [simple-query "search.r-package.org" "https://cran.r-project.org/package=" ""])
          ("Rseek.org (R,search)" . [simple-query "rseek.org" "http://rseek.org/" ""])
          ;; Julia
          ("JuliaHub (julia,package,search)" . [simple-query "juliahub.com" "https://juliahub.com/ui/Search?q=" "&type=packages"])
          ("Julia Packages Search" . [simple-query "juliapackages.com" "https://juliapackages.com/packages?search=" ""])
          ;; Go
          ("Go Packages Search" . [simple-query "pkg.go.dev" "https://pkg.go.dev/search?q=" ""])
          ;; Rust
          ("crates.io (rust,packages,search)" . [simple-query "crates.io" "https://crates.io/search?q=" ""])
          ;; Swift
          ("Swift Forums (swift,forum)" . [simple-query "forums.swift.org" "https://forums.swift.org/search?q=" ""])
          ("Swift Package Index (swift,package,search)" . [simple-query "swiftpackageindex.com" "https://swiftpackageindex.com/search?query=" ""])
          ;; Lua
          ("LuaRocks (lua,packages,search)" . [simple-query "luarocks.org" "https://luarocks.org/search?q=" ""])
          ;; MATLAB
          ("MATLAB - MathWorks Help Center Search (docs,search)" . [simple-query "mathworks.cn" "https://ww2.mathworks.cn/support/search.html?q=" ""])
          ("MATLAB Answers - MathWorks (ask,question,docs,search)" . [simple-query "mathworks.cn" "https://ww2.mathworks.cn/matlabcentral/answers/?search_origin=ans_lp&term=" ""])
          ;; Programmer Search
          ("开发者搜索 ^Beta (programmer,search)"  . [simple-query "kaifa.baidu.com" "https://kaifa.baidu.com/searchPage?wd=" "&module=SEARCH"])
          ;; Search Code
          ("Grepper (code,search)" . [simple-query "grepper.com" "https://www.grepper.com/search.php?q=" ""])
          ("GitHub Repo Search" . [simple-query "github.com" "https://github.com/search?ref=simplesearch&q=" "&type=repositories"])
          ("GitHub Code Search" . [simple-query "github.com" "https://github.com/search?ref=simplesearch&q=" "&type=code"])
          ("GitHub Code Search - Clojure" . [simple-query "github.com" "https://github.com/search?ref=simplesearch&q=" "+language%3AClojure+&type=code"])
          ("GitHub Code Search - ClojureScript" . [simple-query "github.com" "https://github.com/search?q=" "+path%3A*.cljs&type=code"])
          ("GitHub Code Search - Emacs Lisp" . [simple-query "github.com" "https://github.com/search?ref=simplesearch&q=" "+language%3A%22Emacs+Lisp%22+&type=code"])
          ("GitHub Code Search - Python" . [simple-query "github.com" "https://github.com/search?ref=simplesearch&q=" "+language%3APython+&type=code"])
          ("GitHub Code Search - JavaScript" . [simple-query "github.com" "https://github.com/search?ref=simplesearch&q=" "+language%3AJavaScript+&type=code"])
          ("GitHub Code Search - Java" . [simple-query "github.com" "https://github.com/search?ref=simplesearch&q=" "+language%3AJava+&type=code"])
          ("searchcode.com: (code,search)" . [simple-query "searchcode.com" "http://searchcode.com/?q=" ""])
          ("Dig.Codes (code,search)" . [simple-query "dig.codes" "https://dig.codes/s?q=" ""])
          ("hello (code,search)" . [simple-query "sayhello.so" "https://beta.sayhello.so/search?q=" ""])
          ("phind (AI,code,search)" . [simple-query "phind.com" "https://phind.com/search?q=" ""])
          ;; Linux
          ("commandlinefu.com: (linux,command,search)" . [simple-query "commandlinefu.com" "https://www.commandlinefu.com/commands/matching/" "/cG9zdA==/sort-by-votes"])
          ("die.net: (linux,man pages)" . [simple-query "www.die.net" "https://www.die.net/search/?q=" ""])
          ("command-not-found: (linux,package,search)" . [simple-query "command-not-found.com" "https://command-not-found.com/search?q=" ""])
          ("Arch Linux Wiki" .
           [simple-query "wiki.archlinux.org" "https://wiki.archlinux.org/index.php/Special:Search&fulltext=Search?search=" ""])
          ("Liquid Web" . [simple-query "liquidweb.com" "https://www.liquidweb.com/kb/search/?q=" ""])
          ;; Linux Packages
          ("pkgs.org: (linux,package,search)" . [simple-query "pkgs.org" "https://pkgs.org/search/?q=" ""])
          ("Ubuntu Linux Packages Search" . [simple-query "packages.ubuntu.com" "https://packages.ubuntu.com/search?keywords=" ""])
          ("Debian Linux Packages Search" . [simple-query "packages.debian.org" "https://packages.debian.org/search?keywords=" ""])
          ("Arch Linux Packages Search" . [simple-query "archlinux.org" "https://archlinux.org/packages/?q=" ""])
          ("Flathub (Flatpak,linux,package,search)" . [simple-query "flathub.org" "https://flathub.org/apps/search/" ""])
          ;; Apple macOS
          ("Apple Support Search" .
           [simple-query "support.apple.com" "https://support.apple.com/kb/index?q=" "&locale=en_US&type=organic&page=search"])
          ("Apple Support Community" . [simple-query "discussions.apple.com" "https://discussions.apple.com/search?q=" ""])
          ("Ask Different: Answers for your Apple Questions" .
           [simple-query "apple.stackexchange.com" "https://apple.stackexchange.com/search?q=" ""])
          ;; Windows Packages
          ("Chocolatey (windows,package,search)" . [simple-query "chocolatey.org" "https://community.chocolatey.org/packages?q=" ""])
          ;; Software & Application Packages
          ("Repology: the packaging hub - projects (search)" . [simple-query "repology.org" "https://repology.org/projects/?search=" ""])
          ("vcpkg.info: (packages,search)" . [simple-query "vcpkg.info" "https://vcpkg.info/" ""])
          ("SourceForge: software packages (search)" . [simple-query "sourceforge.net" "https://sourceforge.net/directory/?q=" ""])
          ;; Extensions & Plugins
          ("Visual Studio Code Extensions Market (search)" .
           [simple-query "marketplace.visualstudio.com"
                         "https://marketplace.visualstudio.com/search?term=" "&target=VSCode&category=All%20categories&sortBy=Relevance"])
          ("PowerShell Gallery (powershell,packages)" . [simple-query "powershellgallery.com" "https://www.powershellgallery.com/packages?q=" ""])
          ;; Android Apps
          ("APKMirror (Android,App,search,download)" . [simple-query "apkmirror.com" "https://www.apkmirror.com/?s=" "&post_type=app_release&searchtype=apk"])
          ;; UserScript
          ("Userscript.Zone (userscript,search)" . [simple-query "userscript.zone" "https://www.userscript.zone/search?q=" "&source=index"])
          ("Greasy Fork (userscript,search)" . [simple-query "greasyfork.org" "https://greasyfork.org/en/scripts?q=" ""])
          ("OpenUserJS (userscript,search)" . [simple-query "openuserjs.org" "https://openuserjs.org/?q=" ""])
          ;; Chrome Web Store
          ("Chrome Web Store (extensions)" . [simple-query "chrome.google.com" "https://chrome.google.com/webstore/search/" ""])
          ;; RESTful HTTP API tools
          ("Postman (API)" . "https://www.postman.com/")
          ("Hoppscotch (restful,http,api)" . "https://hoppscotch.io/")
          ;; Docker
          ("Docker Hub (search)" . [simple-query "hub.docker.com" "https://hub.docker.com/search?q=" "&type=image"])
          ;; Vagrant
          ("Vagrant Cloud Boxes (search)" . [simple-query "app.vagrantup.com" "https://app.vagrantup.com/boxes/search?q=" "&sort=downloads"])
          ;; Mailing Lists
          ("Google Groups (search)" . [simple-query "groups.google.com" "groups.google.com/groups?q=" ""])
          ;; Scholar
          ("libgen.li (paper,cite,search)" . [simple-query "libgen.li" "http://libgen.li/index.php?req=" ""])
          ;; Web Browser Extensions
          ("Firefox Addons Extensions (search)" . [simple-query "addons.mozilla.org" "https://addons.mozilla.org/en-US/firefox/search/?q=" ""])
          ;; Downloads
          ("The Pirate Bay 海盗湾 (torrent,download)" . [simple-query "thepiratebay.org" "https://thepiratebay.org/search/" ""])
          ;; Web Page Archive
          ("dotEPUB (web-page,save,download)" . [simple-query "" "http://dotepub.com/converter/?url=" "&fmt=epub&imm=1&lang=en"])
          ;; ebooks & documents
          ("Z-Library - zlib (ebook,search,download)" . [simple-query "hk1lib.org" "https://hk1lib.org/s/" ""])
          ("Z-Library - Tor version  (ebook,search,download)" . [simple-query "1lib.org" "https://1lib.org/s/" ""])
          ("Z-Library - book4you.org (ebook,search,download)" . [simple-query "book4you.org" "https://book4you.org/s/" ""])
          ("Library Genesis (libgen,ebook,search)" . [simple-query "libgen.is" "https://libgen.is/search.php?req=" ""])
          ("鸠摩 文档搜索引擎 jiumodiary book" . "https://www.jiumodiary.com/")
          ("蓝菊花 lanjuhua" . "http://www.lanjuhua.com/")
          ("深度开源・文库 (search)" . [simple-query "open-open.com" "https://www.open-open.com/wenku/?kw=" ""])
          ;; Forums
          ("Douban 豆瓣 (search)" . [simple-query "douban.com" "https://www.douban.com/search?source=suggest&q=" ""])
          ;; Videos
          ("YouTube (video,search)" . [simple-query "youtube.com" "http://www.youtube.com/results?aq=f&oq=&search_query=" ""])
          ("Sepia [PeerTube] (video,search)" . [simple-query "sepiasearch.org" "https://sepiasearch.org/search?search=" ""])
          ("Bilibili 哔哩哔哩 (video,search)" . [simple-query "bilibili.com" "https://search.bilibili.com/all?keyword=" ""])
          ("Bilibili 哔哩哔哩视频解析下载 (bilibili,video,downloader)" . "https://bilibili.iiilab.com/")
          ("抖音 (douyin,search)" . [simple-query "douyin.com" "https://www.douyin.com/search/" "?source=normal_search"])
          ("Douban books 豆瓣图书 (book)" . [simple-query "book.douban.com" "http://book.douban.com/subject_search?search_text=" ""])
          ("Douban movies 豆瓣电影 (video,movie)" . [simple-query "movie.douban.com" "http://movie.douban.com/subject_search?search_text=" ""])
          ("IMDb (video,database)" . [simple-query "imdb.com" "https://www.imdb.com/find/?q=" ""])
          ;; Subtitle
          ("zimuku 字幕库 (subtitle,download)" . [simple-query "zimuku.la" "http://www.zimuku.la/search?q=" ""])
          ;; Musics
          ("网易云音乐 NetEase 163 Music (search)" . [simple-query "music.163.com" "http://music.163.com/#/search/m/?s=" ""])
          ("千千音乐 (search)" . [simple-query "music.taihe.com" "https://music.taihe.com/search?word=" ""])
          ;; Social Network
          ("Twitter" . [simple-query "twitter.com" "https://twitter.com/search?q=" ""])
          ("Facebook" . [simple-query "facebook.com" "https://www.facebook.com/search/top/?q=" ""])
          ("微博 weibo" . [simple-query "weibo.com" "https://s.weibo.com/weibo?q=" ""])
          ;; Law
          ("国家法律法规数据库 (china,law,database)" . "https://flk.npc.gov.cn/")
          ("法信 faxin.cn" . [simple-query "faxin.cn" "http://www.faxin.cn/alllibsearch/SearchResult.aspx?usersearchtype=1&keyword=" ""])
          ;; Papers
          ("arXiv e-Print archives" . [simple-query "arxiv.org" "https://arxiv.org/search/?query=" ""])
          ;; Shopping
          ("淘宝 taobao.com" . [simple-query "taobao.com" "https://s.taobao.com/search?q=" ""])
          ("淘宝规则 rule.taobao.com" . [simple-query "rule.taobao.com" "https://rule.taobao.com/search.htm?key=" ""])
          ("千牛卖家工作台" . "https://loginmyseller.taobao.com/")
          ("身份证号查询 IDcard" . [simple-query "qq.ip138.com" "https://qq.ip138.com/idsearch/index.asp?userid=" "&action=idcard"])
          ;; Accounting
          ("随手记" . "https://www.sui.com/report_index.do")
          ;; Porn
          ("PornHub Search (porn)" . [simple-query "pornhub.com" "https://www.pornhub.com/video/search?search=" ""])
          ;; Hentai
          ("Pixiv Encyclopedia (hentai,encyclopedia)" . [simple-query "dic.pixiv.net" "https://dic.pixiv.net/en/search?query=" ""])
          ("Hanime1.me (H,porn)" . [simple-query "hanime.me" "https://hanime1.me/search?query=" "&genre=&sort=&year=&month=&duration="])
          ))

  ;; Setting initial input for command `webjump'.
  (defvar consult-webjump--history nil)

  (defun consult--lookup-candidate (selected candidates &rest _)
    "Lookup SELECTED in CANDIDATES list and return property `consult--candidate'."
    (consult--lookup-prop 'consult--candidate selected candidates))

  (defun consult-webjump--lookup (selected candidates &rest _)
    "Lookup SELECTED in CANDIDATES alist, return webjump item."
    (let* ((candidate (assoc selected candidates))
           (webjump-site-name (car candidate))
           (webjump-site-vector (cdr candidate)))
      candidate))

  (defun consult-webjump ()
    "Open a webjump bookmark or search engine as wrapper of command `webjump'."
    (interactive)
    ;; reference command `webjump' code.
    (let* ((item (consult--read
                  webjump-sites
                  ;; :annotate
                  :prompt "webjump: "
                  :category 'consult-webjump
                  :sort nil
                  :require-match t
                  :initial ".?"
                  :default "Google (search engine)"
                  :inherit-input-method t
                  :history '(:input consult-org--history)
                  :lookup #'consult-webjump--lookup    ; return the `webjump-site-vector' from `webjump-sites'.
                  :preview-key nil
                  ;; :narrow (consult-org--narrow)
                  ;; :state (consult--webjump-state)
                  ;; :group 'bookmark
                  ))
	       (name (car item))
	       (expr (cdr item)))
      (browse-url (webjump-url-fix
		           (cond ((not expr) "")
		                 ((stringp expr) expr)
		                 ((vectorp expr) (webjump-builtin expr name))
		                 ((listp expr) (eval expr t))
		                 ((symbolp expr)
			              (if (fboundp expr)
			                  (funcall expr name)
			                (error "WebJump URL function \"%s\" undefined"
				                   expr)))
		                 (t (error "WebJump URL expression for \"%s\" invalid"
				                   name)))))))  

  (global-set-key [remap webjump] 'consult-webjump)
  )


(provide 'init-emacs-search-engine)

;;; init-emacs-search-engine.el ends here
