;;; init-emacs-tab.el --- init for Emacs tab-bar -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Christopher M. Miles, all rights reserved.

;;; Commentary:



;;; Code:

;;; [ tab-bar ] -- frame-local tabs with named persistent window configurations.

(use-package tab-bar
  :commands (tab-bar-mode tab-bar-select-tab-by-name)
  :custom ((tab-bar-select-tab-modifiers '(control meta)) ; Press [C-M-<N>] to select tab.
           (tab-bar-show t)
           (tab-bar-auto-width nil)
           (tab-bar-tab-hints t)
           (tab-bar-new-button nil)
           (tab-bar-close-button-show nil)
           (tab-bar-separator " | ")
           (tab-bar-new-tab-choice 'scratch-buffer))
  :config
  (defun my/tab-bar-tab-name-format-function (tab i)
    "My customized function of `tab-bar-tab-name-format-function'."
    (let ((current-p (eq (car tab) 'current-tab))
          (tab-name (alist-get 'name tab))
          (tab-num (number-to-string i))
          (tab-num-symbol
           (alist-get i '((0 . "⓪") (1 . "①") (2 . "②") (3 . "③") (4 . "④")
                          (5 . "⑤") (6 . "⑥") (7 . "⑦") (8 . "⑧") (9 . "⑨"))))
          ;; (face (funcall tab-bar-tab-face-function tab))
          )
      (concat (if current-p (concat (nerd-icons-icon-for-buffer) " "))
              ;; (propertize (tab-bar-tab-name-format-default tab i))
              (propertize (when tab-bar-tab-hints tab-num-symbol)
                          'face (if current-p 'tab-bar-tab 'tab-bar-tab-inactive))
              (propertize " "
                          'face (if current-p 'tab-bar-tab 'tab-bar-tab-inactive))
              (propertize tab-name
                          'face (if current-p 'tab-bar-tab 'tab-bar-tab-inactive)))))
  (setq tab-bar-tab-name-format-function 'my/tab-bar-tab-name-format-function))

;;; [ tab-line ] -- window-local tabs with window buffers.

;; (use-package tab-line
;;   :commands (global-tab-line-mode tab-line-mode))

;;; [ tabspaces ] -- Leverage tab-bar and project for buffer-isolated workspaces.

;; (use-package tabspaces
;;   :ensure t
;;   :custom ((tabspaces-use-filtered-buffers-as-default t))
;;   :hook (after-init . tabspaces-mode))



(provide 'init-emacs-tab)

;;; init-emacs-tab.el ends here
