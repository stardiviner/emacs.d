;;; init-emacs-minibuffer.el --- init Emacs minibuffer

;;; Commentary:

;;; Code:

;;; [ minibuffer ]

(setq y-or-n-p-use-read-key t)

;; (setq-default max-mini-window-height 6)

;; recursive minibuffers
(setq enable-recursive-minibuffers t)   ; enable to use minibuffer recursively.
(if (booleanp enable-recursive-minibuffers)
    (minibuffer-depth-indicate-mode t))

;; minibuffer prompt face properties
;; Do not allow the cursor in the minibuffer prompt.
(setq minibuffer-prompt-properties
      '(read-only t cursor-intangible t face minibuffer-prompt))
(add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

(minibuffer-electric-default-mode t)

(setq minibuffer-completion-confirm nil
      minibuffer-auto-raise t
      minibuffer-allow-text-properties t
      ;; minibuffer-frame-alist
      ;; minibuffer-history-position t
      )

;;; For `completing-read-multiple'
;; - "[ \t]*,[ \t]*" -> "," (default)
;; - "[ \t]*;[ \t]*" -> ";"
;; - "[ \t]*:[ \t]*" -> ":"
;; (setq crm-separator "[ \t]*,[ \t]*")

(setq read-buffer-completion-ignore-case t
      read-file-name-completion-ignore-case t)

;;; ( [M-:] `eval-expression' )

;; (setq eval-expression-debug-on-error t
;;       eval-expression-print-level nil ; 4, nil,
;;       eval-expression-print-length nil)

(defun my/eval-expression-lisp-setup ()
  "Setup `eval-expression' for Lisp editing."
  (when (memq this-command '(eval-expression
                             eval-last-sexp
                             pp-eval-expression
                             eval-expression-with-eldoc
                             eldoc-eval-expression
                             ibuffer-do-eval
                             ibuffer-do-view-and-eval))
    (with-eval-after-load 'rainbow-delimiters
      (rainbow-delimiters-mode-enable))
    (cond
     ((fboundp 'paredit-mode) (paredit-mode 1)
      ;; FIXME: `paredit-RET' Cause minibuffer key [RET] is newline instead of `eval-expression'.
      (define-key read--expression-map [remap paredit-RET] 'read--expression-try-read))
     ((fboundp 'smartparens-strict-mode) (smartparens-strict-mode 1)))))

(add-hook 'eval-expression-minibuffer-setup-hook #'my/eval-expression-lisp-setup)
;; (add-hook 'minibuffer-setup-hook 'my/eval-expression-lisp-setup)

;;; [ savehist ] -- save minibuffer history.

(use-package savehist
  :ensure t
  :defer t
  :custom (savehist-autosave-interval (* 60 100))
  :hook (after-init . savehist-mode))

;;; [ vertico ] -- VERTical Interactive COmpletion

(use-package vertico
  :ensure t
  :custom ((vertico-count 7)
           (vertico-cycle t))
  :hook (after-init . vertico-mode)
  :bind (:map vertico-map
              ("C-h"   . minibuffer-completion-help)
              ("<tab>" . vertico-insert)              ; insert the selected candidate into text are.
              ("C-TAB" . minibuffer-complete)
              ("C-j"   . minibuffer-force-complete-and-exit)
              ("C-g"   . abort-minibuffers)
              ;; ("<escape>" . minibuffer-keyboard-quit) ; close minibuffer.
              ;; ("C-}" . vertico-next-group)
              ;; ("C-{" . vertico-previous-group)
              )

  :config
  
  ;; vertico extensions: control how vertico displayed.
  (use-package vertico-multiform
    :custom ((vertico-multiform-categories '((file) ; `find-file'
                                             (library reverse indexed) ; `load-library'
                                             (imenu (:not indexed mouse)) ; `imenu'
                                             (symbol (vertico-sort-function . vertico-sort-alpha)) ; [C-h f]
                                             ;; (t reverse)
                                             ))
             (vertico-multiform-commands '((embark-act (:not posframe)))))
    :bind (:map vertico-map
                ("C-'" . vertico-quick-jump) ; like `ace-jump'
                ("C-i" . vertico-quick-insert) ; quick insert candidate in minibuffer input.
                ("C-o" . vertico-quick-exit) ; quick select candidate and exit minibuffer.
                ("M-G" . vertico-multiform-grid)
                ("M-F" . vertico-multiform-flat)
                ("M-R" . vertico-multiform-reverse)
                ("M-U" . vertico-multiform-unobtrusive))
    :hook (vertico-mode . vertico-multiform-mode))

  (use-package vertico-directory        ; Commands for Ido-like directory navigation
    :demand t
    :bind (:map vertico-map
                ("RET" . vertico-directory-enter)
                ("DEL" . vertico-directory-delete-char)
                ("<backspace>" . vertico-directory-delete-char)
                ("M-DEL" . vertico-directory-delete-word)
                ("C-<backspace>" . vertico-directory-delete-word))
    ;; Tidy shadowed file names: When using a command for selecting a file in the minibuffer, the
    ;; following fixes the path so the path you select doesn’t have prepended junk left behind.
    :hook (rfn-eshadow-update-overlay . vertico-directory-tidy))

  (use-package vertico-repeat
    ;; Make sure vertico state is saved for `vertico-repeat'.
    :hook (minibuffer-setup . vertico-repeat-save))
  
  ;; The `completion-at-point' command is usually bound to M-TAB or TAB. In case you want to use
  ;; Vertico for `completion-at-point'/`completion-in-region', you can use the function
  ;; `consult-completion-in-region' provided by the Consult package.
  ;; Use `consult-completion-in-region' if Vertico is enabled.
  ;; Otherwise use the default `completion--in-region' function.
  (setq completion-in-region-function
        (lambda (&rest args)
          (apply (if vertico-mode
                     #'consult-completion-in-region
                   #'completion--in-region)
                 args)))

  ;; `vertico-indexed-mode' (`indexed') allows you to select candidates with prefix arguments [M-<N> + RET].
  ;; This is designed to be a faster alternative to selecting a candidate with `vertico-next' and `vertico-previous'.
  
  ;; Prefix the current candidate with a symbol character to indicate current candidate.
  (defun my/vertico--format-candidate-prefix--advice (orig cand prefix suffix index _start)
    (setq cand (funcall orig cand prefix suffix index _start))
    (concat
     (if (characterp ?➡)
         (if (= vertico--index index)
             (propertize "➡ " 'face '(:foreground "VioletRed1"))
           "  ")
       (if (= vertico--index index)
           (propertize "-> " 'face '(:foreground "VioletRed1"))
         "   "))
     cand))
  (advice-add #'vertico--format-candidate :around #'my/vertico--format-candidate-prefix--advice))

(use-package vertico-prescient ; enable `prescient' in `vertico'
  :if (featurep 'prescient)
  :ensure t
  :hook (vertico-mode . vertico-prescient-mode))

;;; NOTE: The `vertico-posframe' -> `posframe' always suddenly caused Emacs crash.

;; (use-package vertico-posframe         ; Using posframe to show Vertico
;;   :ensure t
;;   :after vertico-multiform
;;   :custom ((vertico-posframe-poshandler #'posframe-poshandler-point-window-center)
;;            (vertico-posframe-parameters '((top . 0) (left . 0)
;;                                           (min-height . 1) (min-width . 1)
;;                                           (left-fringe . 0) (right-fringe . 0)
;;                                           (ns-transparent-titlebar . t)       ; transparent title bar
;;                                           (alpha-background . 85)             ; true alpha transparent
;;                                           (tool-bar-lines . 0)                ; no tool bar
;;                                           (menu-bar-lines . 0)                ; no menu bar
;;                                           (scroll-bar . nil)                  ; no scroll bar
;;                                           (vertical-scroll-bars . nil)        ; no vertical scroll bar
;;                                           (internal-border-width . 8)         ; internal border width
;;                                           (internal-border-color . "#68228b") ; internal border color
;;                                           (right-divider-width . 1)           ; window divider line width
;;                                           ))
;;            (vertico-posframe-min-height 13)
;;            (vertico-posframe-height 13)
;;            (vertico-posframe-border-width 2)
;;            (vertico-count 10))
;;   ;; :custom-face
;;   ;; (vertico-posframe-border   ((t (:background "SkyBlue2"))))
;;   ;; (vertico-posframe-border-2 ((t (:background "red"))))
;;   :init (vertico-posframe-mode 1)
;;   ;; delete default vertico-posframe frame to ensure posframe border color works.
;;   ;; (add-hook 'after-init-hook #'vertico-posframe-cleanup)
;;   :config
;;   ;; let `vertico-posframe' work well with `vertico-multiform'.
;;   (setq vertico-multiform-commands
;;         (append vertico-multiform-commands
;;                 `(,(if (featurep 'vertico-posframe)
;;                        '(execute-extended-command indexed posframe)
;;                      '(execute-extended-command indexed))
;;                   ,(if (featurep 'vertico-posframe)
;;                        '(consult-complex-command (indexed posframe))
;;                      '(consult-complex-command (indexed :not posframe))) ; [C-x M-:] conditionally disable posframe
;;                   ,(if (featurep 'vertico-posframe)
;;                        '(consult-imenu reverse indexed posframe)
;;                      '(consult-imenu reverse indexed))
;;                   ,(if (featurep 'vertico-posframe)
;;                        '(consult-grep indexed posframe)
;;                      '(consult-grep reverse))
;;                   ,(if (featurep 'vertico-posframe)
;;                        '(consult-line
;;                          posframe
;;                          (vertico-posframe-poshandler . posframe-poshandler-frame-top-center)
;;                          (vertico-posframe-border-width . 3)
;;                          ;; NOTE: This is useful when emacs is used in both in X and
;;                          ;; terminal, for posframe do not work well in terminal, so
;;                          ;; vertico-buffer-mode will be used as fallback at the
;;                          ;; moment.
;;                          (vertico-posframe-fallback-mode . vertico-buffer-mode))
;;                      '(consult-line indexed))
;;                   ,(if (featurep 'vertico-posframe)
;;                        '(consult-location reverse posframe)
;;                      '(consult-location reverse))
;;                   ,(if (featurep 'vertico-posframe)
;;                        '(consult-buffer indexed posframe)
;;                      '(consult-buffer indexed))
;;                   ,(if (featurep 'vertico-posframe)
;;                        '(consult-yank-pop indexed posframe (vertico-posframe-border-width . 2))
;;                      '(consult-yank-pop indexed reverse))
;;                   ;; NOTE: Disable [C-c C-j] `org-goto' which always caused Emacs crash.
;;                   ,(when (featurep 'vertico-posframe)
;;                      '(org-goto indexed posframe))
;;                   ,(when (featurep 'vertico-posframe)
;;                      '(consult-outline indexed posframe))
;;                   ,(when (featurep 'vertico-posframe)
;;                      '(org-set-property indexed posframe))
;;                   ,(if (featurep 'vertico-posframe)
;;                        '(org-set-tags-command
;;                          indexed posframe
;;                          (vertico-sort-function . vertico-sort-history-length-alpha))
;;                      '(org-set-tags-command indexed))
;;                   ,(if (featurep 'vertico-posframe)
;;                        '("flyspell-correct-*" grid reverse posframe)
;;                      '("flyspell-correct-*" grid reverse))
;;                   )))
;;   )

;;; [ consult ] -- Useful search and navigation commands.

(use-package consult
  :ensure t
  :custom ((consult-narrow-key "<")
           (consult-widen-key ">")
           (consult-locate-args (cl-case system-type
                                  (gnu/linux "locate --ignore-case --existing --regex")
                                  (darwin "mdfind"))))
  :bind (([remap switch-to-buffer] . consult-buffer)
         ([remap switch-to-buffer-other-window] . consult-buffer-other-window)
         ([remap switch-to-buffer-other-frame] . consult-buffer-other-frame)
         ([remap recentf-open-files] . consult-recent-files)
         ([remap repeat-complex-command] . consult-complex-command)
         ("C-x M-:" . consult-complex-command) ; original command `repeat-complex-command' keybinding is [C-x M-Esc].
         ;; ("C-c m" . consult-mode-command)
         ([remap yank-pop] . consult-yank-pop) ; `consult-yank-from-kill-ring'
         ([remap apropos] . consult-apropos)
         ("<help> a" . consult-apropos)
         ("C-c ." . imenu)
         ([remap imenu] . consult-imenu)
         ;; ("" . consult-imenu-multi)
         ;; mark-ring
         ([remap jump-to-mark] . consult-mark)
         ([remap pop-global-mark] . consult-global-mark)
         ([remap list-registers] . consult-register)
         ([remap insert-register] . consult-register-load)
         ([remap point-to-register] . consult-register-store)
         ([remap bookmark-jump] . consult-bookmark)
         ([remap kmacro-call-macro] . consult-kmacro)
         ([remap goto-line] . consult-goto-line)
         ([remap man] . consult-man)
         ([remap load-theme] . consult-theme)
         ([remap ripgrep] . consult-ripgrep) ; use [C-u] universal prefix to interactive select (current) dir.
         ([remap rg-project] . consult-ripgrep) ; use [C-u] universal prefix to interactive select (current) dir.
         ;; ([remap compile-goto-error] . consult-compile-error)
         ([remap flycheck-list-errors] . consult-flycheck)
         ;; M-s bindings (search-map)
         ("M-s f" . consult-find)
         ([remap locate] . consult-locate)
         ("M-s F" . consult-locate)
         ([remap grep] . consult-grep) ; [M-s g] ; use [C-u] universal prefix to interactive select (current) dir.
         ("M-s g" . consult-grep) ; use [C-u] universal prefix to interactive select (current) dir.
         ([remap vc-git-grep] . consult-git-grep)
         ("M-s G" . consult-git-grep)
         ("M-s r" . consult-ripgrep)
         ("M-s l" . consult-line)
         ("M-s L" . consult-line-multi)
         ("M-s k" . consult-keep-lines)
         ("M-s u" . consult-focus-lines)
         ;; Isearch integration
         ("M-s e" . consult-isearch-history)
         (:map isearch-mode-map
               ("M-e" . consult-isearch-history)   ;; orig. isearch-edit-string
               ("M-s e" . consult-isearch-history) ;; orig. isearch-edit-string
               ("M-s l" . consult-line)            ;; needed by consult-line to detect isearch
               ("M-s L" . consult-line-multi))     ;; needed by consult-line to detect isearch
         (:map outline-mode-map ("C-c C-j" . consult-outline)))
  :init
  (advice-add #'multi-occur :override #'consult-multi-occur)
  ;; Enable automatic preview at point in the *Completions* buffer.
  ;; This is relevant when you use the default completion UI,
  ;; and not necessary for vertico, selectrum, etc.
  ;; :hook (completion-list-mode . consult-preview-at-point-mode)
  :config
  ;; Adjust [C-x b] sources priority for bookmark over recent-file.
  (setq consult-buffer-sources
        '(consult--source-hidden-buffer
          consult--source-modified-buffer
          consult--source-buffer
          consult--source-bookmark
          consult--source-recent-file
          consult--source-file-register
          ;; WARNING: Those `project' sources invoke `project-find-functions' which slow down performance.
          ;; consult--source-project-buffer
          ;; consult--source-project-buffer-hidden
          ;; consult--source-project-recent-file-hidden
          ))

  ;; live previews manually
  ;; (consult-preview-key (kbd "M-."))
  (consult-customize
   consult-buffer consult-buffer-other-window consult-buffer-other-frame
   consult-bookmark consult-recent-file consult-xref
   consult-grep consult-git-grep consult-ripgrep
   consult--source-recent-file consult--source-project-recent-file consult--source-bookmark
   :preview-key '("M-." :debounce 0.5))
  ;; Preview immediately on M-., on up/down after 0.5s, on any other key after 1s
  (consult-customize consult-theme
                     :preview-key
                     '("M-."
                       :debounce 0.5 "<up>" "<down>"
                       :debounce 1 any))

  ;; Use Consult to select `xref' locations with preview.
  (setq xref-show-xrefs-function #'consult-xref
        xref-show-definitions-function #'consult-xref)

  ;; bind `list-registers' (`consult-register') command to the register prefix keybinding map [C-x r x].
  (with-eval-after-load 'init-emacs-register ; for `register-prefix' map.
    (define-key register-prefix (kbd "x") 'consult-register))

  ;; Optionally configure a function which returns the project root directory.
  ;; There are multiple reasonable alternatives to chose from.
  (cond
   ((fboundp 'project-current)
    (setq consult-project-root-function
          (lambda ()
            (when-let (project (project-current))
              (car (project-roots project))))))
   ((fboundp 'projectile-project-root)
    (setq-default consult-project-root-function 'projectile-project-root))
   ((fboundp 'vc-root-dir)
    (setq consult-project-root-function #'vc-root-dir))
   ((fboundp 'locate-dominating-file)
    (setq consult-project-root-function (lambda () (locate-dominating-file "." ".git")))))

  ;; set command `consult-buffer' with initial input "*" to match more buffers.
  (consult-customize consult-buffer :initial ".?")
  ;; set command `consult-ripgrep' with initial input.
  (consult-customize consult-ripgrep
                     :initial (consult--async-split-initial (thing-at-point 'symbol)))
  )

;;; [ consult-dir ] -- insert paths into minibuffer prompts in Emacs.

(use-package consult-dir
  :ensure t
  :custom ((consult-dir-shadow-filenames nil))
  :bind (("C-x C-d" . consult-dir)
         :map minibuffer-local-completion-map
         ("C-x C-d" . consult-dir)
         ("C-x C-j" . consult-dir-jump-file)
         :map vertico-map
         ("C-x C-d" . consult-dir)
         ("C-x C-j" . consult-dir-jump-file)))

;;; [ embark ] -- Emacs Mini-Buffer Actions Rooted in Keymaps.

(use-package embark
  :ensure t
  :ensure embark-consult
  :after (vertico consult)
  :commands (embark-act embark-dwim embark-collect embark-export embark-live)
  :bind (("C-." . embark-act)
         (:map minibuffer-local-map
               ("C-." . embark-act)
               ("C-;" . embark-dwim)
               ("C-c C-c" . embark-collect)
               ("C-c C-e" . embark-export)
               ("C-c C-l" . embark-live))
         ([remap describe-bindings] . embark-bindings)) ; [C-h b]
  :custom ((embark-mixed-indicator-delay 1.0)
           ;; optionally replace the key help `which-key' with a `completing-read' interface. usage: <keybinding_prefix> + [C-h/?]
           (prefix-help-command #'embark-prefix-help-command)
           (embark-help-key "C-h"))
  :init
  (add-to-list 'display-buffer-alist '("^\\*Embark Collect: .*\\*" . (display-buffer-below-selected)))
  (add-to-list 'display-buffer-alist '("^\\*Embark Export: .*\\*" . (display-buffer-below-selected)))
  (add-to-list 'display-buffer-alist '("^\\*Embark Export Grep\\*" . (display-buffer-below-selected)))
  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil (window-parameters (mode-line-format . none))))
  :config
  ;; [ `embark-collect' ]
  ;; [ embark-consult ] -- Consult integration for Embark.
  (require 'embark-consult)
  ;; Preview minor mode for *Completions* buffers.
  ;; When moving around in the *Completions* buffer, the candidate at point is automatically previewed.
  ;; (add-hook 'embark-collect-mode-hook #'consult-preview-at-point-mode)
  ;; If you don't want the minor mode automatically on and prefer to
  ;; trigger the consult previews manually use this instead:
  (keymap-set embark-collect-mode-map "C-j" #'consult-preview-at-point)

  ;; [ `embark-export' ]
  ;; embark export consult candidates as buffer content.
  (define-key minibuffer-local-map (kbd "C-c C-e") 'embark-export)
  ;; support `wgrep' in `embark-export' buffer.
  (add-hook 'embark-after-export-hook #'wgrep-change-to-wgrep-mode)
  (define-key embark-collect-mode-map (kbd "C-c C-p") 'wgrep-change-to-wgrep-mode)
  ;; support command `consult-ripgrep' and then `wgrep'.
  (defun embark-export-write ()
    "Export the current consult candidates to a writable buffer if possible.
Support exporting `consult-grep', `consult-ripgrep' to wgrep buffer, file to wdired, and `consult-location' to `occur-edit'."
    (interactive)
    (require 'embark)
    (require 'wgrep)
    (pcase-let ((`(,type . ,candidates)
                 (run-hook-with-args-until-success 'embark-candidate-collectors)))
      (pcase type
        ('consult-grep (let ((embark-after-export-hook #'wgrep-change-to-wgrep-mode))
                         (embark-export)))
        ('consult-ripgrep (let ((embark-after-export-hook #'wgrep-change-to-wgrep-mode))
                            (embark-export)))
        ('file (let ((embark-after-export-hook #'wdired-change-to-wdired-mode))
                 (embark-export)))
        ('consult-location (let ((embark-after-export-hook #'occur-edit-mode))
                             (embark-export)))
        (x (user-error "embark category %S doesn't support writable export." x)))))
  (define-key minibuffer-local-map (kbd "C-c C-e") 'embark-export-write)

  ;; [ embark-indicators ]
  
  ;; `embark-indicators' for `embark-act' [C-.].
  (cond
   ;; display `embark-indicators' with `posframe'.
   ((featurep 'posframe)
    (setq embark-verbose-indicator-display-action '(posframe-show)))
   
   ;; Use `which-key' as `embark-indicators' like a key menu prompt for `embark-act'.
   ((featurep 'which-key)
    (with-eval-after-load "which-key"
      (defun embark-which-key-indicator ()
        "An embark indicator that displays keymaps using which-key.
The which-key help message will show the type and value of the
current target followed by an ellipsis if there are further
targets."
        (lambda (&optional keymap targets prefix)
          (if (null keymap)
              (which-key--hide-popup-ignore-command)
            (which-key--show-keymap
             (if (eq (plist-get (car targets) :type) 'embark-become)
                 "Become"
               (format "Act on %s '%s'%s"
                       (plist-get (car targets) :type)
                       (embark--truncate-target (plist-get (car targets) :target))
                       (if (cdr targets) "…" "")))
             (if prefix
                 (pcase (lookup-key keymap prefix 'accept-default)
                   ((and (pred keymapp) km) km)
                   (_ (key-binding prefix 'accept-default)))
               keymap)
             nil nil t (lambda (binding)
                         (not (string-suffix-p "-argument" (cdr binding))))))))
      
      (add-to-list 'embark-indicators 'embark-which-key-indicator)
      
      (defun embark-hide-which-key-indicator (fn &rest args)
        "Hide the which-key indicator immediately when using the completing-read prompter."
        (which-key--hide-popup-ignore-command)
        (let ((embark-indicators
               (remq #'embark-which-key-indicator embark-indicators)))
          (apply fn args)))
      
      (advice-add #'embark-completing-read-prompter :around #'embark-hide-which-key-indicator)
      (setq embark-verbose-indicator-display-action nil)))
   
   ;; display `embark-indicators' at right side window.
   (t (setq embark-verbose-indicator-display-action '(display-buffer-in-side-window (side . right))))))

;;; [ marginalia ] -- Rich annotations in the minibuffer.

(use-package marginalia
  :ensure t
  ;; :custom (marginalia-align 'right)
  :hook (after-init . marginalia-mode)
  :config
  (add-to-list 'marginalia-command-categories '(flycheck-error-list-set-filter . builtin))
  (add-to-list 'marginalia-command-categories '(persp-switch-to-buffer . buffer))
  (add-to-list 'marginalia-command-categories '(projectile-find-file . project-file))
  (add-to-list 'marginalia-command-categories '(projectile-recentf . project-file))
  (add-to-list 'marginalia-command-categories '(projectile-switch-to-buffer . buffer))
  (add-to-list 'marginalia-command-categories '(projectile-switch-project . project-file)))

;;; [ Ivy ]

;; (load "init-ivy")

;;; [ Helm ]

(load "init-helm")


(provide 'init-emacs-minibuffer)

;;; init-emacs-minibuffer.el ends here
