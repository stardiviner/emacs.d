;;; init-emacs-highlight.el --- init for highlight
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:


(unless (boundp 'highlight-prefix)
  (define-prefix-command 'highlight-prefix))
(global-set-key (kbd "M-g h") 'highlight-prefix)

;;; [ hi-lock ]

;; unbind default keybindings
(with-eval-after-load 'hi-lock
  (unbind-key (kbd "C-x w .") hi-lock-map)
  (unbind-key (kbd "C-x w b") hi-lock-map)
  (unbind-key (kbd "C-x w h") hi-lock-map)
  (unbind-key (kbd "C-x w i") hi-lock-map)
  (unbind-key (kbd "C-x w l") hi-lock-map)
  (unbind-key (kbd "C-x w p") hi-lock-map)
  (unbind-key (kbd "C-x w r") hi-lock-map))

;; rebind commands
(define-key highlight-prefix (kbd "M-p") 'highlight-symbol-at-point)
(define-key highlight-prefix (kbd "M-r") 'highlight-regexp)
(define-key highlight-prefix (kbd "M-w") 'highlight-phrase)
(define-key highlight-prefix (kbd "M-u") 'unhighlight-regexp)
(define-key highlight-prefix (kbd "M-l") 'highlight-lines-matching-regexp)

(defface highlight-region-face
  '((t (:foreground "black" :background "VioletRed1")))
  "Face used for command `highlight-region'."
  :group 'highlight)

(defun highlight-region ()
  "Highlight region with background color."
  (interactive)
  (if (or (region-active-p) (use-region-p))
      (let* ((beg (region-beginning))
             (end (region-end))
             (overlays (overlays-in beg end))
             (highlighted-p (member 'highlight-region-face
                                    (mapcar
                                     (lambda (ov) (overlay-get ov 'face))
                                     overlays)))
             (hl-bg-colors '("cyan" "green" "yellow" "pink" "LightBlue" "orange" "gray")))
        (if (and overlays highlighted-p)
            (progn
              (mapcar
               (lambda (ov)
                 (when (overlay-get ov 'overlay-id)
                   (delete-overlay ov)))
               overlays)
              (message "Deleted the highlight-region overlay."))
          (setq ov (make-overlay beg end))
          (if (bound-and-true-p hl-bg-index)
              ;; loop colors instead of increase and out of colors list bounds.
              (if (>= hl-bg-index (length hl-bg-colors))
                  (setq hl-bg-index 0)
                (cl-incf hl-bg-index))
            (setq hl-bg-index 0))
          (set-face-attribute 'highlight-region-face nil :background (nth hl-bg-index hl-bg-colors))
          (overlay-put ov 'overlay-id ov)
          (overlay-put ov 'face 'highlight-region-face)
          (message "Highlighted region with overlay."))
        (deactivate-mark))
    (user-error "[WARNING] You need to select a region to highlight.")))

(define-key highlight-prefix (kbd "r") 'highlight-region)

;;; [ symbol-overlay ] -- highlighting symbols with keymap-enabled overlays.

(use-package symbol-overlay
  :ensure t
  :defer t
  :delight symbol-overlay-mode
  :bind (:map highlight-prefix
              ("h" . symbol-overlay-put)
              ("M-h" . symbol-overlay-toggle-in-scope)
              ("t" . symbol-overlay-toggle-in-scope)
              ("s" . symbol-overlay-isearch-literatelly)
              ("p" . symbol-overlay-jump-prev)
              ("n" . symbol-overlay-jump-next)
              ("d" . symbol-overlay-jump-to-definition)
              ("M-," . symbol-overlay-echo-mark)
              ("c" . symbol-overlay-save-symbol)
              ("k" . symbol-overlay-remove-all)
              ("R" . symbol-overlay-rename)
              ("q" . symbol-overlay-query-replace)
              ("P" . symbol-overlay-switch-backward)
              ("N" . symbol-overlay-switch-forward))
  :custom-face
  (symbol-overlay-default-face
   ((t `(:inherit t :foreground nil
                  :background ,(cl-case (frame-parameter nil 'background-mode)
                                 ('light
                                  (color-darken-name (face-background 'default) 10))
                                 ('dark
                                  (color-darken-name (face-background 'default) 5))))))))


(provide 'init-emacs-highlight)

;;; init-emacs-highlight.el ends here
