;;; init-emacs-color.el --- init for Color Manippulation
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ rainbow-mode ] -- colorize color names in buffers

;; (use-package rainbow-mode
;;   :ensure t
;;   :defer t
;;   :delight rainbow-mode
;;   :hook (emacs-lisp-mode css-mode html-mode))

;;; [ colorful-mode ] -- Preview any color format like color hex and color names.

(use-package colorful-mode
  :vc (:url "git@github.com:DevelopmentCool2449/colorful-mode.git")
  :delight colorful-mode
  :hook (prog-mode text-mode)
  :init (global-colorful-mode t)
  :commands (colorful-change-or-copy-color
             colorful-convert-and-change-color
             colorful-convert-and-copy-color))


(provide 'init-emacs-color)

;;; init-emacs-color.el ends here
