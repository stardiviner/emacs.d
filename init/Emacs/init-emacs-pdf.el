;;; init-emacs-pdf.el --- init for PDF.
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ doc-view ] -- Document viewer for Emacs.

(use-package doc-view
  :defer t
  :hook (doc-view-minor-mode . doc-view-fit-width-to-window))

;;; [ pdf-tools ] -- Emacs support library for PDF files.

(use-package pdf-tools
  :ensure t
  :defer t
  :preface
  ;; (pdf-tools-install-noverify)
  (pdf-tools-install)
  :mode ("\\.pdf\\'" . pdf-view-mode)
  ;; :magic ("%PDF" . pdf-view-mode) ; for PDF binary header byte.
  :custom ((pdf-view-use-scaling t)     ; open PDF scaled to fit page.
           ;; speed-up pdf-tools by don't try to find unicode.
           (pdf-view-use-unicode-ligther nil))
  :init
  ;; (add-to-list 'display-buffer-alist '("\\.pdf\\(<[^>]+>\\)?$" . (display-buffer-below-selected)))
  (add-to-list 'display-buffer-alist '("\\*Outline .*pdf\\*" . (display-buffer-below-selected)))
  (add-to-list 'display-buffer-alist '("\\*PDF-Occur\\*" . (display-buffer-below-selected)))
  :config
  ;; helpful accessibility shortcuts
  (define-key pdf-view-mode-map (kbd "q") 'kill-current-buffer)

  ;; always enable `pdf-view-themed-minor-mode'.
  ;; (add-hook 'pdf-view-mode-hook #'pdf-view-themed-minor-mode)
  (add-hook 'pdf-view-mode-hook #'auto-revert-mode)

  (defun my/pdf-view-themed-colors-reset (&optional theme)
    "Reset pdf-view themed colors to fit color themes."
    (when (eq frame-background-mode 'dark)
      ;; Change already opened PDF buffer colors to follow dark theme.
      (mapc
       (lambda (buffer)
         (if-let ((filename (buffer-file-name buffer)))
             (when (string-equal (file-name-extension filename) "pdf")
               (with-current-buffer buffer
                 ;; Change `pdf-view-midnight-minor-mode' colors based on dark theme.
                 ;; (setq pdf-view-midnight-colors
                 ;;       (cons (frame-parameter nil 'foreground-color)
                 ;;             (frame-parameter nil 'background-color)))
                 ;; (cl-case (frame-parameter nil 'background-mode)
                 ;;   ('light (pdf-view-midnight-minor-mode -1))
                 ;;   ('dark (pdf-view-midnight-minor-mode 1)))
                 ;; Replace `pdf-view-midnight-minor-mode' with `pdf-view-themed-minor-mode'.
                 (pdf-view-themed-minor-mode 1))
               ;; programatically refresh (auto revert buffer) PDF buffers.
               (with-current-buffer buffer
                 (revert-buffer nil 'noconfirm)))))
       (buffer-list))))
  
  (add-hook 'load-theme-after-hook #'my/pdf-view-themed-colors-reset 93)
  (if (featurep 'circadian)
      (add-hook 'circadian-after-load-theme-hook #'my/pdf-view-themed-colors-reset))
  
  ;; [ annotation ]
  (add-hook 'pdf-view-mode-hook #'pdf-annot-minor-mode)
  ;; save after adding annotation comment
  (advice-add 'pdf-annot-edit-contents-commit :after 'save-buffer)

  (defun my-pdf-tools-setup ()
    ;; auto slice page white spans
    (pdf-view-auto-slice-minor-mode 1)
    ;; Vim like basic scroll keys.
    (define-key pdf-view-mode-map (kbd "j") 'pdf-view-next-line-or-next-page)
    (define-key pdf-view-mode-map (kbd "k") 'pdf-view-previous-line-or-previous-page)
    ;; change key [k] to [K] to avoid mis-press.
    ;; (define-key pdf-view-mode-map (kbd "k") nil)
    (pdf-outline-minor-mode 1)
    (pdf-isearch-minor-mode 1))
  (add-hook 'pdf-view-mode-hook #'my-pdf-tools-setup)

  ;; close all opened PDFs by pdf-tools to save read positions before kill Emacs.
  (defun pdf-tools-save-positions-before-kill ()
    "Save all opened pdf-view-mode files positions before kill Emacs."
    (dolist (buffer (buffer-list))
      (with-current-buffer buffer
        (when (derived-mode-p 'pdf-view-mode)
          (kill-buffer-and-window)))))
  (add-hook 'kill-emacs-hook #'pdf-tools-save-positions-before-kill))

;;; [ pdf-view-restore ] -- support for opening last known pdf position in pdf-view-mode.

(use-package pdf-view-restore
  :ensure t
  :after pdf-tools
  :custom (pdf-view-restore-filename "~/.config/emacs/.pdf-view-restore")
  :hook (pdf-view-mode . pdf-view-restore-mode)
  ;; FIXME hook on `find-file-hook' to fix `desktop-save-mode' restore session invalid issue.
  :config (add-hook 'find-file-hook #'pdf-view-restore))

;;; [ saveplace-pdf-view ] -- saveplace support in pdf-view buffers for Emacs.

(use-package saveplace-pdf-view
  :ensure t
  :after pdf-tools
  :init (save-place-mode 1))

;;; [ org-pdftools ] -- Support for links to documents in pdfview mode.

(use-package org-pdftools
  :ensure t
  :after pdf-tools
  :hook (org-mode . org-pdftools-setup-link))

;; [ org-noter ] -- Emacs document annotator, using Org-mode.

(use-package org-noter
  :ensure t
  :defer t
  :after pdf-tools
  :commands (org-noter)
  :custom (org-noter-auto-save-last-location t)
  :init (with-eval-after-load "init-org-mode" ; after `Org-prefix' loaded
          (define-key Org-prefix (kbd "n") 'org-noter)))

;;; [ org-noter-pdftools ] -- Integration between org-pdftools and org-noter

(use-package org-noter-pdftools
  :ensure t
  :after org-noter
  :config
  (with-eval-after-load 'pdf-annot
    (add-hook 'pdf-annot-activate-handler-functions #'org-noter-pdftools-jump-to-note)))

;; [ paperless ] -- Emacs assisted PDF document filing.

;; (use-package paperless
;;   :ensure t
;;   :defer t
;;   :commands (paperless)
;;   :init (setq paperless-capture-directory "~/Downloads"
;;               paperless-root-directory "~/Org"))

;;; Org Mode helper command to insert PDF current page link to Org.
(defun my/pdf-tools-insert-org-link-of-current-pdf-page ()
  "Org Mode helper command to insert a link of PDF current page."
  (interactive)
  (pdf-tools-assert-pdf-buffer)
  (let ((page (number-to-string (pdf-view-current-page)))
        (file (pdf-view-buffer-file-name)))
    (kill-new
     (format "[[pdfview:%s::%s][%s at %s]]."
             file page (car (last (s-split "/" file))) page))))

;;; [ pdfgrep ] -- Grep PDF for searching PDF.

(use-package pdfgrep
  :ensure t
  :defer t
  :after pdf-tools
  :commands (pdfgrep pdfgrep-mode)
  :hook (pdf-view-mode . pdfgrep-mode))


(provide 'init-emacs-pdf)

;;; init-emacs-pdf.el ends here
