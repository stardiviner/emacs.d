;;; init-emacs-font.el --- init for Emacs font settings -*- lexical-binding: t; -*-

;;; Commentary:

;; the value is in 1/10pt, so 100 will give you 10pt, etc
;;
;; - [C-u C-x =]
;; - [M-x describe-font]
;; - [M-x describe-fontset]
;; - ‘font-family-list’
;; - from command: $ fc-list
;; - verify/validate fontset with `view-hello-file' [C-h h].
;;
;; Emacs set font functions:
;;
;; - `set-face-attribute'
;; - `set-frame-font'
;;
;; - "DejaVu Sans Mono"
;; - "Noto Sans CJK SC"
;; - "Noto Sans Mono CJK SC"
;; - "Hack"
;; - "Fira Sans"
;; - "Sarasa Mono SC"
;; - "Sarasa Nerd"
;; - "Maple Mono"
;; - "Operator-caskabold"
;; - "Monaspace Radon"

;;; Code:

;;; [ set font for Emacs ]

(defun my/set-default-font (&optional theme)
  (cl-case system-type
    ;; Linux
    (gnu/linux
     (set-face-attribute 'default nil :family "DejaVu Sans Mono")
     (dolist (charset '(kana han cjk-misc bopomofo))
       (set-fontset-font (frame-parameter nil 'font) charset
                         (font-spec :family "Source Han Sans CN") nil 'append))
     (set-fontset-font t 'symbol
                       (font-spec :family "Noto Color Emoji") nil 'prepend)
     (set-fontset-font t 'greek
                       (font-spec :family "Symbola") nil 'prepend))
    ;; macOS
    (darwin
     ;; (set-face-attribute 'default nil :family "DejaVu Sans Mono") ; :height 100
     ;; (set-frame-parameter nil 'font "DejaVu Sans Mono 12")
     ;; (set-frame-font "DejaVu Sans Mono 12" nil t)

     ;; (set-face-attribute 'default nil :family "Fira Code")
     ;; (set-frame-font "Fira Code 12" nil t)
     
     (set-face-attribute 'default nil :family "FiraCode Nerd Font" :weight 'medium)
     ;; (set-face-attribute 'default nil :family "FiraCode Nerd Font Mono" :weight 'medium)
     ;; (set-face-attribute 'default nil :family "Hack Nerd Font" :weight 'medium)
     ;; (set-face-attribute 'default nil :family "Maple Mono" :weight 'normal)
     ;; (set-frame-font "FiraCode Nerd Font Mono 12" nil t) ; NOTE: This line will make font weight thin.

     ;; (set-face-attribute 'default nil :family "Cascadia Code" :weight 'medium)
     ;; (set-frame-font "Cascadia Code 12" nil t)

     ;; (set-face-attribute 'default nil :family "Comic Mono")
     ;; (set-frame-font "Comic Mono 13" nil t)

     ;; CJK: Chinese, Japanese, Korean
     (dolist (charset '(kana han cjk-misc bopomofo))
       (set-fontset-font (frame-parameter nil 'font) charset
                         ;; hand-write chinese fonts: Hannotate SC, HanziPen SC, Yuppy SC, Libian SC, Wawati SC, LingWai SC,
                         ;; normal chinese fonts: PingFang SC, Lantinghei SC, Heiti SC, Kaiti SC, Songti SC,
                         ;; ancient chinese fonts: Weibei SC, Xingkai SC, Yuanti SC,
                         (font-spec :family "PingFang SC") nil 'prepend))

     ;; Emoji
     (set-fontset-font t 'emoji  (font-spec :family "Noto Color Emoji") nil 'prepend)
     (set-fontset-font t 'symbol (font-spec :family "Noto Color Emoji") nil 'prepend))
    ;; Windows
    (windows-nt
     ;; (set-face-attribute 'default nil :family "DejaVu Sans Mono")
     (set-face-attribute 'default nil :family "Cascadia Code PL")
     (dolist (charset '(kana han cjk-misc bopomofo))
       (set-fontset-font (frame-parameter nil 'font) charset
                         (font-spec :family "Source Han Sans CN") nil 'prepend))))

  ;; Use ligature & cursive font for special keywords.
  (set-face-attribute 'font-lock-keyword-face nil
                      :font "Operator-caskabold" :family "Operator-caskabold"
                      :slant 'italic :weight 'bold)
  (set-face-attribute 'font-lock-operator-face nil :weight 'bold)
  (set-face-attribute 'font-lock-type-face nil
                      :family "Monaspace Krypton"
                      :slant 'italic :weight 'medium)
  (set-face-attribute 'font-lock-property-name-face nil
                      :family "Monaspace Radon"
                      :slant 'oblique)
  (set-face-attribute 'font-lock-constant-face nil :slant 'reverse-oblique))

(add-hook 'after-init-hook #'my/set-default-font)
;; (add-hook 'load-theme-after-hook #'my/set-default-font)

;;; [ ligature ] -- Display typographical ligatures in Emacs.

;; maps ordinary graphemes (characters) to fancy ligatures, if both your version of Emacs and the font supports it.
(use-package ligature
  :ensure t
  :config
  ;; (set-face-attribute 'default nil :family "Fira Code")
  ;; (set-face-attribute 'default nil :family "Cascadia Code PL")

  ;; Enable the "www" ligature in every possible major mode
  (ligature-set-ligatures 't '("www"))
  ;; Enable traditional ligature support in eww-mode, if the
  ;; `variable-pitch' face supports it
  (ligature-set-ligatures 'eww-mode '("ff" "fi" "ffi"))
  ;; Enable all Cascadia Code font ligatures in programming modes
  (ligature-set-ligatures 'prog-mode '("|||>" "<|||" "<==>" "<!--" "####" "~~>" "***" "||=" "||>"
                                       ":::" "::=" "=:=" "===" "==>" "=!=" "=>>" "=<<" "=/=" "!=="
                                       "!!." ">=>" ">>=" ">>>" ">>-" ">->" "->>" "-->" "---" "-<<"
                                       "<~~" "<~>" "<*>" "<||" "<|>" "<$>" "<==" "<=>" "<=<" "<->"
                                       "<--" "<-<" "<<=" "<<-" "<<<" "<+>" "</>" "###" "#_(" "..<"
                                       "..." "+++" "/==" "///" "_|_" "www" "&&" "^=" "~~" "~@" "~="
                                       "~>" "~-" "**" "*>" "*/" "||" "|}" "|]" "|=" "|>" "|-" "{|"
                                       "[|" "]#" "::" ":=" ":>" ":<" "$>" "==" "=>" "!=" "!!" ">:"
                                       ">=" ">>" ">-" "-~" "-|" "->" "--" "-<" "<~" "<*" "<|" "<:"
                                       "<$" "<=" "<>" "<-" "<<" "<+" "</" "#{" "#[" "#:" "#=" "#!"
                                       "##" "#(" "#?" "#_" "%%" ".=" ".-" ".." ".?" "+>" "++" "?:"
                                       "?=" "?." "??" ";;" "/*" "/=" "/>" "//" "__" "~~" "(*" "*)"
                                       "\\\\" "://"))
  (ligature-set-ligatures 'markdown-mode '(("=" (rx (+ "=") (? (| ">" "<"))))
                                           ("-" (rx (+ "-")))))
  
  ;; Enables ligature checks globally in all buffers. You can also do it
  ;; per mode with `ligature-mode'.
  (global-ligature-mode t))

;;; [ Font Selecting by Binary Search ]

(defun font-selecting-by-binary-search ()
  "Compare two monospaced fonts until only one is left."
  (interactive)
  (let ((line (line-number-at-pos)) queue)
    ;; Extract fixed-width font names
    (dolist (fam (x-family-fonts))
      (when (aref fam 5)
        (push (symbol-name (aref fam 0)) queue)))
    ;; Check if there are enough fonts
    (when (<= (length queue) 1)
      (user-error "Not enough fixed-width fonts for binary Search"))
    ;; Shuffle font list (should use Fischer-Yates)
    (setq queue (sort (delete-dups queue) (lambda (_ _) (= (random 2) 0))))
    ;; Start binary search
    (save-window-excursion
      (while (< 1 (length queue))
        (let ((buf-a (clone-indirect-buffer "font-a" nil))
              (buf-b (clone-indirect-buffer "font-b" nil))
              (font-a (pop queue))
              (font-b (pop queue)))
          (delete-other-windows)
          (unwind-protect
              (let* ((win-a (selected-window))
                     (win-b (split-window-right)))
                (set-window-buffer win-a buf-a)
                (with-selected-window win-a
                  (face-remap-add-relative 'default :family font-a)
                  (setq mode-line-format " Font A")
                  (goto-char (point-min))
                  (forward-char line))
                (set-window-buffer win-b buf-b)
                (with-selected-window win-b
                  (face-remap-add-relative 'default :family font-b)
                  (setq mode-line-format " Font B")
                  (goto-char (point-min))
                  (forward-char line))
                (let ((pref (let (c r)
                              (while (not r)
                                (setq c (read-char  "Prefer font A or font B?"))
                                (cond
                                 ((memq c '(?a ?A))
                                  (setq r font-a))
                                 ((memq c '(?b ?b))
                                  (setq r font-b))
                                 ((message "Invalid choice"))))
                              r)))
                  (setq queue (nconc queue (list pref)))))
            (kill-buffer buf-a)
            (kill-buffer buf-b)))))
    (message "You appear to prefer the font %S." (car queue))))

;;; [ textsize ] -- Configure frame text size automatically.

;; (use-package textsize
;;   :ensure t
;;   :defer t
;;   :custom (textsize-default-points 12)
;;   :commands (textsize-mode
;;              textsize-increment textsize-decrement textsize-reset)
;;   :init (textsize-mode))



(provide 'init-emacs-font)

;;; init-emacs-font.el ends here
