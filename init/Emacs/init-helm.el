;;; init-helm.el --- simple configure Helm
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ Helm ] -- Helm is an Emacs incremental and narrowing framework.

(use-package helm
  :ensure t
  :defer t
  :custom ((helm-input-idle-delay 0.1)        ; fix Helm fast respond to input caused failed issue.
           ;; (helm-display-function 'helm-display-buffer-in-own-frame) ; (setq helm-display-function 'helm-default-display-buffer)
           (helm-split-window-inside-p t)     ; helm popup in or override current window?
           (helm-split-window-default-side 'below)
           (helm-display-buffer-default-width 40)
           (helm-display-buffer-default-height 15)
           (helm-echo-input-in-header-line t) ; echo input in header line
           (helm-mini-default-sources '(helm-source-buffers-list
                                        helm-source-bookmarks
                                        helm-source-recentf
                                        helm-source-buffer-not-found)))
  ;; :bind (([remap execute-extended-command] . helm-M-x)
  ;;        ("M-x" . helm-M-x)
  ;;        ([remap switch-to-buffer] . helm-mini)
  ;;        ([remap yank-pop] . helm-show-kill-ring)
  ;;        ([remap yas-insert-snippet] . helm-yas-complete))
  ;; :hook (after-init . helm-mode)
  :init (helm-top-poll-mode 1)
  :hook (helm-minibuffer-set-up . helm-hide-minibuffer-maybe))

;;; [ helm-posframe ] -- Using posframe to show helm window.

(use-package helm-posframe
  :if (featurep 'zoom)
  :ensure t
  :init (helm-posframe-enable))

;;; [ helm-fuz ] -- Integrate Helm and Fuz.

;; (use-package helm-fuz
;;   :ensure t
;;   :init (helm-fuz-mode 1))


(provide 'init-helm)

;;; init-helm.el ends here
