;;; init-emacs-search.el --- init search utilities for Emacs.
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

(unless (boundp 'search-prefix)
  (define-prefix-command 'search-prefix))
(global-set-key (kbd "C-c s") 'search-prefix)

(load "init-emacs-search-isearch")
(load "init-emacs-search-occur")
;; (load "init-emacs-search-grep")
(load "init-emacs-search-wgrep")
;; (load "init-emacs-search-ag")
;; (load "init-emacs-search-pt")
(load "init-emacs-search-ripgrep")
(load "init-emacs-search-similar")
(load "init-emacs-search-finder")
(load "init-emacs-search-engine")


(provide 'init-emacs-search)

;;; init-emacs-search.el ends here
