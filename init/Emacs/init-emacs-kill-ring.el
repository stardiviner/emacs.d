;;; init-emacs-kill-ring.el --- init for Emacs Kill Ring.
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ vundo ] -- Vundo (visual undo) displays the undo history as a tree and lets you move in the tree to go back to previous buffer states.

(use-package vundo
  :ensure t
  :custom ((vundo-glyph-alist vundo-unicode-symbols))
  ;; :custom-face (vundo-default ((t (:family "Symbola"))))
  :commands (vundo)
  :init (add-to-list 'display-buffer-alist '("^ \\*vundo tree\\*" . (display-buffer-below-selected))))


(provide 'init-emacs-kill-ring)

;;; init-emacs-kill-ring.el ends here
