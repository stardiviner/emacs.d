;;; init-emacs-encoding.el --- Encoding settings for Emacs. -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Christopher M. Miles, all rights reserved.

;;; Commentary:



;;; Code:
;;; [ mule ] -- basic commands for multilingual environment.

(require 'mule)

;; encoding: prefer UTF-8 everywhere
(set-language-environment               "UTF-8")     ;; System default coding
(prefer-coding-system                   'utf-8)      ;; prefer
(set-buffer-file-coding-system          'utf-8-unix) ;;
(set-charset-priority                   'unicode)    ;;
(set-clipboard-coding-system            'utf-8)      ;; clipboard
(set-default-coding-systems             'utf-8)      ;; buffer/file: 打开文件时的默认编码
(set-file-name-coding-system            'utf-8-unix) ;; unix/linux/macos
(set-keyboard-coding-system             'utf-8-unix) ;; keyboard
(set-next-selection-coding-system       'utf-8-unix) ;; selection
(set-selection-coding-system            'utf-8)      ;; selection
(set-terminal-coding-system             'utf-8-unix) ;; terminal
(setq coding-system-for-read            'utf-8)      ;;
(setq default-buffer-file-coding-system 'utf-8)      ;;
(setq locale-coding-system              'utf-8)      ;; local

;; (setq buffer-file-coding-system 'utf-8-unix
;;       default-file-name-coding-system 'utf-8-unix
;;       default-keyboard-coding-system 'utf-8-unix
;;       default-process-coding-system '(utf-8-unix . utf-8-unix)
;;       default-sendmail-coding-system 'utf-8-unix
;;       default-terminal-coding-system 'utf-8-unix)

;; locale
(setq system-time-locale "C")

;;; auto guess file buffer encoding. (last is highest priority)
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Recognize-Coding.html#Recognize-Coding
;; (prefer-coding-system 'cp950)
;; (prefer-coding-system 'gb2312)
;; (prefer-coding-system 'cp936)
;; (prefer-coding-system 'gb18030)
;; (prefer-coding-system 'utf-16)
;; (prefer-coding-system 'utf-8-dos)
;; (prefer-coding-system 'utf-8-unix)

;;; open file with specified default encoding.
;; (add-to-list 'file-coding-system-alist '("\\.extension???" . utf-8))

;;; [ Glyphless characters ] -- (invisible zero width space characters)

(use-package glyphless-mode
  :if (not (version<= emacs-version "29"))
  :delight glyphless-display-mode
  ;; Minor mode for displaying glyphless characters in the current buffer.
  ;; If enabled, all glyphless characters will be displayed as boxes
  ;; that display their acronyms.
  :hook ((after-init . glyphless-display-mode)
         (prog-mode . glyphless-display-mode)
         (org-mode . glyphless-display-mode))
  :config
  (defun my/glyphless-char-display-mark ()
    "Highlight ZERO WIDTH chars in all buffers."
    (interactive)
    (let ((charnames (list "BYTE ORDER MARK"
                           "ZERO WIDTH NO-BREAK SPACE"
                           "ZERO WIDTH SPACE"
                           "RIGHT-TO-LEFT MARK"
                           "RIGHT-TO-LEFT OVERRIDE"
                           "LEFT-TO-RIGHT MARK"
                           "OBJECT REPLACEMENT CHARACTER"
                           "ZERO WIDTH JOINER"
                           "ZERO WIDTH NON-JOINER")))
      (dolist (name charnames)
        ;; see info node "info:elisp#Glyphless Chars" for available values.
        (set-char-table-range glyphless-char-display
                              (char-from-name name) 'hex-code))
      (set-face-attribute 'glyphless-char nil
                          :inherit 'escape-glyph
                          :family "Cascadia Code"
                          ;; :height 0.6
                          :foreground "GhostWhite" :background "HotPink")))

  (add-hook 'glyphless-display-mode-hook #'my/glyphless-char-display-mark))

(provide 'init-emacs-encoding)

;;; init-emacs-encoding.el ends here
