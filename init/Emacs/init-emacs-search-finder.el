;;; init-emacs-search-finder.el --- init for command find
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

(unless (boundp 'find-prefix)
  (define-prefix-command 'find-prefix))

(define-key search-prefix (kbd "f") 'find-prefix)

;;; [ counsel-fd ] -- counsel interface for fd.

(use-package counsel-fd
  :ensure t
  :defer t
  :commands (counsel-fd-file-jump counsel-fd-dired-jump)
  :bind (:map find-prefix
              ("f" . counsel-fd-file-jump)
              ("d" . counsel-fd-dired-jump)))

;;; [ fd-dired ] -- find-dired alternative using fd.

(use-package fd-dired
  :ensure t
  :defer t
  :custom (fd-dired-display-in-current-window nil)
  :commands (fd-dired)
  :bind (:map find-prefix ("s" . fd-dired))
  :init
  ;; A helper command for run "fd" under project root and support [C-u] to use current working directory.
  (defun projectile-find-file-with-fd-dired (project-root)
    "Run `fd-dired' under projectile root."
    (interactive "P")
    (let ((project-root (if (= (prefix-numeric-value current-prefix-arg) 4)
                            default-directory
                          (projectile-project-root))))
      (setq default-directory project-root)
      (projectile-with-default-dir project-root
        (call-interactively #'fd-dired))))
  (with-eval-after-load 'projectile
    (define-key projectile-command-map (kbd "C-f") 'projectile-find-file-with-fd-dired))
  (with-eval-after-load 'project
    (define-key project-prefix-map (kbd "C-f") 'projectile-find-file-with-fd-dired))
  :config
  (defun fd-dired--auto-kill-fd-buffer ()
    "Auto kill *Fd* search result buffers."
    (when-let (fd-buffer (get-buffer "*Fd*"))
      (kill-buffer fd-buffer))
    (mapc 'kill-buffer
          (seq-filter (lambda (buf) (string-match-p "^\\*Fd.*\\*" (buffer-name buf))) (buffer-list))))
  (add-hook 'kill-emacs-hook #'fd-dired--auto-kill-fd-buffer))

;;; [ find-file-in-project ] -- Find file/directory and review Diff/Patch/Commit efficiently.

;; (use-package find-file-in-project
;;   :ensure t
;;   :defer t
;;   :commands (find-file-in-project find-file-in-project-by-selected)
;;   :bind ("M-t" . find-file-in-project) ; same with `counsel-git'
;;   :init (setq ffip-use-rust-fd t))


(provide 'init-emacs-search-finder)

;;; init-emacs-search-finder.el ends here
