;;; init-emacs-emoji.el --- init Emoji for Emacs.
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:
;;; ----------------------------------------------------------------------------
;;; [ display Unicode Emoji ]

;;; A font that supports emoji is needed. The best results are obtained with
;;; "Noto Color Emoji" or "Symbola". It might be necessary to instruct Emacs to
;;; use such font with a line like the following.
;; (set-fontset-font t 'symbol
;;                   (font-spec :family "Noto Color Emoji") nil 'prepend)

;; set color emoji font
(cl-case system-type
  (gnu/linux
   (set-fontset-font t 'emoji ("Noto Color Emoji" . "iso10646-1") nil 'append))
  (darwin
   (set-fontset-font t 'emoji '("Apple Color Emoji" . "iso10646-1") nil 'prepend))
  (windows-nt
   (set-fontset-font t 'emoji ("Noto Color Emoji" . "iso10646-1") nil 'append)))

;;; [ emojify ] display emojis in Emacs.

;; (use-package emojify
;;   :ensure t
;;   :defer t
;;   :custom ((emojify-emojis-dir (concat user-emacs-directory "emojis"))
;;            (emojify-program-contexts '(comments string))
;;            ;; (emojify-display-style 'unicode)
;;            )
;;   :commands (global-emojify-mode emojify-download-emoji)
;;   :init (global-emojify-mode 1)
;;   :config
;;   ;; set color emoji font
;;   (cl-case system-type
;;     (gnu/linux
;;      (set-fontset-font t 'emoji ("Noto Color Emoji" . "iso10646-1") nil 'append))
;;     (darwin
;;      (set-fontset-font t 'emoji '("Apple Color Emoji" . "iso10646-1") nil 'prepend))
;;     (windows-nt
;;      (set-fontset-font t 'emoji ("Noto Color Emoji" . "iso10646-1") nil 'append))))

;;; [ company-emojify ] -- company-mode completion for emojify.

;; (use-package company-emojify
;;   :ensure t
;;   :defer t
;;   :commands (company-emojify)
;;   :init (with-eval-after-load 'company
;;           (setq-default company-backends
;;                         (let ((default-value (default-value 'company-backends)))
;;                           (add-to-list 'company-backends 'company-emojify 'append)))))

;;; [ ivy-emoji ] -- Insert emojis with Ivy.

(use-package ivy-emoji
  :ensure t
  :defer t
  :commands (ivy-emoji))


;;; ----------------------------------------------------------------------------

(provide 'init-emacs-emoji)

;;; init-emacs-emoji.el ends here
