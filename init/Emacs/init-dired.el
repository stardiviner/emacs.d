;;; init-dired.el --- init Dired for Emacs
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ Dired ] -- Directory editing mode.

(use-package dired
  :ensure direx
  :defer t
  :bind (("C-x d" . dired)
         :map dired-mode-map
         ("j" . dired-next-line)
         ("k" . dired-previous-line)
         ("g" . dired-do-redisplay)
         ("F" . find-name-dired))
  :custom ((insert-directory-program (cl-case system-type (darwin (purecopy "gls")) (gnu/linux (purecopy "ls"))))
           (dired-listing-switches (concat dired-listing-switches "ht")) ; human-readable size, time sorting.
           (dired-kill-when-opening-new-dired-buffer t) ; kill the current buffer when selecting a new directory.
           (dired-auto-revert-buffer t) ; auto refresh dired when file changes.
           (dired-dwim-target t) ; Dired tries to guess another dired window as default target directory.
           ;; (dired-hide-details-hide-information-lines t) ; `dired-hide-details-mode'
           (dired-create-destination-dirs 'ask) ; ask before creation.
           ;; allow dired to be able to delete or copy a whole dir.
           (dired-recursive-copies 'always)
           (dired-recursive-deletes 'top) ; 'top means ask once.
           (dired-mouse-drag-files t) ; allow the mouse to drag files from inside a Dired buffer.
           )
  :hook ((dired-mode . turn-on-auto-revert-mode) ; auto refresh Dired buffer after files update.
         (dired-mode . toggle-truncate-lines) ; don't wrap long lines to break single line
         )
  :init
  (put 'dired-find-alternate-file 'disabled nil) ; key [a] in Dired.
  (add-to-list 'display-buffer-alist '("^\\*Dired log\\*" . (display-buffer-below-selected))) ; `dired-log-buffer'
  (add-to-list 'display-buffer-alist '("^\\*shell:.*\\*" . (display-buffer-below-selected))) ; & `dired-do-async-shell-command'
  :config
  ;; open file with `eww'.
  (when (featurep 'eww)
    (defun dired-open-file-with-eww ()
      "Open Dired file at point with `eww'."
      (interactive)
      (eww-open-file (dired-get-file-for-visit)))
    ;; (define-key dired-mode-map (kbd "e") 'dired-open-file-with-eww)
    )

  ;; [ wdired ] -- rename files editing their names in dired buffers.
  (use-package wdired
    :defer t
    :custom (wdired-allow-to-change-permissions t)
    :commands (wdired-change-to-wdired-mode)
    :bind (:map dired-mode-map ("C-c C-p" . wdired-change-to-wdired-mode)))

  (use-package dired-x                  ; extra Dired functionality
    :defer t
    ;; don't bind [C-x C-j] from `dired-x'. (conflict with `ace-window')
    :preface (setq dired-bind-jump nil)
    :config
    ;; ignore specific files
    ;; toggle `dired-omit-mode' to hide hidden files with [C-x M-o]
    ;; (add-hook 'dired-mode-hook #'dired-omit-mode)
    (setq dired-omit-files
          (concat dired-omit-files
                  "\\|^.DS_STORE$\\|^.projectile$"
                  "\\(?:.*\\.\\(?:aux\\|log\\|synctex\\.gz\\|run\\.xml\\|bcf\\|am\\|in\\)\\'\\)\\|^\\.\\|-blx\\.bib")))

  ;; `dired-do-*' commands
  (use-package dired-aux
    :config
    ;; Add new guess shell command on marked files like `dired-guess-shell-alist-default'.
    (add-to-list 'dired-guess-shell-alist-user `("\\.mpe?g\\'\\|\\.avi\\'"
                                                 ,(cond
                                                   ((executable-find "mpv") "mpv")
                                                   ((executable-find "mplayer") "mplayer"))))
    (add-to-list 'dired-guess-shell-alist-user `("\\.rmvb\\'"
                                                 ,(cond
                                                   ((executable-find "mpv") "mpv")
                                                   ((executable-find "mplayer") "mplayer"))))

    ;; Add new compress rules
    (cl-case system-type
      (gnu/linux
       (add-to-list 'dired-compress-file-alist '("\\.zip\\'" . "zip %o %i"))
       (add-to-list 'dired-compress-files-alist '("\\.zip\\'" . "zip -r %o %i"))
       (add-to-list 'dired-compress-file-suffixes '("\\.zip\\'" "" "unzip %i")))
      (darwin
       (add-to-list 'dired-compress-file-alist '("\\.zip\\'" . "zip %o %i"))
       (add-to-list 'dired-compress-files-alist '("\\.zip\\'" . "zip -r %o %i"))
       (add-to-list 'dired-compress-file-suffixes '("\\.zip\\'" "" "unzip %i"))))
    (cond
     ((executable-find "rar")
      (add-to-list 'dired-compress-file-alist '("\\.rar\\'" . "rar a %o %i"))
      (add-to-list 'dired-compress-files-alist '("\\.rar\\'" . "rar a %o %i"))
      (add-to-list 'dired-compress-file-suffixes '("\\.rar\\'" "" "rar e %i %o")))
     ((executable-find "unrar")
      (add-to-list 'dired-compress-file-alist '("\\.rar\\'" . "unrar a %o %i"))
      (add-to-list 'dired-compress-files-alist '("\\.rar\\'" . "unrar a %o %i"))
      (add-to-list 'dired-compress-file-suffixes '("\\.rar\\'" "" "unrar e %i %o"))))
    ;; CDisplay Archive Comic Book formats
    (cl-case system-type
      (gnu/linux
       (add-to-list 'dired-compress-file-alist '("\\.cbz\\'" . "zip %o %i"))
       (add-to-list 'dired-compress-files-alist '("\\.cbz\\'" . "zip -r %o %i"))
       (add-to-list 'dired-compress-file-suffixes '("\\.cbz\\'" "" "unzip -o -d %i")))
      (darwin
       (add-to-list 'dired-compress-file-alist '("\\.cbz\\'" . "zip -r %o %i"))
       (add-to-list 'dired-compress-files-alist '("\\.cbz\\'" . "zip -r %o %i"))
       (add-to-list 'dired-compress-file-suffixes '("\\.cbz\\'" "" "unzip -o -d %i"))))
    (cl-case system-type
      (gnu/linux
       (add-to-list 'dired-compress-file-alist '("\\.cbr\\'" . "rar a %o %i"))
       (add-to-list 'dired-compress-files-alist '("\\.cbr\\'" . "rar a %o %i"))
       (add-to-list 'dired-compress-file-suffixes '("\\.cbr\\'" "" "rar e %i")))
      (darwin
       (add-to-list 'dired-compress-file-alist '("\\.cbr\\'" . "rar a %o %i"))
       (add-to-list 'dired-compress-files-alist '("\\.cbr\\'" . "rar a %o %i"))
       (add-to-list 'dired-compress-file-suffixes '("\\.cbr\\'" "" "rar e %i"))))
    (cl-case system-type
      (gnu/linux
       (add-to-list 'dired-compress-file-alist '("\\.cb7\\'" . "7z a %o %i"))
       (add-to-list 'dired-compress-files-alist '("\\.cb7\\'" . "7z a %o %i"))
       (add-to-list 'dired-compress-file-suffixes '("\\.cb7\\'" "" "7z e %i")))
      (darwin
       (add-to-list 'dired-compress-file-alist '("\\.cb7\\'" . "7z a %o %i"))
       (add-to-list 'dired-compress-files-alist '("\\.cb7\\'" . "7z a %o %i"))
       (add-to-list 'dired-compress-file-suffixes '("\\.cb7\\'" "" "7z e %i"))))
    (cl-case system-type
      (gnu/linux
       (add-to-list 'dired-compress-file-alist '("\\.cbt\\'" . "tar -cf %o %i"))
       (add-to-list 'dired-compress-files-alist '("\\.cbt\\'" . "tar -cfr %o %i"))
       (add-to-list 'dired-compress-file-suffixes '("\\.cbt\\'" "" nil)))
      (darwin
       (add-to-list 'dired-compress-file-alist '("\\.cbt\\'" . "tar -cf %o %i"))
       (add-to-list 'dired-compress-files-alist '("\\.cbt\\'" . "tar -cfr %o %i"))
       (add-to-list 'dired-compress-file-suffixes '("\\.cbt\\'" "" nil))))
    
    (defun my/dired-do-compress--select-archive-type (&optional arg)
      "Select compress archive file type before Dired compress [Z] `dired-do-compress'."
      (let ((the-suffix (replace-regexp-in-string
                         "[\\\\|\\\\']" "" ; "\\.zip\\'" -> ".zip"
                         (substring-no-properties
                          (completing-read
                           "[Dired] select archive file type for compressing: "
                           (mapcar 'car dired-compress-file-suffixes))))))
        (unless (string-prefix-p "." the-suffix)
          (set 'the-suffix (concat "." the-suffix)))
        (if (file-directory-p (dired-get-filename))
            (setq dired-compress-directory-default-suffix the-suffix)
          (setq dired-compress-file-default-suffix the-suffix))))
    
    ;; `dired-do-compress' -> `dired-compress' -> `dired-compress-file' -> `dired-compress-directory-default-suffix'
    (advice-add 'dired-do-compress :before #'my/dired-do-compress--select-archive-type)
    )

  ;; [ image-dired ] -- use dired to browse and manipulate your images.
  (use-package image-dired
    :defer t
    :custom ((image-dired-dir (expand-file-name ".thumbnails" "~")))
    :commands (image-dired))
  
  ;; [ dired-async ] -- Asynchronous Dired actions.
  ;; (use-package async
  ;;   :ensure t
  ;;   :init (require 'dired-async)
  ;;   :bind (("R" . dired-async-do-rename)
  ;;          ("C" . dired-async-do-copy)))

  ;; helper functions for Dired.
  (defun my/dired-too-many-files-p (&optional args)
    "Return t only when Dired current directory files less then 500, other wise return nil."
    (length< (directory-files default-directory) 500))
  )

;; colorful Dired
(use-package diredfl
  :ensure t
  :defer t
  :hook (dired-mode . diredfl-mode))

;;; [ nerd-icons-dired ] -- Shows icons for each file in Dired mode.

(use-package nerd-icons-dired
  :ensure t
  :delight nerd-icons-dired-mode
  :hook (dired-mode . nerd-icons-dired-mode)
  :config (advice-add 'nerd-icons-dired-mode :before-while #'my/dired-too-many-files-p))

;; Insert subdirectories in a tree-like fashion.
(if (fboundp 'dired-maybe-insert-subdir)
    (progn
      (defvar-local dired-subdir--state nil)
      (defun dired-subdir-toggle ()
        (interactive)
        (if dired-subdir--state
            (progn
              (dired-kill-subdir)
              (setq-local dired-subdir--state nil))
          (progn
            (call-interactively 'dired-maybe-insert-subdir)
            (setq-local dired-subdir--state t))))
      (define-key dired-mode-map (kbd "TAB") 'dired-subdir-toggle))
  (use-package dired-subtree
    :ensure t
    :defer t
    :commands (dired-subtree-cycle)
    :bind (:map dired-mode-map ("TAB" . dired-subtree-cycle))))

;; browse directory with sudo privileges.
(use-package dired-toggle-sudo
  :ensure t
  :defer t
  :commands (dired-toggle-sudo)
  :bind (:map dired-mode-map ("#" . dired-toggle-sudo)))

;; Edit Filename At Point in an Emacs' dired buffer
(use-package dired-efap
  :ensure t
  :defer t
  :commands (dired-efap dired-efap-click)
  :bind (:map dired-mode-map ([f2] . dired-efap) ([down-mouse-1] . dired-efap-click))
  :init (setq dired-efap-use-mouse t))

;; Live-narrowing of search results for dired
(if (featurep 'consult)
    (define-key dired-mode-map (kbd "/") 'consult-focus-lines)
  (use-package dired-narrow
    :ensure t
    :defer t
    :commands (dired-narrow)
    :bind (:map dired-mode-map ("/" . dired-narrow))))

;; [S]: Persistent quick sorting of dired buffers in various ways.
(use-package dired-quick-sort
  :ensure t
  :defer t
  :commands (dired-quick-sort)
  :init (dired-quick-sort-setup))

;; [ dired-git-info ] -- Show Git info in Dired.
(use-package dired-git-info
  :ensure t
  :defer t
  :commands (dired-git-info-mode)
  :hook (dired . dired-git-info-mode)
  :bind (:map dired-mode-map (")" . dired-git-info-mode)))

;; [ dired-git ] -- Git integration for Dired.
;; (use-package dired-git
;;   :ensure t
;;   :hook (dired . dired-git-mode))

;;; [ find-dupes-dired ] -- Find dupes using /external command/ (`fdupes' / `jdupes') and handle them in `dired-mode'.

(use-package find-dupes-dired
  :ensure t
  :defer t
  :commands (find-dupes-dired))

;;; [ dired-duplicates ] -- Find duplicate files locally and remotely.

(use-package dired-duplicates
  :ensure t
  :defer t
  :commands (dired-duplicates))

;;; [ dired-sync(-transient) ] -- asynchronous "rsync" from Dired.

(use-package dired-rsync
  :ensure t
  :ensure dired-rsync-transient
  :defer t
  ;; :custom ((dired-rsync-options "-az --info=progress2"))
  :commands (dired-rsync dired-rsync-transient)
  :bind (:map dired-mode-map ("C-c C-r" . dired-rsync-transient)))

;; [ diredc ] -- dired extensions and convenience features inspired from "midnight commander".
;; (use-package diredc
;;   :ensure t
;;   :defer t
;;   :commands (diredc)
;;   :bind (([remap dired-other-frame] . diredc)
;;          ;; ([remap dired] . diredc)
;;          ;; ("s-<f11>" . diredc)
;;          ))

;; [ make-it-so ] -- Transform files with Makefile recipes.
;; (use-package make-it-so
;;   :ensure t
;;   :defer t
;;   :init (mis-config-default)
;;   :config (setq mis-recipes-directory
;;                 (concat user-emacs-directory "init/extensions/make-it-so-recipes/")))

;;; [ diffed ] -- *Diffed* is for /recursive/ =diff=, like *Dired* is for =ls=.

(use-package diffed
  :ensure t
  :commands (diffed))

;;; [ media-progress ] -- Display position where you stopped playing media in mpv.

(use-package media-progress
  :ensure t
  :ensure media-progress-dired
  :custom (media-progress-dired-auto-hide-details-p nil)
  :commands (media-progress-dired-mode))

;;; [ casual-dired ] -- An opinionated `transient'-based porcelain for the Emacs file manager Dired.

(use-package casual-dired
  :ensure t
  :bind (:map dired-mode-map ("M-o" . 'casual-dired-tmenu)))


(provide 'init-dired)

;;; init-dired.el ends here
