;;; init-emacs-encrypt.el --- init Emacs encrypt & decrypt

;;; Commentary:


;;; Code:

;;; [ pinentry ] --- GnuPG Pinentry server implementation

(use-package pinentry
  :ensure t
  :if (executable-find "pinentry")
  :hook (after-init . pinentry-start))

;;; [ epa ] -- the EasyPG Assistant: transparent, automatic encryption and decryption.

(use-package epa
  ;; force Emacs to use its own internal password prompt instead of an external
  ;; pinentry program.
  :preface (setenv "GPG_AGENT_INFO" nil)
  :custom ((epa-pinentry-mode 'loopback) ; let EasyPG Assistant to use loopback for pinentry.
           ;; cache passphrase for symmetric encryption.
           ;; For security reasons, this option is turned off by default and
           ;; not recommended to use.  Instead, consider using gpg-agent which
           ;; does the same job in a safer way.
           (epa-file-cache-passphrase-for-symmetric-encryption t)
           (epa-file-inhibit-auto-save t)
           (epa-keyserver "keys.openpgp.org")
           (epa-file-encrypt-to '(;; "F09F650D7D674819892591401B5DF1C95AE89AC3"
                                  ;; "32A8581A6E137ABD26DA2F570251FA6886EB6B77"
                                  "stardiviner" "numbchild@gmail.com"))
           (epa-file-select-keys (if (null epa-file-encrypt-to) t nil)))
  :commands (epa-search-keys)
  :init (epa-file-enable)
  (add-to-list 'display-buffer-alist '("^\\*Keys\\*" . (display-buffer-below-selected)))
  :config
  ;; Decrypt to load session at Emacs startup beginning to avoid pause prompt.
  (my/json-read-value my/account-file 'ejc-sql-postgresql))

;;; [ redacted ] -- redacted-mode is an Emacs minor mode to hide your current text.

(use-package redacted
  :ensure t
  :defer t
  :commands (redacted-mode))



(provide 'init-emacs-encrypt)

;;; init-emacs-encrypt.el ends here
