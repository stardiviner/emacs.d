;;; init-emacs-completion.el --- my Emacs completion frameworks init

;;; Commentary:


;;; Code:

;;; [ completion ] -- *Completion* buffer

;;; Usage:
;;
;; - `completion-at-point-functions' is a special hook.
;;    add a completion command into it with mode locally.
;;    (add-hook 'completion-at-point-functions 'completion-function nil t)

;; NOTE: The `completion-styles' is used by `completion-at-point-functions', `company-capf', `ivy', `completing-read' etc.
;; So modify it will affect all places.
;; The available styles are listed in `completion-styles-alist'.
;; [[info:emacs#Completion Styles][emacs#Completion Styles]]
(setq-default completion-styles '(basic partial-completion emacs22))
(setq-default completion-category-overrides
              '((command (styles . (partial-completion substring shorthand)))
                (file (styles . (partial-completion substring)))
                (buffer (styles . (partial-completion substring)))
                (project-file (styles . (partial-completion substring)))
                (bookmark (styles . (substring partial-completion basic)))
                (unicode-name (styles . (substring partial-completion)))
                (info-menu (styles . (substring)))))
(setq-default completion-ignore-case t)

(setq history-delete-duplicates t)

;;; [ prescient ] -- Better sorting and filtering.

;; (use-package prescient
;;   :ensure t
;;   :init (setq-default completion-styles (add-to-list 'completion-styles 'prescient)))

;;; [ orderless ] -- Completion style for matching regexps in any order.

;; This package provides an `orderless' completion style that divides
;; the pattern into components (space-separated by default), and
;; matches candidates that match all of the components in any order.

(use-package orderless
  :ensure t
  :ensure pyim
  :init (setq-default completion-styles (add-to-list 'completion-styles 'orderless))
  :custom ((orderless-matching-styles
            '(orderless-literal
              orderless-prefixes
              orderless-initialism
              orderless-regexp
              ;; orderless-flex                       ; Basically fuzzy finding
              ;; orderless-strict-leading-initialism
              ;; orderless-strict-initialism
              ;; orderless-strict-full-initialism
              ;; orderless-without-literal          ; Recommended for dispatches instead
              ))
           (orderless-component-separator " +\\|[-/]") ; spaces, hyphen or slash.
           )
  :config
  ;; support PinYin matching
  (require 'pyim)
  (defun my/orderless-pinyin-filter (orig_func component)
    (when (minibufferp (current-buffer)) ; only apply matching in minibuffer, not in company etc.
      (let ((processed_input (funcall orig_func component)))
        ;; Don't pyim pinyin match on large text input to avoid Emacs suspend.
        (if (< (length processed_input) 10)
            (pyim-cregexp-build processed_input)
          processed_input))))
  (defun my/orderless-pinyin-filter-toggle ()
    "Toggle `my/orderless-pinyin-filter' advice in minibuffer by this command keybinding."
    (interactive)
    (if (advice-member-p 'my/orderless-pinyin-filter 'orderless-regexp)
        (progn
          (advice-remove  'orderless-regexp #'my/orderless-pinyin-filter)
          (set-cursor-color "GreenYellow")
          (message "\n    -> The orderless filter advice by pyim pinyin DISABLED!"))
      (advice-add 'orderless-regexp :around #'my/orderless-pinyin-filter)
      (set-cursor-color "DodgerBlue")
      (message "\n    -> The orderless filter advice by pyim pinyin ENABLED!")))
  (with-eval-after-load 'vertico
    (define-key vertico-map (kbd "M-\\") 'my/orderless-pinyin-filter-toggle))
  ;; Don't automatically enable this pinyin filter in minibuffer, and don't keep the changed advice state.
  (defun my/orderless-pinyin-filter-initialize ()
    "Don't enable the advice of `my/orderless-pinyin-filter' at first enter minibuffer."
    (when (advice-member-p 'my/orderless-pinyin-filter 'orderless-regexp)
      (advice-remove 'orderless-regexp #'my/orderless-pinyin-filter)
      (set-cursor-color "GreenYellow")))
  (add-hook 'minibuffer-mode-hook #'my/orderless-pinyin-filter-initialize)

  ;; `basic-remote': For `TRAMP' saved remote connections completion with `vertico'.
  (defun my/basic-remote-try-completion (string table pred point)
    (and (vertico--remote-p string)
         (completion-basic-try-completion string table pred point)))
  (defun my/basic-remote-all-completions (string table pred point)
    (and (vertico--remote-p string)
         (completion-basic-all-completions string table pred point)))
  (add-to-list 'completion-styles-alist
               '(basic-remote           ; Name of `completion-style'
                 my/basic-remote-try-completion my/basic-remote-all-completions nil))
  (add-to-list 'completion-category-overrides '(file (styles basic-remote orderless)))
  )



(provide 'init-emacs-completion)

;;; init-emacs-completion.el ends here
