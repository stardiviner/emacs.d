;;; init-emacs-navigation.el --- init Emacs Navigation.
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ scroll ]

(setq scroll-preserve-screen-position t
      scroll-margin 3
      scroll-conservatively 97
      make-cursor-line-fully-visible nil
      fast-but-imprecise-scrolling t)

;;; [ pixel-scroll-mode ] -- Scroll a line smoothly.

;;; Scrolling.
;; Good speed and allow scrolling through large images (pixel-scroll).
;; Note: Scroll lags when point must be moved but increasing the number
;;       of lines that point moves in pixel-scroll.el ruins large image
;;       scrolling. So unfortunately I think we'll just have to live with
;;       this.

(use-package pixel-scroll
  ;; :custom ((pixel-dead-time 0) ; Never go back to the old scrolling behaviour.
  ;;          (pixel-resolution-fine-flag t) ; Scroll by number of pixels instead of lines (t = frame-char-height pixels).
  ;;          (mouse-wheel-scroll-amount '(1)) ; Distance in pixel-resolution to scroll each mouse wheel event.
  ;;          (mouse-wheel-progressive-speed nil)) ; Progressive speed is too fast for me.
  :commands (pixel-scroll-mode pixel-scroll-precision-mode)
  :hook ((org-mode . pixel-scroll-mode)
         (org-mode . pixel-scroll-precision-mode)))

;; [ recenter ]

(setq recenter-positions '(top middle bottom))

;;; [ centered-cursor-mode ] -- cursor stays vertically centered.

;;; FIXME: centered-cursor-mode in minibuffer caused [C-v] remapped.
;; (use-package centered-cursor-mode
;;   :ensure t
;;   :defer t
;;   :commands (global-centered-cursor-mode centered-cursor-mode)
;;   :hook (after-init . global-centered-cursor-mode))

;;; [ movement ]

(setq track-eol t) ; always track end of line when moving at end of line.

;; set sentence-end to recognize chinese punctuation.
;; (setq sentence-end "\\([。！？]\\|……\\|[.?!][]\"')}]*\\($\\|[ \t]\\)\\)[ \t\n]*")


;;; [ Mark ] --- [C-SPC / C-@] + [C-u C-SPC / C-u C-@] + [C-`] / [M-`]

(setq set-mark-command-repeat-pop t)

(defun push-mark-no-activate ()
  "Pushes `point' to `mark-ring' and does not activate the region.
Equivalent to \\[set-mark-command] when \\[transient-mark-mode] is disabled"
  (interactive)
  (push-mark (point) t nil)
  (message "Pushed mark to ring"))

(global-set-key (kbd "C-`") 'push-mark-no-activate)

(defun jump-to-mark ()
  "Jumps to the local mark, respecting the `mark-ring' order.
This is the same as using \\[set-mark-command] with the prefix argument."
  (interactive)
  (set-mark-command 1))

(global-set-key (kbd "M-`") 'jump-to-mark)

;; (defun exchange-point-and-mark-no-activate ()
;;   "Identical to \\[exchange-point-and-mark] but will not activate the region."
;;   (interactive)
;;   (exchange-point-and-mark)
;;   (deactivate-mark nil))
;; (define-key global-map [remap exchange-point-and-mark] 'exchange-point-and-mark-no-activate)

(defadvice pop-to-mark-command (around ensure-new-position activate)
  "When popping the mark, continue popping until the cursor actually moves."
  (let ((p (point)))
    (dotimes (i 10)
      (when (= p (point)) ad-do-it))))


;;; [ ace-jump-mode -- Ace Jump Mode ]

(use-package ace-jump-mode
  :ensure t
  :defer t
  :delight ace-jump-mode
  :custom ((ace-jump-mode-scope 'window)
           ;; enable a more powerful jump back function from ace jump mode
           (ace-jump-sync-emacs-mark-ring t))
  :commands (ace-jump-mode)
  :bind (("C-'" . ace-jump-mode))
  :config (with-eval-after-load 'org (define-key org-mode-map (kbd "C-'") 'ace-jump-mode)))

(use-package ace-pinyin
  :ensure t
  :defer t
  :delight ace-pinyin-mode
  ;; :custom ((ace-pinyin-use-avy nil) ; keep using `ace-jump-mode' instead of `avy'.
  ;;          (ace-pinyin-simplified-chinese-only-p nil)) ; also support traditional chinese characters.
  :commands (ace-pinyin-dwim ace-pinyin-jump-word)
  :init (ace-pinyin-global-mode)
  :bind (([remap ace-jump-mode] . ace-pinyin-dwim))
  :config (with-eval-after-load 'org (define-key org-mode-map [remap ace-jump-mode] 'ace-pinyin-dwim)))

;;; [ Imenu ] -- [M-g i] [M-x imenu]

(use-package imenu
  :ensure t
  :custom (imenu-auto-rescan t)
  :init
  ;; rebind `imenu' [M-g i] keybinding to another Emacs completing interface command.
  (global-set-key [remap imenu] (cond
                                 (vertico-mode 'consult-imenu)
                                 (ivy-mode 'counsel-imenu)
                                 (helm-mode 'helm-imenu))))

;;; [ imenu-list ] -- Show imenu entries in a separate buffer.

(use-package imenu-list
  :ensure t
  :commands (imenu-list)
  :bind (([remap imenu] . imenu-list))
  :init (add-to-list 'display-buffer-alist '("^\\*Ilist*\\'" . (display-buffer-in-side-window))))


(provide 'init-emacs-navigation)

;;; init-emacs-navigation.el ends here
