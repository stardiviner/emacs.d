;;; init-emacs-popup.el --- init popup settings for Emacs.
;;
;;; Commentary:

;;; Code:

;;; use GTK+ tooltips for Emacs widget tooltips.
;; (setq x-gtk-use-system-tooltips nil) ; set to `nil' to fix face un-customized issue.

;;; [ tooltip ]

;;; [ popup ]

(use-package popup
  :ensure t
  :defer t
  :bind (:map popup-menu-keymap
              ("M-n" . popup-next)
              ("M-p" . popup-previous)
              ("M-j" . popup-select)))

;;; [ inline-docs ] -- show contextual docs with inline style.

;; (use-package inline-docs
;;   :ensure t
;;   :defer t
;;   :init (setq inline-docs-position 'above))

;;; [ quick-peek ] -- An inline pop-up library for Emacs Lisp.

;; (use-package quick-peek
;;   :ensure t
;;   :defer t
;;   :commands (quick-peek-hide)
;;   :init (add-hook 'post-command-hook 'quick-peek-hide))

;;; [ peek ] -- Enables you to quick peek marked region or definition.

(use-package peek
  :vc (:url "https://github.com/meow_king/peek")
  :custom
  ;; one line before the place found by `xref-find-definitions' will also appear in peek window 
  (peek-xref-surrounding-above-lines 1)
  (peek-overlay-position 'below)            ;; 'below, 'above
  ;; eldoc integration
  ;; (peek-enable-eldoc-message-integration t) ; `eldoc-message-function' integration
  ;; (peek-enable-eldoc-display-integration t) ; `eldoc-display-functons'  integration for Emacs version >= 28.1
  ;; eldoc message overlay at two lines below the point
  ;; It's recommended to set the eldoc message overlay below the point since the pop up of
  ;; the peek overlay may cause visual shaking
  ;; (peek-eldoc-message-overlay-position 1)
  :init (global-peek-mode 1)
  :config
  ;; Keybindings 
  ;; default keybindings in peek-mode-keymap
  (define-key peek-mode-keymap (kbd "M-n") 'peek-next-line)
  (define-key peek-mode-keymap (kbd "M-p") 'peek-prev-line)
  ;; replace `xref' [M-./,] keybinding
  (define-key esc-map "." #'peek-xref-definition-dwim)
  (define-key esc-map "," #'peek-xref-definition-dwim)
  ;; Besides making `peek-enable-eldoc-display-integration' to t, you may want
  ;; to remove other eldoc display functions.
  ;; (remove-hook 'eldoc-display-functions 'eldoc-display-in-buffer)
  )

;;; [ posframe ] -- Pop a posframe (just a child-frame) at point.

(use-package posframe
  :ensure t
  :defer t
  :config
  (with-eval-after-load 'lsp-mode
    (defun my/update-lsp-signature-posframe-color (theme)
      (setq lsp-signature-posframe-params
            (list :poshandler #'posframe-poshandler-point-bottom-left-corner-upward
                  :background-color (face-attribute 'tooltip :background)
                  :min-width 60
                  :height 20
                  :width 60
                  :border-width 5
                  :border-color (color-lighten-name (face-background 'default) 10))))
    (my/update-lsp-signature-posframe-color nil)
    (add-hook 'load-theme-after-hook #'my/update-lsp-signature-posframe-color 81)))

;;; [ popweb ] -- Show popup web window for Emacs.

;; (use-package popweb
;;   :vc (:url "git@github.com:manateelazycat/popweb.git")
;;   :init
;;   (add-to-list 'load-path (expand-file-name "~/Code/Emacs/popweb/"))
;;   (add-to-list 'load-path (expand-file-name "~/Code/Emacs/popweb/extension/"))
;;   (add-to-list 'load-path (expand-file-name "~/Code/Emacs/popweb/extension/dict/"))
;;   (add-to-list 'load-path (expand-file-name "~/Code/Emacs/popweb/extension/latex/"))
;;   (require 'popweb-dict-bing)
;;   (require 'popweb-dict-youdao)
;;   (require 'popweb-latex)
;;   :commands (popweb-latex-mode
;;              popweb-dict-bing-input popweb-dict-youdao-input
;;              popweb-dict-bing-pointer popweb-dict-youdao-pointer))


(provide 'init-emacs-popup)

;;; init-emacs-popup.el ends here
