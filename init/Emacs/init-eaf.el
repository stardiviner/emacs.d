;;; init-eaf.el --- init for emacs-application-framework -*- lexical-binding: t; -*-

;;; Commentary:



;;; Code:

;;; [ emacs-application-framework (eaf) ]

(use-package eaf
  :vc (:url "git@github.com:emacs-eaf/emacs-application-framework.git")
  :ensure epc
  :ensure deferred
  :ensure ctable
  :custom (;; (eaf-enable-debug t)
           (eaf-kill-process-after-last-buffer-closed nil)
           ;; (eaf-start-python-process-when-require t) ; 关闭EAF最后一个Buffer后不关闭Python进程
           ;; (eaf-browser-continue-where-left-off t) ; (require 'eaf) 的时候就启动Python进程
           (eaf-browser-enable-adblocker t))
  :init
  (require 'eaf-demo)
  (require 'eaf-mindmap)
  (require 'eaf-org-previewer)
  (require 'eaf-file-browser)
  (require 'eaf-airshare)
  (require 'eaf-file-sender)
  (require 'eaf-jupyter)
  (require 'eaf-browser)
  (require 'eaf-camera)
  (require 'eaf-video-player)
  (require 'eaf-image-viewer)
  (require 'eaf-file-manager)
  (require 'eaf-terminal)
  (require 'eaf-vue-demo)
  (require 'eaf-markdown-previewer)
  (require 'eaf-pdf-viewer)
  (require 'eaf-js-video-player)
  (require 'eaf-music-player)
  (require 'eaf-netease-cloud-music)
  (require 'eaf-system-monitor) 
  ;; [ `eaf-org' ] Org Mode integration
  ;; set overriding option before loading `eaf-org' to execute if condition.
  :config
  ;; advice on ‘find-file’ [C-x C-f] to open PDF/Epub files with ‘eaf-open’.
  (defun adviser-find-file (orig-fn file &rest args)
    (let ((fn (if (commandp 'eaf-open) 'eaf-open orig-fn)))
      (pcase (file-name-extension file)
        ("pdf"  (apply fn file nil))
        ("epub" (apply fn file nil))
        (_      (apply orig-fn file args)))))
  (advice-add 'find-file :around #'adviser-find-file)
  
  (add-to-list 'display-buffer-alist '("\\*eaf pdf outline\\*" . (display-buffer-below-selected)))
  
  (eaf-setq eaf-camera-save-path "~")

  ;; find-file
  ;; (setq eaf-find-file-ext-blacklist '("pdf"))

  ;; [ Dired ]
  (setq eaf-find-alternate-file-in-dired t)
  (define-key dired-mode-map (kbd "M-RET") 'eaf-open-this-from-dired)

  ;; [ web browser ]
  ;; (setq eaf-browser-continue-where-left-off t)
  
  ;; use EAF as default web browser for Emacs.
  (defun eaf-toggle-default-browser ()
    "Toggle overriding default web browser with EAF web browser."
    (interactive)
    (if (eq browse-url-browser-function 'eaf-open-browser)
        (progn
          (setq browse-url-browser-function 'browse-url-firefox)
          (message "Now revert default browser to your default function."))
      (setq browse-url-browser-function 'eaf-open-browser)
      (message "Now setting default browser to EAF Web Browser.")))
  ;; let `eaf-open-browser' support HiDPI screen
  (eaf-setq eaf-browser-default-zoom  "2")

  ;; EAF app follow Emacs theme
  (eaf-setq eaf-browser-dark-mode "follow")
  (eaf-setq eaf-pdf-dark-mode "ignore")
  
  ;; set EAF web browser proxy
  (defvar eaf-proxy-list '("socks5:127.0.0.1:1086" "http:127.0.0.1:8118"))
  (defun eaf-toggle-proxy (&optional proxy)
    "Toggle proxy for EAF."
    (interactive (if (and (null eaf-proxy-type) (null eaf-proxy-host))
                     (list (completing-read "Select Proxy: " eaf-proxy-list))
                   nil))
    (if proxy
        (let* ((list (split-string proxy ":"))
               (proxy-type (car list))
               (proxy-host (cadr list))
               (proxy-port (caddr list)))
          (setq eaf-proxy-type proxy-type
                eaf-proxy-host proxy-host
                eaf-proxy-port proxy-port))
      (setq eaf-proxy-type nil
            eaf-proxy-host nil
            eaf-proxy-port nil)))
  
  ;; exclude EAF buffers from `desktop-save-mode'.
  (with-eval-after-load 'desktop
    (add-to-list 'desktop-modes-not-to-save 'eaf-mode))

  ;; eaf-org
  (require 'eaf-org)
  (setq eaf-org-override-pdf-links nil)

  ;; eaf-pdf
  (eaf-bind-key scroll_up "C-n" eaf-pdf-viewer-keybinding)
  (eaf-bind-key scroll_down "C-p" eaf-pdf-viewer-keybinding)
  (eaf-bind-key take_photo "p" eaf-camera-keybinding)
  (eaf-bind-key nil "M-q" eaf-browser-keybinding)
  
  ;; eaf-interleave integration
  (add-hook 'eaf-pdf-viewer-hook 'eaf-interleave-app-mode)
  (add-hook 'eaf-browser-hook 'eaf-interleave-app-mode)
  (add-hook 'org-mode-hook 'eaf-interleave-mode)
  (setq eaf-interleave-org-notes-dir-list '("~/org/interleave/"))
  (setq eaf-interleave-split-direction 'vertical)
  (setq eaf-interleave-disable-narrowing t)
  (setq eaf-interleave-split-lines 20)
  (define-key eaf-interleave-mode-map (kbd "M-.") 'eaf-interleave-sync-current-note)
  (define-key eaf-interleave-mode-map (kbd "M-p") 'eaf-interleave-sync-previous-note)
  (define-key eaf-interleave-mode-map (kbd "M-n") 'eaf-interleave-sync-next-note)
  (define-key eaf-interleave-app-mode-map (kbd "C-c M-i") 'eaf-interleave-add-note)
  (define-key eaf-interleave-app-mode-map (kbd "C-c M-o") 'eaf-interleave-open-notes-file)
  (define-key eaf-interleave-app-mode-map (kbd "C-c M-q") 'eaf-interleave-quit)
  
  ;; eaf-elfeed
  (require 'eaf-elfeed)
  (with-eval-after-load 'elfeed
    ;; (define-key elfeed-search-mode-map (kbd "RET") 'eaf-elfeed-open-url)
    (define-key elfeed-show-mode-map (kbd "RET") 'eaf-elfeed-open-url))
  )



(provide 'init-eaf)

;;; init-eaf.el ends here
