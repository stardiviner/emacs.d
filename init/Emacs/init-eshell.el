;;; init-eshell.el --- init for Eshell

;;; Commentary:



;;; Code:

;;; [ Eshell ] (Emacs Shell)

(use-package eshell
  :ensure t
  :defer t
  :commands (eshell)
  :bind ("C-x !" . eshell)
  :preface (setenv "PAGER" "cat") ; change PAGER from `less' to `cat'.
  :custom ((eshell-visual-subcommands '(("git" "log" "diff" "show")))
           (eshell-prompt-function (lambda () (if (= (user-uid) 0) " # " " $ "))))
  :init (add-to-list 'display-buffer-alist '("^\\*eshell\\*\\'" . (display-buffer-below-selected)))
  :config
  ;; Jumping directories in Eshell.
  (defun eshell/z (&optional regexp)
    "Navigate to a previously visited directory in eshell, or to
any directory proferred by `consult-dir'."
    (let ((eshell-dirs (delete-dups
                        (mapcar 'abbreviate-file-name
                                (ring-elements eshell-last-dir-ring)))))
      (cond
       ((and (not regexp) (featurep 'consult-dir))
        (let* ((consult-dir--source-eshell `(:name "Eshell"
                                                   :narrow ?e
                                                   :category file
                                                   :face consult-file
                                                   :items ,eshell-dirs))
               (consult-dir-sources (cons consult-dir--source-eshell
                                          consult-dir-sources)))
          (eshell/cd (substring-no-properties
                      (consult-dir--pick "Switch directory: ")))))
       (t (eshell/cd (if regexp (eshell-find-previous-directory regexp)
                       (completing-read "cd: " eshell-dirs)))))))
  )

;;; [ eshell-toggle ] -- Show/hide eshell at the bottom of active window with directory of its buffer.

(use-package eshell-toggle
  :ensure t
  :defer t
  :commands (eshell-toggle)
  :bind ("C-x !" . eshell-toggle)
  :custom ((eshell-toggle-run-command "pwd")
           ;; (eshell-toggle-init-function 'eshell-toggle-init-eshell)
           ;; (eshell-toggle-default-directory "~")
           (eshell-toggle-use-projectile-root nil)
           (eshell-toggle-use-git-root t)
           (eshell-toggle-name-separator "/")))

;;; [ eshell-prompt-extras ] -- Display extra information and color for your eshell prompt.

(use-package eshell-prompt-extras
  :ensure t
  :custom ((eshell-highlight-prompt nil)
           (eshell-prompt-function 'epe-theme-lambda)))

;;; [ cape ] -- (Completion At Point Extensions)

(use-package corfu
  :ensure t
  ;; :hook (eshell-mode . corfu-mode)
  :config
  ;; Enable corfu + cape completing in Eshell.
  (defun my/eshell-completion-setup ()
    (cond
     (company-mode
      ;; (setq-local completion-at-point-functions '())
      (company-mode 1))
     (corfu-mode
      (setq-local completion-at-point-functions
                  (list #'pcomplete-completions-at-point
                        #'cape-file
                        (if (featurep 'yasnippet-capf)
                            #'yasnippet-capf
                          (cape-company-to-capf 'company-yasnippet))
                        #'cape-history
                        #'cape-emoji
                        #'cape-abbrev))
      (corfu-mode 1))))
  (add-hook 'eshell-mode-hook #'my/eshell-completion-setup))

;;; [ eshell-bookmark ] -- Integrate bookmarks with EShell.

(use-package eshell-bookmark
  :ensure t
  :defer t
  :after eshell
  :custom (counsel-bookmark-avoid-dired nil)
  ;; NOTE avoid `eshell-bookmark' open docker-tramp eshell buffer by `counsel'
  ;; with `counsel-find-file' instead of corresponding eshell bookmark buffer.
  :hook (eshell-mode . eshell-bookmark-setup))


(provide 'init-eshell)

;;; init-eshell.el ends here
