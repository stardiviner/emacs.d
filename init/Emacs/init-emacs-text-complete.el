;;; init-emacs-text-complete.el --- init for Programming Completion
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ completion-at-point-functions (capf) ]

;; (setq-default completion-at-point-functions '())

;;; [ pcomplete ] --- Programmable, Context-Sensitive Completion Library

(use-package pcomplete
  :defer t
  :custom (pcomplete-ignore-case t))

;;; [ external-completion ] -- Let external tools control completion style.

(use-package external-completion
  :ensure t)

;;; [ auto-complete ] -- Auto Completion for GNU Emacs

(use-package auto-complete
  :ensure t
  :defer t
  :commands (auto-complete-mode global-auto-complete-mode)
  :custom ((ac-delay 0.2)
           (ac-quick-help-delay 0.2))
  ;; :init (global-auto-complete-mode 1) ; use auto-complete globally
  :config
  ;; load `ac-source-yasnippet'
  (require 'auto-complete-config)
  ;; set default auto-complete source
  (setq-default ac-sources
                '(ac-source-yasnippet
                  ac-source-abbrev
                  ;; ac-source-dabbrev
                  ;; ac-source-dictionary
                  ac-source-words-in-same-mode-buffers))
  ;; auto raise popup menu
  (setq ac-auto-show-menu t)
  ;; custom keybindings
  (setq ac-use-menu-map t)
  (define-key ac-menu-map (kbd "M-j") 'ac-complete)
  ;; quick help
  (define-key ac-completing-map (kbd "M-h") 'ac-quick-help)
  (define-key ac-completing-map (kbd "C-M-n") 'ac-quick-help-scroll-down)
  (define-key ac-completing-map (kbd "C-M-p") 'ac-quick-help-scroll-up)

  ;; [ ac-capf ] -- auto-complete source of completion-at-point
  ;; (use-package ac-capf
  ;;   :ensure t
  ;;   :init (ac-capf-setup) ; global
  ;;   :config (add-to-list 'ac-sources 'ac-source-capf))
  )

;;; [ company ]

(use-package company
  :ensure t
  ;; :ensure company-emoji
  ;; :ensure company-tabnine
  :defer t
  :delight company-mode
  ;; disable initialize loading all company backends.
  :preface
  (setq company-backends nil)
  (setq company-global-modes '(not org-mode mu4e-compose-mode))
  :custom ((company-etags-modes nil)    ; disable `company-etags'
           (company-minimum-prefix-length 3)
           (completion-ignore-case t)   ; complete candidates ignore case-sensitive when typing.
           (company-idle-delay 0.2)
           (company-tooltip-idle-delay 0.2)
           (company-echo-delay 0)
           ;; decide when to auto commit insertion of the selected completion candidate.
           ;; (company-insertion-on-trigger t)
           ;; (company-insertion-triggers '(?\  ?\) ?.))
           (company-selection-wrap-around t)     ; loop over candidates
           (company-tooltip-align-annotations t) ; align annotations to the right tooltip border.
           (company-show-numbers nil)
           ;; (company-auto-update-doc t) ; company updates the doc-buffer whenever the selection changes.
           (company-search-regexp-function #'company-search-flex-regexp)
           (company-frontends '(company-pseudo-tooltip-unless-just-one-frontend
                                company-preview-if-just-one-frontend
                                company-echo-metadata-frontend))
           (company-backends '((;; company-emoji
                                ;; company-tabnine
                                company-capf ; `completion-at-point-functions'
                                :separate company-yasnippet
                                ;; :separate company-tempo  ; tempo: flexible template insertion
                                :separate company-keywords)
                               (company-dabbrev-code :with company-abbrev)
                               company-files))
           (company-dabbrev-other-buffers t)   ; only from same major-mode
           (company-dabbrev-code-everywhere t) ; offer completions in comments and strings
           )
  ;; :hook (;; (after-init . global-company-mode)
  ;;        (prog-mode . company-mode))
  :init (add-to-list 'display-buffer-alist '("^\\*company-documentation\\*" . (display-buffer-below-selected)))
  :config  
  (defun my-company-add-backend-locally (backend)
    "Add a backend in my custom way.

(add-hook 'major-mode-hook
            #'(lambda (my-company-add-backend-locally 'company-backend)))"
    (unless (eq (if (listp (car company-backends))
                    (caar company-backends)
                  (car company-backends))
                backend)
      ;; (setq-local company-backends (cl-adjoin 'company-capf company-backends :test #'equal))
      (setq-local company-backends
                  (cons
                   (list backend :with (if (listp (car company-backends))
                                           (caar company-backends)
                                         (car company-backends)))
                   (cdr company-backends)))))

  ;; [ company-ispell ]
  (use-package company-ispell
    :config
    ;; hide `company-ispell' echo message "Starting 'look' process".
    (use-package shut-up
      :ensure t
      :init
      (defun ispell-silent (orig-func &rest args)
        "Silent ispell lookup words message."
        (shut-up (apply orig-func args)))
      (advice-add 'ispell-lookup-words :around 'ispell-silent))
    
    ;; only enable `company-ispell' in `prog-mode' string or comments.
    (defun company-ispell-only-in-prog-comments (origin-func &rest args)
      (interactive)
      (when (and (derived-mode-p 'prog-mode)
                 (company-in-string-or-comment))
        (apply origin-func args)))
    (advice-add 'company-ispell-available :around #'company-ispell-only-in-prog-comments))
  
  ;; [ company-spell ] -- spell checking provided by your terminal via company.
  
  (use-package company-spell
    :ensure t
    :custom ((company-spell-command "aspell")
             (company-spell-args "-a soundslike"))
    ;; :config (add-to-list 'company-backends  'company-spell t)
    )
  
  ;; [ keybindings ]

  ;; (global-set-key (kbd "<tab>") 'company-indent-or-complete-common)
  ;; (define-key company-mode-map (kbd "<tab>") 'company-complete)
  ;; (define-key company-mode-map (kbd "TAB") 'company-complete)
  ;; (define-key company-mode-map [tab] 'company-complete)
  (define-key company-mode-map (kbd "C-M-i") 'company-complete)
  (define-key company-mode-map (kbd "M-<tab>") 'company-complete)

  ;; yasnippet
  ;; `yas-expand', `yas-expand-from-trigger-key'
  (define-key company-active-map [tab] 'yas-expand-from-trigger-key)

  ;; navigation
  (define-key company-active-map "\t" nil)
  (define-key company-active-map [tab] nil)
  (define-key company-active-map (kbd "<tab>") nil)
  (define-key company-active-map (kbd "<S-tab>") nil)
  (define-key company-active-map (kbd "M-j") 'company-complete-selection)
  (define-key company-active-map (kbd "M-i") 'company-complete-common)
  (define-key company-active-map (kbd "M-g") 'company-abort)
  (define-key company-active-map (kbd "M-h") 'company-show-doc-buffer)
  (define-key company-active-map (kbd "M-l") 'company-show-location)
  ;; (setq company-search-regexp-function #regexp-quote)
  (define-key company-active-map (kbd "M-s") 'company-search-candidates)
  (define-key company-active-map (kbd "C-M-s") 'company-filter-candidates)
  ;; nested searching map.
  (define-key company-search-map (kbd "C-g") 'company-search-abort)
  (define-key company-search-map (kbd "M-g") 'company-search-toggle-filtering)
  (define-key company-search-map (kbd "M-s") 'company-search-repeat-forward)
  (define-key company-search-map (kbd "M-r") 'company-search-repeat-backward)
  (define-key company-search-map (kbd "M-n") 'company-search-repeat-forward)
  (define-key company-search-map (kbd "M-p") 'company-search-repeat-backward)
  (define-key company-search-map (kbd "M-o") 'company-search-kill-others)
  (define-key company-search-map (kbd "M-j") 'company-complete-selection)

  ;; [ company-yasnippet ]
  ;; make `company-yasnippet' work for prefix like `%link_to'.
  ;; (setq-default yas-key-syntaxes (list "w_" "w_." "w_.()"
  ;;                                      #'yas-try-key-from-whitespace))
  
  ;; (defun company-mode/backend-with-yas (backend)
  ;;   (if (or (not company-mode/enable-yas) (and (listp backend) (member 'company-yasnippet backend)))
  ;;       backend
  ;;     (append (if (consp backend) backend (list backend))
  ;;             '(:with company-yasnippet))))
  ;;
  ;; (setq company-backends (mapcar #'company-mode/backend-with-yas company-backends))
  
  ;; [ company-transformers ]
  ;; NOTE: disable customize `company-transformers' to fix python-mode company candidates
  ;; sorting.
  (setq company-transformers '(company-sort-by-backend-importance
                               company-sort-prefer-same-case-prefix
                               ;; company-sort-by-occurrence
                               ))
  ;; (add-to-list 'company-transformers #'delete-dups)
  
  ;; [ enable company-mode in minibuffer ]
  ;; (defun company-mode-minibuffer-setup ()
  ;;   "Enable company-mode in minibuffer."
  ;;   (company-mode 1)
  ;;   (setq-local company-tooltip-limit 4)
  ;;   (setq-local company-tooltip-minimum 1))
  ;; (add-hook 'eval-expression-minibuffer-setup-hook 'company-mode-minibuffer-setup) ; [M-:]
  ;; (add-hook 'minibuffer-setup-hook 'company-mode-minibuffer-setup) ; [M-!] NOTE: should not enable this hook, it will apply in ALL minibuffer.

  ;;; [ company-quickhelp ] -- Popup documentation for completation candidates.
  (use-package company-quickhelp
    :ensure t
    :after company
    :custom ((company-quickhelp-use-propertized-text t)
             (company-quickhelp-delay 0.01)) ; set to `nil' to trigger popup doc manually.
    :hook (company-mode . company-quickhelp-mode)
    :init (add-to-list 'company-frontends 'company-quickhelp-frontend)
    (define-key company-active-map (kbd "M-h") 'company-quickhelp-manual-begin))
  )

;; [ company-tabnine ] -- A company-mode backend for TabNine, the all-language autocompleter.

(use-package tabnine
  :ensure t
  ;; NOTE: `tabnine' & `company-tabnine' caused `magit' (`transient') "branch([b])" error.
  ;; :vc (:url "https://github.com/shuxiao9058/tabnine")
  :commands (tabnine-install-binary
             tabnine-mode
             tabnine-start-process tabnine-restart-server tabnine-kill-process
             tabnine-complete)
  :diminish "⌬"
  :custom ((tabnine-wait 0.5)
           (tabnine-minimum-prefix-length 0)
           (tabnine-executable-args (list "--log-level" "Error" "--no-lsp" "false")))
  :init (add-to-list 'completion-at-point-functions #'tabnine-completion-at-point)
  :hook ((on-first-input . tabnine-start-process)
         (kill-emacs . tabnine-kill-process)
         ;; ((prog-mode vterm-mode) . tabnine-mode)
         )
  :bind
  (:map tabnine-mode-map
        ;; ("C-TAB"   . tabnine-accept-completion-by-word)
        ;; ("C-<tab>" . tabnine-accept-completion-by-word)
        ("TAB"   . tabnine-accept-completion)
        ("<tab>" . tabnine-accept-completion))
  (:map tabnine-completion-map
        ("M-f"        . tabnine-accept-completion-by-word)
        ("M-<return>" . tabnine-accept-completion-by-line)
        ("C-g" . tabnine-clear-overlay)
        ("M-[" . tabnine-previous-completion)
        ("M-]" . tabnine-next-completion)
        ;; ("C-n" . tabnine-next-completion)
        ;; ("C-p" . tabnine-previous-completion)
        )
  :config
  (use-package company-tabnine
    :ensure t
    :defer t
    :after company
    :custom (company-tabnine-max-restart-count 3)
    ;; :config
    ;; only enable `company-tabnine' in some modes.
    ;; (defun company-tabnine-enable ()
    ;;   (make-local-variable 'company-backends)
    ;;   (add-to-list 'company-backends 'company-tabnine))
    ;; (dolist (hook '(java-mode-hook
    ;;                 c-mode-hook c++-mode-hook 
    ;;                 python-mode-hook
    ;;                 js-mode-hook typescript-mode-hook
    ;;                 lisp-mode-hook emacs-lisp-mode-hook common-lisp-mode-hook))
    ;;   (add-hook hook #'company-tabnine-enable))
    )
  )

;;; [ corfu ] -- Completion Overlay Region FUnction.

(use-package corfu
  :ensure t
  ;; Use the `orderless' completion style.
  ;; Enable `partial-completion' for files to allow path expansion.
  ;; You may prefer to use `initials' instead of `partial-completion'.
  :ensure orderless
  :demand t
  :custom ((corfu-auto t)               ; enable auto trigger popup.
           (corfu-auto-prefix 2)
           ;; (corfu-auto-commands '()) ; auto trigger commands. Better use `add-to-list'.
           (corfu-separator ?\s)
           (corfu-cycle t)              ; optionally enable cycling for `corfu-next/previous'.
           ;; (corfu-sort-function 'corfu-sort-length-alpha)
           ;; A few more useful configurations...
           ;; TAB cycle if there are only few candidates
           (completion-cycle-threshold 3)
           ;; Enable indentation+completion using the TAB key.
           ;; Completion is often bound to M-TAB.
           (tab-always-indent 'complete)
           (corfu-bar-width 1.0)
           ;; `corfu-popupinfo'
           (corfu-popupinfo-resize nil)
           (corfu-popupinfo-max-height 30))

  :hook ((prog-mode . corfu-mode)
         (eshell-mode . corfu-mode)
         ;; setting `corfu-sort-function' to `corfu-history--sort'.
         (corfu-mode . corfu-history-mode)
         ;; candidate information popup for corfu.
         (corfu-mode . corfu-popupinfo-mode))
  ;; :init (corfu-global-mode)
  :bind (:map corfu-map
              ("TAB" . corfu-next)
              ([tab] . corfu-next)
              ([backtab] . corfu-previous)
              ("M-n" . corfu-next)
              ("M-p" . corfu-previous)
              ("<return>" . corfu-insert)
              ("M-j" . corfu-insert)
              ("M-d" . corfu-show-documentation)
              ("M-l" . corfu-show-location)
              ("M-g" . corfu-quit)
              ("<escape>" . corfu-quit)
              ("C-g" . corfu-quit)
              ("C-n" . (lambda ()
                         (interactive)
                         (corfu-quit)
                         (next-line)))
              ("C-p" . (lambda ()
                         (interactive)
                         (corfu-quit)
                         (previous-line)))
              ;; Interaction between corfu <-> orderless: treat `corfu-separator' (SPACE) as orderless separator.
              ;; It will input orderless field component separator. reference `orderless-component-separator'.
              ;; The command `corfu-insert-separator' is bound to keybinding [M-SPC] in `corfu-map' by default.
              ;; You can change it to [S-SPC] to avoid conflict with macOS global short [Opt-SPC].
              ("SPC" . corfu-insert-separator)
              ("M-g" . corfu-quit) ; quit corfu popup in upper SPC bind command `corfu-insert-separator'.
              ("s-SPC" . (lambda ()     ; quit and insert literal whitespace.
                           (interactive)
                           (corfu-quit)
                           (insert " ")))
              ;; ("SPC" . self-insert-command)
              ("M-h" . corfu-popupinfo-toggle)   ; manually trigger `corfu-popupinfo' for candidate.
              ("C-h" . corfu-info-documentation) ; manually trigger `corfu-info' for candidate.
              )

  :config
  ;; completing with `corfu' in the `eval-expression' minibuffer.
  (defun corfu-enable-in-eval-expression-minibuffer ()
    "Enable Corfu in the [M-:] eval-expression minibuffer if `completion-at-point' is bound."
    (when (where-is-internal #'completion-at-point (list (current-local-map)))
      (setq-local corfu-auto t)         ; Enable/disable auto completion
      (setq-local corfu-auto-prefix 1)
      (setq-local corfu-echo-delay nil ;; Disable automatic echo and popup
                  corfu-popupinfo-delay nil)
      (setq-local completion-at-point-functions (list #'cape-file
                                                      #'cape-elisp-symbol
                                                      #'cape-keyword
                                                      #'cape-history
                                                      #'cape-abbrev #'cape-dabbrev))
      (when (local-variable-p 'completion-at-point-functions)
        (corfu-mode 1))))
  (add-hook 'eval-expression-minibuffer-setup-hook #'corfu-enable-in-eval-expression-minibuffer)

  ;; completing with `corfu' in the minibuffer.
  ;; (defun corfu-enable-in-minibuffer ()
  ;;   "Enable Corfu in the minibuffer."
  ;;   )
  ;; (add-hook 'minibuffer-setup-hook #'corfu-enable-in-minibuffer)

  ;; [ cape ] -- Completion At Point Extensions.

  ;; Let your completions fly! This package provides additional completion
  ;; backends in the form of Capfs (completion-at-point-functions).
  ;;
  ;; `cape-abbrev': Complete abbreviation (add-global-abbrev, add-mode-abbrev).
  ;; `cape-dabbrev': Complete word from current buffers.
  ;; `cape-dict': Complete word from dictionary file.
  ;; `cape-elisp-block': Complete Elisp in Org or Markdown code block.
  ;; `cape-elisp-symbol': Complete Elisp symbol.
  ;; `cape-emoji': Complete Emoji, e.g. :smiley:
  ;; `cape-file': Complete file name.
  ;; `cape-history': Complete from Eshell, Comint or minibuffer history.
  ;; `cape-keyword': Complete programming language keyword.
  ;; `cape-line': Complete entire line from file.
  ;; `cape-rfc1345': Complete Unicode char using RFC 1345 mnemonics.
  ;; `cape-sgml': Complete Unicode char from SGML entity, e.g., &alpha.
  ;; `cape-tex': Complete Unicode char from TeX command, e.g. \hbar.
  
  (use-package cape
    :ensure t
    ;; [ yasnippet-cape ] -- A simple capf (Completion-At-Point Function) for completing yasnippet snippets.
    :ensure yasnippet-capf
    :config
    ;; Setting `completion-at-point-functions', used by `completion-at-point'.
    (setq-default completion-at-point-functions
                  (list
                   #'cape-file
                   (if (featurep 'yasnippet-capf)
                       #'yasnippet-capf
                     (cape-company-to-capf 'company-yasnippet))
                   #'cape-keyword
                   #'cape-sgml
                   #'cape-tex
                   #'cape-emoji
                   #'cape-abbrev
                   ;; #'cape-line
                   )))

  ;; set corfu popup frame background color.
  ;; (set-frame-parameter corfu--frame 'background-color (face-attribute 'default :background nil 'default))
  ;; (add-to-list 'corfu--frame-parameters `(background-color . ,(face-attribute 'default :background nil 'default)))
  ;; (add-to-list 'corfu--buffer-parameters `(background-color . ,(face-attribute 'default :background nil 'default)))
  )

;;; [ corfu-prescient ] -- provides an interface for using prescient.el to sort and filter candidates in Corfu menus.

(use-package corfu-prescient ; [M-s] prefix for `prescient' filters on `corfu-map'
  :if (featurep 'prescient)
  :ensure t
  :hook (corfu-mode . corfu-prescient-mode))

;;; [ nerd-icons-corfu ] -- Icons for corfu via nerd-icons.

(use-package nerd-icons-corfu
  :ensure t
  :config (add-to-list 'corfu-margin-formatters #'nerd-icons-corfu-formatter))


(provide 'init-emacs-text-complete)

;;; init-emacs-text-complete.el ends here
