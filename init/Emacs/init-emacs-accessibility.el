;;; init-emacs-accessibility.el --- init for Accessibility
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ talonscript-mode ] -- Major mode for Talon Voice's .talon files.

(use-package talonscript-mode
  :ensure t
  :mode ("\\.talon\\'" . talonscript-mode))


(provide 'init-emacs-accessibility)

;;; init-emacs-accessibility.el ends here
