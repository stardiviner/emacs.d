;;; init-emacs-search-wgrep.el --- init for wgrep.
;;; -*- coding: utf-8 -*-

;;; Commentary:

;;; writable grep or other search engine result buffer which can apply changes to files.

;;; Code:

;;; [ wgrep ] -- writable grep buffer and apply the changes to files.

(use-package wgrep
  :ensure t
  :defer t
  :commands (wgrep-change-to-wgrep-mode)
  :custom ((wgrep-enable-key (kbd "C-c C-p"))
           (wgrep-auto-save-buffer nil))
  :config (advice-add 'wgrep-finish-edit :after #'wgrep-save-all-buffers))


(provide 'init-emacs-search-wgrep)

;;; init-emacs-search-wgrep.el ends here
