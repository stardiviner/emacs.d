;;; wolfram-console.el --- Wolfram REPL comint support -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Authors: stardiviner <numbchild@gmail.com>
;; Package-Requires: ((emacs "26.1"))
;; Version: 0.1
;; Keywords: org
;; Homepage: 
;; SPDX-License-Identifier: GPL-2.0-only

;; Copyright (C) 2022-2023 Free Software Foundation, Inc.
;;
;; ob-wolfram is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; ob-wolfram is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
;; License for more details.
;;
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;; This package source code is based on GitHub repo:
;; https://github.com/ryanswilson59/ob-wolfram
;;
;; I added some new features, like:
;; - using "wolframscript" for Wolfram REPL instead of command "wolfram".
;; - support no session evaluating by running command "wolframscript -code <body>".
;; - adding complete org babel session related functions which referenced from ob-python.
;;
;; Usage
;;
;; (require 'ob-wolfram)
;;
;; Evaluate org-mode source block:
;;
;; #+begin_src wolfram :session
;; Print[2+2]
;; #+end_src
;;
;; You can specify header argument ":session" or not.

;;; Code:

(require 'comint)

(defgroup wolfram-console nil
  "REPL integration extention for `wolfram'"
  :group 'applications
  :group 'wolfram
  :prefix "wolfram-console-")


(defcustom wolfram-console-cmd-args '()
  "Arguments to be passed to the wolfram-console-cmd on start"
  :type 'string
  :group 'wolfram-console)

(defcustom wolfram-console-cmd-init-file nil
  "Full path to the file who's contents are sent to the
  wolfram-console-cmd on start

Should be NIL if there is no file not the empty string"
  :type 'string
  :group 'wolfram-console)

(defcustom wolfram-console-cmd "wolframscript"
  "Name of the executable used for the wolfram REPL session"
  :type 'string
  :group 'wolfram-console)

(defcustom wolfram-console-cmd-buffer-name "wolfram"
  "Name of the buffer which contains the wolfram-console-cmd session"
  :type 'string
  :group 'wolfram-console)

(defun wolfram-console-create-session ()
  "Starts a comint session wrapped around the wolfram-console-cmd"
  (setq comint-process-echoes t)
  (apply 'make-comint wolfram-console-cmd-buffer-name
         wolfram-console-cmd wolfram-console-cmd-init-file wolfram-console-cmd-args))
;;TODO add hooks?
;; (mapc
;;  (lambda ( comint-hook-sym )
;;    (let ((local-comint-hook-fn-sym
;;           (intern
;;            (replace-regexp-in-string
;;             "s$" "" (concat "j-console-" (symbol-name comint-hook-sym))))))
;;      (when (symbol-value local-comint-hook-fn-sym)
;;        (add-hook comint-hook-sym (symbol-value local-comint-hook-fn-sym)))))
;;  '(comint-input-filter-functions
;;    comint-output-filter-functions
;;    comint-preoutput-filter-functions))


(defun wolfram-console-ensure-session ()
  "Checks for a running wolfram-console-cmd comint session and either
  returns it or starts a new session and returns that"
  (or (get-buffer (format "*%s*" wolfram-console-cmd-buffer-name))
      (progn
        (wolfram-console-create-session)
        (get-buffer (format "*%s*" wolfram-console-cmd-buffer-name)))))



(provide 'wolfram-console)

;;; wolfram-console.el ends here
