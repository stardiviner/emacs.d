;;; zone-words.el --- Discover words to describe emotions while you zone out.

;;; Commentary:
;; Discover words to describe emotions while you zone out.

;;; Usage:
;;; - set zone to use zone-words with:
;;;   `(setq zone-programs [zone-words])'
;;;
;;; - Preview with [M-x zone-words-preview].

;;; Code:

(require 'zone)

;; From http://www.psychpage.com/learning/library/assess/feelings.html
(defvar zone-words-emotions-dictionary--unpleasant-feelings
  '("irritated" "lousy" "upset" "incapable" "enraged" "disappointed" "doubtful" "alone" "hostile" "discouraged" "uncertain" "paralyzed" "insulting" "ashamed" "indecisive" "fatigued" "sore" "powerless" "perplexed" "useless" "annoyed" "diminished" "embarrassed" "inferior" "upset" "guilty" "hesitant" "vulnerable" "hateful" "dissatisfied" "shy" "empty" "unpleasant" "miserable" "stupefied" "forced" "offensive" "detestable" "disillusioned" "hesitant" "bitter" "repugnant" "unbelieving" "despair" "aggressive" "despicable" "skeptical" "frustrated" "resentful" "disgusting" "distrustful" "distressed" "inflamed" "abominable" "misgiving" "woeful" "provoked" "terrible" "lost" "pathetic" "incensed" "in despair" "unsure" "tragic" "infuriated" "sulky" "uneasy" "cross" "bad" "pessimistic" "dominated" "worked up" "tense" "boiling" "fuming" "indignant" "insensitive" "fearful" "crushed" "tearful" "dull" "terrified" "tormented" "sorrowful" "nonchalant" "suspicious" "deprived" "pained" "neutral" "anxious" "pained" "grief" "reserved" "alarmed" "tortured" "anguish" "weary" "panic" "dejected" "desolate" "bored" "nervous" "rejected" "desperate" "preoccupied" "scared" "injured" "pessimistic" "cold" "worried" "offended" "unhappy" "disinterested" "frightened" "afflicted" "lonely" "lifeless" "timid" "aching" "grieved" "shaky" "victimized" "mournful" "restless" "heartbroken" "dismayed" "doubtful" "agonized" "threatened" "appalled" "cowardly" "humiliated" "quaking" "wronged" "menaced" "alienated" "wary"))

;; From http://www.psychpage.com/learning/library/assess/feelings.html
(defvar zone-words-emotions-dictionary--pleasant-feelings
  '("understanding" "great" "playful" "calm" "confident" "gay" "courageous" "peaceful" "reliable" "joyous" "energetic" "easy" "lucky" "liberated" "comfortable" "amazed" "fortunate" "optimistic" "pleased" "free" "delighted" "provocative" "encouraged" "sympathetic" "overjoyed" "impulsive" "clever" "interested" "gleeful" "free" "surprised" "satisfied" "thankful" "frisky" "content" "receptive" "important" "animated" "quiet" "accepting" "festive" "spirited" "certain" "kind" "ecstatic" "thrilled" "relaxed" "satisfied" "wonderful" "serene" "glad" "cheerful" "bright" "sunny" "blessed" "merry" "reassured" "elated" "jubilant" "loving" "concerned" "eager" "impulsive" "considerate" "affected" "keen" "free" "affectionate" "fascinated" "earnest" "sure" "sensitive" "intrigued" "intent" "certain" "tender" "absorbed" "anxious" "rebellious" "devoted" "inquisitive" "inspired" "unique" "attracted" "nosy" "determined" "dynamic" "passionate" "snoopy" "excited" "tenacious" "admiration" "engrossed" "enthusiastic" "hardy" "warm" "curious" "bold" "secure" "touched" "brave" "sympathy" "daring" "close" "challenged" "loved" "optimistic" "comforted" "re-enforced" "confident" "hopeful"))

(defun zone-words-emotions-dictionary-lookup-emotion ()
  (zone-words-emotions-dictionary--random-item
   (zone-words-emotions-dictionary--random-item
    (list
     zone-words-emotions-dictionary--pleasant-feelings
     zone-words-emotions-dictionary--unpleasant-feelings))))

(defun zone-words-emotions-dictionary--random-item (items)
  (nth (random (length items)) items))

(defvar zone-words-top-margin 2)
(defvar zone-words-left-margin 5)
(defvar zone-words-word-definition-margin 1)
(defvar zone-words-word-pause 10)
(defvar zone-words--word-lookup-func nil)
(defvar zone-words-word-word-wrap 75)

(defface zone-words--word-face
  '((((class color) (background light))
     :foreground "DarkGrey" :weight bold :height 2.0 :inherit variable-pitch)
    (((class color) (background dark))
     :foreground "white" :weight bold :height 2.5 :inherit variable-pitch)
    (t :height 1.5 :weight bold :inherit variable-pitch))
  "Face for title" :group 'zone-words)

;;;###autoload
(defun zone-words-preview ()
  "Previews `zone-words'."
  (interactive)
  (let ((zone-programs [zone-words]))
    (zone)))

(defun zone-words ()
  "Display words to describe your emotions while you zone out."
  (delete-other-windows)
  (setq mode-line-format nil)
  (setq-local cursor-type nil)
  (zone-fill-out-screen (window-width) (window-height))
  (while (not (input-pending-p))
    (delete-region (point-min) (point-max))
    (goto-char (point-min))
    (zone-words--insert-vertical-space zone-words-top-margin)
    (let ((term (zone-words--word-lookup)))
      (insert (zone-words-word--position-text (car term)))
      (add-text-properties (line-beginning-position) (point)
                           (list 'face 'zone-words--word-face))
      (zone-words--insert-vertical-space zone-words-word-definition-margin)
      (insert (zone-words-word--position-text (cdr term))))
    (zone-park/sit-for (point-min) zone-words-word-pause)))

(defun zone-words-word--position-text (text)
  "Indent and wrap TEXT using `zone-words-left-margin' and `zone-words-word-word-wrap'."
  (zone-words-word--indent-text
   zone-words-left-margin
   (zone-words-word--word-wrap text zone-words-word-word-wrap)))

(defun zone-words-word--indent-text (len text)
  "Indent by LEN characters, given TEXT."
  (replace-regexp-in-string
   "^"
   (zone-words-word--string-repeat " " len)
   text))

;; From s.el.
(defun zone-words-word--word-wrap (s len)
  "If S is longer than LEN, wrap the words with newlines."
  (with-temp-buffer
    (insert s)
    (let ((fill-column len))
      (fill-region (point-min) (point-max)))
    (buffer-substring-no-properties (point-min) (point-max))))

;; From s.el.
(defun zone-words-word--string-repeat (s num)
  "Make a string of S repeated NUM times."
  (let (ss)
    (while (> num 0)
      (setq ss (cons s ss))
      (setq num (1- num)))
    (apply 'concat ss)))

(defun zone-words--word-lookup ()
  "Look up a term and return a cons with term and definition."
  (if (functionp zone-words--word-lookup-func)
      (funcall zone-words--word-lookup-func)
    (let* ((word (zone-words-emotions-dictionary-lookup-emotion))
           (definition (if (executable-find "wn")
                           (shell-command-to-string (format "wn %s -over" word))
                         "\n\nFor the definition, you need wordnet installed  on your machine.\n\nInstall with:\n\nbrew install wordnet (Mac OS)\n\napt-get install wordnet (Linux)"
                         (user-error "[zone-words.el] Error: The 'WordNet' is not installed."))))
      (cons word definition))))

(defun zone-words--insert-vertical-space (n)
  "Insert vertical space (ie. N new lines)."
  (when (< n 1)
    (error "Argument must be positive"))
  (while (> n 0)
    (insert "\n")
    (setq n (- n 1))))

(provide 'zone-words)

;;; zone-words.el ends here
