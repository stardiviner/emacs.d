;;; org-mode-clocking-outside.el --- Display Org clocking outside of Emacs -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Christopher M. Miles, all rights reserved.

;;; Commentary:



;;; Code:

(require 'org-clock)

(defvar org-mode-clock-timer)
(defvar org-mode-clocking-outside--tempfile "~/.org-mode-clocking-outside.txt")


(defun org-mode-clocking-outside-write-to-tempfile ()
  "Save current Clockign to a file ~/.org-mode-clocking-outside.txt, so it can be read by org-mode-clocking-outside.py"
  (if org-clock-current-task
      (write-region (org-clock-get-clock-string) nil org-mode-clocking-outside--tempfile nil 'quiet)
    (write-region "Idle?" nil org-mode-clocking-outside--tempfile nil 'quiet)))

(defun org-mode-clocking-outside--clock-in ()
  "Function of hook on org-clock-in-hook to cancel the org-mode-clocking-outside timer."
  (setq org-mode-clock-timer (run-with-timer 10 10 'org-mode-clocking-outside-write-to-tempfile)))

(defun org-mode-clocking-outside--clock-out ()
  "Function of hook on org-clock-out-hook to write stopped message to outside file."
  (write-region "Idle?" nil org-mode-clocking-outside--tempfile nil 'quiet))

(add-hook 'org-clock-in-hook #'org-mode-clocking-outside--clock-in)
(add-hook 'org-clock-out-hook #'org-mode-clocking-outside--clock-out)



(provide 'org-mode-clocking-outside)

;;; org-mode-clocking-outside.el ends here
