;;; bionic-reading.el --- Bionic Reading minor mode. -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Free Software Foundation, Inc.
;;; AUTHOR: Xah Lee
;;; DATE: 2022-06-06
;;; URL: http://xahlee.info/emacs/emacs/elisp_bionic_reading.html
;;; MAINTAINER: stardiviner (numbchild@gmail.com)

;;; Commentary:

;;; Xah Lee created the `infu-bionic-reading-region' and
;;; `infu-bionic-reading-buffer' basic prototype code, then I added new code to
;;; make it as a package minor mode.

;;; Code:

(defvar bionic-reading-face nil "a face for `bionic-reading-region'.")

(setq bionic-reading-face 'bold)
;; 'bold
;; 'error
;; 'warning
;; 'highlight
;; or any value of M-x list-faces-display

(defun bionic-reading-buffer ()
  "Bold the first few chars of every word in current buffer."
  (bionic-reading-region (point-min) (point-max)))

(defun bionic-reading-region (Begin End)
  "Bold the first few chars of every word in region."
  (let (xBounds xWordBegin xWordEnd)
    (save-excursion
      (narrow-to-region Begin End)
      (goto-char (point-min))
      (while (forward-word)
        ;; bold the first half of the word to the left of cursor
        (setq xBounds (bounds-of-thing-at-point 'word))
        (setq xWordBegin (car xBounds))
        (setq xWordEnd (cdr xBounds))
        (setq xBoldEndPos (+ xWordBegin (1+ (/ (- xWordEnd xWordBegin) 2))))
        (put-text-property xWordBegin xBoldEndPos
                           'font-lock-face bionic-reading-face)))))

(defun bionic-reading-disable-region (Begin End)
  "Disable bionic reading in region."
  (let (xBounds xWordBegin xWordEnd)
    (save-excursion
      (narrow-to-region Begin End)
      (goto-char (point-min))
      (while (forward-word)
        ;; bold the first half of the word to the left of cursor
        (setq xBounds (bounds-of-thing-at-point 'word))
        (setq xWordBegin (car xBounds))
        (setq xWordEnd (cdr xBounds))
        (setq xBoldEndPos (+ xWordBegin (1+ (/ (- xWordEnd xWordBegin) 2))))
        (put-text-property xWordBegin xBoldEndPos
                           'font-lock-face nil)))))

(defun bionic-reading-disable-buffer ()
  "Bold the first few chars of every word in current buffer."
  (bionic-reading-disable-region (point-min) (point-max)))

(define-minor-mode bionic-reading-mode
  "Bionic Reading minor mode By Xah Lee."
  :init-value nil
  :lighter " Bionic-Reading"
  :group 'bionic-reading
  (if bionic-reading-mode
      (bionic-reading-buffer)
    (bionic-reading-disable-buffer)))


(provide 'bionic-reading)

;;; bionic-reading.el ends here
