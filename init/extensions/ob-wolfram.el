;;; ob-wolfram.el --- Add Org Mode Babel support for Wolfram -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Authors: stardiviner <numbchild@gmail.com>
;; Package-Requires: ((emacs "26.1"))
;; Version: 0.1
;; Keywords: org
;; Homepage: 
;; SPDX-License-Identifier: GPL-2.0-only

;; Copyright (C) 2022-2023 Free Software Foundation, Inc.
;;
;; ob-wolfram is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; ob-wolfram is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
;; License for more details.
;;
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;; This package source code is based on GitHub repo:
;; https://github.com/ryanswilson59/ob-wolfram
;;
;; I added some new features, like:
;; - using "wolframscript" for Wolfram REPL instead of command "wolfram".
;; - support no session evaluating by running command "wolframscript -code <body>".
;; - adding complete org babel session related functions which referenced from ob-python.
;;
;; Usage
;;
;; (require 'ob-wolfram)
;;
;; Evaluate org-mode source block:
;;
;; #+begin_src wolfram :session
;; Print[2+2]
;; #+end_src
;;
;; You can specify header argument ":session" or not.

;;; Code:

(require 'ob)

(defvar org-babel-tangle-lang-exts)
(add-to-list 'org-babel-tangle-lang-exts '("wolfram" . "wls"))

(defcustom org-babel-wolfram-command "wolframscript"
  "Command to run wolfram"
  :group 'org-babel
  :version "26.3"
  :package-version '(Org . "9.0")
  :type 'string)

(defvar org-babel-default-header-args:wolfram '())

(defun org-babel-expand-body:wolfram (body _params)
  "Expand Body according to PARAMS, return the expanded body. except not, this is a lazy
dumb implementation"
  body)

(defun org-babel-execute:wolfram (body params)
  "Execute a block of wolfram code BODY.
PARAMS are given by org-babel.
This function is called by `org-babel-execute-src-block'"
  (message "executing wolfram source code block")
  (let* ((sessionp (cdr (assq :session params)))
         (full-body (org-babel-expand-body:wolfram body params)))
    (org-babel-wolfram-initiate-session sessionp)
    (if (string= sessionp "none")
        (with-temp-buffer
          ;; e.g. (call-process "wolframscript" nil t nil "-code" "2+2")
          (call-process org-babel-wolfram-command nil t nil "-code" body)
          (buffer-string))
      (org-babel-wolfram-eval-string full-body))))

(defun org-babel-wolfram-eval-string (str)
  "Sends STR to the `wolfram-console-cmd' session and executes it."
  (let ((session (wolfram-console-ensure-session)))
    (with-current-buffer session
      (goto-char (point-max))
      (insert (format "%s\n" str))
      (let ((origin-point (point)))
        (comint-send-input)
        (sit-for .1)
        (let ((begin (save-excursion
                       (goto-char origin-point)
                       (forward-line 2)
                       (point)))
              (end (point-max)))
          (replace-regexp-in-string
           "^In\[[0-9]*\]:?=\s*\n" ""  ; clear comint prompt
           (buffer-substring-no-properties begin end)))))))

(defun org-babel-prep-session:wolfram (session params)
  "Prepare SESSION according to the header arguments in PARAMS.
VARS contains resolved variable references."
  ;; TODO:
  )

(defun org-babel-load-session:wolfram (session body params)
  "Load BODY into SESSION."
  ;; TODO:
  )

(defun org-babel-variable-assignments:wolfram (params)
  "Return a list of Wolfram statements assigning the block's variables."
  ;; TODO:
  )

(defun org-babel-wolfram-var-to-wolfram (var)
  "Convert an elisp value to a wolfram variable.
Convert an elisp value, VAR, into a string of Wolfram source code
specifying a variable of the same value."
  ;; TODO:
  )

(defvar org-babel-wolfram-buffers '((:default . "*wolfram*")))

(defun org-babel-wolfram-session-buffer (session)
  "Return the buffer associated with SESSION."
  (cdr (assoc session org-babel-wolfram-buffers)))

(defun org-babel-wolfram-initiate-session (&optional session)
  "Initiate a wolfram session.
SESSION is a parameter given by org-babel."
  (unless (string= session "none")
    (require 'wolfram-console)
    (wolfram-console-ensure-session)))



(provide 'ob-wolfram)

;;; ob-wolfram.el ends here
