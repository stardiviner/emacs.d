;;; init-tool-AI.el --- init file for AI tools -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Christopher M. Miles, all rights reserved.

;;; Commentary:



;;; Code:

(load "init-GPT")



(provide 'init-tool-AI)

;;; init-tool-AI.el ends here
