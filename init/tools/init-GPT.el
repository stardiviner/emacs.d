;;; init-GPT.el --- init for GPT tools -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Christopher M. Miles, all rights reserved.

;;; Commentary:



;;; Code:

(setq chatgpt-available? nil)

;;; [ chatgpt-shell ] -- Interaction mode for ChatGPT

(use-package chatgpt-shell
  :ensure t
  :if chatgpt-available?
  :custom (;; (chatgpt-shell-api-url-base "https://api.openai.com") ; NOTE: Don't use this because IP will ban your ChatGPT account.
           ;; (chatgpt-shell-openai-key (my/json-read-value my/account-file 'chatgpt-shell-openai-key))
           ;; (chatgpt-shell-api-url-base "https://chatapi-stardiviner.bb8bot.com")
           ;; (chatgpt-shell-openai-key (my/json-read-value my/account-file 'chatgpt-shell-openai-key-part))
           ;; (chatgpt-shell-api-url-base "https://chatapi.bb8bot.com")
           ;; (chatgpt-shell-openai-key (my/json-read-value my/account-file 'chatgpt-shell-openai-key))
           (chatgpt-shell-api-url-base "https://chat.int8.co")
           (chatgpt-shell-openai-key (my/json-read-value my/account-file 'chatgpt-shell-openai-key))
           (chatgpt-shell-additional-curl-options '("--proxy" "socks5h://127.0.0.1:7890")) ; specify proxy for chatgpt-shell invoked curl.
           (chatgpt-shell-request-timeout 10)
           (chatgpt-shell-logging t))
  :commands (chatgpt-shell)
  :init
  (add-to-list 'display-buffer-alist '("^\\*chatgpt\\*" . (display-buffer-below-selected)))
  (add-to-list 'display-buffer-alist '("^\\*chatgpt-log\\*" . (display-buffer-below-selected)))
  ;; [ ob-chatgpt-shell ] -- Org babel functions for ChatGPT evaluation
  (use-package ob-chatgpt-shell
    :ensure t
    :config (ob-chatgpt-shell-setup)
    (require 'tempo)
    (tempo-define-template "source block chatgpt"
                           '("#+begin_src chatgpt-shell" n
                             p n
                             "#+end_src")
                           "<chatgpt" 'org-tempo-tags)))

;; [ dall-e-shell ] -- Interaction mode for DALL-E
;; `dall-e-shell' is a comint-based DALL-E shell for Emacs.

(use-package dall-e-shell
  :ensure t
  :if chatgpt-available?
  :defer t
  ;; :custom (dall-e-shell-openai-key "")
  :commands (dall-e-shell)
  :init
  ;; [ ob-dall-e-shell ] -- Org babel functions for DALL-E evaluation
  (use-package ob-dall-e-shell
    :ensure t
    :config ; (ob-dall-e-shell-setup)
    (org-babel-do-load-languages 'org-babel-load-languages
                                 (append org-babel-load-languages
                                         '((dall-e-shell . t))))
    (add-to-list 'org-src-lang-modes '("dall-e-shell" . text))
    (require 'tempo)
    (tempo-define-template "source block dall-e"
                           '("#+begin_src dall-e-shell" n
                             p n
                             "#+end_src")
                           "<dalle" 'org-tempo-tags)))

;;; [ mind-wave ] -- 

;; (use-package mind-wave
;;   :vc (:url "git@github.com:manateelazycat/mind-wave.git")
;;   :commands (mind-wave-chat-mode
;;              mind-wave-chat-ask mind-wave-chat-ask-with-multiline mind-wave-chat-ask-send-buffer
;;              mind-wave-change-model
;;              mind-wave-summary-web mind-wave-summary-video
;;              mind-wave-explain-word mind-wave-explain-point
;;              mind-wave-explain-code mind-wave-comment-code
;;              mind-wave-generate-code
;;              mind-wave-refactory-code-with-input mind-wave-refactory-code
;;              mind-wave-translate-to-english))

;;; [ llm ] -- Interface to pluggable llm backends.

(use-package llm
  :ensure t
  :if chatgpt-available?
  :config
  ;; setting up providers
  ;; "Open AI"
  (require 'llm-openai)
  (setq llm-refactoring-provider (make-llm-openai :key (my/json-read-value my/account-file 'chatgpt-shell-openai-key)))
  (setq llm-warn-on-nonfree nil))

;;; [ ellama ] -- ollama client for Emacs. [C-c e] prefix

(use-package ellama
  :ensure t
  :ensure llm
  :custom ((ellama-keymap-prefix "C-c a")
           (ellama-user-nick "stardiviner") ; The user nick in logs.
           (ellama-language "Chinese") ; The language for Ollama translation `ellama-translate'. Default language is "English".
           (ellama-buffer-mode #'markdown-mode) ; The major mode for the Ellama logs buffer. Default mode is `markdown-mode'.
           (ellama-auto-scroll t)
           (ellama-chat-done-callback 'sound-voice-complete)
           (ellama-provider
            (progn
              (declare-function make-llm-ollama "llm-ollama")
              (require 'llm-ollama)
              (make-llm-ollama :chat-model "llama3" :embedding-model "llama3")))
           (ellama-providers ; [C-u M-x ellama-chat] to interactively select model.
            `(("default model" . ellama-provider)
		      ,(if (and ellama-ollama-binary (file-exists-p ellama-ollama-binary))
			       '("ollama model" . (ellama-get-ollama-local-model))))))
  :commands (ellama-provider-select
             ellama-chat ellama-ask-about ellama-ask-selection ellama-ask-line
             ellama-complete
             ellama-code-complete ellama-code-review ellama-code-improve ellama-code-add ellama-code-edit
             ellama-translate ellama-translate-buffer ellama-define-word
             ellama-summarize ellama-summarize-webpage
             ellama--cancel-current-request-and-quit)
  :init (add-to-list 'display-buffer-alist '("^ellama.*" . (display-buffer-reuse-window display-buffer-below-selected)))
  :config (ellama-setup-keymap))



(provide 'init-GPT)

;;; init-GPT.el ends here
