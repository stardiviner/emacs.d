;;; init-leetcode.el --- init for LeetCode

;;; Commentary:



;;; Code:

;;; [ leetcode.el ] -- An Emacs LeetCode client.

;; (use-package leetcode
;;   :vc (:url "https://github.com/kaiwk/leetcode.el")
;;   :ensure request-deferred
;;   :commands (leetcode)
;;   :config
;;   (setq leetcode-account "stardiviner"
;;         leetcode-password (my/json-read-value my/account-file 'leetcode)
;;         ;; `clojure' is not supported by LeetCode yet.
;;         leetcode-prefer-language "python3"))



(provide 'init-leetcode)

;;; init-leetcode.el ends here
