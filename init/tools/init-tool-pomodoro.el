;;; init-tool-pomodoro.el --- init for Pomodoro technique.
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ pomidor ] -- Pomidor is a simple and cool pomodoro technique timer.

(use-package pomidor
  :ensure t
  :custom ((pomidor-sound-tick nil)
           (pomidor-sound-tack nil))
  :commands (pomidor))

;;; [ pomm ] -- Yet another Pomodoro timer implementation.

(use-package pomm
  :ensure t
  :commands (pomm-transient))


(provide 'init-tool-pomodoro)

;;; init-tool-pomodoro.el ends here
