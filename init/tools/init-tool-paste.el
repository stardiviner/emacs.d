;;; init-tool-paste.el --- init Emacs paste tool

;;; Commentary:

;;; Code:


(unless (boundp 'paste-prefix)
  (define-prefix-command 'paste-prefix))
(define-key tools-prefix (kbd "p") 'paste-prefix)

(defun my/kill-with-linenum-wrap-for-paste (beg end)
  "Kill region selected code with line number and wrapped with unicode symbols."
  (interactive "r")
  (save-excursion
    (goto-char end)
    (skip-chars-backward "\n \t")
    (setq end (point))
    (let* ((chunk (buffer-substring beg end))
           (chunk (concat
                   (format "╭──────── #%-d ─ %s ──\n│ "
                           (line-number-at-pos beg)
                           (or (buffer-file-name) (buffer-name))
                           )
                   (replace-regexp-in-string "\n" "\n│ " chunk)
                   (format "\n╰──────── #%-d ─" 
                           (line-number-at-pos end)))))
      (kill-new chunk)))
  (deactivate-mark))

(define-key paste-prefix (kbd "k") 'my/kill-with-linenum-wrap-for-paste)

;;; [ ox-gist ] -- Easily share an Org-mode subtree as a gist. [C-c C-e G]

(use-package ox-gist
  :ensure t)

;;; [ ox-gfm ] -- Github Flavored Markdown Back-End for Org Export Engine. [C-c C-e g]

;;; convert selected region to Markdown and copy to clipboard for pasting
;;; on sites like GitHub, and Stack Overflow.
(use-package ox-gfm
  :ensure t
  :init
  (defun my/paste-org-to-gfm ()
    "Convert selected region to GitHub Flawed Markdown and copy to clipboard.
For pasting on sites like GitHub, and Stack Overflow."
    (interactive)
    (require 'ox-gfm)
    (unless (org-region-active-p) (user-error "No active region selected"))
    (gui-set-selection
     'CLIPBOARD
     (org-export-string-as
      (buffer-substring (region-beginning) (region-end))
      'gfm t
      '(:with-toc nil))))
  (define-key paste-prefix (kbd "m") 'my/paste-org-to-gfm))

(defun my/paste-org-to-md ()
  "Convert selected region to Markdown and copy to clipboard.
For pasting on sites like GitHub, and Stack Overflow."
  (interactive)
  (require 'ox-md)
  (unless (org-region-active-p) (user-error "No active region selected"))
  (gui-set-selection
   'CLIPBOARD
   (org-export-string-as
    (buffer-substring (region-beginning) (region-end))
    'md t
    '(:with-toc nil)))
  (deactivate-mark))

(define-key paste-prefix (kbd "M") 'my/paste-org-to-md)

;;; copy formatted text from org-mode to applications.
(defun my/paste-org-to-html ()
  "Export region to HTML, and copy it to the clipboard.
For pasting source code in Email."
  (interactive)
  (require 'ox-html)
  (unless (org-region-active-p) (user-error "No active region selected"))
  (gui-set-selection
   'CLIPBOARD
   (htmlize-region-for-paste (region-beginning) (region-end)))
  (deactivate-mark))

(define-key paste-prefix (kbd "h") 'my/paste-org-to-html)
;;; `htmlize-buffer' (convert current buffer into HTML output)
(define-key paste-prefix (kbd "H") 'htmlize-buffer)


;;; [ yagist ] -- Yet Another gist

(use-package yagist
  :ensure t
  :defer t
  :custom (yagist-github-token (my/json-read-value my/account-file 'yagist))
  :bind (:map paste-prefix
              ("p" . yagist-region-or-buffer)
              ("P" . yagist-region-or-buffer-private)
              ("r" . yagist-region)
              ("b" . yagist-buffer)
              ("l" . yagist-list))
  :config
  ;; Fix `yagist' detect major-mode issue.
  (defun yagist-anonymous-file-name-for-org-babel ()
    "Fix `yagist' detect major-mode issue."
    (let* ((ext (cdr (assoc
                      (replace-regexp-in-string "-mode" "" (symbol-name major-mode))
                      org-babel-tangle-lang-exts))
                ;; (cdr (assoc (nth 0 (org-babel-get-src-block-info)) org-babel-tangle-lang-exts))
                ))
      (setq buffer-file-name
            (concat (file-name-sans-extension (buffer-file-name)) (format ".%s" ext)))))
  (advice-add 'yagist-anonymous-file-name :before #'yagist-anonymous-file-name-for-org-babel))

;;; [ igist ] -- Work seamlessly with GitHub gists from Emacs.

(use-package igist
  :ensure t
  :custom (igist-current-user-name "stardiviner")
  :commands (igist-dispatch)
  :bind ((:map paste-prefix ("g" . igist-dispatch))
         (:map igist-list-mode-map ("M-o" . igist-dispatch))))

;;; [ carbon.now.sh ] -- a service that creates beautiful images of your code.

(use-package carbon-now-sh
  :ensure t
  :defer t
  :commands (carbon-now-sh)
  :bind (:map paste-prefix ("i" . carbon-now-sh)))

;;; [ linkode ] -- Send buffer or region code to linkode.org to generate an image.

(use-package linkode
  :ensure t
  :defer t
  :commands (linkode-region linkode-buffer))

;;; [ germanium ] -- Generate image from source code using germanium.

(use-package germanium
  :ensure t
  :defer t
  :custom ((germanium-completion-function 'completing-read))
  :commands (germanium-buffer-to-png germanium-region-to-png germanium-install)
  :bind (:map paste-prefix ("s" . germanium-region-to-png) ("S" . germanium-buffer-to-png)))

;;; [ 0x0 ] -- Upload file sharing to 0x0.st

(use-package 0x0
  :ensure t
  :commands (0x0-dwim             ; chooses the right one of the following, based on location or previous commands.
             0x0-upload-text      ; will upload the active region or if not existent the entire buffer
             0x0-upload-file      ; will upload a selected file
             0x0-upload-kill-ring ; uploads the content of the kill ring
             0x0-popup            ; creates and displays a buffer, lets you upload its contents later
             0x0-shorten-uri      ; prompts for a uri, shortens it
             ))


(provide 'init-tool-paste)

;;; init-tool-paste.el ends here
