;;; init-stack-overflow.el --- init for Stack Exchange
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ SX ] -- Stack Exchange

(use-package sx
  :ensure t
  :defer t
  :custom ((sx-question-mode-display-buffer-function #'pop-to-buffer)
           (sx-question-mode-recenter-line 2))
  :commands (sx-tab-all-questions sx-inbox sx-open-link sx-tab-unanswered-my-tags sx-ask sx-search)
  :init
  ;; manage popup windows
  (add-to-list 'display-buffer-alist '("^\\*question-list: All-Questions \\*$" . (display-buffer-below-selected)))
  (add-to-list 'display-buffer-alist '("^\\*sx-question\\*$" . (display-buffer-same-window)))
  (add-to-list 'display-buffer-alist '("^\\*sx draft answer.*\\*$" . (display-buffer-below-selected)))
  ;; (use-package texfrag ; show MathJax in HTML and sx.el
  ;;   :ensure t
  ;;   :hook (sx-question-mode . texfrag-mode))
  )


(provide 'init-stack-exchange)

;;; init-stack-exchange.el ends here
