;;; init-tool-translate.el --- init file for translate. -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Christopher M. Miles, all rights reserved.

;;; Commentary:



;;; Code:

(unless (boundp 'dictionary-prefix)
  (define-prefix-command 'dictionary-prefix))
(define-key tools-prefix (kbd "d") 'dictionary-prefix)

;;; [ english-teacher ] -- translate sentence following point.

;; (use-package english-teacher
;;   :vc (:url "git@github.com:loyalpartner/english-teacher.el.git")
;;   :commands (english-teacher-smart-translate)
;;   :hook ((Info-mode Man-mode Woman-Mode elfeed-show-mode eww-mode) . english-teacher-follow-mode))

;;; [ google-translate ] -- Emacs interface to Google Translate.

(use-package google-translate
  ;; :ensure t
  :load-path "~/Code/Emacs/google-translate/"
  :defer t
  :bind (:map dictionary-prefix
              ("t"   . google-translate-smooth-translate)
              ("C-t" . google-translate-at-point)
              ("M-t" . google-translate-query-translate)
              ("C-r" . google-translate-at-point-reverse)
              ("M-r" . google-translate-query-translate-reverse)
              ("C-b" . google-translate-buffer)
              ("C-p" . google-translate-paragraphs-overlay)
              ("M-p" . google-translate-paragraphs-insert))
  :custom ((google-translate-show-phonetic t)
           (google-translate-pop-up-buffer-set-focus t)
           (google-translate-default-source-language "auto") ; "auto", "en"
           ;; (google-translate-default-target-language "zh-CN") ; `google-translate-supported-languages'
           (google-translate-translation-directions-alist '(("en" . "zh-CN")
                                                            ("zh-CN" . "en")
                                                            ("zh-CN" . "ja")
                                                            ("zh-CN" . "ko"))) ; for `google-translate-smooth-translate' + [C-n/p]
           (google-translate-listen-program (executable-find "mpv"))
           (google-translate-pop-up-buffer-set-focus t)
           (google-translate-result-to-kill-ring t))
  :init (add-to-list 'display-buffer-alist '("^\\*Google Translate\\*" . (display-buffer-below-selected)))
  (require 'google-translate-smooth-ui)
  ;; translate web page URL link.
  (defun google-translate-webpage (url)
    "Translate web page URL and open in web browser."
    (interactive (list (read-from-minibuffer
                        "URL: "
                        (or (thing-at-point 'url)
                            (funcall interprogram-paste-function)))))
    (browse-url
     (format "http://translate.google.com/translate?js=n&sl=auto&tl=zh-CN&u=%s" url)))

  (define-key dictionary-prefix (kbd "u") 'google-translate-webpage)
  
  ;; support `google-translate' in `pdf-view-mode' buffer.
  (defun my/google-translate-mark-pdf-view-page (orig-func &rest args)
    (interactive)
    (let ((buffer (current-buffer)))
      (when (eq major-mode 'pdf-view-mode)
        (pdf-view-mark-whole-page))
      (apply orig-func args)
      (with-current-buffer buffer
        (deactivate-mark))))
  (advice-add 'google-translate-at-point :around #'my/google-translate-mark-pdf-view-page)
  
  ;; enable proxy for translate.google.com
  ;; (setq url-proxy-services '(("http"  . "127.0.0.1:7890")
  ;;                            ("https" . "127.0.0.1:7890")))
  )

;;; [ immersive-translate ]  -- Immersive Bilingual Translation

(use-package immersive-translate
  :ensure t
  :custom ((immersive-translate-backend 'baidu)
           (immersive-translate-baidu-appid "20190818000327398"))
  :commands (immersive-translate-auto-mode
             immersive-translate-buffer immersive-translate-paragraph
             immersive-translate-clear immersive-translate-abort)
  :hook
  (elfeed-show-mode . immersive-translate-setup)
  (nov-pre-html-render . immersive-translate-setup))

;;; [ ob-translate ] -- allows you to translate blocks of text within org-mode.

(use-package ob-translate
  :vc t :load-path "~/Code/Emacs/ob-translate"
  :defer t
  :commands (org-babel-execute:translate)
  :init
  ;; add translate special block into structure template alist.
  (with-eval-after-load 'org
    (add-to-list 'org-structure-template-alist '("t" . "translate")))
  :config
  (add-to-list 'org-babel-safe-header-args :src)
  (add-to-list 'org-babel-safe-header-args :dest)
  (add-to-list 'org-babel-load-languages '(translate . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages))

;;; [ org-translate ] -- Org-based translation environment.

(use-package org-translate
  :ensure t
  :defer t
  :commands (org-translate-mode))

;;; [ translate-mode ] -- Paragraph-oriented Emacs minor mode for doing translation jobs.

(use-package translate-mode
  :ensure t
  :defer t
  :commands (translate-select-reference-buffer
             translate-open-reference-file
             translate-toggle-highlight translate-sync-cursor-to-current-paragraph))



(provide 'init-tool-translate)

;;; init-tool-translate.el ends here
