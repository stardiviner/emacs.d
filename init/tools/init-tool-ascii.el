;;; init-tool-ascii.el --- init for ASCII
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

(unless (boundp 'ascii-prefix)
  (define-prefix-command 'ascii-prefix))
(define-key editing-prefix (kbd "a") 'ascii-prefix)


;;; [ figlet ]

(defun my-figlet-region (&optional b e)
  "Region select text, then execute command [M-x my-figlet-region]."
  (interactive "r")
  (shell-command-on-region b e "toilet" (current-buffer) t))

;;; [ helm-rage ] -- A Helm source for raging. Allows you to spice up your commit message with rage comics or various memes.

(use-package helm-rage
  :if (featurep 'helm)
  :ensure t
  :defer t
  :commands (helm-rage)
  :bind (:map ascii-prefix ("a" . helm-rage)))

;;; [ boxquote ] -- quote text with a semi-box.

(use-package boxquote
  :ensure t
  :defer t
  :commands (boxquote-narrow-to-boxquote-content
             boxquote-boxquote boxquote-unbox
             boxquote-text
             boxquote-region boxquote-unbox-region)
  :init
  (unless (boundp 'boxquote-prefix)
    (define-prefix-command 'boxquote-prefix))
  (define-key ascii-prefix (kbd "q") 'boxquote-prefix)
  :bind (:map narrow-map
              ("q" . boxquote-narrow-to-boxquote-content)
              :map boxquote-prefix
              ("q" . boxquote-boxquote)
              ("u" . boxquote-unbox)
              ("t" . boxquote-text)
              ("r" . boxquote-region)
              ("U" . boxquote-unbox-region)
              ("b" . boxquote-buffer)
              ("f" . boxquote-defun)
              ("c" . boxquote-shell-command)
              ("F" . boxquote-describe-function)
              ("K" . boxquote-describe-key)
              ("V" . boxquote-describe-variable)
              ("C-w" . boxquote-kill)
              ("C-y" . boxquote-yank)
              ("p" . boxquote-paragraph)))

;;; [ boxes ] -- ASCII boxes unlimited! Boxes is a command line filter program which draws ASCII art boxes around your input text.

(use-package boxes
  :ensure t
  ;; :ensure-system-package boxes
  :defer t
  :commands (boxes-command-on-region boxes-create boxes-remove)
  :init
  (unless (boundp 'boxes-prefix)
    (define-prefix-command 'boxes-prefix))
  (define-key ascii-prefix (kbd "b") 'boxes-prefix)
  :bind (:map boxes-prefix
              ("r" . boxes-command-on-region)
              ("b" . boxes-create)
              ("k" . boxes-remove)))


(provide 'init-tool-ascii)

;;; init-tool-ascii.el ends here
