;;; init-tool-NAS.el --- init file for NAS -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Christopher M. Miles, all rights reserved.

;;; Commentary:



;;; Code:

;;; [ Synology NAS ]

(defvar my/synology-nas-ip-address "192.168.31.252")

(defun my/synology-nas-file-station-open-path-for-current-buffer (&optional path)
  "Synology NAS File Station open path for current buffer."
  (interactive)
  (let ((path (or path default-directory)))
    (cl-flet ((url-construct (lambda (path)
                               ;; http://192.168.31.252:5000/?launchApp=SYNO.SDS.App.FileStation3.Instance&launchParam=openfile%3D%252FOrg%252FBeauty%252F&SynoToken=UJe2sTv2pFkCM
                               (concat (format "http://%s:5000/" my/synology-nas-ip-address)
                                       "?launchApp=SYNO.SDS.App.FileStation3.Instance"
                                       "&launchParam=openfile"
                                       (string-replace
                                        "%2F" "2F" ; "%3D%25%2FOrg%25%2FBeauty%25%2F" -> "%3D%252FOrg%252FBeauty%252F"
                                        (url-hexify-string ; "=%/Org%/Beauty%/" -> "%3D%25%2FOrg%25%2FBeauty%25%2F"
                                         (concat "="       ; -> "=%/Org%/Beauty%/"
                                                 (string-replace "/" "%/" path) ; "/Org/Beauty/" -> "%/Org%/Beauty%/"
                                                 )))
                                       ;; "&SynoToken=UJe2sTv2pFkCM"
                                       ))))
      (cond
       ;; mounted "~/Org/" Shared Folder
       ((string-match-p "/Org/" path)
        (let ((path-1 (string-replace
                       (cl-case system-type
                         (gnu/linux (concat "/home/" user-login-name))
                         (darwin (concat "/Users/" user-login-name)))
                       ""
                       (expand-file-name (or path default-directory)))))
          (when (string-match-p "/Org/" path)
            (browse-url
             (url-construct
              (if (f-directory? path)
                  (file-name-as-directory path-1) ; make sure path to be end with path separator "/".
                (file-name-directory path-1)))))))
       ;; macOS mounted Shared Folders under "/Volumes/"
       ((string-match-p "^/Volumes/.*" path)
        (let ((path (string-replace "/Volumes/" "" default-directory)))
          (browse-url (url-construct path))))))))

;; TEST: (my/synology-nas-file-station-open-path-for-current-buffer "/Users/stardiviner/Org/Beauty")

;;; [ QNAP NAS ]

(defvar my/qnap-nas-ip-address "192.168.31.251")

(defun my/qnap-nas-file-station-open-path-for-current-buffer (&optional path)
  "QNAP NAS File Station open path for current buffer."
  (interactive)
  (let ((path (or path default-directory)))
    (cond
     ;; mounted "~/Org/" Shared Folder
     ((string-match-p "/Org/" path)
      (let ((path (file-name-directory
                   (string-replace
                    (cl-case system-type
                      (gnu/linux (concat "/home/" user-login-name))
                      (darwin (concat "/Users/" user-login-name)))
                    ""
                    (expand-file-name
                     (or path default-directory))))))
        (when (string-match-p "/Org/" path)
          (browse-url
           ;; FIXME: does not support Chinese folder name in URL path.
           (concat (format "https://%s:5001" my/qnap-nas-ip-address)
                   (format "/cgi-bin/fm.html?path=%s" path))))))
     ;; macOS mounted Shared Folders like "/Volumes/Data/"
     ((string-match-p "^/Volumes/.*" path)
      (let ((path (string-replace "/Volumes/" "" default-directory)))
        (browse-url
         (concat (format "https://%s:5001" my/qnap-nas-ip-address)
                 (format "/cgi-bin/fm.html?path=%s" path))))))))

;; TEST: (my/qnap-nas-file-station-open-path-for-current-buffer "/Users/stardiviner/Org/Wiki/Things/QNAP/QNAP NAS/")




(provide 'init-tool-NAS)

;;; init-tool-NAS.el ends here
