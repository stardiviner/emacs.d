;;; init-pass.el --- init for pass

;;; Commentary:



;;; Code:

;;; [ password-store ] -- password store (pass) support.

(use-package password-store
  :ensure t)

;;; [ auth-password-store ] -- integrate Emacs' `auth-source' with `password-store'.

;; (use-package auth-password-store
;;   :ensure t
;;   :config
;;   ;; Emacs < 26
;;   ;; (auth-pass-enable)
;;   ;; Emacs >= 26
;;   (auth-source-pass-enable)
;;   )

;;; [ password-store-otp ] -- Password store (pass) OTP extension support.

(use-package password-store-otp
  :ensure t
  ;; :custom (password-store-otp-screenshots-path)
  )

;;; [ pass ] -- major mode for password-store.

(use-package pass
  :ensure t)

;;; [ passmm ] -- minor mode that uses `Dired' to display all password files from the password-store (pass).

(use-package passmm
  :ensure t)

;;; [ ivy-pass ] -- Ivy interface for pass.

(use-package ivy-pass
  :ensure t
  :commands (ivy-pass))



(provide 'init-pass)

;;; init-pass.el ends here
