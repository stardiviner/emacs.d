;;; init-tool-rss.el --- init for elfeed
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ elfeed ] -- An Emacs web feeds client

(use-package elfeed
  :ensure t
  :defer t
  ;; For debugging
  ;; :preface (setq elfeed-log-level 'debug)
  :commands (elfeed elfeed-update)
  :bind (:map elfeed-search-mode-map ("g" . elfeed-update))
  :custom ((elfeed-db-directory (expand-file-name ".elfeed" user-emacs-directory))
           ;; specify proxy for elfeed backend cURL.
           (elfeed-curl-extra-arguments '("--proxy" "socks5h://127.0.0.1:7890"))
           ;; elfeed startup initial tags
           (elfeed-initial-tags '(unread))

           (elfeed-feeds
            '(;; Programming
              ;; ("http://blog.stackoverflow.com/feed/" programming stackoverflow)
              ;; ("http://programmers.blogoverflow.com/feed/" programming stackoverflow)
              ;; Emacs
              ;; ("https://www.reddit.com/r/emacs/.rss" emacs reddit) ; Reddit r/Emacs
              ;; ("https://planet.emacslife.com/atom.xml" emacs planet) ; Planet Emacslife
              ;; ("https://www.reddit.com/r/orgmode/.rss" org-mode reddit) ; Reddit r/Org-mode
              ;; ("https://emacs-china.org/posts.rss" emacs china)  ; Emacs China 最新帖子
              ;; ("https://emacs-china.org/latest.rss" emacs china) ; Emacs China 最新话题
              ("https://sachachua.com/blog/feed/atom/" blog emacs)              ; Sacha Chua Blog
              ("http://feeds.feedburner.com/TheKitchinResearchGroup" blog emacs) ; The Kitchin Research Group
              ("https://200ok.ch/atom.xml" blog emacs)                           ; 200ok.ch
              ;; ("https://www.bytedude.com/feed.xml" blog emacs) ; ByteDude
              ;; Web
              ;; ("https://blog.mozilla.org/feed/" mozilla) ; Mozilla Blog
              ;; ("http://hacks.mozilla.org/feed/" mozilla web) ; Mozilla Hacks
              ;; macOS
              ;; Linux
              ;; ("https://www.linux.com/rss/feeds.php" linux) ; linux.com
              ;; ("http://lwn.net/headlines/newrss" linux) ; lwn.net
              ;; ("http://linux.cn/rss.xml" linux) ; Linux.cn
              ;; Arch Linux
              ;; ("https://archlinux.org/feeds/planet" linux) ; Arch Linux RSS
              ;; Kali Linux
              ("http://www.kali.org/feed/" kali linux subscribe webkit) ; Kali Linux Blog
              ;; DevOps
              ;; ("https://www.digitalocean.com/community/tutorials/feed" linux devops)
              ;; Geek News
              ("http://www.solidot.org/index.rss" solidot news) ; Solidot
              ;; ("http://slashdot.org/index.rss" slashdot news)
              ;; ("http://news.ycombinator.com/rss" hackernews programmer news)
              ;; ("http://reddit.com/.rss" reddit news)
              ;; ("http://feeds.howtogeek.com/HowToGeek" geek)
              ("https://fullcirclemagazine.org/index.xml" linux subscribe webkit) ; Full Circle Magazine (full site)
              ("https://fullcirclemagazine.org/magazines/index.xml" linux subscribe webkit) ; Full Circle Magazine (only magazines)
              ("https://fullcirclemagazine.org/podcasts/index.xml" linux subscribe podcast webkit) ; Full Circle Magazine (only podcast weekly news)
              ("https://fullcirclemagazine.org/news/index.xml" linux subscribe news webkit) ; Full Circle Magazine (news articles)
              ;; Common Lisp
              ;; ("http://planet.lisp.org/rss20.xml" lisp) ; Lisp Planet
              ;; Clojure
              ("https://insideclojure.org/feed.xml" clojure) ; Inside Clojure
              ;; ("http://planet.clojure.in/atom.xml" clojure) ; Planet Clojure
              ;; ("http://www.lispcast.com/feed" clojure) ; LispCast
              ("https://feeds.therepl.net/therepl" clojure) ; The REPL
              ;; ("https://www.reddit.com/r/Clojure/.rss" clojure reddit) ; subreddit "r/Clojure"
              ;; ("https://stackoverflow.com/feeds/tag?tagnames=clojure&sort=newest" clojure) ; StackOverflow Clojure feed
              ;; ("https://feeds.soundcloud.com/users/soundcloud:users:627190089/sounds.rss" ClojureScript podcast) ; ClojureScript Podcast
              ("http://blog.jayfields.com/feeds/posts/default" clojure blog)
              ("http://corfield.org/atom.xml" clojure blog)    ; Sean Corfield Blog
              ("https://www.spacjer.com/feed.xml" clojure blog) ; Rafal Spacjer Blog
              ("https://dragan.rocks/feed.xml" clojure blog)    ; dragan.rocks
              ;; PostgreSQL
              ;; ("https://planet.postgresql.org/rss20.xml" postgresql) ; Planet PostgreSQL
              ;; Programmer Blogs
              ("http://feed.williamlong.info/" blog news)                ; 月光博客
              ("http://www.ruanyifeng.com/blog/atom.xml" blog ruanyifeng webkit) ; 阮一峰
              ("https://writings.stephenwolfram.com/feed/" blog) ; Stephen Wolfram Writings (author of Mathematicas, Wolfram)
              ;; ("https://feedpress.me/thetechnium" blog webkit) ; The Technium - KK
              ;; ("https://manateelazycat.github.io/feed.xml" blog) ; manateelazycat 懒猫 王勇
              ("https://protesilaos.com/feeds/" blog) ; Protesilaos Stavrou
              ("https://overreacted.io/rss.xml" blog)
              ("https://nalaginrut.com/feed/atom" blog) ; Samson's Machete "穆垒"
              ("https://www.byvoid.com/zht/feed" blog)
              ;; ("https://shibumi.dev/posts/index.xml" blog) ; shibumi
              ("https://cireu.github.io/rss.xml" blog)     ; cireu
              ("http://feeds.feedburner.com/felixcat" blog) ; Felix (felixonmars)
              ;; ("http://blog.binchen.org/rss.xml" blog) ; chen bin
              ("http://feeds.feedburner.com/tkf" blog) ; Takafumi Arakaki
              ("https://karthinks.com/index.xml" blog) ; Karthinks
              ("https://scripter.co/index.xml" blog)   ; scripter.co
              ;; ("https://scripter.co/atom.xml" blog) ; scripter.co
              ("https://ciechanow.ski/atom.xml" blog) ; Bartosz Ciechanowski
              ("https://xenodium.com/rss.xml" blog emacs) ; Alvaro Ramirez's notes
              ;; Subscribe
              ;; ("https://github.com/blog/all.atom" github subscribe) ; GitHub Blog
              ("http://www.salttiger.com/feed/" ebook) ; Salttiger
              ("https://code.visualstudio.com/feed.xml" subscribe vscode webkit) ; Visual Studio Code
              ;; VPS
              ("https://blog.laoda.de/atom.xml" blog vps) ; 我不是咕咕鸽
              ;; Podcasts
              ;; ("https://feeds.pacific-content.com/commandlineheroes" programming) ; Command-line Heros podcast
              ("https://emacstalk.codeberg.page/podcast/index.xml" podcast emacs) ; EmacsTalk Podcast
              ;; ("https://lexfridman.com/feed/podcast/" podcast) ; Lex Fridman Podcast
              ("https://feeds.transistor.fm/thoughts-on-functional-programming-podcast-by-eric-normand" podcast clojure) ; Eric Normand
              ))
           )
  :init (advice-add 'elfeed :after #'elfeed-update) ; auto update after entering elfeed.
  ;; (run-at-time nil (* 4 60 60) #'elfeed-update) ; auto update elfeed every 4 hours.
  ;; FIXME: `elfeed' use `switch-to-buffer'.
  (add-to-list 'display-buffer-alist '("^\\*elfeed-search\\*" . (display-buffer-below-selected)))
  :config
  ;; (define-key elfeed-search-mode-map (kbd "#") 'elfeed-search-set-filter)

  (defalias 'elfeed-search-toggle-all-star ; [m], [*]
    (elfeed-expose #'elfeed-search-toggle-all 'star)
    "Toggle the `star' tag to all selected entries.")
  (define-key elfeed-search-mode-map (kbd "m") 'elfeed-search-toggle-all-star)

  ;; Auto close elfeed buffers when quit Emacs.
  (defun elfeed-quit ()
    "Close elfeed buffers."
    (interactive)
    (elfeed-db-save)
    (dolist (buffer '("*elfeed-log*" "*elfeed-search*" "*elfeed-entry*"))
      (when (buffer-live-p (get-buffer buffer))
        (with-current-buffer buffer
          (kill-buffer)))))
  (define-key elfeed-search-mode-map (kbd "q") 'elfeed-quit)
  (add-hook 'kill-emacs-hook #'elfeed-quit)

  ;; support Org Mode Capture template
  (defun my/org-capture-elfeed-title ()
    (with-current-buffer "*elfeed-entry*"
      (elfeed-entry-title elfeed-show-entry)))
  (defun my/org-capture-elfeed-date ()
    (with-current-buffer "*elfeed-entry*"
      (format-time-string
       "[%Y-%m-%d %a %H:%M]"
       (seconds-to-time (elfeed-entry-date elfeed-show-entry)))))
  (defun my/org-capture-elfeed-source ()
    (with-current-buffer "*elfeed-entry*"
      (elfeed-compute-base (elfeed-entry-feed-id elfeed-show-entry))))
  (defun my/org-capture-elfeed-url ()
    (with-current-buffer "*elfeed-entry*"
      (let* ((entry elfeed-show-entry)
             (feed (elfeed-entry-feed elfeed-show-entry))
             (base-url (and feed (elfeed-compute-base (elfeed-feed-url feed)))))
        (elfeed-entry-link entry))))
  (defun my/org-capture-elfeed-content ()
    (with-current-buffer "*elfeed-entry*"
      (let* ((content (elfeed-deref (elfeed-entry-content elfeed-show-entry)))
             (content-type (elfeed-entry-content-type elfeed-show-entry)))
        (when content
          (if (eq content-type 'html)
              (progn
                (unless (fboundp 'org-web-tools--html-to-org-with-pandoc)
                  (require 'org-web-tools))
                ;; (let ((org-web-tools-pandoc-sleep-time 5))
                ;;   (org-web-tools--html-to-org-with-pandoc content))
                (org-web-tools--html-to-org-with-pandoc content))
            (insert content))))))

  (with-eval-after-load 'org-capture
    (add-to-list 'org-capture-templates
                 `("r" ,(format "%s\tCapture elfeed RSS feed content to Org buffer for Org refile"
                                (nerd-icons-faicon "nf-fa-rss_square" :face 'nerd-icons-blue))
                   entry (file "")
                   "* %(my/org-capture-elfeed-title)
:PROPERTIES:
:URL:            %(my/org-capture-elfeed-url)
:SOURCE:         %(my/org-capture-elfeed-source)
:DATE(original): %(my/org-capture-elfeed-date)
:DATE: %u
:END:

%(my/org-capture-elfeed-content)"
                   :empty-lines 1 :jump-to-captured t
                   :refile-targets ((org-buffer-list :regexp . "Trend News")))
                 :append))

  ;; auto re-format elfeed entry org-capture buffer.
  (defun my/elfeed-format-org-capture-buffer ()
    "A helper command to Delete org-capture elfeed-entry ending backslash \\."
    (save-excursion
      (goto-char (point-min))
      (replace-string "\\" "")
      (replace-string " " " ")
      ;; using regex replace using \( \)\{2,\}  which means 2 or more consecutive spaces and replace that with 1 space.
      (replace-regexp "\\( \\)\\{2,\\}" " ")
      (org-mark-subtree)                      ; or `org-mark-element', `mark-whole-buffer'
      (call-interactively 'org-fill-paragraph) ; or `fill-paragraph'
      ))
  
  (add-hook 'org-capture-mode-hook #'my/elfeed-format-org-capture-buffer)

  ;; Open elfeed entry URL.
  ;; `elfeed-search-browse-url'
  (defun elfeed-show-browse-link (&optional url)
    (interactive "P")
    (browse-url (elfeed-entry-link elfeed-show-entry)))
  (define-key elfeed-show-mode-map (kbd "o") 'elfeed-show-browse-link)

  ;; Download link media with youtube-dl.
  (defun elfeed-youtube-dl (&optional use-generic-p)
    "Use youtube-dl to download the link media."
    (interactive "P")
    (let ((entries (elfeed-search-selected)))
      (cl-loop for entry in entries
               do (elfeed-untag entry 'unread)
               when (elfeed-entry-link entry)
               do (let ((default-directory "~/Downloads"))
                    (async-shell-command (format "youtube-dl %s" it))))
      (mapc #'elfeed-search-update-entry entries)
      (unless (use-region-p) (forward-line))))

  (define-key elfeed-search-mode-map (kbd "d") 'elfeed-youtube-dl)

  ;; display icons for elfeed tags.
  (defun nerd-icon-for-elfeed-tags (tags)
    "Generate Nerd Font icon based on elfeed TAGS, Returns default icon if no match."
    (cond ((member "youtube" tags)  (nerd-icons-faicon "nf-fa-youtube_play" :face '(:foreground "#FF0200")))
          ((member "instagram" tags) (nerd-icons-faicon "nf-fa-instagram" :face '(:foreground "#FF00B9")))
          ((member "emacs" tags) (nerd-icons-sucicon "nf-custom-emacs" :face '(:foreground "#9A5BBE")))
          ((member "github" tags) (nerd-icons-faicon "nf-fa-github" :face '(:foreground "black")))
          ((member "blog" tags) (nerd-icons-mdicon "nf-md-text_box_edit_outline" :face '(:foreground "green")))
          ((member "news" tags) (nerd-icons-mdicon "nf-md-newspaper_variant_outline" :face '(:foreground "black")))
          (t (nerd-icons-faicon "nf-fae-feedly" :face '(:foreground "#2AB24C")))))

  (defun my/elfeed-search-print-entry--better-default (entry)
    "Print ENTRY to the *elfeed-search* buffer with my custom displaying."
    (let* ((date (elfeed-search-format-date (elfeed-entry-date entry)))
           (date-width (car (cdr elfeed-search-date-format)))
           (title (concat (or (elfeed-meta entry :title)
                              (elfeed-entry-title entry) "")
                          ;; NOTE: insert " " for overlay to swallow
                          " "))
           (title-faces (elfeed-search--faces (elfeed-entry-tags entry)))
           (feed (elfeed-entry-feed entry))
           (feed-title (when feed (or (elfeed-meta feed :title) (elfeed-feed-title feed))))
           (tags (mapcar #'symbol-name (elfeed-entry-tags entry)))
           (tags-str (mapconcat (lambda (s) (propertize s 'face 'elfeed-search-tag-face)) tags ","))
           (title-width (- (frame-width)
                           ;; (window-width (get-buffer-window (elfeed-search-buffer) t))
                           date-width elfeed-search-trailing-width))
           (title-column (elfeed-format-column
                          title (elfeed-clamp
                                 elfeed-search-title-min-width
                                 title-width
                                 elfeed-search-title-max-width) :left))
           
           ;; Title/Feed ALIGNMENT
           (align-to-feed-pixel (+ date-width
                                   (max elfeed-search-title-min-width
                                        (min title-width elfeed-search-title-max-width)))))
      (insert (propertize date 'face 'elfeed-search-date-face) " ")
      (insert (propertize title-column 'face title-faces 'kbd-help title))
      (put-text-property (1- (point)) (point) 'display `(space :align-to ,align-to-feed-pixel))
      ;; (when feed-title (insert " " (propertize feed-title 'face 'elfeed-search-feed-face) " "))
      (when feed-title
        (insert " " (concat (nerd-icon-for-elfeed-tags tags) " ")
                (propertize feed-title 'face 'elfeed-search-feed-face) " "))
      (when tags (insert "(" tags-str ")"))))

  ;; Function to print entries into the *elfeed-search* buffer.
  (setq elfeed-search-print-entry-function #'my/elfeed-search-print-entry--better-default)
  )

;;; [ elfeed-summary ] -- Feed summary interface for elfeed.

;; (use-package elfeed-summary
;;   :ensure t
;;   :defer t
;;   :commands (elfeed-summary))

;;; [ elfeed-tube ] -- Youtube integration for Elfeed, the feed reader for Emacs.

;; (use-package elfeed-tube
;;   :ensure t
;;   :ensure elfeed-tube-mpv
;;   :after elfeed
;;   :demand t
;;   :custom ((elfeed-tube-auto-save-p nil) ;; t is auto-save (not default)
;;            (elfeed-tube-auto-fetch-p t) ;;  t is auto-fetch (default)
;;            (elfeed-tube-captions-languages '("zh-Hans" "en" "english (auto generated)")))
;;   :commands (elfeed-tube-add-feeds elfeed-tube-fetch)
;;   :bind (:map elfeed-show-mode-map
;;               ("F" . elfeed-tube-fetch)
;;               ([remap save-buffer] . elfeed-tube-save)
;;               :map elfeed-search-mode-map
;;               ("F" . elfeed-tube-fetch)
;;               ([remap save-buffer] . elfeed-tube-save))
;;   :config (elfeed-tube-setup))

;;; [ elfeed-webkit] -- Render elfeed entries in embedded webkit widgets.

(use-package elfeed-webkit
  :if (featurep 'xwidget-internal)
  :after elfeed
  :ensure t
  :custom (elfeed-webkit-auto-tags '("webkit"))
  :bind (:map elfeed-show-mode-map ("t" . elfeed-webkit-toggle))
  :init (elfeed-webkit-auto-enable-by-tag))

;;; [ elcast ] -- Play podcast within elfeed.

(require 'elcast)
(setq elcast-player-params '("--socks" "127.0.0.1:7890" "--verbose" "2" "--no-color"))

;;; [ elfeed-web ] -- Web Interface to Elfeed

(use-package elfeed-web
  :ensure t
  :custom (httpd-port "8081") ; default "8080" conflict with MPD.
  :commands (elfeed-web-start elfeed-web-stop))


(provide 'init-tool-rss)

;;; init-tool-rss.el ends here
