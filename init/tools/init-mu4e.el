;;; init-mu4e.el --- init for mu4e.

;;; Commentary:




;;; Code:

(defun my/mu4e-set-mu4e-mu-version ()
  "Use different version mu4e and mu on different platforms."
  (interactive)
  (cond
   ;; Use latest source code version after compiled.
   ((file-exists-p (expand-file-name "~/Code/Emacs/mu/build/mu/mu"))
    (setq mu4e-mu-binary (expand-file-name "~/Code/Emacs/mu/build/mu/mu"))
    (setq mu-version (string-trim (shell-command-to-string
                                   (format "%s --version | head -n 1 | cut -d ' ' -f 2"
                                           mu4e-mu-binary))))
    (setq mu4e-mu-version mu-version)
    (setq mu-mu4e-load-path (expand-file-name "~/Code/Emacs/mu/mu4e/")))
   ((and (eq system-type 'darwin) (file-exists-p "/opt/homebrew/bin/mu"))
    ;; Use macOS Homebrew installed version "mu".
    (setq mu4e-mu-binary "/opt/homebrew/bin/mu")
    (setq mu-version (string-trim (shell-command-to-string
                                   (format "%s --version | head -n 1 | cut -d ' ' -f 2"
                                           mu4e-mu-binary))))
    (setq mu4e-mu-version mu-version)
    (setq mu-mu4e-load-path (concat "/opt/homebrew/Cellar/mu/" mu-version "/share/emacs/site-lisp/mu/mu4e/")))))

(my/mu4e-set-mu4e-mu-version)

(use-package mu4e
  :load-path mu-mu4e-load-path
  ;; :preface (setq mu4e-mu-debug t)
  :preface (my/mu4e-set-mu4e-mu-version)
  :custom ((mail-user-agent 'mu4e-user-agent) ; use mu4e as default for compose [C-x m].
           (mu4e-user-mail-address-list '("numbchild@gmail.com"
                                          "stardiviner@icloud.com"
                                          "stardiviner@outlook.com"
                                          "stardiviner@qq.com"
                                          "348284894@qq.com"))
           ;; Maildir
           (mu4e-sent-folder "/Send")
           (mu4e-drafts-folder "/Drafts")
           ;; (mu4e-refile-folder "/Archives")
           (mu4e-trash-folder "/Trash")
           (mu4e-completing-read-function 'completing-read))
  :commands (mu4e)
  :init
  (add-to-list 'display-buffer-alist '("^\\*mu4e-main\\*" . (display-buffer-below-selected))) ; `mu4e-main-buffer-name'
  (add-to-list 'display-buffer-alist '("^\\*mu4e-log\\*"  . (display-buffer-below-selected))) ; `mu4e--log-buffer-name'
  (add-to-list 'display-buffer-alist '("^\\*mu4e-sexp-at-point\\*" . (display-buffer-below-selected))) ; `mu4e--sexp-buffer-name'
  (add-to-list 'display-buffer-alist '("^ \\*mu4e-raw-view\\*" . (display-buffer-below-selected))) ; `mu4e--view-raw-buffer-name'
  :config
  ;; Reset mu and mu4e version
  (my/mu4e-set-mu4e-mu-version)

  ;; the maildirs you use frequently; access them with 'j' ('jump')
  (setq mu4e-maildir-shortcuts
        '((:maildir   "/INBOX"                      :key ?i)
          (:maildir   "/Send"                       :key ?s)
          (:maildir   "/Drafts"                     :key ?d)
          (:maildir   "/Trash"                      :key ?t)
          (:maildir   "/Work"                       :key ?w)
          (:maildir   "/Emacs/help"                 :key ?e)
          (:maildir   "/Emacs/devel"                :key ?E)
          (:maildir   "/Emacs/Org-mode"             :key ?O)
          (:maildir   "/Emacs/mu"                   :key ?m)
          (:maildir   "/Lisp/"                      :key ?l)
          (:maildir   "/Clojure"                    :key ?c)
          (:maildir   "/ClojureScript"              :key ?k)
          (:maildir   "/JavaScript"                 :key ?j)
          (:maildir   "/SQL/PostgreSQL/general"     :key ?p)
          (:maildir   "/NoSQL/Neo4j"                :key ?n)
          (:maildir   "/Newsletter/Clojure"         :key ?C)
          (:maildir   "/Newsletter/Stack Overflow"  :key ?S)
          (:maildir   "/Newsletter/ProductHunt"     :key ?P)))

  ;; main view
  ;; Use precise (but relatively slow) alignment for columns.
  (setq mu4e-headers-precise-alignment t)
  (setq mu4e-main-hide-fully-read t)

  (setq mu4e-mailing-list-patterns '("\\`\\([-_a-z0-9.]+\\)-list"))

  ;; threads fold when first displaying header buffer.
  (setopt mu4e-thread--fold-status t)
  (add-hook 'mu4e-headers-mode-hook #'mu4e-thread-fold-all) ; press [TAB] to expand
  (add-hook 'mu4e-thread-mode-hook #'mu4e-thread-fold-apply-all)
  
  ;; only show thread subject once
  (setq mu4e-headers-fields
        '((:flags .  10)
          (:human-date  . 12)
          (:from  . 22)
          ;; (:mailing-list  .   10)
          ;; (:thread-subject . 30)
          (:subject . nil)))
  
  ;; [ Get/Update Mail ] -- [C-c C-u]
  ;; Raise warning when fetching email tool command not available.
  (unless (executable-find "getmail")
    (user-error "[mu4e] The command `getmail' is not available, please install it."))

  (cond
   ((executable-find "getmail")         ; installed with command "python -m pip install getmail6".
    (setq mu4e-get-mail-command
          (concat "getmail --getmaildir=~/.config/getmail/"
                  " " "--rcfile=stardiviner@outlook.com"
                  " " "--rcfile=stardiviner@qq.com"
                  ;; " && "
                  ;; "/opt/homebrew/bin/proxychains4 -f ~/.config/proxychains/proxychains.conf"
                  ;; " " "getmail --getmaildir=~/.config/getmail/"
                  " " "--rcfile=numbchild@gmail.com"
                  )))
   ((executable-find "offlineimap")
    (setq mu4e-get-mail-command (concat (executable-find "offlineimap") " -q -u basic")))
   ((executable-find "mbsync")
    (setq mu4e-get-mail-command (concat (executable-find "mbsync") " -aV"))))
  
  ;; [ mu4e-modeline-mode ] -- Minor mode for showing mu4e information on the modeline.
  ;; (setq mu4e-modeline-support t) ; enabled by default.
  ;; (setq mu4e-display-update-status-in-modeline t)
  ;; (mu4e-modeline-mode 1) ; `mu4e' invoke `mu4e-modeline-mode' by default.

  ;; rename files when moving - needed for `mbsync':
  (setq mu4e-change-filenames-when-moving t)

  ;; [ Send Mail ]
  ;; send function
  (setq send-mail-function 'sendmail-send-it
        message-send-mail-function 'sendmail-send-it)
  ;; send program selection: `sendmail' or `msmtp'.
  (cond
   ((executable-find "sendmail")
    (setq sendmail-program (executable-find "sendmail")))
   ((executable-find "msmtp")
    (require 'smtpmail)
    (setq sendmail-program (executable-find "msmtp"))))

  ;; [ Index ]
  ;; background update & indexing
  (setq mu4e-update-interval (* 60 30))
  (setq mu4e-hide-index-messages t)
  (setq mu4e-index-update-error-warning nil) ; don't display mu4e update warning messages.

  ;; [ Compose ]
  (defun my/mu4e-compose-mode-setup ()
    (auto-save-mode -1)                 ; Don't auto save mu4e drafts.
    (turn-on-auto-fill)
    (cond
     ((featurep 'jit-spell) (jit-spell-mode 1))
     ((featurep 'flyspell) (flyspell-mode 1)))
    (display-fill-column-indicator-mode)
    (if company-mode (company-mode -1))
    (corfu-mode 1)
    (cond
     (company-mode
      (setq-local completion-at-point-functions
                  `(org-contacts-message-complete-function
                    ,(if (fboundp 'mu4e--compose-complete-contact-field)
                         mu4e--compose-complete-contact-field
                       mu4e~compose-complete-contact)
                    mail-completion-at-point-function
                    message-completion-function)))
     (corfu-mode
      (setq-local completion-at-point-functions
                  (list (if (featurep 'yasnippet-capf)
                            #'yasnippet-capf
                          (cape-company-to-capf 'company-yasnippet))
                        (cape-capf-super
                         #'org-contacts-message-complete-function
                         (if (fboundp 'mu4e--compose-complete-contact-field)
                             #'mu4e--compose-complete-contact-field
                           #'mu4e~compose-complete-contact)
                         #'mail-completion-at-point-function
                         #'message-completion-function)
                        #'cape-abbrev
                        (cape-company-to-capf 'company-spell)))))
    (when (featurep 'emojify) (emojify-mode 1)))
  (add-hook 'mu4e-compose-mode-hook #'my/mu4e-compose-mode-setup)

  ;; Auto set reply email's `user-mail-address' based on the email senter email address.
  (defun my/mu4e-auto-rule-set-user-mail-address ()
    "Auto set reply email's `user-mail-address' based on the email senter email address."
    (when mu4e-compose-parent-message
      (let ((to (cadar (mu4e-message-field mu4e-compose-parent-message :to))))
        (when (string-suffix-p "@qq.com" to)
          (setq-local user-mail-address "stardiviner@qq.com")))))
  (add-hook 'mu4e-compose-pre-hook #'my/mu4e-auto-rule-set-user-mail-address)

  ;; hide all 'draft' messages:
  (setq mu4e-headers-hide-predicate
        (lambda (msg) (member 'draft (mu4e-message-field msg :flags))))

  ;; Example that hides all 'trashed' messages:
  ;; (setq mu4e-headers-hide-predicate
  ;;       (lambda (msg)
  ;;         (member 'trashed (mu4e-message-field msg :flags))))

  (setq mu4e-compose-keep-self-cc t)    ; keep myself on the Cc: list.
  ;; don't include self (that is, any member of `mu4e-user-mail-address-list') in replies.
  (setq mu4e-compose-dont-reply-to-self t)

  (define-key mu4e-headers-mode-map (kbd "r") 'mu4e-compose-reply)
  (define-key mu4e-headers-mode-map (kbd "R") 'mu4e-headers-mark-for-refile)
  (define-key mu4e-view-mode-map (kbd "r") 'mu4e-compose-reply)
  (define-key mu4e-view-mode-map (kbd "R") 'mu4e-view-mark-for-refile)
  (define-key mu4e-headers-mode-map (kbd "N") 'mu4e-headers-next-unread)

  ;; sign & encrypt
  (require 'mml)
  (require 'mml2015)
  (require 'epg-config)
  (setq mml2015-use 'epg
        epg-user-id "5AE89AC3"
        ;; sign and encrypt with specified sender self’s key in GnuPG.
        mml-secure-openpgp-signers '("5AE89AC3")
        mml-secure-openpgp-encrypt-to-self nil
        mml-secure-openpgp-sign-with-sender nil)
  
  (setq mu4e-compose-crypto-policy '(sign-all-messages encrypt-all-message))
  ;; OR:
  ;; auto sign email
  ;; (add-hook 'message-send-hook 'mml-secure-message-sign-pgpauto)
  ;; (add-hook 'mu4e-compose-mode-hook 'mml-secure-message-sign-pgpauto)
  ;; (add-hook 'mu4e-compose-mode-hook #'mml-secure-message-sign-pgpmime)
  ;; auto encrypt outgoing message
  ;; (add-hook 'message-send-hook 'mml-secure-message-encrypt-pgpauto)
  ;; (add-hook 'mu4e-compose-mode-hook 'mml-secure-message-encrypt-pgpauto)

  ;; [ Search ] -- [s/S]  [:references [regexp]] in search query.
  (add-to-list 'mu4e-header-info-custom
               '(:references
                 :name "References"
                 :shortname "References"
                 :help "Reference of this thread"
                 :function (lambda (msg) (format "%s" (mu4e-message-field msg :References)))))

  ;; [ spam filtering ]
  (require 'mu4e-contrib)
  (setq mu4e-register-as-spam-cmd "bogofilter -Ns < %s"
        mu4e-register-as-ham-cmd "bogofilter -Sn < %s")
  (add-to-list 'mu4e-headers-actions
               '("SMark as spam" . mu4e-register-msg-as-spam) t)
  (add-to-list 'mu4e-headers-actions
               '("HMark as ham" . mu4e-register-msg-as-ham) t)
  (add-to-list 'mu4e-view-actions
               '("SMark as spam" . mu4e-view-register-msg-as-spam) t)
  (add-to-list 'mu4e-view-actions
               '("HMark as ham" . mu4e-view-register-msg-as-ham) t)

  ;; support `org-store-link' in mu4e for [[mu4e:..]] links.
  (require 'mu4e-org)
  (setq mu4e-org-link-query-in-headers-mode t)
  
  ;; [ HTML Email ]
  ;; use #=begin_export html ... #+end_export
  ;; (setq org-mu4e-convert-to-html t)
  ;;
  ;; (use-package org-mime
  ;;   :hook ((message-send . org-mime-confirm-when-no-multipart) ; remind you didn't use HTML
  ;;          ;; automatically htmlize message on sending email.
  ;;          (message-send . org-mime-htmlize)))

  ;; add find relative on same thread search to mu4e actions. [a/A]
  (defun my/mu4e-view-related-search (msg)
    "Search `MSG' for related messages to the current one."
    (let* ((msgid (mu4e-msg-field msg :message-id)))
      (switch-to-buffer "*mu4e-headers*")
      (setq mu4e-headers-include-related t)
      (mu4e-headers-search (concat "\"msgid:" msgid "\""))))
  (add-to-list 'mu4e-headers-actions '("related thread" . my/mu4e-view-related-search) t)
  (add-to-list 'mu4e-view-actions '("relative thread" . my/mu4e-view-related-search) t)

  ;; [ Message view ]
  (setq mu4e-view-fields '(:from :to :cc
                                 :subject
                                 :date
                                 :flags
                                 :maildir
                                 :attachments
                                 :mailing-list
                                 :references
                                 :tags
                                 :signature
                                 :decryption)
        ;; show e-mail address after names of contacts
        ;; From: field. or press [M-RET] to view.
        mu4e-view-show-addresses t)
  (setq mu4e-view-scroll-to-next nil)  ; don't open next email when SPC scroll to bottom of message.

  ;; [ Cite ]
  ;; (add-hook 'mu4e-view-mode-hook 'mu4e-view-toggle-hide-cited)
  (setq message-cite-style message-cite-style-gmail)

  (add-hook 'mu4e-view-mode-hook #'turn-on-visual-line-mode)

  ;; [ viewing images inline ]
  (setq mu4e-view-show-images t)

  ;; [ displaying rich-text messages ]
  (add-to-list 'mu4e-view-actions '("browser view HTML message" . mu4e-action-view-in-browser) t)

  ;; Attachments
  (setq mu4e-attachment-dir (expand-file-name "~/Downloads"))

  ;; Apply patch in email body
  (add-to-list 'mu4e-view-actions '("git am (apply mailbox patch)" . mu4e-action-git-apply-mbox))

  ;; Refiling -- [r]
  (setq mu4e-refile-folder              ; dynamic refiling
        (lambda (msg)
          (cond
           ;; mu discuss Google Groups
           ((mu4e-message-contact-field-matches
             msg :to "mu-discuss@googlegroups.com")
            "/Emacs/mu")
           ;; delete all Cron getmail error messages which is network
           ;; unavailable error.
           ((string-match "Cron .* getmail.*"
                          (or (mu4e-message-field msg :subject) ""))
            ;; (mu4e-message-contact-field-matches msg :from "Cron Daemon")
            "/Trash")
           ;; everything else goes to /archive
           ;; *important* to have a catch-all at the end!
           (t "/archive"))))

  ;; Bookmarks -- [b]
  (setq mu4e-bookmarks
        `((:name "My participated threads" :key ?b
                 :query ,(concat "maildir:\"/Send\" AND flag:unread "
                                 " OR contact:/.*stardiviner/ "
                                 " OR contact:/.*numbchild@gmail.com/ "
                                 " OR contact:/.*stardiviner@outlook.com/ "
                                 " OR contact:/.*stardiviner@icloud.com/ "))
          (:name "Today's new messages" :key ?n
                 :query "date:today..now flag:new")
          (:name "Today's messages" :key ?d
                 :query "date:today..now")
          (:name "This week's messages" :key ?w
                 :query "date:1w..now")
          (:name "Replied messages" :key ?r
                 :query "flag:replied")
          (:name  "Unread messages" :key ?u
                  :query "flag:unread AND NOT flag:trashed")
          (:name "Flagged messages" :key ?f
                 :query "flag:flagged flag:unread")
          (:name "Passed  messages" :key ?d
                 :query "flag:passed")
          (:name "Messages with images" :key ?p
                 :query "mime:image/*"
                 :hide t)
          (:name "Big messages" :key ?B
                 :query "size:5M..500M")))

  ;; Marking
  (define-key mu4e-headers-mode-map (kbd "f") 'mu4e-headers-mark-for-flag)
  (define-key mu4e-headers-mode-map (kbd "m") 'mu4e-headers-mark-for-something)
  (define-key mu4e-headers-mode-map (kbd "M") 'mu4e-headers-mark-for-move)

  ;; maintaining an address-book with org-contacts
  ;; Usage: [a o] in headers view & message view :: using the `org-capture' mechanism.
  (require 'org-contacts)
  ;; (load "init-org-contacts")
  (autoload 'org-contacts-setup-completion-at-point "org-contacts")
  (setq mu4e-org-contacts-file (car org-contacts-files))
  (add-to-list 'mu4e-headers-actions
               '("org-contact-add" . mu4e-action-add-org-contact) t)
  (add-to-list 'mu4e-view-actions
               '("org-contact-add" . mu4e-action-add-org-contact) t)
  ;; For mail completion, only consider emails that have been seen in the last 6
  ;; months. This gets rid of legacy mail addresses of people.
  (setq mu4e-compose-complete-only-after
        (format-time-string "%Y-%m-%d" (time-subtract (current-time) (days-to-time 150))))
  ;; org-capture template for contact in email.
  (defsubst my/mu4e-msg-at-point ()
    "Return the message object property list at point."
    (cl-case major-mode
      (mu4e-headers-mode (get-text-property (point) 'msg))
      (mu4e-view-mode mu4e--view-message)))
  
  (add-to-list 'org-capture-templates
               `("E" ,(format "%s\tCapture mu4e Email contact into org-contacts"
                              (nerd-icons-faicon "nf-fa-address_card_o" :face 'nerd-icons-blue))
                 entry (file ,(car org-contacts-files))
                 "* %(let ((mu4e-headers-from-or-to-prefix nil)) (mu4e~headers-from-or-to (my/mu4e-msg-at-point)))
:PROPERTIES:
:NAME(English): %(mu4e~headers-from-or-to (my/mu4e-msg-at-point))
:NICK:
:GENDER: %^{Gender|Male|Female|Transgender}
:EMAIL: [[mailto:%(plist-get (car (mu4e-message-field (my/mu4e-msg-at-point) :from)) :email)]]
:RELATIONSHIP: %^{Relationship|Internet|Meet|Friend|Good Friend|Boy Friend|Girl Friend|Workmate|Classmate|Schoolmate}
:FIRST-MEET: First time see him in mailing list \"%(plist-get (or (car (mu4e-message-field (my/mu4e-msg-at-point) :cc)) (car (mu4e-message-field (my/mu4e-msg-at-point) :to))) :email)\".
:SKILLS: %^{Skills|Programming|Economy}
:Programming-Skills: %^{Programming Skills|Emacs|Web|Computer System|Cloud Computation}
:Programming-Languages: %^{Programming Languages|LISP|Common Lisp|Clojure|Emacs Lisp|Java|C/C++|Python|Ruby|PHP}
:END:
"
                 :empty-lines 1)
               :append)
  )

;; [mu4e-marker-icons ] -- Display icons for mu4e markers.

(use-package mu4e-marker-icons
  :ensure t
  :init (mu4e-marker-icons-mode 1))

;;; [ mu4e-column-faces ] -- using individual faces for the columns in mu4e’s email overview.

(unless (featurep 'mu4e-marker-icons)
  (use-package mu4e-column-faces
    :ensure t
    :after mu4e
    :init (mu4e-column-faces-mode)))

;;; [ mu4e-views ] -- View emails in mu4e using xwidget-webkit.

;; (use-package mu4e-views
;;   :ensure t
;;   :after mu4e
;;   :bind (:map mu4e-headers-mode-map
;; 	            ("v" . mu4e-views-mu4e-select-view-msg-method)
;; 	            ("M-n" . mu4e-views-cursor-msg-view-window-down)
;; 	            ("M-p" . mu4e-views-cursor-msg-view-window-up))
;;   :custom ((mu4e-views-completion-method 'ivy)
;;            (mu4e-views-default-view-method "html")
;;            (mu4e-views-next-previous-message-behaviour 'stick-to-current-window))
;;   :init (mu4e-views-mu4e-use-view-msg-method "html"))

;;; [ mu4e-alert ] -- Desktop notification for mu4e.

;; (use-package mu4e-alert
;;   :ensure t
;;   :init (mu4e-alert-enable-notifications)
;;   (if (featurep 'doom-modeline) (setq doom-modeline-mu4e t)))

;;; [ consult-mu ] -- Use consult to search mu4e dynamically or asynchronously.

(use-package consult-mu
  :vc (:url "https://github.com/armindarvish/consult-mu" :rev :newest)
  :after (mu4e consult)
  :commands (consult-mu))



(provide 'init-mu4e)

;;; init-mu4e.el ends here
