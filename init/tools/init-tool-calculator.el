;;; init-tool-calculator.el --- init Tool Calculator
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:


(unless (boundp 'calculator-prefix)
  (define-prefix-command 'calculator-prefix))
(define-key tools-prefix (kbd "C") 'calculator-prefix)

(define-key calculator-prefix (kbd "c") 'calc)
(define-key calculator-prefix (kbd "q") 'quick-calc) ; 'calc-keypad

;;; [ Calc ]

(use-package calc
  :commands (calc quick-calc)
  :custom (calc-complex-format 'i) ; complex number style: x + yi.
  :config
  ;; usefull mini calculator
  (defun mini-calc (expr &optional arg)
    "Calculate expression
If ARG is given, then insert the result to current-buffer"
    (interactive (list (read-from-minibuffer "Enter expression: ") current-prefix-arg))
    (let ((result (calc-eval expr)))
      (if arg (insert result) (message (format "Result: [%s] = %s" expr result)))))
  (define-key calculator-prefix (kbd "x") 'mini-calc))

;;; [ ob-calc ] -- Babel Functions for Emacs `calc'

(use-package ob-calc
  :defer t
  :commands (org-babel-execute:calc)
  :config
  (add-to-list 'org-babel-load-languages '(calc . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages))

;;; [ ob-mathomatic ] -- Org Babel support for Mathomatic Computer Algebra System.

(use-package ob-mathomatic
  :after org
  :config
  (org-babel-do-load-languages
   'org-babel-load-languages
   (append org-babel-load-languages '((mathomatic . t))))
  ;; add ":wolfram" header argument as safe for `org-lint'.
  (add-to-list 'org-babel-header-arg-names 'mathomatic)
  (add-to-list 'org-babel-default-header-args:mathomatic '(:eval . "yes")))

;;; [ Casual ] -- An opinionated `transient'-based porcelain to support the casual usage of Emacs `calc'.

(use-package casual
  :ensure t
  :after calc
  :commands (casual-main-menu)
  :init (define-key calc-mode-map (kbd "C-o") 'casual-main-menu)
  ;; auto open `casual-main-menu' after open `calc'.
  (advice-add 'calc :after 'casual-main-menu))


(provide 'init-tool-calculator)

;;; init-tool-calculator.el ends here
