;;; init-emms.el --- init for EMMS
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ EMMS ]

(use-package emms
  :ensure t
  :defer t
  :custom (emms-source-file-default-directory "~/Music/")
  :commands (emms emms-stop)
  :init (add-to-list 'display-buffer-alist '("^\\*EMMS Playlist\\*" . (display-buffer-below-selected)))
  :config
  (emms-all)
  ;; [ players ]
  (emms-default-players)
  ;; play:
  ;; next: 'emms-next-noerror
  ;; random: 'emms-random
  ;; only play current song: 'emms-stop
  (setq emms-player-next-function 'emms-next-noerror)

  ;; [ Playlist ]
  ;; (setq emms-repeat-playlist nil) ; don't repeat the playlist after the last track.

  ;; custom playlist track format
  ;; for playlist like `emms-player-mpd'.
  (setq emms-track-description-function 'my/emms-info-track-description)
  (defun my/emms-info-track-description (track)
    "Return a description of TRACK."
    (let ((artist (emms-track-get track 'info-artist))
          (title  (emms-track-get track 'info-title)))
      (cond
       ((and artist title)
        (format "[ %s ] - %s" artist title))
       (title
        title)
       (t
        (emms-track-simple-description track)))))
  
  ;; [ Track ]
  
  ;; [ Score ]
  (emms-score 1)

  ;; [ Encoding ]
  (require 'emms-info-mp3info)
  (setq emms-info-mp3info-coding-system '(utf-8 gbk)
        emms-cache-file-coding-system 'utf-8)

  ;; [ EMMS mode-line ]
  (defun my/emms-mode-line-info ()
    (let* ((track (emms-playlist-current-selected-track))
           ;; (description (emms-track-description (emms-playlist-current-selected-track)))
           (title (file-name-nondirectory (cdr (assoc 'name track)))))
      (format emms-mode-line-format
              (s-truncate
               (/ (/ (frame-width) 2) 4)
               title))))
  (setq emms-mode-line-mode-line-function 'my/emms-mode-line-info)
  
  ;; [ Streams: Radio, Podcasts ]

  ;; Switch to the radio buffer
  (defun my-emms-streams ()
    "Switch to streams buffer, if does not exists, then start emms-streams."
    (interactive)
    (let ((buf (get-buffer emms-stream-buffer-name)))
      (if buf
          (switch-to-buffer buf)
        (emms-streams))))

  ;; [ Key Bindings ]
  ;; (setq emms-info-asynchronously nil)
  (define-key emms-playlist-mode-map (kbd "Q") 'emms-stream-quit) ; really quit EMMS Stream.
  ;; (setq emms-playlist-default-major-mode 'emms-mark-mode)

  ;; [ MPD ] -- [M-x emms-player-mpd-connect]
  (require 'emms-player-mpd)

  (add-to-list 'emms-info-functions 'emms-info-mpd)
  (add-to-list 'emms-player-list 'emms-player-mpd t)
  
  (setq emms-player-mpd-server-name "127.0.0.1"
        emms-player-mpd-server-port "6600")
  (setq emms-player-mpd-music-directory "~/Music/music/")
  
  ;; [ MPV ]
  (require 'emms-player-mpv)
  
  ;; [ BBT: SBaGen ]
  (define-emms-simple-player sbagen '(file) (emms-player-simple-regexp "sbg") "sbagen")
  )

;;; [ consult-emms ] -- Consult interface to Emacs MultiMedia System.

(use-package consult-emms
  ;; :ensure t
  :vc (:fecther github :repo "Hugo-Heagren/consult-emms")
  :commands (consult-emms-library consult-emms-playlists consult-emms-metaplaylist consult-emms-current-playlist)
  :config
  (defun consult-emms--playlist (buffer)
    "Select a track from EMMS buffer BUFFER.

BUFFER is a string, the name of a buffer.

https://emacs-china.org/t/consult-emms-emms/21883"
    ;; `consult-emms--playlist-source-from-buffer' does most of the work
    ;; of forming the args for us, and it's a good idea to avoid code
    ;; duplication, so we use it here. BUT, it forms a source for
    ;; `consult--multi', which is different from the arg list taken by
    ;; `consult--read', so we have to transform it a bit.
    (let* ((raw-args (consult-emms--playlist-source-from-buffer buffer))
	       (items (plist-get raw-args :items))
	       (action (plist-get raw-args :action))
	       ;; TODO Get this list programatically
	       (allowed '(:prompt :predicate :require-match ;; Keywords in `consult--read'
		                      :history :default :keymap
		                      :category :initial :narrow
		                      :add-history :annotate :state
		                      :preview-key :sort :group
		                      :inherit-input-method))
	       ;; Use only arg keys used by `consult--read'
	       (filtered-args (cl-loop for (key value) on raw-args by 'cddr
				                   if (member key allowed)
				                   collect key and collect value))
	       (read-args (append `(:prompt ,(format "EMMS playlist <%s>: " buffer))
			                  filtered-args))
	       ;; Lots of the actions use text properties as variables, so
	       ;; make sure they persist through minibuffer choice
	       (minibuffer-allow-text-properties t)
	       (raw-track (apply 'consult--read `(,items ,@read-args)))
           (track (cl-loop for item in items
                           until (string= item raw-track)
                           finally return item)))
      ;; Using the action extracted above guarantees that the behaviour
      ;; will be the same as with the corresponding source
      (unless (string-empty-p raw-track)
        (funcall action track)))))

;;; [ org-emms ] -- Play multimedia files from org-mode with EMMS.

(use-package org-emms
  :after emms
  :ensure t)


(provide 'init-emms)

;;; init-emms.el ends here
