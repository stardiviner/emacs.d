;;; init-tool-irc.el --- init for IRC.

;;; Commentary:



;;; Code:

;;; [ Circe ] -- have sane defaults, and integrates well with the rest of the editor.

(use-package circe
  :ensure t
  :defer t
  :commands (circe)
  :custom ((circe-default-nick "stardiviner")
           (circe-network-options `(("Libera Chat" ; [irc.libera.chat]
                                     :user "stardiviner"
                                     :pass ,(my/json-read-value my/account-file 'libera)
                                     :use-tls t
                                     :nickserv-nick "stardiviner"
                                     :nickserv-password ,(my/json-read-value my/account-file 'libera)
                                     :sasl-username "stardiviner"
                                     :sasl-password ,(my/json-read-value my/account-file 'libera)
                                     :sasl-external nil
                                     :sasl-strict nil
                                     :channels (;; "#libera"
                                                "#emacs" ; "#org-mode" "#emacs-beginners" "#erc" "#gnus"
                                                "#lisp" "#python"
                                                ))

                                    ("OFTC" ; [irc.oftc.net]
                                     :user "stardiviner"
                                     :pass ,(my/json-read-value my/account-file 'oftc)
                                     :use-tls t
                                     :nickserv-nick "stardiviner"
                                     :nickserv-password ,(my/json-read-value my/account-file 'oftc)
                                     :sasl-username "stardiviner"
                                     :sasl-password ,(my/json-read-value my/account-file 'oftc)
                                     :channels ("#linode"))
                                    
                                    ("Freenode" ; [irc.freenode.net]
                                     :user "stardiviner"
                                     :pass ,(my/json-read-value my/account-file 'irc)
                                     :use-tls t
                                     :nickserv-nick "stardiviner"
                                     :nickserv-password ,(my/json-read-value my/account-file 'irc)
                                     :sasl-username "stardiviner"
                                     :sasl-password ,(my/json-read-value my/account-file 'irc)
                                     :channels ("#emacs"))
                                    ))
           ;; (circe-channels '(:after-auth "#emacs" "#clojure")) ; should not set this variable manually.
           (circe-reduce-lurker-spam t)
           (circe-format-say "{nick:-10s}│ {body}") ; align nick names and messages.
           ;; lui fill column width
           (lui-fill-column 100)
           ;; spelling checking
           (lui-flyspell-p t)
           (lui-flyspell-alist '(("#hamburg" "german8") (".*" "american"))))

  :init
  ;; switch to first active channel buffer
  (defun circe-connect-or-switch-to-tracking-buffer ()
    "Connect to IRC server or switch to first active channel buffer."
    (interactive)
    (require 'circe)
    (let ((circe-server-buffer (get-buffer "irc.libera.chat:6697")))
      (if (buffer-live-p (or circe-server-buffer circe-server-buffer))
          (if (not (seq-empty-p tracking-buffers))
              (let ((buf (get-buffer (or tracking-start-buffer (car tracking-buffers)))))
                (setq tracking-start-buffer (get-buffer (car tracking-buffers)))
                (setq tracking-buffers (cdr tracking-buffers))
                ;; (switch-to-buffer-other-window tracking-start-buffer)
                (with-current-buffer buf
                  (tracking-next-buffer)))
            (message "There is no more active tracking circe IRC channel buffer...")
            (switch-to-buffer-other-window circe-server-buffer))
        (call-interactively 'circe))))  
  (define-key tools-prefix (kbd "i") 'circe-connect-or-switch-to-tracking-buffer)
  
  :config
  (define-key circe-mode-map (kbd "C-c SPC") 'tracking-next-buffer)

  ;; switch channel buffers
  (defun circe-switch-channel-buffer ()
    "Switch channel buffers."
    (interactive)
    (ivy-read "circe switch to IRC channel buffer: " #'internal-complete-buffer
              :keymap ivy-switch-buffer-map
              :preselect (buffer-name (other-buffer (current-buffer)))
              :action #'ivy--switch-buffer-action
              ;; `ivy--switch-buffer-matcher'
              :matcher (lambda (regexp candidates)
                         (ivy--re-filter regexp
                                         (sort
                                          (seq-filter
                                           (lambda (candidate)
                                             (let* ((buffer (get-buffer candidate))
                                                    (mode (with-current-buffer buffer major-mode)))
                                               (or
                                                ;; IRC server buffer: irc.channel:port
                                                (eq mode 'circe-server-mode)
                                                ;; IRC channel buffer: "#emacs@Libera Chat".
                                                (eq mode 'circe-channel-mode)
                                                ;; private conversation buffer: nickname@channel
                                                (eq mode 'circe-query-mode)
                                                )))
                                           candidates)
                                          (lambda (a b)
                                            (let ((order '((circe-channel-mode . 1) (circe-query-mode . 2) (circe-server-mode . 3))))
                                              (<
                                               (cdr (assoc (with-current-buffer a major-mode) order))
                                               (cdr (assoc (with-current-buffer b major-mode) order))))))))
              :caller 'ivy-switch-buffer-other-window))
  (define-key circe-mode-map (kbd "C-c C-b") 'circe-switch-channel-buffer)

  (when (featurep 'doom-modeline) (setq doom-modeline-irc-buffers t))
  
  ;; words completion
  (defun my/circe-company-setup ()
    (my-company-add-backend-locally 'company-spell))
  (add-hook 'circe-channel-mode-hook #'my/circe-company-setup)
  ;; Enable the Color Nicks module for Circe.
  (require 'circe-color-nicks)
  (enable-circe-color-nicks)
  ;; auto use paste service for long single line.
  (require 'lui-autopaste)
  (add-hook 'circe-channel-mode-hook #'enable-lui-autopaste)
  ;; Logging
  (require 'lui-logging)
  (enable-lui-logging-globally)
  ;; show track reading position bar line.
  (enable-lui-track-bar)

  ;; hide "NAMES" displaying when connected to channel.
  ;; https://github.com/emacs-circe/circe/issues/298
  ;; https://github.com/emacs-circe/circe/blob/9d703f481a2c65f2b17edcc2b05412f9865d24af/circe.el#L3493
  (with-eval-after-load 'circe
    (circe-set-display-handler "353" 'circe-display-ignore)
    (circe-set-display-handler "366" 'circe-display-ignore))
  (defvar my/circe-names-width 66)
  (defun my/string-ci-< (a b)
    (string< (downcase a) (downcase b)))
  (defun my/nicks-to-lines (nicks line-length)
    (with-temp-buffer
      (setq fill-column line-length)
      (insert (mapconcat 'identity nicks " "))
      (goto-char (point-min))
      (fill-paragraph)
      (split-string (buffer-string) "\n")))
  (defun my/circe-display-nicks ()
    (interactive)
    (when (eq major-mode 'circe-channel-mode)
      (let* ((nicks (sort (circe-channel-nicks) 'my/string-ci-<))
             (nick-lines (my/nicks-to-lines nicks my/circe-names-width)))
        (dolist (nick-line nick-lines)
          (circe-display-server-message nick-line)))))
  (with-eval-after-load 'circe
    (define-key circe-channel-mode-map (kbd "C-c C-n") 'my/circe-display-nicks))

  ;; Auto ping some user I'm online when I connected to IRC server.
  (defvar my/irc-friends-list '(""))
  
  (defun my/circe-online-status-notify ()
    "Auto ping some user I'm online when I connected to IRC server."
    (let ((circe-server-name "Libera Chat")
          (circe-server-buffer (get-buffer "irc.libera.chat:6697")))
      (with-current-buffer circe-server-buffer
        (dolist (friend my/irc-friends-list)
          (circe-command-QUERY friend)
          (circe-command-MSG friend "I'm online now.")))))
  
  (add-hook 'circe-server-connected-hook #'my/circe-online-status-notify)

  ;; [ circe-display-images ] -- Display images in channel.
  (require 'circe-display-images)
  (add-hook 'circe-server-connected-hook #'enable-circe-display-images)

  ;; [ circe-chanop ] -- Provide common channel operator commands `circe-command-*'.
  (require 'circe-chanop)
  )

;;; [ circe-notifications ] -- Add desktop notifications to Circe.

(use-package circe-notifications
  :ensure t
  :custom ((circe-notifications-alert-style (cl-case system-type
                                              (gnu/linux 'libnotify)
                                              (darwin 'osx-notifier)))
           (circe-notifications-watch-strings '("macOS" "Mac" "macos")))
  :hook (circe-server-connected . enable-circe-notifications))


(provide 'init-tool-irc)

;;; init-tool-irc.el ends here
