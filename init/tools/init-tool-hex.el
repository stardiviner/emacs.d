;;; init-tool-hex.el --- init for Hex.
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ hexl ] -- hexlify and dehexlify binary file content.

(use-package hexl
  :defer t
  :commands (hexl-mode hexl-find-file hexlify-buffer))

;;; [ nhexl-mode ] -- Minor mode to edit files via hex-dump format.

(use-package nhexl-mode
  :ensure t
  :defer t
  :commands (nhexl-mode)
  :hook (hexl-mode . nhexl-mode)
  :config
  ;; hexl-isearch: Let isearch search the binary instead of the hexl buffer.
  (with-eval-after-load 'hexl-mode
    (load (expand-file-name "init/extensions/hexl-isearch.el" user-emacs-directory))))

;;; [ hexview-mode ]

;; (use-package hexview-mode
;;   :ensure t
;;   :defer t
;;   :config (define-key prog-code-prefix (kbd "h") 'hexview-find-file))

;;; [ elf-mode ] -- Show symbols in binaries.

(use-package elf-mode
  :ensure t
  :defer t
  :mode ("\\.\\(?:a\\|so\\)\\'" . elf-mode))

;;; [ intel-hex-mode ] -- An Emacs mode for Intel hex files.

(use-package intel-hex-mode
  :ensure t
  :defer t)

;;; [ poke ] -- poke.el is an Emacs interface for GNU poke, the extensible editor for structured binary data.

(use-package poke
  :if (executable-find "poke")
  :ensure t)

;;; [ ob-base64 ] -- Decode and interpret base64 as images, binary (hexl) or raw (plaintext possibly).

(use-package ob-base64
  :ensure t
  :if (executable-find "xviewer")
  :init
  (add-to-list 'org-babel-load-languages '(base64 . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  (add-to-list 'org-src-lang-modes '("base64" . hexl)) ; open base64 source block with `hexl-mode'
  (add-to-list 'org-babel-tangle-lang-exts '("base64" . "bin"))
  ;; add ":type" header argument as safe for `org-lint'.
  (add-to-list 'org-babel-header-arg-names 'type)
  :config (add-to-list 'org-babel-default-header-args:base64 '(:eval . "yes")))

(provide 'init-tool-hex)

;;; init-tool-hex.el ends here
