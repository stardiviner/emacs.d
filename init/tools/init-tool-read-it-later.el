;;; init-tool-read-it-later.el --- init for Read It Later. -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Christopher M. Miles, all rights reserved.

;;; Commentary:



;;; Code:

;;; [ pocket-reader ] -- Emacs client for Pocket reading list (getpocket.com).

;; (use-package pocket-reader
;;   :ensure t
;;   :commands (pocket-reader pocket-reader-add-link))



(provide 'init-tool-read-it-later)

;;; init-tool-read-it-later.el ends here
