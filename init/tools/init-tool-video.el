;;; init-tool-video.el --- init file for video processing -*- lexical-binding: t; -*-

;;; Commentary:



;;; Code:

;;; [ mpv ] -- control mpv for easy note-taking. A potpourri of helper functions to control a mpv process via its IPC interface.

(use-package mpv
  :ensure t
  :defer t
  :commands (mpv-play
             mpv-pause mpv-kill
             mpv-seek-forward mpv-seek-backward
             mpv-speed-increase mpv-speed-decrease
             mpv-insert-playback-position mpv-seek-to-position-at-point))

;;; [ empv ] -- An Emacs media player, media library manager, radio player, YouTube frontend.

(use-package empv
  :ensure t
  :defer t
  :custom ((empv-audio-dir "~/Music/My_Music/")
           (empv-video-dir "~/Downloads/"))
  :commands (empv-play empv-play-directory empv-play-video empv-play-audio empv-play-radio))

;;; [ ffmpeg ] -- ffmpeg command wrapper for Emacs.

(use-package ffmpeg-utils
  :ensure t
  :defer t
  :commands (ffmpeg-utils-cut-clip))

;;; [ vlc ] -- VideoLAN VLC Media Player Control through the Web Interface's API.

;; (use-package vlc
;;   :ensure t
;;   :defer t
;;   :commands (vlc-play))

;;; [ waveform ] -- Display a waveform in Emacs and use it to navigate.

(use-package waveform
  :vc (:url "git@github.com:sachac/waveform-el.git")
  :ensure transient
  :ensure context-transient
  :defer t
  :commands (waveform-show)
  :config
  ;; reference `waveform-mode-map'
  (context-transient-define waveform-transient
    :doc "Transient menu for `waveform-mode'"
    :mode 'waveform-mode
    :menu
    [["Seek"
      ("[left-click]" "Copy the time at point and seek to it." waveform-select)
      ("[right-click]" "Copy the time at point and sample on it." waveform-sample)
      ("[left]" "Seek backward" waveform-mpv-seek-backward)
      ("[right]" "Seek forward" waveform-mpv-seek-forward)
      ("[S-left]" "Seek backward 1 second." waveform-mpv-seek-backward-small)
      ("[S-right]" "Seek forward 1 second." waveform-mpv-seek-forward-small)
      ("[DEL]" "Seek to mark position." waveform-mpv-return-to-mark)
      ("j" "Jump to position (seconds or timestamp)." waveform-mpv-seek-to :transient nil)
      ("." "Step one frame forward." waveform-mpv-frame-step :transient nil)
      ("," "Step one frame backward." waveform-mpv-frame-back-step :transient nil)]
     ["Control"
      ("␣" "pause" mpv-pause :transient nil)
      (">" "speed increase" mpv-speed-increase :transient nil)
      ("<" "speed decrease" mpv-speed-decrease :transient nil)]
     ["Operations"
      ("p" "copy position" waveform-mpv-position :transient nil)]
     ["misc"
      ("f" "Change the output format to FORMAT." waveform-set-output-format)
      ("q" "quit" waveform-quit :transient nil)]]))



(provide 'init-tool-video)

;;; init-tool-video.el ends here
