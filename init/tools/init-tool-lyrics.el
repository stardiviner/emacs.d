;;; init-tool-lyrics.el --- init file for Lyrics -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Christopher M. Miles, all rights reserved.

;;; Commentary:



;;; Code:

;;; [ ob-lyrics ]

(add-to-list 'org-src-lang-modes '("lyrics" . text))



(provide 'init-tool-lyrics)

;;; init-tool-lyrics.el ends here
