;;; init-tool-dictionary.el --- init my Emacs dictionary.
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

(unless (boundp 'dictionary-prefix)
  (define-prefix-command 'dictionary-prefix))
(define-key tools-prefix (kbd "d") 'dictionary-prefix)

;;; [ sdcv ] -- Interface for sdcv (StartDict console version).

(use-package sdcv
  :ensure t
  ;; :vc (:url "https://github.com/manateelazycat/sdcv")
  :ensure posframe
  :preface (setenv "STARDICT_DATA_DIR" (expand-file-name "~/.config/stardict/dict"))
  :custom ((sdcv-say-word-p t)
           ;; a simple dictionary list for popup display
           (sdcv-dictionary-simple-list '(;; "懒虫简明英汉词典"
                                          ;; "懒虫简明汉英词典"
                                          ;; "WordNet"
                                          "牛津英汉双解美化版"))
           ;; a complete dictionary list for buffer display
           (sdcv-dictionary-complete-list '(;; "WordNet"
                                            "牛津英汉双解美化版"
                                            "朗道英汉字典5.0"
                                            "朗道汉英字典5.0"))
           (sdcv-word-pronounce-command ""))
  :commands (sdcv-search-pointer sdcv-search-pointer+ sdcv-search-input sdcv-search-input+)
  :bind (:map dictionary-prefix ("C-d" . sdcv-search-pointer) ("M-d" . sdcv-search-input)))

;;; [ external-dict ] -- Query word smartly with external dictionary like goldendict, Bob etc.

(use-package external-dict
  :ensure t
  :bind (:map dictionary-prefix ("d" . external-dict-dwim)))


(provide 'init-tool-dictionary)

;;; init-tool-dictionary.el ends here
