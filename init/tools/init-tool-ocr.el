;;; init-tool-ocr.el --- init file for OCR -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Christopher M. Miles, all rights reserved.

;;; Commentary:

;;; OCR functionality.

;;; Code:

(defun ocr-with-app ()
  "Invoke application's OCR functionality and return the OCR recognized text result."
  (interactive)
  (cl-case system-type
    (darwin
     (cond
      ((file-exists-p "/Applications/CleanShot X.app")
       (ns-do-applescript
        "do shell script \"open cleanshot://capture-text\""))
      ((file-exists-p "/Applications/Bob.app")
       (ns-do-applescript
        "tell application \"Bob\"
      launch
      ocr
      end tell"))))
    (gnu/linux
     (cond
      ((executable-find "screenshot") ; TODO
       (let ((screenshot (funcall 'screenshot))) ; TODO
         (stardiviner/python-command-to-string
          "import easyocr"
          "reader = easyocr.Reader(['ch_sim','en'])"
          (format "result = reader.readtext('%s')" screenshot))))))))



(provide 'init-tool-ocr)

;;; init-tool-ocr.el ends here
