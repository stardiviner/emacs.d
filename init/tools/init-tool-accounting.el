;;; init-tool-accounting.el --- init for Accounting in Emacs.
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ ledger-mode ] -- Helper code for use with the "ledger" command-line tool.

(use-package ledger-mode
  :ensure t
  :defer t
  :hook (ledger-mode . outline-minor-mode)
  :bind (:map ledger-mode-map ("TAB" . org-cycle))
  :config (font-lock-add-keywords 'ledger-mode outline-font-lock-keywords)
  :mode ("\\.ledger\\'" . ledger-mode))

(use-package company-ledger
  :ensure t
  :after ledger-mode
  :defer t
  :config
  (defun my-company-ledger-setup ()
    (my-company-add-backend-locally 'company-ledger))
  (add-hook 'ledger-mode-hook #'my-company-ledger-setup))

(use-package flycheck-ledger
  :ensure t
  :after ledger-mode
  :defer t
  :hook (ledger-mode . flycheck-mode)
  :config (add-hook 'ledger-mode-hook (lambda () (flycheck-select-checker 'ledger))))

;; [ ob-ledger ] -- org-babel functions for ledger evaluation.

(use-package ob-ledger
  :defer t
  :after org
  :init
  (add-to-list 'org-babel-load-languages '(ledger . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  (add-to-list 'org-babel-tangle-lang-exts '("ledger" . "ledger"))
  (setq org-babel-default-header-args:ledger '((:results . "output") (:cmdline . "bal"))))

(with-eval-after-load 'org-capture
  (setq org-capture-templates
        (append org-capture-templates
                `(("l" ,(format "%s\tledger" (nerd-icons-faicon "nf-fa-money" :face 'nerd-icons-green)))
                  ;; Expense
                  ("le" ,(format "%s\tExpenses 支出" (nerd-icons-mdicon "nf-md-minus_circle_outline" :face 'nerd-icons-red)))
                  ("les" ,(format "%s\tShopping 购物" (nerd-icons-mdicon "nf-md-shopping_outline" :face 'nerd-icons-silver))
                   plain (file (lambda () (concat org-directory "/Accounting/finances.ledger")))
                   "%(org-read-date) %^{Event}\n expenses:shopping:%^{category}  %^{Amount}\n%?"
                   :empty-lines-before 1)
                  ("lef" ,(format "%s\tFood 食物" (nerd-icons-mdicon "nf-md-food_variant" :face 'nerd-icons-yellow))
                   plain (file (lambda () (concat org-directory "/Accounting/finances.ledger")))
                   "%(org-read-date) %^{Event}\n expenses:food:%^{meat,breakfast,lunch,dinner}  %^{Amount}\n%?"
                   :empty-lines-before 1)
                  ("let" ,(format "%s\tTraffic 交通" (nerd-icons-mdicon "nf-md-traffic_light_outline" :face 'nerd-icons-purple))
                   plain (file (lambda () (concat org-directory "/Accounting/finances.ledger")))
                   "%(org-read-date) %^{Event}\n expenses:traffic:%^{bus,train,plane}  %^{Amount}\n%?"
                   :empty-lines-before 1)
                  ("leh" ,(format "%s\tHouse Rent 房租" (nerd-icons-mdicon "nf-md-home" :face 'nerd-icons-silver))
                   plain (file (lambda () (concat org-directory "/Accounting/finances.ledger")))
                   "%(org-read-date) %^{Event}\n expenses:house rent:  %^{Amount}\n%?"
                   :empty-lines-before 1)
                  ("lee" ,(format "%s\tElectric fee 电费" (nerd-icons-mdicon "nf-md-meter_electric_outline" :face 'nerd-icons-blue))
                   plain (file (lambda () (concat org-directory "/Accounting/finances.ledger")))
                   "%(org-read-date) %^{Event}\n expenses:house rent:  %^{Amount}\n%?"
                   :empty-lines-before 1)
                  
                  ;; Income
                  ("li" ,(format "%s\tIncome 收入" (nerd-icons-mdicon "nf-md-plus_circle_outline" :face 'nerd-icons-green)))
                  ("lis" ,(format "%s\tSalary 工资收入" (nerd-icons-mdicon "nf-md-receipt_text_plus_outline" :face 'nerd-icons-green))
                   plain (file (lambda () (concat org-directory "/Accounting/finances.ledger")))
                   "%(org-read-date) %^{Event}\n income:salary:%^{account}  %^{Amount}\n%?"
                   :empty-lines-before 1)
                  ("lit" ,(format "%s\tTaobao 淘宝收入" (nerd-icons-mdicon "nf-md-store_plus" :face 'nerd-icons-green))
                   plain (file (lambda () (concat org-directory "/Accounting/finances.ledger")))
                   "%(org-read-date) %^{Event}\n income:salary:%^{account}  %^{Amount}\n%?"
                   :empty-lines-before 1)
                  
                  ;; Transfer
                  ("lt" ,(format "%s\tTransfer 转账" (nerd-icons-mdicon "nf-md-bank_transfer" :face 'nerd-icons-silver)))
                  ("lto" ,(format "%s\tsave money on online account 存钱到虚拟账户" (nerd-icons-mdicon "nf-md-currency_jpy" :face 'nerd-icons-green))
                   (file (lambda () (concat org-directory "/Accounting/finances.ledger")))
                   "%(org-read-date) %^{Event}\n transfer:%^{source} -> %^{ZhiFuBao}  %^{Amount}\n%?"
                   :empty-lines-before 1)
                  ("lto" ,(format "%s\tsave money in Bank 存钱到银行" (nerd-icons-mdicon "nf-md-bank_transfer_in" :face 'nerd-icons-green))
                   (file (lambda () (concat org-directory "/Accounting/finances.ledger")))
                   "%(org-read-date) %^{Event}\n transfer:%^{source} -> %^{ZhiFuBao}  %^{Amount}\n%?"
                   :empty-lines-before 1)
                  ("ltb" ,(format "%s\tTake out money from Bank 从银行取钱" (nerd-icons-mdicon "nf-md-bank_transfer_out" :face 'nerd-icons-red))
                   (file (lambda () (concat org-directory "/Accounting/finances.ledger")))
                   "%(org-read-date) %^{Event}\n transfer:%^{source} -> %^{bank}  %^{Amount}\n%?"
                   :empty-lines-before 1)
                  ("ltc" ,(format "%s\ttake out Cash 提现" (nerd-icons-mdicon "nf-md-cash_refund" :face 'nerd-icons-red))
                   (file (lambda () (concat org-directory "/Accounting/finances.ledger")))
                   "%(org-read-date) %^{Event}\n transfer:%^{source} -> cash  %^{Amount}\n%?"
                   :empty-lines-before 1)
                  
                  ;; Debt
                  ("ld" ,(format "%s\tDebt 债务" (nerd-icons-mdicon "nf-md-cash_clock" :face 'nerd-icons-red-alt)))
                  ("ldr" ,(format "%s\tRent 租金" (nerd-icons-mdicon "nf-md-cash_check" :face 'nerd-icons-silver))
                   plain (file (lambda () (concat org-directory "/Accounting/finances.ledger")))
                   "%(org-read-date) %^{Event}\n debt:rent:%^{people}  %^{Amount}\n%?"
                   :empty-lines-before 1)
                  ("ldb" ,(format "%s\tBorrow 借贷" (nerd-icons-mdicon "nf-md-cash_lock" :face 'nerd-icons-orange))
                   plain (file (lambda () (concat org-directory "/Accounting/finances.ledger")))
                   "%(org-read-date) %^{Event}\n debt:borrow:%^{people}  %^{Amount}\n%?"
                   :empty-lines-before 1)
                  
                  ;; Assets
                  ("la" ,(format "%s\tAssets 资产" (nerd-icons-mdicon "nf-md-piggy_bank_outline" :face 'nerd-icons-green)))
                  ("lab" ,(format "%s\tBank 银行" (nerd-icons-mdicon "nf-md-bank_outline" :face 'nerd-icons-silver))
                   plain (file (lambda () (concat org-directory "/Accounting/finances.ledger")))
                   "%(org-read-date) %^{Event}\n assets:bank:%^{bank}  %^{Amount}\n%?"
                   :empty-lines-before 1)
                  ("lao" ,(format "%s\tOnline Accounts 虚拟账户" (nerd-icons-mdicon "nf-md-wallet_outline" :face 'nerd-icons-green))
                   plain (file (lambda () (concat org-directory "/Accounting/finances.ledger")))
                   "%(org-read-date) %^{Event}\n assets:online-account:%^{ZhiFuBao}  %^{Amount}\n%?"
                   :empty-lines-before 1)))))

;;; [ hledger-mode ] -- Major mode for editing hledger.

;; (use-package hledger-mode
;;   :ensure t
;;   :mode (("\\.journal\\'" . hledger-mode)
;;          ("\\.hledger\\'" . hledger-mode))
;;   :commands (hledger-run-command hledger-jentry)
;;   :config
;;   (setq hledger-jfile (expand-file-name (concat org-directory "/Accounting/hledger.journal")))
;;
;;   (defun my/hledger-mode-setup ()
;;     ;; for company-mode
;;     (my-company-add-backend-locally 'hledger-company)
;;     ;; for auto-complete
;;     (setq-local ac-sources '(hledger-ac-source)))
;;   (add-hook 'hledger-mode-hook #'my/hledger-mode-setup)
;;
;;   ;; [ ob-hledger ]
;;   (require 'ob-hledger)
;;   (add-to-list 'org-babel-load-languages '(hledger . t))
;;   (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
;;   (add-to-list 'org-babel-tangle-lang-exts '("hledger" . "hledger")))



(provide 'init-tool-accounting)

;;; init-tool-accounting.el ends here
