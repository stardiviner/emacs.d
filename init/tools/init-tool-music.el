;;; init-tool-music.el --- init EMMS
;;; -*- coding: utf-8 -*-

;;; Commentary:


;;; Code:

;;; [ EMMS ]

;; (load "init-emms")

;;; [ listen.el ] -- Audio/Music player with `transient' menu.

(use-package listen
  :ensure t
  :commands (listen)
  :custom ((listen-directory "~/Music/My_Musics/"))
  :init
  ;; load player backend supports
  (progn
    (load-library "listen-mpd")
    (when (executable-find "vlc")
      (load-library "listen-vlc"))))

;;; [ mpdired ] -- A Dired-like client for Music Player Daemon (MPD)

(use-package mpdired
  :ensure t
  :custom-face (mpdired-song ((t (:inherit link))))
  :commands (mpdired)
  :init (add-to-list 'display-buffer-alist '("^\\*MPDired (.*)\\*" . (display-buffer-below-selected))))

;;; [ mingus ] -- MPD Interface

(use-package mingus
  :ensure t
  :defer t
  :custom ((mingus-mpd-config-file "~/.mpd/mpd.conf")
           (mingus-dired-add-keys t))
  :commands (mingus mingus-browse)
  :init
  (add-to-list 'display-buffer-alist '("^\\*Mingus\\*" . (display-buffer-below-selected)))
  (add-to-list 'display-buffer-alist '("^\\*Mingus Browser\\*" . (display-buffer-below-selected)))
  :config (define-key mingus-playlist-mode-map (kbd "SPC") 'mingus-pause))


(provide 'init-tool-music)

;;; init-tool-music.el ends here
