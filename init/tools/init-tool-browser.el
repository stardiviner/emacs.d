;;; init-tool-browser.el --- init for Browser
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ browse-url ] -- default browser function

(use-package browse-url
  :defer t
  :commands (browse-url-at-point)
  :config
  (defun my/macos-get-default-browser ()
    "Return the macOS system default web browse name."
    ;; (string-trim (shell-command-to-string "defaults read ~/Library/Preferences/com.apple.LaunchServices/com.apple.launchservices.secure | awk -F'\"' '/http;/{print window[(NR)-1]}{window[NR]=$2}'"))
    ;; Or:
    (url-unhex-string
     (ns-do-applescript
      "
use framework \"AppKit\"
use AppleScript version \"2.4\"
use scripting additions

property NSWorkspace : a reference to current application's NSWorkspace
property NSURL : a reference to current application's NSURL

set wurl to NSURL's URLWithString:\"https://www.apple.com\"
set thisBrowser to (NSWorkspace's sharedWorkspace)'s ¬
                    URLForApplicationToOpenURL:wurl
set appname to (thisBrowser's absoluteString)'s lastPathComponent()'s ¬
                stringByDeletingPathExtension() as text
return appname as text")))

  (cl-case system-type
    (gnu/linux
     (setq browse-url-chrome-program (executable-find "google-chrome-unstable"))
     ;; (setq browse-url-firefox-program (executable-find "firefox-developer-edition"))
     (setq browse-url-edge-program (executable-find "microsoft-edge-dev"))
     ;; set generic browser program for `browse-url-generic'.
     (setq-default browse-url-generic-program browse-url-firefox-program)
     ;; This is used by the `browse-url-at-point', `browse-url-at-mouse', and
     ;; `browse-url-of-file' commands.
     (setq-default browse-url-browser-function 'browse-url-firefox))
    (darwin
     (setq browse-url-chrome-program "/Applications/Google Chrome.app/Contents/MacOS/Google Chrome")
     (setq browse-url-firefox-program "/Applications/Firefox.app/Contents/MacOS/firefox")
     (setq browse-url-arc-program "/Applications/Arc.app/Contents/MacOS/Arc")

     ;; Setting generic browser program for `browse-url-generic' through `browse-url-generic-program'.
     ;;
     ;; The `browse-url-browser-function' is used by the `browse-url-at-point',
     ;; `browse-url-at-mouse', and `browse-url-of-file' commands.

     (pcase (my/macos-get-default-browser)
       ("Arc"
        (setq-default browse-url-generic-program browse-url-arc-program)
        (setq-default browse-url-browser-function 'browse-url-default-macosx-browser))
       ("Google Chrome"
        (setq-default browse-url-generic-program browse-url-chrome-program)
        (setq-default browse-url-browser-function 'browse-url-chrome)))))
  
  ;; Open with external program by pressing [W].
  ;; open file:// with web browser.
  (add-to-list 'browse-url-handlers '("\\`file:" . browse-url-default-browser))
  ;; open irc:// link with Emacs circe.
  ;; (add-to-list 'browse-url-default-handlers '("\\`irc:" . circe))
  )


(load "init-eww")
;; (load "init-w3m")

;;; [ xwidget-webkit ] -- `xwidget-webkit-browse-url'

;;; [ ace-link ] -- easier link selection with ace-mode on many buffer links.

(use-package ace-link ; [C-c M-o]
  :ensure t
  :defer t
  :init (ace-link-setup-default)
  (with-eval-after-load 'mu4e
    (define-key mu4e-view-mode-map (kbd "C-c M-o") 'ace-link-mu4e))
  (with-eval-after-load 'org
    (define-key org-mode-map (kbd "C-c M-o") 'ace-link-org))
  (with-eval-after-load 'org-agenda
    (define-key org-agenda-mode-map (kbd "C-c M-o") 'ace-link-org-agenda)))

(defun my/org-insert-url-decoded ()
  "Decoded the encoded URL.
For example, insert Chinese URL like https://zh.wikipedia.org/wiki/硅谷银行倒闭事件
instead of encoded URL https://zh.wikipedia.org/wiki/%E7%A1%85%E8%B0%B7%E9%93%B6%E8%A1%8C%E5%80%92%E9%97%AD%E4%BA%8B%E4%BB%B6."
  (interactive)
  (if-let ((url-str (thing-at-point-url-at-point)))
      (save-excursion
        (require 'expand-region)
        (er/mark-url)
        (kill-region (region-beginning) (region-end))
        (insert (decode-coding-string (url-unhex-string url-str) 'utf-8)))
    (let ((url-str (read-string "Input encoded URL: ")))
      (insert (decode-coding-string (url-unhex-string url-str) 'utf-8)))))


(provide 'init-tool-browser)

;;; init-tool-browser.el ends here
