;;; init-tool-utilities.el --- init file of utilities -*- lexical-binding: t; -*-

;;; Commentary:



;;; Code:

;;; [ qrencode ] -- QRCode encoder.

(use-package qrencode
  :ensure t
  :defer t
  :commands (qrencode-string qrencode-region qrencode-url-at-point)
  :init (add-to-list 'display-buffer-alist '("^\\*QRCode\\*" . (display-buffer-below-selected))))



(provide 'init-tool-utilities)

;;; init-tool-utilities.el ends here
