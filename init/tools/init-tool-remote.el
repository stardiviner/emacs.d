;;; init-tool-remote.el --- Emacs remote access -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Christopher M. Miles, all rights reserved.

;;; Commentary:



;;; Code:

;;; [ SSH ]

(load "init-SSH")



(provide 'init-tool-remote)

;;; init-tool-remote.el ends here
