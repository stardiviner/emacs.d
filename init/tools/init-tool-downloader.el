;;; init-tool-downloader.el --- init for Downloader
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ download-region ] -- simple in-buffer download manager for Emacs.

(use-package download-region
  :ensure t
  :defer t
  :commands (download-region-as-url))

;;; [ aria2 ] -- a major mode for controlling aria2c RPC daemon downloader.

(use-package aria2
  :vc t
  :load-path "~/Code/Emacs/aria2"
  :defer t
  ;; :custom (aria2-session-file (expand-file-name "session.lock" "/ssh:pi@192.168.31.36:~/.config/aria2/"))
  :commands (aria2-downloads-list)
  :custom ((aria2-download-directory (expand-file-name "~/Downloads")))
  :init (add-to-list 'display-buffer-alist '("\\*aria2: downloads list\\*" . (display-buffer-below-selected))))

;;; [ youtube-dl ] -- A youtube-dl download manager for Emacs.

(use-package youtube-dl
  :load-path "~/Code/Emacs/youtube-dl"
  :demand t
  :custom ((youtube-dl-program (expand-file-name "~/anaconda3/bin/yt-dlp"))
           (youtube-dl-proxy "socks5://127.0.0.1:7890") ; ClashX
           (youtube-dl-proxy-url-list '("youtube.com" "pornhub.com"))
           (youtube-dl-notify t))
  :commands (youtube-dl
             youtube-dl-list
             youtube-dl-download-playlist
             youtube-dl-download-audio youtube-dl-download-subtitle youtube-dl-download-thumbnail)
  :init
  (add-to-list 'display-buffer-alist '("^ \\*youtube-dl list\\*" . (display-buffer-below-selected display-buffer-reuse-window)))
  (add-to-list 'display-buffer-alist '("^ \\*youtube-dl log\\*" . (display-buffer-below-selected)))
  (add-to-list 'display-buffer-alist '("^ \\*youtube-dl vid:.*\\*" . (display-buffer-below-selected)))
  :config
  ;; auto download whole playlist for playlist URL.
  (add-to-list 'youtube-dl-extra-arguments "--yes-playlist" t)
  ;; download all subtitles
  (add-to-list 'youtube-dl-extra-arguments "--all-subs" t)
  (add-to-list 'youtube-dl-extra-arguments "--write-sub" t)
  ;; use cookies from web browser.
  (add-to-list 'youtube-dl-extra-arguments "--cookies-from-browser" t)
  (add-to-list 'youtube-dl-extra-arguments "chrome" t))

(defun my/downloader-douyin-convert-url-open (&optional pasted-text)
  "Convert the douyin share pasted URL into URL then auto open the link."
  (interactive)
  (let ((text (substring-no-properties
               (let ((pasted-text (or pasted-text
                                      (when interprogram-paste-function
                                        (funcall interprogram-paste-function)))))
                 (if (string-empty-p pasted-text)
                     (warn "It's EMPTY in pasting. Try to COPY again.")
                   pasted-text)))))
    (when (string-match "\\(https://v.douyin.com/.*/\\)" text)
      (let* ((url (match-string 1 text))
             (url-struct (url-generic-parse-url url)))
        (if (url-p url-struct)
            (browse-url url)
          (warn "Extracting URL from shared text FAILED!"))))))


(provide 'init-tool-downloader)

;;; init-tool-downloader.el ends here
