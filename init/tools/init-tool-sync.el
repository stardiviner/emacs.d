;;; init-tool-sync.el --- Emacs tools for sync -*- lexical-binding: t; -*-

;;; Commentary:



;;; Code:

;;; [ rsync-mode ] -- A minor mode to automatically rsync entire projects to remote hosts in Emacs.

(use-package rsync-mode
  :ensure t
  :commands (rsync rsync-all))

;;; [ tongbu ] -- Share text/file between your computer and phone.

(use-package tongbu
  :ensure t
  :commands (tongbu))

;;; [ syncthing ] -- Client for Syncthing

(use-package syncthing
  :ensure t
  :commands (syncthing)
  :custom (syncthing-default-server-token "eqe5de3KQhHAHk7rKLyQWbZdZYdTPKxi")) ; Syncthing REST API token



(provide 'init-tool-sync)

;;; init-tool-sync.el ends here
