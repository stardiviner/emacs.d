;;; init-tool-clock.el --- init Clock tools for Emacs
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ timeclock ] -- mode for keeping track of how much you work. similar with `org-clock-in' and `org-clock-out'.

;;; [ time ] -- display time, load and mail indicator in mode line of Emacs.

(use-package time
  :defer t
  :custom ((world-clock-list '(("Asia/Shanghai" "Shanghai")
                               ("Asia/Tokyo" "Tokyo")
                               ("America/Los_Angeles" "Silicon Valley")
                               ("America/New_York" "New York")
                               ("America/Los_Angeles" "Los Angeles")
                               ("Europe/Paris" "Paris")
                               ("Europe/London" "London")
                               )))
  :commands (display-time-mode world-clock)
  :init (add-to-list 'display-buffer-alist '("\\*wclock\\*" . (display-buffer-below-selected))))

;;; [ alarm-clock ] -- An alarm clock for Emacs.

;; (use-package alarm-clock
;;   :ensure t
;;   :defer t
;;   :commands (alarm-clock-set alarm-clock-list-view))

;;; [ tmr ] -- TMR provides facilities for setting timers using a convenient notation.

(use-package tmr
  :ensure t
  :defer t
  :commands (tmr tmr-with-description tmr-tabulated-view))

;;; [ egg-timer ] -- Commonly used intervals for setting timers while working.

;; (use-package egg-timer
;;   :ensure t
;;   :defer t
;;   :commands (egg-timer-schedule))


(provide 'init-tool-clock)

;;; init-tool-clock.el ends here
