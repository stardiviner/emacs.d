;;; init-asymptote.el --- init file for asymptote -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Christopher M. Miles, all rights reserved.

;;; Commentary:



;;; Code:

;;; [ asymptote ] -- A vector graphics language (like metapost).

(use-package ob-asymptote
  :defer t
  :after org
  :init
  (add-to-list 'org-babel-load-languages '(asymptote . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  (add-to-list 'org-babel-tangle-lang-exts '("asymptote" . "asy")))



(provide 'init-asymptote)

;;; init-asymptote.el ends here
