;;; init-tool-audio.el --- init for Audio in Emacs
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:
;;; ----------------------------------------------------------------------------
;;; [ PulseAudio ] -- Use `pactl' to manage PulseAudio volumes.

;; (use-package pulseaudio-control
;;   :ensure t
;;   :defer t)

;;; [ ecasound ] -- command-line multitrack audio processor.

;; (use-package ecasound
;;   :if (eq system-type 'gnu/linux)
;;   :load-path "/usr/share/ecasound/"
;;   :commands (ecasound))

;;; [ SoX.el ] -- helper for interacting with SoX, the Swiss Army knife of sound processing programs.

;; (use-package sox
;;   :vc (:url "https://github.com/vxe/SoX.el")
;;   :commands (sox)
;;   :init (add-to-list 'display-buffer-alist '("^\\*sox\\*.*" . (display-buffer-below-selected))))

(defun sox-record-sound ()
  "Record sound with SoX command in Emacs."
  (interactive)
  (let ((kill-buffer-query-functions nil)
        (buffer (shell "*SoX recording*")))
    (comint-simple-send buffer "sox -t alsa default ~/sox-recording.wav")
    (sit-for 2))
  (message "Recording sound to file '~/sox-recording.wav' in background."))

(add-to-list 'display-buffer-alist '("^\\*SoX recording\\*" . (display-buffer-below-selected)))

;;; ----------------------------------------------------------------------------

(provide 'init-tool-audio)

;;; init-tool-audio.el ends here
