;;; init-tool-subtitle.el --- init for Editing Subtitle
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ Simple Emacs-based Subtitle Editor (SESE) ]

;; (load (expand-file-name "init/extensions/sese.el" user-emacs-directory))
;; (autoload 'sese-mode "sese" "Subtitle Editor major mode" t)
;; (setq auto-mode-alist
;;       (cons '("\\.sese\\'" . sese-mode) auto-mode-alist))

;;; [ subed ] -- Subtitle editor for Emacs

(use-package subed
  :load-path "~/Code/Emacs/subed/subed/"
  :ensure context-transient
  :init
  (require 'subed-srt)
  (require 'subed-vtt)
  (require 'subed-ass)
  :mode (("\\.srt\\'" . subed-srt-mode)
         ("\\.vtt\\'" . subed-vtt-mode)
         ("\\.ass\\'" . subed-ass-mode))
  :config
  (with-eval-after-load 'subed-mode
	;; Remember cursor position between sessions
	(add-hook 'subed-mode-hook 'save-place-local-mode)
	;; Break lines automatically while typing
	(add-hook 'subed-mode-hook 'turn-on-auto-fill)
	;; Break lines at 40 characters
	(add-hook 'subed-mode-hook (lambda () (setq-local fill-column 40)))
	;; Some reasonable defaults
	(add-hook 'subed-mode-hook 'subed-enable-pause-while-typing)
	;; As the player moves, update the point to show the current subtitle
	(add-hook 'subed-mode-hook 'subed-enable-sync-point-to-player)
	;; As your point moves in Emacs, update the player to start at the current subtitle
	(add-hook 'subed-mode-hook 'subed-enable-sync-player-to-point)
	;; Replay subtitles as you adjust their start or stop time with M-[, M-], M-{, or M-}
	(add-hook 'subed-mode-hook 'subed-enable-replay-adjusted-subtitle)
	;; Loop over subtitles
	(add-hook 'subed-mode-hook 'subed-enable-loop-over-current-subtitle)
	;; Show characters per second
	(add-hook 'subed-mode-hook 'subed-enable-show-cps))
  
  (context-transient-define subed-transient
    :doc "Transient for working with subtitles in `subed-srt-mode'"
    :mode 'subed-mode
    :menu
    ["MPV"
     ("o" "Open video file" subed-mpv-play-from-file)
     ("O" "Open video url" subed-mpv-play-from-url)
     ("s" "Toggle sync subs -> player" subed-toggle-sync-player-to-point)
     ("p" "Toggle sync player -> subs" subed-toggle-sync-point-to-player)])
  )

(provide 'init-tool-subtitle)

;;; init-tool-subtitle.el ends here
