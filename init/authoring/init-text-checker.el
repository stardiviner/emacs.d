;;; init-text-checker.el --- init for spell settings.
;;; Commentary:

;;; Code:

;; bind to [M-g] keybindings.
(unless (boundp 'text-checker-prefix)
  (define-prefix-command 'text-checker-prefix))
(global-set-key (kbd "M-g w") 'text-checker-prefix)

;;; [ aspell & ispell ] -- interface to spell checkers.

(use-package ispell
  :ensure t
  :defer t
  :bind (:map text-checker-prefix
              ("s" . ispell-word) ; [M-$]
              ("<tab>" . ispell-complete-word))
  :custom ((ispell-dictionary "english")
           (ispell-silently-savep t))
  :config
  ;; --reverse :: fix `aspell' conflict bug with `ispell'.
  (when (string-equal ispell-program-name "ispell")
    (setq ispell-extra-args '("--reverse" "--sug-mode=ultra" "--lang=en_US")))
  ;; skip regions in Org-mode for ispell.
  (add-to-list 'ispell-skip-region-alist '("[^\000-\377]+"))
  (add-to-list 'ispell-skip-region-alist '(":\\(PROPERTIES\\|LOGBOOK\\):" . ":END:"))
  (add-to-list 'ispell-skip-region-alist '("#\\+begin_src" . "#\\+end_src"))
  (add-to-list 'ispell-skip-region-alist '("#\\+begin_example" . "#\\+end_example")))

;;; [ Flyspell ] -- Emacs built-in on-the-fly spell checker.

(use-package flyspell
  :defer t
  :delight flyspell-mode
  :custom ((flyspell-mode-line-string " Flyspell")
           (flyspell-default-dictionary "en")
           ;; (flyspell-before-incorrect-word-string "✗")
           ;; (flyspell-after-incorrect-word-string "✗")
           (flyspell-consider-dash-as-word-delimiter-flag t)
           (flyspell-issue-message-flag nil)
           (flyspell-use-meta-tab nil)
           (flyspell-auto-correct-binding (kbd "C-;"))
           ;; Remove `handle-switch-frame', `handle-select-window' from `flyspell-deplacement-commands' to avoid frequent flyspell.
           (flyspell-deplacement-commands '(next-line previous-line scroll-up scroll-down)))
  :bind ((:map flyspell-mode-map
               ("C-," . flyspell-goto-next-error))
         (:map text-checker-prefix
               ("m" . flyspell-mode)
               ("w" . flyspell-buffer)
               ("n" . flyspell-goto-next-error)))
  :preface (defun flyspell-latex-mode ()
             "Enable flyspell-mode in tex-mode / latex-mode."
             (interactive)
             (let ((ispell-parser 'tex))
               (if flyspell-mode
                   (progn
                     (flyspell-mode -1)
                     (message "flyspell-mode disabled."))
                 (flyspell-mode 1)
                 (message "flyspell-mode enabled."))))
  :hook (;; (prog-mode . flyspell-prog-mode) ; PERFORMANCE: disable `flyspell' in `prog-mode' for smoother input.
         ;; (text-mode . flyspell-mode)
         ;; (org-mode  . flyspell-mode)
         (markdown-mode . flyspell-mode)
         (latex-mode . flyspell-latex-mode)))

;;; [ flyspell-correct ] -- correcting words with flyspell via custom interface.

(use-package flyspell-correct ; Input "@" to access Actions.
  :ensure t
  :defer t
  :after flyspell
  :delight flyspell-correct-auto-mode
  :bind ((:map flyspell-mode-map
               ("C-;" . flyspell-correct-wrapper)
               ("C-." . flyspell-correct-word-before-point))
         (:map text-checker-prefix
               ("c" . flyspell-correct-word-before-point)))
  ;; Minor mode for automatically correcting word at point.
  ;; :hook (flyspell-mode . flyspell-correct-auto-mode)
  )

;;; [ flycheck-grammarly ] -- Grammarly support for Flycheck.

;; (use-package flycheck-grammarly
;;   :ensure t
;;   :config
;;   (add-hook 'mu4e-compose-mode-hook
;;             (lambda ()
;;               (flycheck-mode 1)
;;               ;; NOTE this `flycheck-grammarly' causes suspend in `mu4e-compose-mode' buffer editing.
;;               (setq-local flycheck-checker 'grammarly-checker))))

;;; [ jinx ] -- Enchanted just-in-time spell checker.

;; (use-package jinx
;;   :ensure t
;;   ;; http://aspell.net/0.61/man-html/Languages-Which-Aspell-can-Support.html
;;   ;; :custom ((jinx-languages '("en")))
;;   :hook ((emacs-startup . global-jinx-mode)
;;          (text-mode . jinx-mode)
;;          (prog-mode . jinx-mode)
;;          (conf-mode . jinx-mode))
;;   :commands (jinx-correct)
;;   :bind (([remap ispell-word] . jinx-correct)
;;          ("C-;" . jinx-correct))
;;   :config
;;   ;; disable for Chinese characters
;;   (add-to-list 'jinx-exclude-regexps '(t "\\cc") 'append))


(provide 'init-text-checker)

;;; init-text-checker.el ends here
