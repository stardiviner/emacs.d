;;; init-org-mode.el --- init for Org-mode
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

(unless (boundp 'Org-prefix)
  (define-prefix-command 'Org-prefix))
(global-set-key (kbd "C-c o") 'Org-prefix)


(let ((org-dir "~/Org"))
  (unless (file-exists-p org-dir)
    (make-directory org-dir))
  (setq org-directory org-dir))

(load "init-org-document-structure")
(load "init-org-view")
(load "init-org-keybindings")
(load "init-org-complete")
(load "init-org-macro")
(load "init-org-table")
(load "init-org-hyperlink")
;; (load "init-org-bibliography")
(load "init-org-todo")
(load "init-org-tag")
(load "init-org-property")
(load "init-org-time")
(load "init-org-clock")
(load "init-org-babel")
(load "init-org-block")
(load "init-org-latex")
(load "init-org-image")
(load "init-org-capture")
(load "init-org-agenda")
(load "init-org-project-management")
(load "init-org-export")
(load "init-org-import")
(when (file-exists-p "~/Org/Website/")
  (load "init-org-publish"))
(load "init-org-search")
(load "init-org-attach")
(load "init-org-protocol")
(load "init-org-extensions")
(load "init-org-programming")
;; (load "init-org-mindmap")
(load "init-org-roam")
(load "init-org-presentation")
(load "init-org-contacts")
(load "init-org-password")
(load "init-org-drill")
;; (load "init-org-mobile")
;; (load "init-org-trello")



(provide 'init-org-mode)

;;; init-org-mode.el ends here
