;;; init-org-contacts.el --- init for Org Contacts
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ org-contacts ] -- Contacts management in Org-mode.

(use-package org-contacts
  :ensure t
  :demand t
  :custom ((org-contacts-files (list (concat org-directory "/Contacts/Contacts.org")))
           (org-contacts-icon-use-gravatar nil)
           (org-contacts-icon-property "AVATAR")
           (org-contacts-birthday-property "BIRTHDAY")
           (org-contacts-birthday-format "BIRTHDAY: %h (%Y)")
           (org-contacts-matcher
            (apply 'concat
                   (mapcar
                    (lambda (property) (concat property "<>\"\"|"))
                    '("NICK" "NAME" "NAME(Chinese)" "NAME(English)"
                      "ALIAS" "WeChat_Name"
                      "EMAIL" "Mailing-List" "MOBILE"
                      "GitHub"
                      "ADDRESS" "ADDRESS(home)" "ADDRESS(live)"
                      "SKILLS" "Programming-Skills" "Programming-Languages" "Programming-Projects"
                      "OCCUPATION" "HOBBIES"
                      "NOTE" "FIRST-MEET")))))
  :hook (message-mode . org-contacts-setup-completion-at-point)
  :config
  ;; add org-capture template for org-contacts.
  (add-to-list 'org-capture-templates
               `("C" ,(format "%s\tRecord a new contact to Contacts.org" (nerd-icons-mdicon "nf-md-card_account_details" :face 'nerd-icons-blue))
                 entry (file ,(expand-file-name (car org-contacts-files)))
                 "* %^{NAME}
:PROPERTIES:
:DIR:  %\\1
:DATE: %^U
:AVATAR: %^{Avatar}
:NICK: %^{Nick}
:NAME(Chinese): %^{Name(Chinese)}
:NAME(English): %^{Name(English)}
:GENDER: %^{Gender|Male|Female|Transgender}
:RELATIONSHIP: %^{Relationship|Internet|Meet|Friend|Good Friend|Boy Friend|Girl Friend|Workmate|Classmate|Schoolmate}
:FIRST-MEET: %^U  %^{How is the first-time meet? when? where? how?}
:MOBILE: %^{Mobile Phone}
:EMAIL: %^{Email}
:HOMEPAGE: %^{Homepage}
:GitHub: %^{GitHub}
:BIRTHDAY: %^U
:BIRTHPLACE: %^{address(birth)}
:ADDRESS(home): %^{address(home)}
:ADDRESS(live): %^{address(live)}
:LANGUAGES: %^{Languages|Chinese|Chinese, English|English|Japanese|Korean}
:EDUCATION: %^{Education}
:School(university):
:SKILLS: %^{Skills|Programming|Economy}
:Programming-Skills: %^{Programming Skills|Emacs|Web|Computer System|Cloud Computation}
:Programming-Languages: %^{Programming Languages|LISP|Common Lisp|Clojure|Emacs Lisp|Java|C/C++|Python|Ruby|PHP}
:OCCUPATION: %^{Occupation|Programmer|Freelancer|Businessman|Servant|Arter}
:HOBBIES: %^{Hobbies|Reading|Music|Movie|Travel}
:END:"
                 :empty-lines 0 :jump-to-captured t
                 :refile-targets (("~/Org/Contacts/Contacts.org" :maxlevel 2)))
               :append)
  
  (unless (boundp 'Org-prefix) (define-prefix-command 'Org-prefix))
  (define-key Org-prefix (kbd "M-c") 'org-contacts) ; create agenda view for contacts matching NAME.

  (when (featurep 'mu4e-compose)
    (with-eval-after-load 'mu4e-compose
      (add-hook 'mu4e-compose-mode-hook 'org-contacts-setup-completion-at-point)))

  ;; org-contacts contact complete
  (defun my/org-contacts-capf-setup ()
    "Setup `completion-at-point-functions' with `org-contacts' in buffer local."
    (add-hook 'completion-at-point-functions 'org-contacts-org-complete-function nil 'local))
  (add-hook 'org-mode-hook #'my/org-contacts-capf-setup)
  
  ;; [C-c C-x p] add "EMAIL" property complete with `mu4e-contacts' hash.
  (when (featurep 'mu4e)
    (require 'mu4e-contacts)
    (defun org-property-complete-email (prompt &rest args)
      ;; Variable `mu4e--contacts-hash' is replaced by `mu4e--contacts-set' after commit "db86e7b5ee35".
      (cond
       ((boundp 'mu4e--contacts-hash)
        (completing-read
         "EMAIL: "
         (let ((contacts))
           (maphash (lambda (addr rank)
                      (setq contacts (cons (cons rank addr) contacts)))
                    mu4e--contacts-hash)
           (setq contacts (sort contacts
                                (lambda(cell1 cell2) (< (car cell1) (car cell2)))))
           (mapcar 'cdr contacts))))
       ((boundp 'mu4e--contacts-set)
        (completing-read
         "EMAIL: "
         (let ((contacts))
           (maphash (lambda (addr rank)
                      (setq contacts (cons (cons rank addr) contacts)))
                    mu4e--contacts-set)
           (setq contacts (sort contacts
                                (lambda(cell1 cell2) (< (car cell1) (car cell2)))))
           (mapcar 'cdr contacts))))))
    (add-to-list 'org-property-set-functions-alist '("EMAIL" . org-property-complete-email)))
  )


(provide 'init-org-contacts)

;;; init-org-contacts.el ends here
