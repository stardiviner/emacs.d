;;; init-org-property.el --- init for Org Properties
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;; Apply properties also for sublevels.
;; This setting is chiefly used during property searches.
;; Turning it on can cause significant overhead when doing a search, which is why it is not on by default.
;; (setq org-use-property-inheritance t)

;; This will be combined with constant `org-global-properties-fixed'
(add-to-list 'org-global-properties '("Effort" . "0:30 0:45 1:00 1:30 2:00"))
(add-to-list 'org-global-properties '("AUTHOR" . "stardiviner"))

(add-to-list 'org-default-properties "LOCATION")
(add-to-list 'org-default-properties "CLASS")

(add-to-list 'org-default-properties "ALIAS")
(add-to-list 'org-default-properties "SOURCE")
(add-to-list 'org-default-properties "EMAIL")
(add-to-list 'org-default-properties "URL")
(add-to-list 'org-default-properties "SCORE")

(add-to-list 'org-default-properties "Source-Code")
(add-to-list 'org-default-properties "ISSUE")
(add-to-list 'org-default-properties "Pull-Request")
(add-to-list 'org-default-properties "COMMIT")
(add-to-list 'org-default-properties "CONFIG")

(add-to-list 'org-default-properties "Translation-Chinese")
(add-to-list 'org-default-properties "Translator")
(add-to-list 'org-default-properties "PAPER")

(add-to-list 'org-default-properties "PRICE")

(add-to-list 'org-default-properties "Wikipedia")
(add-to-list 'org-default-properties "IMDb")
(add-to-list 'org-default-properties "ISBN")
(add-to-list 'org-default-properties "Twitter")
(add-to-list 'org-default-properties "YouTube")
(add-to-list 'org-default-properties "Facebook")
(add-to-list 'org-default-properties "Pixiv")
(add-to-list 'org-default-properties "douban.com")
(add-to-list 'org-default-properties "bilibili.com")
(add-to-list 'org-default-properties "weibo.com")
(add-to-list 'org-default-properties "taobao.com")
(add-to-list 'org-default-properties "JD.com")

(add-to-list 'org-default-properties "番号")
(add-to-list 'org-default-properties "女优")
(add-to-list 'org-default-properties "发行商")
(add-to-list 'org-default-properties "导演")
(add-to-list 'org-default-properties "系列")

;;===================================================================================================

(defmacro org-completing-read-property-construct-function (property-key property-values-list-custom-var)
  "A macro to construct the function of property completing-read."
  (let ((property-str (downcase property-key)))
    ;; Make sure `property-values-list-custom-var' defcustom variable exist.
    (unless (bound-and-true-p property-values-list-custom-var)
      (error "Does not have `defcustom' defined variable %s" (symbol-name 'property-values-list-custom-var)))
    `(defun ,(intern (concat "org-completing-read-property_" property-str))
         (prompt collection
                 &optional predicate require-match initial-input
                 hist def inherit-input-method)
       ,(format "Like `completing-read' but reads a property '%s' value." property-key)
       (let
           ((property-value (org-completing-read prompt
                                                 (append collection ,property-values-list-custom-var)
                                                 predicate require-match initial-input
                                                 hist def inherit-input-method)))
         (unless (member property-value ,property-values-list-custom-var)
           (add-to-list ',property-values-list-custom-var (substring-no-properties property-value) 'append))
         property-value))))

;;===================================================================================================

;;; `org-properties-postprocess-alist': Alist of properties and functions to adjust inserted values.

;;===============================================================================
;;; auto evaluate inline source block in property "EVAL".

(defcustom org-property-eval-keyword "EVALUATE_INLINE"
  "A property keyword for evaluate code."
  :type 'string
  :safe #'stringp
  :group 'org)

(add-to-list 'org-default-properties org-property-eval-keyword)

(defun org-property-eval-on-cycle-expand (&optional state)
  "Evaluate Org inline source block in property value on headline cycle expand."
  (when (memq state '(children)) ; NOTE: Only execute inline code on current headline expand, execlude `subtree' expand.
    (if-let ((inline-src-block (org-entry-get nil org-property-eval-keyword nil)))
        (with-temp-buffer
          (org-mode) ; change `fundamental-mode' to `org-mode' for bellowing `org-element-context'.
          (insert inline-src-block)
          (goto-char (point-min))
          (let* ((context (org-element-context))
                 (lang (org-element-property :language context))
                 (type (org-element-type context))
                 (src-block-info (org-babel-get-src-block-info nil context)))
            (when (eq type 'inline-src-block)
              (org-babel-execute-src-block
               nil src-block-info
               (pcase lang
                 ("sh" `((:session . ,(make-temp-name " *ob-sh-inline-async (sh) ")) (:async . "yes") (:stdin . nil) (:results . "silent")))
                 ("shell" `((:session . ,(make-temp-name " *ob-sh-inline-async (shell) ")) (:async . "yes") (:stdin . nil) (:results . "silent")))
                 ("bash" `((:session . ,(make-temp-name " *ob-sh-inline-async (bash) ")) (:async . "yes") (:stdin . nil) (:results . "silent")))
                 ("zsh" `((:session . ,(make-temp-name " *ob-sh-inline-async (zsh) ")) (:async . "yes")(:stdin . nil) (:results . "silent")))
                 ("python" `((:session . ,(make-temp-name "ob-python-inline-async ")) (:async . "yes") (:results . "silent")))
                 (_ '((:results . "none")))))))))))

(add-hook 'org-cycle-hook #'org-property-eval-on-cycle-expand)

(defvar org-property-eval-templates
  '(;; Found solution to make mpv play video in "EVAL" property inline src block evaluation.
    ;; Add option "--no-terminal" or "--no-input-terminal" for command "mpv".
    ;; It will suppress terminal interaction.
    ;; Add option "--no-control" for command "mpg123" to disable terminal control.
    "src_sh{mpv --no-terminal \"path/to/video.mp4\"}"
    "src_bash{mpg123 --no-control \"path/to/music.mp3\"}"
    "src_python{import os; os.system(\"mpv --no-terminal 'path/to/video.mp4' \")}"))
(org-completing-read-property-construct-function "EVAL" org-property-eval-templates)
(add-to-list 'org-property-set-functions-alist '("EVAL" . org-completing-read-property_eval))

;;; `org-optimize-window-after-visibility-change' which is performance slow.
;; (remove-hook 'org-cycle-hook #'org-optimize-window-after-visibility-change)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;     Auto expand properties drawer and logbook on TAB `org-cycle'.                ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun org-property-drawer-start-position ()
  "Return the position of property drawer start of current heading."
  (save-excursion
    (org-back-to-heading)
    (search-forward-regexp org-property-start-re nil t)))

(defun org-property-drawer-end-position ()
  "Return the position of property drawer end of current heading."
  (save-excursion
    (org-back-to-heading)
    (search-forward-regexp org-property-end-re nil t)))

(defun org-logbook-drawer-start-position ()
  "Return the position of logbook drawer start of current heading."
  (save-excursion
    (org-back-to-heading)
    (search-forward-regexp "^[	 ]*:LOGBOOK:" nil t)))

(defun org-logbook-drawer-end-position ()
  "Return the position of logbook drawer end of current heading."
  (save-excursion
    (org-back-to-heading)
    ;; You MUST search logbook start at first to avoid return the :END: keyword of property drawer.
    (search-forward-regexp "^[	 ]*:LOGBOOK:" nil t)
    (search-forward-regexp "^[	 ]*:END:" nil t)))

(defun my/org-cycle-auto-expand-property-drawer-and-logbook (&optional state)
  "Auto toggle the visibility of property drawer and logbook drawer when content lines is less."
  (when (memq state '(children subtree))
    (let ((property-drawer-lines-max 15)
          (logbook-drawer-lines-max 15))
      (save-excursion
        (org-back-to-heading)
        (let* ((property-drawer-beginning-position (org-property-drawer-start-position))
               (property-drawer-end-position (org-property-drawer-end-position))
               (property-drawer-beginning-line (line-number-at-pos property-drawer-beginning-position))
               (property-drawer-end-line (line-number-at-pos property-drawer-end-position))
               (property-drawer-lines (- property-drawer-end-line property-drawer-beginning-line 1))
               (logbook-drawer-beginning-position (org-logbook-drawer-start-position))
               (logbook-drawer-end-position (org-logbook-drawer-end-position))
               (logbook-drawer-beginning-line (line-number-at-pos logbook-drawer-beginning-position))
               (logbook-drawer-end-line (line-number-at-pos logbook-drawer-end-position))
               (logbook-drawer-lines (- logbook-drawer-end-line logbook-drawer-beginning-line 1)))
          (when property-drawer-beginning-position ; ensure property drawer exist.
            (if (< property-drawer-lines property-drawer-lines-max)
                (org-with-point-at property-drawer-beginning-position
                  (org-fold-hide-drawer-toggle 'off))
              (org-with-point-at property-drawer-beginning-position
                (org-fold-hide-drawer-toggle t))))
          (when logbook-drawer-beginning-position ; ensure logbook drawer exist.
            (if (< logbook-drawer-lines logbook-drawer-lines-max)
                (org-with-point-at logbook-drawer-beginning-position
                  (org-fold-hide-drawer-toggle 'off))
              (org-with-point-at logbook-drawer-beginning-position
                (org-fold-hide-drawer-toggle t)))))))))

(add-hook 'org-cycle-hook #'my/org-cycle-auto-expand-property-drawer-and-logbook)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Auto complete default values for org-mode properties.                                                ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;============================================ directory ============================================

(add-to-list 'org-default-properties "DIR")

(defun org-completing-read-property_dir (prompt collection
                                                &optional predicate require-match initial-input
                                                hist def inherit-input-method)
  "Like `read-directory-name' but reads a property 'DIR' value.
Return a DIR as property's value after completion."
  ;; reference [C-x C-f] `find-file'
  (file-relative-name
   (substring-no-properties
    (read-file-name "Select directory for Org property \"DIR\": "
                    default-directory nil (confirm-nonexistent-file-or-buffer)))
   default-directory))

(add-to-list 'org-property-set-functions-alist '("DIR" . org-completing-read-property_dir))

;;=========================================== date & time ===========================================

(add-to-list 'org-default-properties "DATE")

(defun org-completing-read-property_date (prompt collection
                                                 &optional predicate require-match initial-input
                                                 hist def inherit-input-method)
  "Like `org-read-date' but reads a property 'DATE' value.
Return a DATE as property's value after completion."
  (org-read-date nil nil nil prompt))

(add-to-list 'org-property-set-functions-alist '("DATE" . org-completing-read-property_date))


(add-to-list 'org-default-properties "TIME")

(defun org-completing-read-property_time (prompt collection
                                                 &optional predicate require-match initial-input
                                                 hist def inherit-input-method)
  "Like `org-read-date' but reads a property 'TIME' value.
Return a DATE as property's value after completion."
  ;; FIXME: it doesn't insert time, only date.
  (org-read-date t nil nil prompt))

(add-to-list 'org-property-set-functions-alist '("TIME" . org-completing-read-property_time))

;;================================== org-contacts as AUTHOR source ==================================

(add-to-list 'org-default-properties "NAME")

(with-eval-after-load "org-contacts"
  (defvar org-contacts-all-contacts
    (org-contacts--all-contacts)
    "A variable of all `org-contacts' contacts."))

;; (benchmark-progn
;;   (with-memoization org-contacts-all-contacts
;;     (setq org-contacts-all-contacts (org-contacts--all-contacts))))

(defun org-completing-read-property_contact (prompt collection
                                                    &optional predicate require-match initial-input
                                                    hist def inherit-input-method)
  "Like `completing-read' but reads a property 'NAME' value.
Return a org-contacts link as property's value after completion."
  (let* ((org-contacts-all-contacts (with-memoization org-contacts-all-contacts
                                      (org-contacts--all-contacts)))
         (contact-names-propertized
          (mapcar
           (lambda (plist)
             (let* ((name (plist-get plist :name))
                    (name-chinese (plist-get plist :name-chinese))
                    (name-english (plist-get plist :name-english))
                    (nick (plist-get plist :nick))
                    (email (plist-get plist :email)))
               (propertize name ; <- The `completing-read' select candidate inserted value.
                           'display (concat
                                     (when name (propertize (format "%s " name) :face '(:foreground "ForestGreen")))
                                     (unless (or (null name-english) (string-empty-p name-english))
                                       (propertize (format "%s " name-english) :face '(:foreground "LightSeaGreen")))
                                     (unless (or (null nick) (string-empty-p nick))
                                       (propertize (format "(%s) " nick) :face '(:foreground "LightGray")))))))
           org-contacts-all-contacts))
         (contact-names (mapcar (lambda (plist) (plist-get plist :name)) org-contacts-all-contacts))
         (contact-name (substring-no-properties
                        (completing-read "org-contacts NAME: "
                                         (append contact-names-propertized collection)
                                         predicate require-match initial-input
                                         hist def inherit-input-method))))
    ;; Detect whether input contact is in `org-contacts' existing list.
    (if (member contact-name contact-names)
        (if (yes-or-no-p "[Org mode] property insert org-contacts link?: ")
            (format "[[org-contact:%s]]" contact-name)
          contact-name)
      contact-name)))

(add-to-list 'org-property-set-functions-alist '("NAME" . org-completing-read-property_contact))
(add-to-list 'org-property-set-functions-alist '("AUTHOR" . org-completing-read-property_contact))
(add-to-list 'org-property-set-functions-alist '("WRITER" . org-completing-read-property_contact))
(add-to-list 'org-property-set-functions-alist '("EDITOR" . org-completing-read-property_contact))
(add-to-list 'org-property-set-functions-alist '("ARTIST" . org-completing-read-property_contact))

(add-to-list 'org-default-properties "NICK")
(autoload 'org-contacts-completing-read-nickname "org-contacts")
(add-to-list 'org-property-set-functions-alist '("NICK" . org-contacts-completing-read-nickname))

;;=============================================== book ==============================================

;;; book related properties
(add-to-list 'org-default-properties "AUTHOR")
(add-to-list 'org-default-properties "WRITER")
(add-to-list 'org-default-properties "EDITOR")
(add-to-list 'org-default-properties "ARTIST")
(add-to-list 'org-default-properties "CREATOR")
(add-to-list 'org-default-properties "PUBLISHER")
(add-to-list 'org-default-properties "PUBLISHER(China)")
(add-to-list 'org-default-properties "PUBLISH_DATE")
(add-to-list 'org-default-properties "PRESS")
(add-to-list 'org-default-properties "SERIES")
(add-to-list 'org-default-properties "YEAR")
(add-to-list 'org-default-properties "PAGES")

(defcustom org-property-publisher-list
  '("O'Reilly Media" "Manning Publications" "Packt Publishing" "Apress" "Springer" "No Starch Press" "The Pragmatic Bookshelf"
    "The MIT Press" "Stanford University Press" "University of California Press" "University of Washington Press"
    "Wiley" "Wiley-Blackwell" "For Dummies" "Technics Publications" "BPB Publications" "CRC Press" "Ray Wenderlich" "Razeware LLC"
    "SitePoint" "Wrox" "Kogan Page" "Riverhead Books" "Sourcebooks, Inc." "Quiver Books" "Skyhorse Publishing"
    "Independently Published" "Little, Brown and Company" "Cambridge University Press" "Addison-Wesley" "Addison-Wesley Professional" "IBM Press"
    "World Scientific Publishing Company" "Peachpit Press" "Penguin" "McGraw-Hill" "Prentice Hall" "Payload Media" "Pearson" "Morgan Kaufmann"
    "Stripe Press" "DK Publishing" "Elsevier" "Syngress" "Academic Press" "Nova Science Pub Inc" "Oxford University Press" "Charles River"
    "W. H. Freeman" "Garland Science" "Princeton University Press" "Portfolio" "Penguin Books Ltd" "CreateSpace Independent Publishing Platform"
    "Butterworth-Heinemann" "John Wiley & Sons" "Alpha Science International Ltd." "Cengage Learning" "Humana Press" "Artech House" "Jossey-Bass"
    "Tsinghua University Press" "Harper Business" "Apriorit Inc." "LeanPub" "Jeffrey Ullman" "Payload Media" "National Academies Press"
    "Practical Inspiration Publishing" "Imagine Publishing" "Self Publish" "Routledge" "River Publishers" "Kuhlman Publishing" "Rocky Nook"
    "Syncfusion" "SAGE Publications Ltd" "The Guilford Press" "Esri Press" "friendsofED" "Rheinwerk Publishing Inc" "Orange Education Pvt Ltd"
    "Roaring Brook Press" "Sams Publishing" "Mercury Learning and InforMation" "Palgrave Macmillan" "Wadsworth Publishing Company" "OpenTask"
    "Adobe Press" "Microsoft" "Fertig Publications" "Course Technology PTR"

    "中信出版社" "机械工业出版社" "电子工业出版社" "人民邮电出版社" "中国工信出版集团" "人民卫生出版社" "中国妇女出版社" "江苏科学技术出版社"
    "浙江人民出版社" "上海人民出版社" "中国人民大学出版社" "北京科学技术出版社" "四川科学技术出版社" "北京华章图文信息有限公司" "海南出版社" "湖南科学技术出版社"
    "天津科学技术出版社" "上海译文出版社" "浙江教育出版社" "云南人民出版社" "湖南文艺出版社" "湖北科学技术出版社" "江西教育出版社" "浙江文艺出版社" "人民出版社"
    "人民法院出版社" "法律出版社" "九州出版社" "中华书局" "中国铁道出版社" "航空工业出版社" "中国法制出版社" "中华工商联合出版社" "中国友谊出版公司"
    "解放军出版社" "中国青年出版社" "军事科学出版社" "中国商业出版社" "中国华侨出版社" "人民体育出版社" "现代出版社" "中国纺织出版社" "科学技术文献出版社"
    "高等教育出版社" "清华大学出版社" "北京大学出版社" "复旦大学出版社" "东南大学出版社" "浙江大学出版社" "华中科技大学出版社" "广西师范大学出版社" "厦门大学出版社"
    "上海交通大学出版社" "北京理工大学出版社" "辽宁教育出版社" "辽宁人民出版社" "云南美术出版社" "上海文化出版社" "哈尔滨出版社" "河南科學技術出版社" "中醫古籍出版社"
    "陕西师范大学出版社" "武汉大学出版社" "江苏凤凰文艺出版社" "四川人民出版社" "四川文艺出版社" "天津人民出版社" "古吴轩出版社" "山西科学技术出版社" "江苏人民出版社"
    "河北人民出版社" "河南科学技术出版社" "内蒙古人民出版社" "华文出版社" "华夏出版社" "商务印书馆" "格致出版社" "中译出版社" "知识产权出版社" "时代华文"
    "北京联合出版社" "浙江出版集团数字传媒有限公司" "城邦出版集團" "城邦出版集團 商周出版" "城邦出版集團 麥田" "新星出版社" "生活·读书·新知三联书店" "時報文化出版"
    "宗教文化出版社" "文化发展出版社" "天地出版社" "印刷工业出版社" "百花文艺出版社" "后浪出版公司" "湖南美术出版社" "江西人民出版社" "上海文艺出版社" "重庆出版社"
    "三秦出版社" "上海三联书店" "译林出版社" "浙江科技出版社" "中国画报出版社" "天下雜誌" "天下文化" "重庆大学出版社" "人民军医出版社" "人民日报出版社"
    "民主与建设出版社" "中国轻工业出版社" "吉林科学技术出版社" "中国中医药出版社" "兵器工业出版社" "中国财政经济出版社" "社会科学文献出版社" "城邦文化事業股份有限公司"
    "国防工业出版社" "北京体育大学出版社" "冶金工业出版社" "中国海关出版社" "黑龙江人民出版社" "当代中国出版社" "中国书店出版社" "西北大学出版社" "广西人民出版社"
    "遠足文化事業股份有限公司發行" "聯經出版事業公司" "北京联合出版公司" "南海出版公司" "华东师范大学出版社" "湖南人民出版社" "中国林业出版社" "新华出版社"
    "漫遊者文化" "方言文化" "中国财经出版社" "中央编译出版社" "中国水利水电出版社" "究竟出版社" "漫遊者文化事業股份有限公司" "化学工业出版社" "文汇出版社"
    "作家出版社" "中国经济出版社" "湖南教育出版社" "北方文艺出版社" "北京时代华文书局" "南海出版社" "江苏凤凰科学技术出版社" "西安交通大学出版社" "外语教学与研究出版社"
    "南京大学出版社" "上海辞书出版社" "浙江科学技术出版社" "北京航空航天大学出版社" "北京奥维博世图书发行有限公司" "晋江文学城" "上海科技教育出版社" "科学出版社"
    "中国建筑工业出版社" "同济大学出版社" "上海社会科学院出版社" "上海财经大学出版社" "东方出版社" "上海科学技术出版社" "国际文化出版社" "中国民主法制出版社"
    "先覺出版社" "左岸文化" "青文出版社" "文史资料出版社" "上海古籍出版社" "采實文化" "人民文学出版社" "三采文化股份有限公司" "中国社会科学出版社"
    "中国言实出版社" "紫禁城出版社" "新疆科学技术出版社" "广东科技出版社" "江西科学技术出版社" "南方出版社" "中国发展出版社" "中国出版集团中译出版社"
    "橡實文化" "新世界出版社" "京华出版社" "线装书局" "上海书店" "巴蜀书社" "武汉出版社" "吉林大学出版社" "故宫出版社" "究竟出版社股份有限公司" "上海古籍出版社有限公司"
    "金城出版社" "牛津大学出版社" "天地圖書" "求实出版社" "开放杂志出版社" "大风出版社" "香港中文大学出版社" "博客思出版社" "中国文化传播出版社" "浙江古籍出版社"
    "天地图书出版公司" "警官教育出版社" "香港城市大學出版社" "安徽文艺出版社" "时代国际出版有限公司" "世界知识出版社" "東洋文庫" "台海出版社" "青海人民出版社"
    "北京师范大学出版社" "大樂文化" "世新出版社" "新疆大学出版社" "文津出版社" "人民軍醫出版社" "人民卫生电子音像出版社" "中国金融出版社" "聯經出版事業股份有限公司"
    "中国长安出版社" "东西文库" "群众出版社" "远流出版社" "北京十月文艺出版社" "大是文化" "山东美术出版社" "人民日报社出版社" "中国广播影视出版社" "中国城市出版社"
    "未读·思想家" "中国政法大学出版社" "红旗出版社" "山东友谊出版社" "東方出版社" "明镜出版社" "中医古籍出版社" "福建教育出版社" "北京日报出版社" "貓頭鷹出版社"
    "河南大学出版社" "企业管理出版社" "国际文化出版公司" "陕西人民出版社" "東立出版社" "講談社" "齐鲁书社" "里仁出版社" "时代文艺出版社" "知乎周刊" "旅游教育出版社"
    "江苏文艺出版社" "北京時代化文書局" "中国科学技术出版社" "成都时代出版社" "中国文史出版社" "广东人民出版社" "鹭江出版社")
  "A list of org-mode property 'PUBLISHER' default values."
  :type 'list
  :safe #'listp)

;;; TODO `customize-save-variable'

(org-completing-read-property-construct-function "PUBLISHER" org-property-publisher-list)

(add-to-list 'org-property-set-functions-alist '("PUBLISHER" . org-completing-read-property_publisher))
(add-to-list 'org-property-set-functions-alist '("PUBLISHER(China)" . org-completing-read-property_publisher))
(add-to-list 'org-property-set-functions-alist '("PRESS" . org-completing-read-property_publisher))


(add-to-list 'org-default-properties "LANGUAGE")

;;; https://en.wikipedia.org/wiki/List_of_languages_by_total_number_of_speakers
(defcustom org-property-language-list
  '("English"
    "Simplified Chinese" "Traditional Chinese" "Mandarin Chinese (Standard Chinese)" "Yue Chinese (Cantonese)" "Wu Chinese (Shanghainese)" "Min Nan Chinese (Hokkien)"
    "Japanese" "Korean" "Thai"
    "German" "French" "Spanish" "Russian" "Standard Arabic" "Bengali" "Portuguese")
  "A list of org-mode property 'PUBLISHER' default values."
  :type 'list
  :safe #'listp)

(org-completing-read-property-construct-function "LANGUAGE" org-property-language-list)

(add-to-list 'org-property-set-functions-alist '("LANGUAGE" . org-completing-read-property_language))

;;=========================================== programming ===========================================

(add-to-list 'org-default-properties "Programming-Languages")

(defcustom org-property-programming-language-list
  '("LISP" "Common Lisp" "Scheme" "Racket" "Clojure" "ClojureScript"
    "C" "C++" "Rust" "Go" "Swift" "D" "Nim" "C#" "F#"
    "Shell" "Bash" "Zsh" "AppleScript" "PowerShell"
    "Python" "Ruby" "Perl" "Lua" "Elixir" "Dart"
    "JavaScript" "HTML" "CSS" "PHP" "WebAssembly"
    "R" "Julia" "Matlab" "Octave"
    "Java" "Kotlin" "Groovy" "Scala"
    "Haskell" "Erlang" "OCaml" "ReasonML" "Delphi" "Fortran" "Prolog" "Datalog" "VHDL" "Verilog"
    "Scratch" "LOGO"
    "SQL" "MySQL" "PostgreSQL" "Oracle"
    "TeX" "LaTeX" "Org Mode" "Markdown" "reStructuredText" "YAML" "TOML" "AsciiDoc")
  "A list of org-mode property 'Programming-Languages' default values."
  :type 'list
  :safe #'listp)

(org-completing-read-property-construct-function "Programming-Languages" org-property-programming-language-list)

(add-to-list 'org-property-set-functions-alist '("Programming-Languages" . org-completing-read-property_programming-languages))

;;============================================= License =============================================

(add-to-list 'org-default-properties "LICENSE")

;;; https://en.wikipedia.org/wiki/Comparison_of_free_and_open-source_software_licences
(defcustom org-property-license-list
  '(;; Free Software Foundation
    "GNU Affero General Public License" "GNU General Public License" "GNU Lesser General Public License"
    "BSD License" ; Regents of the University of California
    "MIT License" ; MIT
    ;; Creative Commons
    "Creative Commons Zero" "CC BY" "CC BY-SA"
    "W3C Software Notice and License" ; W3C
    "Python Software Foundation License" ; Python Software Foundation
    "Eclipse Public License" ; Eclipse Foundation
    "Common Development and Distribution License" ; Sun Microsystems
    "Common Public License" "IBM Public License" ; IBM
    "Apple Public Source License" ; Apple Computer
    "Microsoft Public License" ; Microsoft
    "Unlicense")
  "A list of org-mode property 'LICENSE' default values."
  :type 'list
  :safe #'listp)

(org-completing-read-property-construct-function "LICENSE" org-property-license-list)

(add-to-list 'org-property-set-functions-alist '("LICENSE" . org-completing-read-property_license))


(add-to-list 'org-default-properties "COPYRIGHT")


(provide 'init-org-property)

;;; init-org-property.el ends here
