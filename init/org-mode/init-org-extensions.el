;;; init-org-extensions.el --- init for Org Extensions
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ org-crypt ] -- public key encryption for Org entries

;;; The `org-crypt' might slow down org-mode buffers.
(use-package org-crypt
  :after org
  :custom ((org-crypt-key "F09F650D7D674819892591401B5DF1C95AE89AC3")
           (org-crypt-disable-auto-save t))
  ;; To automatically encrypt all necessary entries when saving a file.
  ;; The `org-encrypt-entries' in `before-save-hook' is heavy for big size buffer saving.
  :init (org-crypt-use-before-save-magic)
  ;; auto decrypt entry on org-cycle expand.
  ;; (defun my/org-decrypt-entry (&optional state)
  ;;   (org-decrypt-entry))
  ;; (add-hook 'org-cycle-hook 'my/org-decrypt-entry)
  ;; set keybindings for org-crypt functions.
  :bind (:map org-mode-map
              ("C-c C-r" . org-encrypt-entry)
              ("C-c M-r" . org-decrypt-entry)
              ("C-c C-/" . org-decrypt-entries))
  :config
  ;; Auto complete value of property "CRYPTKEY".
  (add-to-list 'org-default-properties "CRYPTKEY")
  (defcustom org-property-cryptkey-list
    ;; merge my personal key with GnuPG keys list.
    (append (list org-crypt-key)
            ;; reference `epa--list-keys' -> `epg-list-keys' -> `epg--list-keys-1'
            (let ((context (epg-make-context epa-protocol))
                  (name nil)
                  (secret nil))
              (mapcar 'epa--button-key-text (epg-list-keys context name secret))))
    "A list of org-mode property 'CRYPTKEY' default values."
    :type 'list
    :safe #'listp)
  (org-completing-read-property-construct-function "CRYPTKEY" org-property-cryptkey-list)
  (add-to-list 'org-property-set-functions-alist '("CRYPTKEY" . org-completing-read-property_cryptkey))
  ;; add `org-crypt' required tag to default persistent tag list.
  (add-to-list 'org-tag-persistent-alist `(,org-crypt-tag-matcher . ?z)))

;;; [ org-invoice ] -- Help manage client invoices in Org Mode.

(use-package org-invoice
  :autoload org-dblock-write:invoice
  :commands (org-invoice-report))



(provide 'init-org-extensions)

;;; init-org-extensions.el ends here
