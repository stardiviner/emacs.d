;;; init-org-protocol.el --- init for Org Protocol
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ org-protocol ] -- intercept calls from emacsclient to trigger custom actions.

(use-package org-protocol
  :init
  (with-eval-after-load 'org-protocol
    (unless (featurep 'server)
      (require 'server))
    (unless (server-running-p)
      (server-start)))

  ;; [ org-protocol capture ]
  (setq org-capture-templates
        (append org-capture-templates
                `(("P" ,(format "%s\torg-protocol" (nerd-icons-faicon "nf-fa-external_link" :face 'nerd-icons-orange)))
                  ("PP" ,(format "%s\tProtocol Capture new Task.org" (nerd-icons-faicon "nf-fa-chrome" :face 'nerd-icons-red))
                   entry (file ,(concat org-directory "/Tasks/Tasks.org"))
                   "* %^{Title}
:PROPERTIES:
:URL: %:link
:TIME: %U
:END:

#+begin_quote
%i
#+end_quote

%?"
                   :prepend t
                   :empty-lines 1)
                  ("PL" ,(format "%s\tRecord Link to Bookmarks.org" (nerd-icons-mdicon "nf-md-bookmark_plus_outline" :face 'nerd-icons-orange))
                   entry (file ,(concat org-directory "/Bookmarks/Bookmarks.org"))
                   "* [[%:link][%:description]] :bookmark:%^g
:PROPERTIES:
:URL: %:link
:TIME: %U
:DESCRIPTION: %^{description}
:END:

%?"
                   :prepend t
                   :empty-lines 1
                   :jump-to-captured t))))

  
  ;; ;; TODO: setup this option.
  ;; ;; (module-name :property value property: value ...)
  ;; (setq org-protocol-project-alist
  ;;       '(("http://orgmode.org/worg/"
  ;;          :online-suffix ".php"
  ;;          :working-suffix ".org"
  ;;          :base-url "http://orgmode.org/worg/"
  ;;          :working-directory "/home/stardiviner/Org/Worg/")
  ;;         ("http://localhost/org-notes/"
  ;;          :online-suffix ".html"
  ;;          :working-suffix ".org"
  ;;          :base-url "http://localhost/org/"
  ;;          :working-directory "/home/user/org/"
  ;;          :rewrites (("org/?$" . "index.php")))))
  )

;; (use-package org-protocol-capture-html
;;   :ensure t
;;   :defer t
;;   :init
;;   (add-to-list 'org-capture-templates
;;                `("PH" ,(format "%s\torg-protocol-capture-html"
;;                                (nerd-icons-faicon "nf-fa-html5" :face 'nerd-icons-lred))
;;                  entry (file ,(concat org-directory "/Bookmarks/Bookmarks.org"))
;;                  "* [[%:link][%:description]]
;; :PROPERTIES:
;; :TIME: %U
;; :END:
;;
;; %?
;;
;; %:initial"
;;                  :prepend t
;;                  :empty-lines 1
;;                  :jump-to-captured t)
;;                :append))


(provide 'init-org-protocol)

;;; init-org-protocol.el ends here
