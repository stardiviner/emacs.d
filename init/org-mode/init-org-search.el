;;; init-org-search.el --- init for Org search.
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

(setq org-occur-case-fold-search 'smart)

;;; org-agenda [C-c a s] / `org-search-view' search view.

;;; [ `org-goto' ] -- [C-c C-j] command enhancement

(require 'org-goto)
(setq org-goto-interface 'outline-path-completion)
(setq org-goto-max-level 5)

;; org-keys.el: (org-defkey org-mode-map (kbd "C-c C-j") #'org-goto)
(with-eval-after-load "org-keys"
  (cond
   ((featurep 'vertico)
    (if (fboundp 'consult-outline)
        (use-package consult
          :ensure t
          :commands (consult-outline)
          :bind (([remap org-goto] . consult-outline)
                 :map org-mode-map ("C-c C-j" . consult-outline))
          :init (org-defkey org-mode-map [remap org-goto] #'consult-outline))
      (use-package consult
        :ensure t
        :commands (consult-org-heading)
        :bind (([remap org-goto] . consult-org-heading)
               :map org-mode-map ("C-c C-j" . consult-org-heading))
        :init (org-defkey org-mode-map [remap org-goto] #'consult-org-heading)
        ;; set command `consult-org-heading' with initial input "*" to match more Org headlines.
        (require 'consult-org)
        (consult-customize consult-org-heading :initial ".?"))))
   ((featurep 'ivy)
    (use-package counsel
      :ensure t
      :commands (counsel-org-goto counsel-org-goto-all)
      :bind (([remap org-goto] . counsel-org-goto)
             :map org-mode-map ("C-c C-j" . counsel-org-goto))
      :config
      ;; `pyim' matching supports PinYin.
      (when (and ivy-mode (fboundp 'pyim--ivy-cregexp))
        (add-to-list 'ivy-re-builders-alist '(counsel-org-goto . pyim--ivy-cregexp)))))
   ((featurep 'helm-org)
    (use-package helm-org
      :ensure t
      :commands (helm-org-in-buffer-headings)
      :bind (([remap org-goto] . helm-org-in-buffer-headings)
             :map org-mode-map ("C-c C-j" . helm-org-in-buffer-headings))
      :config
      ;; Disable custom helm settings for command `helm-org-in-buffer-headings'.
      (defun helm-org-in-buffer-headings--advice (origin-func &rest args)
        "Setting helm window for command `helm-org-in-buffer-headings'."
        (when zoom-mode
          (let ((helm-split-window-inside-p t)
                (helm-split-window-default-side 'below))
            (apply origin-func args))))
      (advice-add 'helm-org-in-buffer-headings :around #'helm-org-in-buffer-headings--advice)))))

;;; [ org-ql ] -- An Org-mode query language, search command, and agenda-like view.

(use-package org-ql
  :ensure t
  :defer t
  :commands (org-ql-sparse-tree org-ql-find org-ql-search org-ql-view)
  :bind (:map Org-prefix ("C-s" . org-ql-find) ("M-s" . org-ql-search))
  :init (add-to-list 'display-buffer-alist '("^\\*Org QL.*\\*" . (display-buffer-below-selected)))
  (use-package helm-org-ql
    :ensure t
    :requires helm-org-ql
    :custom ((helm-org-ql-reverse-paths nil)
             (helm-org-ql-show-prefix nil))
    :commands (helm-org-ql)
    ;; :bind (:map org-mode-map ([remap org-goto] . helm-org-ql) ("C-c M-j" . helm-org-ql))
    ))

;; search multiple words in files.
(use-package rg
  :ensure t
  :defer t
  :init
  (defun rg-files-with-matches-beginning (dir file-type word)
    "Construct literal rg command end part for the beginning WORD in DIR with FILE-TYPE."
    (format "rg --files-with-matches --null --type %s -e \"%s\" %s" file-type word dir))

  (defun rg-files-with-matches-middle (word)
    "Construct literal rg command end part for the middle WORD."
    (cl-case system-type
      (gnu/linux (format " | xargs --null rg --files-with-matches --null -e \"%s\"" word))
      (darwin (format " | xargs -0 rg --files-with-matches --null -e \"%s\"" word))))

  (defun rg-files-with-matches-end (word)
    "Construct literal rg command end part for the last WORD."
    (cl-case system-type
      (gnu/linux (format " | xargs --null rg --heading -e \"%s\"" word))
      (darwin (format " | xargs -0 rg --heading -e \"%s\"" word))))

  (defun rg-files-construct-command (dir)
    "Construct a literal rg command to search WORDS in DIR."
    (require 'rg)
    (let* ((words (split-string (read-from-minibuffer "Words: ") " "))
           (file-type (completing-read "file type: "
                                       (mapcar 'car (rg-get-type-aliases))
                                       nil nil "org"))
           (file-type-exts (assq file-type (rg-get-type-aliases))))
      (cl-case (length words)
        (1 (format "rg %s" (car words)))
        (2 (concat (rg-files-with-matches-beginning dir file-type (car words))
                   (rg-files-with-matches-end (cdr words))))
        (3 (concat (rg-files-with-matches-beginning dir file-type (car words))
                   (rg-files-with-matches-middle (cadr words))
                   (rg-files-with-matches-end (car (reverse words)))))
        (t (concat (rg-files-with-matches-beginning dir file-type (car words))
                   (car (mapcan
                         (lambda (word) (list (rg-files-with-matches-middle word)))
                         (delq (car (reverse words)) (cdr words))))
                   (rg-files-with-matches-end (car (reverse words))))))))

  (defun rg-search-words-by-files (directory)
    "Search multiple words in files of DIRECTORY as unit instead of line.

The literal rg command looks like this:

rg --files-with-matches --null \"foo\" . | \\
xargs --null rg --files-with-matches --null \"bar\" | \\
... | \\
xargs --null rg --heading \"baz\"

That's it.
"
    ;; interactively select Org default directory or current directory.
    (interactive (list (completing-read "Dir: " `(,(expand-file-name org-directory)
                                                  ,default-directory))))
    (let* ((command (rg-files-construct-command (shell-quote-argument directory))))
      ;; FIXME rg report 0 match, but the result has matches.
      ;; dive into rg.el source code to figure out.
      ;; use `rg-define-search'
      (compilation-start command 'rg-mode)))

  (define-key Org-prefix (kbd "s") 'rg-search-words-by-files))

;;; [ org-recoll ] -- A lightweight Emacs Org Mode wrapper for the recoll full-text search engine.

;; (use-package org-recoll
;;   :vc (:url "https://github.com/alraban/org-recoll")
;;   :defer t
;;   :commands (org-recoll-search org-recoll-update-index)
;;   :bind (:map Org-prefix ("C-s" . org-recoll-search)
;;               :map org-recoll-mode-map
;;               ("M-n" . org-recoll-next-page)
;;               ("M-p" . org-recoll-previous-page)
;;               ("q" . delete-window))
;;   :init
;;   (add-to-list 'display-buffer-alist '("^\\*org-recoll-index\\*" . (display-buffer-below-selected)))
;;   (add-to-list 'display-buffer-alist '("^\\*org-recoll-results\\*" . (display-buffer-below-selected))))



(provide 'init-org-search)

;;; init-org-search.el ends here
