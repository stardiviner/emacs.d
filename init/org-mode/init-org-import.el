;;; init-org-import.el --- Org Mode importer -*- lexical-binding: t; -*-

;;; Commentary:



;;; Code:

;;; [ org-pandoc-import ] -- Save yourself from non-org formats, thanks to pandoc.

(use-package org-pandoc-import
  :vc (:url "https://github.com/tecosaur/org-pandoc-import"
            :main-file "org-pandoc-import.el"
            :lisp-dir "filters" :lisp-dir "preprocessors")
  :commands (org-pandoc-import-as-org org-pandoc-import-to-org)
  :config
  (require 'org-pandoc-import-transient)
  (org-pandoc-import-transient-mode 1))

;;; [ edit-as-format ] -- Edit document as other format, as long as your format is supported by *Pandoc*.

(use-package edit-as-format
  :ensure t
  :commands (edit-as-format)
  :config
  (defun edit-org-as-markdown ()
    (interactive)
    (edit-as-format "org" "markdown"))
  (defun edit-as-github-flavored-markdown ()
    (interactive)
    (edit-as-format nil "gfm")))

;;; [ xmind-org ] -- Generate Org mode outlines from an XMind mindmap file.

(use-package xmind-org
  :ensure t
  :commands (xmind-org-insert-file))



(provide 'init-org-import)

;;; init-org-import.el ends here
