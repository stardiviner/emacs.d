;;; init-org-view.el --- init for Org View
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

(setq org-ellipsis (if (char-displayable-p ?⏷) "\t⏷" nil))

(setq org-pretty-entities t             ; for `org-toggle-pretty-entities'
      org-use-sub-superscripts '{})

(setq org-fontify-quote-and-verse-blocks t)

;;; indent

;; (setq org-adapt-indentation 'headline-data)
;; (setq org-startup-indented t) ; enable `org-indent-mode' at startup.
;; (add-hook 'org-mode-hook 'org-indent-mode)

;;; line wrap
;; (use-package simple
;;   :delight visual-line-mode
;;   :hook (org-mode . visual-line-mode))

;;; [ org-num ] -- Dynamic Headlines Numbering.

;; (use-package org-num
;;   :custom ((org-startup-numeroted nil)        ; use "#+startup: num" file locally instead.
;;            (org-num-skip-footnotes t)
;;            (org-num-skip-unnumbered t))
;;   :commands (org-num-mode)
;;   :hook (org-mode . org-num-mode))

;;; [ org-sticky-header ] -- Show off-screen Org heading at top of window.

;; (use-package org-sticky-header
;;   :ensure t
;;   :hook (org-mode . org-sticky-header-mode))

;;; [ shrface ] -- It is a shr faces package. Org Like faces for shr, dash-docs, eww, nov.el, mu4e and more!

(use-package shrface
  :ensure t
  :defer t
  :config
  ;; Enable source codes highlight.
  (use-package shr-tag-pre-highlight
    :ensure t
    :defer t
    :config
    (add-to-list 'shr-external-rendering-functions '(pre . shr-tag-pre-highlight))))

;;===============================================================================
;;; NOTE: This setting will cause read-only-mode keybinding overriding.
(setq view-read-only t) ; enable view mode after enabled `read-only-mode' [C-x C-q].

;;; colorize the horizontal rule line.
(defun my/org-mode-colorize-horizontal-lines ()
  "Colorize the horizontal lines in org-mode."
  (font-lock-add-keywords
   nil
   '(("^-\\{5,\\}"  0 '(:foreground "MediumPurple1" :weight bold)))))

(add-hook 'org-mode-hook #'my/org-mode-colorize-horizontal-lines)

(setq org-hide-leading-stars t)

;;; [ org-modern ] -- Modern looks for Org

(use-package org-modern
  :ensure t
  :after org
  :custom ((org-modern-star 'replace)
           (org-modern-hide-stars nil)
           (org-modern-replace-stars
            (vector
             ;; disk -> folder -> file style
             ;; folder -> open folder -> inbox -> book -> text -> file -> floppy -> bookmark
             ;;===========================================================
             (nerd-icons-faicon "nf-fa-hdd_o")
             (nerd-icons-faicon "nf-fa-folder_o")
             (nerd-icons-faicon "nf-fa-folder_open_o")
             (nerd-icons-faicon "nf-fa-inbox")
             (nerd-icons-faicon "nf-fa-book")
             (nerd-icons-faicon "nf-fa-file_text_o")
             (nerd-icons-faicon "nf-fa-file_o")
             (nerd-icons-faicon "nf-fa-floppy_o")
             (nerd-icons-faicon "nf-fa-bookmark_o")))
           )
  :hook (after-init . global-org-modern-mode))

;;; [ org-margin ] -- Outdent headlines in Emacs Org mode.

;;; TODO: only enable `org-margin-mode' in small .org files.
;; (use-package org-margin
;;   :vc (:url "git@github.com:rougier/org-margin.git")
;;   :init (require 'org-margin)
;;   :hook (org-mode . org-margin-mode))

;;; Show the current node name in the header line.

(defun org-mode-show-outline-path-in-header ()
  (setq-local header-line-format '(:eval (ignore-errors (org-compute-node-name-for-header)))))

(defun org-compute-node-name-for-header ()
  (interactive)
  (let ((head-line-level 0)
        current-headline
        (str "")
        (outline-path (org-get-outline-path t)))
    (while outline-path
      (unless (eq head-line-level 0)
        (setq str (concat str (propertize " 〉 " 'face '(:inherit default :foreground "red")))))
      (setq current-headline (car outline-path))
      (put-text-property 0 (length current-headline)
                         'face (nth head-line-level org-level-faces)
                         current-headline)
      (setq str (concat str current-headline))
      (setq head-line-level (1+ head-line-level))
      (setq outline-path (cdr outline-path)))
    str))

(add-hook 'org-mode-hook #'org-mode-show-outline-path-in-header)


(provide 'init-org-view)

;;; init-org-view.el ends here
