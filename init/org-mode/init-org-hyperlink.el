;;; init-org-hyperlink.el --- init for Org Hyperlinks
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; Check out variable `org-link-parameters'.

(setq org-indirect-buffer-display 'current-window
      org-link-use-indirect-buffer-for-internals t
      org-link-keep-stored-after-insertion t)

;; (setq org-link-file-path-type 'adaptive)

(define-key org-mode-map (kbd "M-,") 'org-mark-ring-goto)

;;; [ org-id ] -- Global identifiers for Org entries.

;;; use :ID: property for Org link [[id:24192FC4-4974-4830-8460-13178DD8FB36][link description]].

;; (use-package org-id
;;   :custom
;;   ((org-id-link-to-org-use-id 'create-if-interactive-and-no-custom-id)
;;    (org-id-track-globally t)
;;    (org-id-locations-file (locate-user-emacs-file ".org-id-locations"))))


;;; don't display "*Org Links*" buffer.
;; (add-to-list 'display-buffer-alist '(("Org Links" display-buffer-no-window (allow-no-window . t))))

;; (add-hook 'org-follow-link-hook #'sound-tick)

;;; [ org-simple-ref ] -- Insert Org mode internal links supporting consult and embark.

(use-package org-simple-ref
  :commands (org-simple-ref-insert-ref-link)
  :bind (:map org-mode-map ("C-c l" . org-simple-ref-insert-ref-link)))

;;; Open and play GIF image in Emacs buffer.
(defun my/open-and-play-gif-image (file &optional link)
  "Open and play GIF image `FILE' in Emacs buffer.
Optional for Org-mode file: `LINK'."
  (let ((gif-image (create-image file))
        (tmp-buf (get-buffer-create "*Org-mode GIF image animation*")))
    (switch-to-buffer tmp-buf)
    (erase-buffer)
    (insert-image gif-image)
    (image-animate gif-image nil t)
    (local-set-key (kbd "q") 'kill-current-buffer)))

(defun emms-play-file-for-org (file &optional link)
  "An wrapper function on `emms-play-file'."
  (emms-play-file file))

(setq org-file-apps
      '((auto-mode . emacs)
        (directory . emacs)
        (remote . emacs)
        (t . default)))

;;; Links are now customizable
;;
;; Links can now have custom colors, tooltips, keymaps, display behavior, etc.
;; Links are now centralized in `org-link-parameters'.
;; (add-to-list 'org-link-parameters '())
;; `org-link-types'
;; `org-link-set-parameters'

(setq org-link-frame-setup
      '((vm . vm-visit-folder-other-frame)
        (vm-imap . vm-visit-imap-folder-other-frame)
        (gnus . org-gnus-no-new-news)
        (file . find-file) ; `find-file-other-window'
        (wl . wl-other-frame)))

;;; DEMO:
;; (setq org-file-apps '(("\\.pdf::\\([A-z%]+\\)\\'" . "<command> %s %1")))
;;                         +--+--+ +----`---------+                |  |
;;                            `----------`------------------------+  |
;;                                        `--------------------------+

;; [C-c C-o] with optional prefix argument [C-u]
;; 
;; - [C-c C-o] Normally, files will be opened by an appropriate application (see `org-file-apps').
;; - [C-u] + [C-c C-o] If the optional prefix argument ARG is non-nil, Emacs will visit the file.
;; - [C-u C-u] + [C-c C-o] With a double prefix argument, try to open outside of Emacs, in the application the system uses for this file type.

(cl-case system-type
  (gnu/linux
   (add-to-list 'org-file-apps
                `(,(if (executable-find "firefox")
                       `("\.x?html?\\'" . "firefox %s")
                     `("\.x?html?\\'" . "google-chrome-unstable %s"))))
   
   (dolist (pair '(;; PDF
                   ;; use Okular
                   ;; ("\\.pdf\\'" . "okular %s")
                   ;; ("\\.pdf::\\([[:digit:]]+\\)\\'" . "okular -p %1 %s")
                   ;; CHM
                   ("\\.chm\\'" . "kchmviewer %s")
                   ;; DjVu
                   ("\\.djvu\\'" . emacs) ; use Emacs package "djvu" `djvu-read-mode' and external program "djvused".
                   ;; eBook
                   ;; ("\\.epub\\'" . "okular %s") ; it is opened by `ereader', and `nov'.
                   ("\\.mobi\\'" . "ebook-viewer %s")
                   ("\\.azw3\\'" . "ebook-viewer %s")
                   ("\\.azw\\'" . "ebook-viewer %s")
                   ;; eComic
                   ("\\.cbr\\'" . "ebook-viewer %s")
                   ("\\.cbz\\'" . "ebook-viewer %s")
                   ;; Image
                   ;; ("\\.png\\'" . "sxiv %s")
                   ;; ("\\.jpg" . "sxiv %s")
                   ;; ("\\.jpeg" . "sxiv %s")
                   ("\\.gif\\'" . "sxiv -a -f %s")
                   ;; ("\\.gif\\'" . "gwenview %s")
                   ;; ("\\.gif\\'" . my/open-and-play-gif-image)
                   ;; ("\\.svg\\'" . "feh --magick-timeout 5 %s")
                   ;; ("\\.svg\\'" . "display %s") ; Emacs built-in support display svg
                   ;; Mind Maps
                   ("\\.mm\\'" . "freeplane %s")
                   ;; Office
                   ;; Open Text Document
                   ("\\.odt\\'"  . "libreoffice %s") ; Text Documentation
                   ("\\.ods\\'"  . "libreoffice %s") ; Spreadsheet
                   ("\\.odp\\'"  . "libreoffice %s") ; Presentation
                   ("\\.odf\\'"  . "libreoffice %s") ; Database / Formula
                   ;; Windows Office
                   ("\\.doc\\'"  . "libreoffice %s")
                   ("\\.ppt\\'"  . "libreoffice %s")
                   ("\\.xls\\'"  . "libreoffice %s")
                   ("\\.docx\\'" . "libreoffice %s")
                   ("\\.pptx\\'" . "libreoffice %s")
                   ("\\.xlsx\\'" . "libreoffice %s")
                   ;; Video
                   ("\\.mp4\\'"  . "mpv --ontop --slang=zho %s")
                   ("\\.mpeg\\'" . "mpv --ontop --slang=zho %s")
                   ("\\.mkv\\'"  . "mpv --ontop --slang=zho %s")
                   ("\\.mov\\'"  . "mpv --ontop --slang=zho %s")
                   ("\\.ogv\\'"  . "mpv --ontop --slang=zho %s")
                   ("\\.webm\\'" . "mpv --ontop --slang=zho %s")
                   ("\\.flv\\'"  . "mpv --ontop --slang=zho %s")
                   ("\\.f4v\\'"  . "mpv --ontop --slang=zho %s")
                   ("\\.rmvb\\'" . "mpv --ontop --slang=zho %s")
                   ("\\.avi\\'"  . "mpv --ontop --slang=zho %s")
                   ;; support seek video position with "mpv --start=<timestamp 00:10>"
                   ("\\.mp4::\\([0-5][0-9]:[0-5][0-9]\\|[0-9][0-9]:[0-5][0-9]:[0-5][0-9]\\)\\'" . "mpv --ontop --slang=zho --start=%1 %s")
                   ("\\.mpeg::\\([0-5][0-9]:[0-5][0-9]\\|[0-9][0-9]:[0-5][0-9]:[0-5][0-9]\\)\\'" . "mpv --ontop --slang=zho --start=%1 %s")
                   ("\\.mkv::\\([0-5][0-9]:[0-5][0-9]\\|[0-9][0-9]:[0-5][0-9]:[0-5][0-9]\\)\\'" . "mpv --ontop --slang=zho --start=%1 %s")
                   ("\\.mov::\\([0-5][0-9]:[0-5][0-9]\\|[0-9][0-9]:[0-5][0-9]:[0-5][0-9]\\)\\'" . "mpv --ontop --slang=zho --start=%1 %s")
                   ("\\.ogv::\\([0-5][0-9]:[0-5][0-9]\\|[0-9][0-9]:[0-5][0-9]:[0-5][0-9]\\)\\'" . "mpv --ontop --slang=zho --start=%1 %s")
                   ("\\.webm::\\([0-5][0-9]:[0-5][0-9]\\|[0-9][0-9]:[0-5][0-9]:[0-5][0-9]\\)\\'" . "mpv --ontop --slang=zho --start=%1 %s")
                   ("\\.flv::\\([0-5][0-9]:[0-5][0-9]\\|[0-9][0-9]:[0-5][0-9]:[0-5][0-9]\\)\\'" . "mpv --ontop --slang=zho --start=%1 %s")
                   ("\\.f4v::\\([0-5][0-9]:[0-5][0-9]\\|[0-9][0-9]:[0-5][0-9]:[0-5][0-9]\\)\\'" . "mpv --ontop --slang=zho --start=%1 %s")
                   ("\\.rmvb::\\([0-5][0-9]:[0-5][0-9]\\|[0-9][0-9]:[0-5][0-9]:[0-5][0-9]\\)\\'" . "mpv --ontop --slang=zho --start=%1 %s")
                   ("\\.avi::\\([0-5][0-9]:[0-5][0-9]\\|[0-9][0-9]:[0-5][0-9]:[0-5][0-9]\\)\\'" . "mpv --ontop --slang=zho --start=%1 %s")
                   
                   ;; Audio
                   ("\\.mp3\\'"  . "mpv %s")
                   ("\\.ogg\\'"  . "mpv %s")
                   ("\\.wav\\'"  . "mpv %s")
                   ("\\.m4a\\'"  . "mpv %s")
                   ("\\.m4s\\'"  . "mpv %s")
                   ("\\.opus\\'"  . "mpv %s")
                   ("\\.midi\\'" . "timidity %s")
                   ;; 3D
                   ("\\.blend\\'" . "blender %s")
                   ))
     (add-to-list 'org-file-apps pair))
   
   ;; (add-to-list 'org-file-apps '("\\.swf\\'" . "gnash %s"))
   )

  (darwin
   (dolist (pair '(;; Image
                   ("\\.png\\'" . "qlmanage -p %s")
                   ("\\.jpg" . "qlmanage -p %s")
                   ("\\.jpeg" . "qlmanage -p %s")
                   ("\\.gif\\'" . "qlmanage -p %s")
                   ("\\.svg\\'" . "qlmanage -p %s")
                   ("\\.avif\\'" . "qlmanage -p %s")
                   ("\\.eps\\'" . "open %s")
                   ;; PDF
                   ;; ("\\.pdf\\'" . "open -a Preview %s")
                   ;; ("\\.pdf::\\([[:digit:]]+\\)\\'" . "open -a Preview %s --args %1") ; FIXME: don't know the command-line argument to goto page.
                   ;; DjVu
                   ("\\.djvu\\'" . emacs) ; use Emacs package "djvu" `djvu-read-mode' and external program "djvused".
                   ;; eBook
                   ;; ("\\.epub\\'" . "open %s")
                   ("\\.mobi\\'" . "open %s")
                   ("\\.azw3\\'" . "open %s")
                   ("\\.azw\\'" . "open %s")
                   ;; Comic
                   ("\\.cbr\\'" . "open %s") ; "ebook-viewer %s"
                   ("\\.cbz\\'" . "open %s") ; "ebook-viewer %s"
                   ;; Mind Maps
                   ("\\.mm\\'"  . "open %s")
                   ;; ("\\.mm\\'"  . "open -a \"XMind\" %s")
                   
                   ;; Office
                   ;; Open Text Document
                   ("\\.odt\\'"  . "open %s") ; Text Documentation
                   ("\\.ods\\'"  . "open %s") ; Spreadsheet
                   ("\\.odp\\'"  . "open %s") ; Presentation
                   ("\\.odf\\'"  . "open %s") ; Database / Formula
                   ;; Windows Office
                   ("\\.doc\\'"  . "open %s")
                   ("\\.ppt\\'"  . "open %s")
                   ("\\.xls\\'"  . "open %s")
                   ("\\.docx\\'" . "open %s")
                   ("\\.pptx\\'" . "open %s")
                   ("\\.xlsx\\'" . "open %s")
                   ;; macOS Office
                   ("\\.pages\\'" . "open -a Pages %s") ; Pages.app
                   ("\\.key\\'" . "open -a Keynote %s") ; Keynote.app
                   ("\\.numbers\\'" . "open -a Numbers %s") ; Numbers.app
                   
                   ;; Video
                   ("\\.mp4\\'"  . "mpv --ontop --slang=zho %s")
                   ("\\.mpeg\\'" . "mpv --ontop --slang=zho %s")
                   ("\\.mkv\\'"  . "mpv --ontop --slang=zho %s")
                   ("\\.mov\\'"  . "mpv --ontop --slang=zho %s")
                   ("\\.ogv\\'"  . "mpv --ontop --slang=zho %s")
                   ("\\.webm\\'" . "mpv --ontop --slang=zho %s")
                   ("\\.flv\\'"  . "mpv --ontop --slang=zho %s")
                   ("\\.f4v\\'"  . "mpv --ontop --slang=zho %s")
                   ("\\.rmvb\\'" . "mpv --ontop --slang=zho %s")
                   ("\\.avi\\'"  . "mpv --ontop --slang=zho %s")
                   ;; support seek video position with "mpv --start=<timestamp 00:10:00>"
                   ("\\.mp4::\\([0-5][0-9]:[0-5][0-9]\\|[0-9][0-9]:[0-5][0-9]:[0-5][0-9]\\)\\'" . "mpv --ontop --slang=zho --start=%1 %s")
                   ("\\.mpeg::\\([0-5][0-9]:[0-5][0-9]\\|[0-9][0-9]:[0-5][0-9]:[0-5][0-9]\\)\\'" . "mpv --ontop --slang=zho --start=%1 %s")
                   ("\\.mkv::\\([0-5][0-9]:[0-5][0-9]\\|[0-9][0-9]:[0-5][0-9]:[0-5][0-9]\\)\\'" . "mpv --ontop --slang=zho --start=%1 %s")
                   ("\\.mov::\\([0-5][0-9]:[0-5][0-9]\\|[0-9][0-9]:[0-5][0-9]:[0-5][0-9]\\)\\'" . "mpv --ontop --slang=zho --start=%1 %s")
                   ("\\.ogv::\\([0-5][0-9]:[0-5][0-9]\\|[0-9][0-9]:[0-5][0-9]:[0-5][0-9]\\)\\'" . "mpv --ontop --slang=zho --start=%1 %s")
                   ("\\.webm::\\([0-5][0-9]:[0-5][0-9]\\|[0-9][0-9]:[0-5][0-9]:[0-5][0-9]\\)\\'" . "mpv --ontop --slang=zho --start=%1 %s")
                   ("\\.flv::\\([0-5][0-9]:[0-5][0-9]\\|[0-9][0-9]:[0-5][0-9]:[0-5][0-9]\\)\\'" . "mpv --ontop --slang=zho --start=%1 %s")
                   ("\\.f4v::\\([0-5][0-9]:[0-5][0-9]\\|[0-9][0-9]:[0-5][0-9]:[0-5][0-9]\\)\\'" . "mpv --ontop --slang=zho --start=%1 %s")
                   ("\\.rmvb::\\([0-5][0-9]:[0-5][0-9]\\|[0-9][0-9]:[0-5][0-9]:[0-5][0-9]\\)\\'" . "mpv --ontop --slang=zho --start=%1 %s")
                   ("\\.avi::\\([0-5][0-9]:[0-5][0-9]\\|[0-9][0-9]:[0-5][0-9]:[0-5][0-9]\\)\\'" . "mpv --ontop --slang=zho --start=%1 %s")
                   
                   ;; Audio
                   ("\\.midi\\'" . "open %s")
                   ("\\.mp3\\'"  . "qlmanage -p %s")
                   ("\\.ogg\\'"  . "qlmanage -p %s")
                   ("\\.wav\\'"  . "qlmanage -p %s")
                   ("\\.m4a\\'"  . "qlmanage -p %s")
                   ("\\.m4s\\'"  . "mpv %s")
                   ("\\.opus\\'"  . "mpv %s")
                   ;; 3D
                   ("\\.blend\\'" . "open -a Blender %s") ; Blender.app
                   ))
     (add-to-list 'org-file-apps pair)))
  
  (windows-nt
   ))

(add-to-list 'org-file-apps '("\\.jar\\'" . "java -jar %s"))

;; Fix org-mode open .zip, .rar, .tar etc archive files problem.
(add-to-list 'org-file-apps '("\\.zip\\'" . find-file))
(add-to-list 'org-file-apps '("\\.rar\\'" . find-file))

;;; System platform specific `org-file-apps'.

;;; for Linux:
;;; xdg-open, kde-open, gnome-open.
;; (when (boundp 'org-file-apps-gnu)
;;   (setcdr (assq 'system org-file-apps-gnu) "xdg-open %s"))

;;; for macOS
;;; open [[file+sys:/Applications/*.app]] with command "open".
(defun my/macos-open-app (app &optional file)
  "A function to open macOS App in Emacs."
  (if (bound-and-true-p file)
      (format "open %s -a %s"
              (shell-quote-argument file)
              (shell-quote-argument app))
    (format "open %s" (shell-quote-argument app))))

(when (eq system-type 'darwin)
  (add-to-list 'org-file-apps-macos '("\\.app\\'" . my/macos-open-app)))

;;; Open .pdf, .epub file link with EAF.
(defun eaf-open-for-org (file &optional link)
  "An wrapper function on `eaf-open'."
  (eaf-open file))
(when (featurep 'eaf)
  (with-eval-after-load 'org
    ;; documents
    (add-to-list 'org-file-apps '("\\.pdf\\'" . eaf-open-for-org))
    (add-to-list 'org-file-apps '("\\.epub\\'" . eaf-open-for-org))
    (add-to-list 'org-file-apps '("\\.djvu\\'" . eaf-open-for-org))
    (add-to-list 'org-file-apps '("\\.xps\\'" . eaf-open-for-org))
    (add-to-list 'org-file-apps '("\\.oxps\\'" . eaf-open-for-org))
    (add-to-list 'org-file-apps '("\\.cbz\\'" . eaf-open-for-org))
    (add-to-list 'org-file-apps '("\\.fb2\\'" . eaf-open-for-org))
    (add-to-list 'org-file-apps '("\\.fbz\\'" . eaf-open-for-org))
    (when (not (eq system-type 'darwin))
      ;; images
      (add-to-list 'org-file-apps '("\\.gif\\'" . eaf-open-for-org))
      ;; videos
      (add-to-list 'org-file-apps '("\\.avi\\'" . eaf-open-for-org))
      (add-to-list 'org-file-apps '("\\.mp4\\'" . eaf-open-for-org))
      (add-to-list 'org-file-apps '("\\.mkv\\'" . eaf-open-for-org))
      (add-to-list 'org-file-apps '("\\.mov\\'" . eaf-open-for-org))
      (add-to-list 'org-file-apps '("\\.ogv\\'" . eaf-open-for-org))
      (add-to-list 'org-file-apps '("\\.webm\\'" . eaf-open-for-org)))))

;;; Open video file links by selecting video player interactively.
(defun my/org-open-video-file (file &optional link)
  "A wrapper function to open video file link with an interactive selection of video players."
  (let ((player (completing-read "Video Player: "
                                 (list "mpv"
                                       "EAF"
                                       "SMPlayer"
                                       "MPlayer")))
        (name (file-name-base file)))
    (pcase player
      ("mpv" (start-process
              (format "mpv --ontop %s" name)
              (format "*mpv %s*" name)
              "mpv"
              "--slang=zho" file))
      ("EAF" (eaf-open file))
      ("SMPlayer" (start-process
                   (format "smplayer %s" name)
                   (format "*smplayer %s*" name)
                   "smplayer"
                   file))
      ("MPlayer" (start-process
                  (format "mplayer %s" name)
                  (format "*mplayer %s*" name)
                  "mplayer"
                  "-slang=zh" file)))))
(when (featurep 'eaf)
  (with-eval-after-load 'org
    (add-to-list 'org-file-apps '("\\.avi\\'" . my/org-open-video-file))
    (add-to-list 'org-file-apps '("\\.mp4\\'" . my/org-open-video-file))
    (add-to-list 'org-file-apps '("\\.mkv\\'" . my/org-open-video-file))
    (add-to-list 'org-file-apps '("\\.mov\\'" . my/org-open-video-file))
    (add-to-list 'org-file-apps '("\\.ogv\\'" . my/org-open-video-file))
    (add-to-list 'org-file-apps '("\\.webm\\'" . my/org-open-video-file))))

;;; `eshell:' org-link `eshell:' support for EShell
(use-package ol-eshell)

;; `elisp:'
(setq org-confirm-elisp-link-function 'yes-or-no-p)
;; `shell:'
(setq org-confirm-shell-link-function 'yes-or-no-p)
(add-to-list 'display-buffer-alist '("^\\*Org Shell Output\\*" . (display-buffer-below-selected)))

;; `irc:'
(use-package ol-irc
  :custom (org-irc-client 'erc)
  :config (if (and (featurep 'erc)) (load "init-erc")))

;;; `info:' link.
(use-package ol-info)

;; `man:'
;; [[man:command(N)::STRING][command(N]]
;; [[woman:command(N)::STRING][command(N)]]
(use-package ol-man
  :after org
  :custom (org-man-command 'man)
  :config
  ;; [man:]
  (defun org-man--complete-man ()
    (require 'man)
    (let* ((Man-completion-cache) ; <- implementation detail in man.el
           (org-man--command (completing-read "Org mode [man:] link completion: " 'Man-completion-table)))
      (kill-new org-man--command)
      (concat "man:" org-man--command)))

  (defun org-man--insert-man-description (link description)
    (string-trim-left link "man:"))

  (org-link-set-parameters "man"
                           :complete 'org-man--complete-man
                           :insert-description 'org-man--insert-man-description)

  ;; [woman:]
  (defun org-man--init-woman-cache (&optional re-cache) ;; <- implementation detail in woman.el
    (unless (and (not re-cache)
                 (or
                  (and woman-expanded-directory-path
                       woman-topic-all-completions)
                  (woman-read-directory-cache)))
      (setq woman-expanded-directory-path
            (woman-expand-directory-path woman-manpath woman-path))
      (setq woman-topic-all-completions
            (woman-topic-all-completions woman-expanded-directory-path))
      (woman-write-directory-cache)))

  (defun org-man--complete-woman ()
    (require 'woman)
    (org-man--init-woman-cache)
    (concat "woman:"
            (completing-read "Org mode [woman:] link completion: " woman-topic-all-completions)))

  (defun org-man--insert-woman-description (link description)
    (string-trim-left link "woman:"))

  (org-link-set-parameters "woman"
                           :complete 'org-man--complete-woman
                           :insert-description 'org-man--insert-woman-description
                           :follow 'org-man-open
                           :store 'org-man-store-link
                           :export 'org-man-export))

;; `occur:'
;; [[occur:my-file.txt#regex]] :: to open a file and run occur with the regex on it.
(defun org-occur-link-open (uri)
  "Visit the file specified by `URI', and run `occur' on the fragment.
  \(anything after the first '#') in the `URI'."
  (let ((list (split-string uri "#")))
    (org-open-file (car list) t)
    (occur (mapconcat 'identity (cdr list) "#"))))
(org-link-set-parameters "occur" :follow #'org-occur-link-open)

;;; `grep:'
;;  [[grep:regexp][regexp (grep)]]
(defun org-grep-link-open (regexp)
  "Run `rgrep' with `REGEXP' as argument."
  (grep-compute-defaults)
  (rgrep regexp "*" (expand-file-name "./")))
(org-link-set-parameters "grep" :follow #'org-grep-link-open)

;; `git:'
(use-package ol-git-link
  :if (string-match-p ".*stardiviner.*" (locate-library "org"))
  :init
  ;; add file path completion support for `git:' and `gitbare:'
  (org-link-set-parameters "git" :complete 'org-git-complete-link)
  ;; TODO: add a function to complete git: link. parse git repo metadata, show in available candidates.
  (defun org-git-complete-link ()
    "Use the existing file name completion for file.
Links to get the file name, then ask the user for the page number
and append it."
    (concat (replace-regexp-in-string "^file:" "git:" (org-file-complete-link))
            "::"
            (read-from-minibuffer "branch:" "1")
            "@"
            (read-from-minibuffer "date:" "{2017-06-24}")
            "::"
            (read-from-minibuffer "line:" "1"))))

;;; [ orgit ] -- support for Org links to Magit buffers.
;;; `orgit:'
;;; `orgit-log:'
;;; `orgit-rev:'
(use-package orgit
  :ensure t
  :custom (orgit-log-save-arguments t))

;;; [ orgit-forge ] -- Org links to Magit Forge issue buffers.
;;; `orgit-topic:'
;; (use-package orgit-forge
;;   :ensure t)


;;; [ Link abbreviations ]

;; NOTE: you can NOT contain chinese string in "link name". Org-mode does not
;; support it.

;; [C-c C-l] insert link completion.
(setq org-link-abbrev-alist
      '(("RFC" . "https://datatracker.ietf.org/doc/rfc%s")
        ;; ("RFC" . "https://www.rfc-editor.org/search/rfc_search_detail.php?rfc=%s")
        ;; search engines
        ("Google" . "http://www.google.com/search?q=%s")
        ("DuckDuckGo" . "https://duckduckgo.com/?q=%s")
        ("Blekko" . "https://blekko.com/#?q=%s")
        ("Bing" . "http://cn.bing.com/search?q=")
        ("Baidu" . "http://www.baidu.com/s?wd=%s")
        ;; Wiki
        ("Wikipedia" . "http://en.wikipedia.org/w/index.php?search=%s")
        ("Wikipedia-zh_CN" . "https://zh.wikipedia.org/w/index.php?search=%s")
        ("Wikia" . "http://www.wikia.com/index.php?search=%s")
        ("Baidu_BaiKe" . "http://baike.baidu.com/search/none?word=%s")
        ;; Q & A
        ("Quora" . "https://www.quora.com/search?q=%s")
        ("ZhiHu" . "http://www.zhihu.com/search?q=%s&type=question")
        ("Baidu_ZhiDao" . "http://zhidao.baidu.com/search?word=%s")
        ("Baidu_JingYan" . "http://jingyan.baidu.com/search?word=%s")
        ;; ISBN
        ;; ("ISBN" . "http://isbndb.com/search/all?query=%s")
        ("ISBN" . "http://www.openisbn.com/search.php?q=%s&isbn=1")
        ;; Maps
        ("Baidu_Maps" . "http://map.baidu.com/?q=%s")
        ("Google_Maps" . "http://maps.google.com/maps?q=%s")
        ("OpenStreetMap" . "https://www.openstreetmap.org/search?query=%s")
        ;; Social Networks
        ("Twitter" . "https://twitter.com/%s")
        ("Facebook" . "https://www.facebook.com/%s")
        ;; Programming
        ("Stack_Overflow" . "http://stackoverflow.com/search?q=%s")
        ("S.E_Programmers" . "http://programmers.stackexchange.com/search?q=%s")
        ;; Emacs
        ("Emacs_Wiki" . "www.emacswiki.org/emacs?search=%s")
        ("S.E_Emacs" . "http://emacs.stackexchange.com/search?q=%s")
        ;; API Search
        ("Mozilla_Developer" . "https://developer.mozilla.org/en-US/search?q=%s")
        ("{API}Search_apis.io" . "http://apis.io/?search=%s")
        ;; Code Search
        ("search_code" . "http://searchcode.com/?q=%s")
        ("GitHub" . "https://github.com/search?q=%s")
        ("Bitbucket" . "https://bitbucket.org/repo/all?name=%s")
        ("Launchpad" . "https://launchpad.net/+search?field.text=%s")
        ("Code_Project" . "http://www.codeproject.com/search.aspx?q=%s")
        ("CodePlex" . "https://www.codeplex.com/site/search?query=%s")
        ("Gitorious" . "https://gitorious.org/search?q=%s")
        ("SourceForge" . "https://sourceforge.net/directory/?q=%s")
        ("Freecode" . "http://freecode.com/search?q=%s")
        ("Active_State" . "http://code.activestate.com/search/#q=%s")
        ("Ohloh_Code" . "http://code.ohloh.net/search?s=%s")
        ("Snipplr" . "http://snipplr.com/search.php?q=%s")
        ;; chinese code search
        ("Coding" . "https://coding.net/search?q=%s")
        ("Geakit" . "https://geakit.com/search?q=%s")
        ("Git_OSC_Open_Source_China" . "https://git.oschina.net/search?search=%s")
        ;; Bug Track System
        ("CVE" . "https://cve.mitre.org/cgi-bin/cvename.cgi?name=%s")
        ;; Lisp
        ("lispdoc" . "http://lispdoc.com/?q=%s")
        ;; Clojure
        ;; Java
        ("Maven" . "https://search.maven.org/search?q=%s")
        ("Java JSR" . "https://jcp.org/en/jsr/detail?id=%s") ; Java Specification Requests
        ("Java JEP" . "https://openjdk.java.net/jeps/%s") ; JEP
        ;; Python
        ("Python_3_Documentation" . "http://docs.python.org/3/search.html?q=%s")
        ;; Ruby
        ("Ruby-Doc" . "http://ruby-doc.com/search.html?q=%s")
        ;; Perl
        ("Perl_CPAN" . "http://search.cpan.org/search?mode=all&query=%s")
        ;; PHP
        ("PHP_online_documentation" . "http://cn2.php.net/results.php?q=%s&p=manual")
        ;; JavaScript
        ("JavaScript_Mozilla" . "https://developer.mozilla.org/en-US/search?q=%s")
        ;; HTML
        ;; CSS
        ;; Book
        ("DouBan_Books" . "http://book.douban.com/subject_search?search_text=%s")
        ;; Movie
        ("IMDb" . "http://www.imdb.com/title/%s")
        ("DouBan_Movies" . "http://movie.douban.com/subject_search?search_text=%s")
        ;; Social Networks
        ("weibo" . "https://s.weibo.com/weibo?q=")
        ))


;;; open image link to edit

(setq org-image-link-edit-cmd "gimp")

(defun org-image-link-edit ()
  "Open the image at point for editing."
  (interactive)
  (let* ((context (org-element-context))
         (type (plist-get (cadr context) :type))
         (path (plist-get (cadr context) :path)))
    (if (not (eq (car-safe context) 'link))
        (user-error "Not on a link")
      (async-start-process
       "org-download-edit"
       org-image-link-edit-cmd
       (lambda (p)
         (message (format "%s" p)))
       (pcase type
         ("file" (org-link-unescape path))
         ("attachment" (org-attach-expand path)))))))

(define-key Org-prefix (kbd "E") 'org-image-link-edit)

;;; [ helm-org-names ] -- Helm navigation of source blocks, tables and figures in Org Mode.

;; (use-package helm-org-names
;;   :ensure t
;;   :defer t
;;   :commands (helm-org-names))

;;; [ org-quick-peek ] -- Quick inline peeks at agenda items and linked nodes in Org-mode.

;; (use-package org-quick-peek
;;   :vc (:url "https://github.com/alphapapa/org-quick-peek"))

;;; [ org-kindle ] -- Make Emacs bridge between Org Mode and Kindle.

(use-package org-kindle
  :ensure t
  :defer t
  :commands (org-kindle-send-to-device org-kindle-sync-notes)
  :init (add-to-list 'display-buffer-alist '("^\\*org-kindle:.*\\*" . (display-buffer-below-selected))))

;;; [ org-screen ] -- Integrate Org Mode with screen.

(use-package org-screen
  :commands (org-screen))

;;; [ org-link-beautify ] -- beautify org links with intuitive icons.

(use-package org-link-beautify
  :ensure t
  ;; :preface (setq org-link-beautify-enable-debug-p t)
  :custom ((org-link-beautify-async-preview nil)
           (org-link-beautify-audio-preview (and (when (eq system-type 'darwin) nil)
                                                 org-link-beautify-audio-preview))
           (org-link-beautify-text-preview nil)
           (org-link-beautify-archive-preview nil)
           (org-link-beautify-subtitle-preview nil))
  :commands (org-link-beautify-mode)
  :hook (after-init . org-link-beautify-mode))

;;; [ org-transclusion ] -- Emacs package to enable transclusion with Org Mode.

(use-package org-transclusion
  :ensure t
  :commands (org-transclusion-mode
             org-transclusion-make-from-link
             org-transclusion-add org-transclusion-add-all)
  :init
  ;; A hydra interface for org-transclusion.
  (require 'hydra)
  
  (defmacro hydra-org-transclusion--detect-transclude-at-point-wrapper (&rest body)
    `(let ((line-text (buffer-substring-no-properties
                       (line-beginning-position) (line-end-position))))
       (if (string-match-p "#\\+transclude:" line-text)
           (save-excursion
             ,@body)
         (user-error "You'r not on #+transclude: [[link]] line."))))

  (macroexpand-all
   '(hydra-org-transclusion--detect-transclude-at-point-wrapper
     (move-beginning-of-line nil)
     (insert "#+transclude: ")
     (message "[org-transclusion] inserted the #transclude: annotation marker.")))

  (defhydra hydra-org-transclusion (:color blue :hint nil :exit nil)
    "
^Toggle status^                                    ^Insert Options^
^------------------^                               ^---------------------^
_t_: insert #transclude: annotation marker.        _d_: :disable-auto
_a_: add #+transclude marker at point.             _l_: :level N
_A_: add ALL transclusions in current buffer.      _o_: :only-contents
_D_: Remove transcluded text at point.             _x_: :exclude-elements \"elements\"
_s_: :src <language>                               _e_: :expand-links
                                                 _h_: :rest \"header-arguments\"
                                                 _n_: :lines N-M
                                                 _g_: :end \"search-term\"
^------------------------------------------------------------------------------^
"
    ("t" (lambda ()
           (interactive)
           (beginning-of-line) (insert "#+transclude: ")
           (message "[org-transclusion] inserted the #transclude: annotation marker."))
     :color blue)
    ("F" (lambda ()
           (interactive)
           (save-excursion
             (add-file-local-variable 'eval '(org-transclusion-mode 1))
             (message "The file-local variable -> eval: (org-transclusion-mode 1) added.")))
     "add file-local variable for auto enable"
     :color red)
    ("M" org-transclusion-mode "toggle org-transclusion-mode" :color red)
    ("a" org-transclusion-add :color amaranth)
    ("A" org-transclusion-add-all :color amaranth)
    ("D" org-transclusion-remove :color amaranth)
    ("L" org-transclusion-live-sync-start :color teal)
    ("d" (lambda ()
           (interactive)
           (hydra-org-transclusion--detect-transclude-at-point-wrapper
            (move-end-of-line nil)
            (insert " :disable-auto")))
     :color pink)
    ("l" (lambda ()
           (interactive)
           (hydra-org-transclusion--detect-transclude-at-point-wrapper
            (move-end-of-line nil)
            (let ((current-headline-level
                   (save-mark-and-excursion
                     (org-previous-visible-heading 1)
                     (org-element-property :level (org-element-at-point)))))
              (insert
               (format " :level %s"
                       (read-string "Set org-transclusion content headline level: "
                                    (number-to-string (1+ current-headline-level))))))))
     :color pink)
    ("o" (lambda ()
           (interactive)
           (hydra-org-transclusion--detect-transclude-at-point-wrapper
            (end-of-line) (insert " :only-contents")))
     :color pink)
    ("x" (lambda ()
           (interactive)
           (hydra-org-transclusion--detect-transclude-at-point-wrapper
            (move-end-of-line nil)
            (insert (format " :exclude-elements \"%s\""
                            (completing-read-multiple
                             "[org-transclusion] :exclude-elements "
                             org-element-all-elements)))))
     :color pink)
    ("e" (lambda ()
           (interactive)
           (hydra-org-transclusion--detect-transclude-at-point-wrapper
            (end-of-line) (insert " :expand-links")))
     :color pink)
    ("s" (lambda ()
           (interactive)
           (hydra-org-transclusion--detect-transclude-at-point-wrapper
            (move-end-of-line nil)
            (insert (format " :src %s"
                            (completing-read
                             "[org-transclusion] :src "
                             (mapcar 'car org-babel-load-languages))))))
     :color pink)
    ("h" (lambda ()
           (interactive)
           (hydra-org-transclusion--detect-transclude-at-point-wrapper
            (move-end-of-line nil)
            (insert (format " :rest \"%s\""
                            (read-string
                             "[org-transclusion] header arguments: "
                             ":results output")))))
     :color pink)
    ("n" (lambda ()
           (interactive)
           (hydra-org-transclusion--detect-transclude-at-point-wrapper
            (move-end-of-line nil)
            (insert (format " :lines %d-%d"
                            (read-number "[org-transclusion] lines start from: ")
                            (read-number "[org-transclusion] lines end at: ")))))
     :color pink)
    ("g" (lambda ()
           (interactive)
           (hydra-org-transclusion--detect-transclude-at-point-wrapper
            (move-end-of-line nil)
            (insert (format " :end \"%s\""
                            (read-string "[org-transclusion] search term: ")))))
     :color pink)
    )

  (define-key org-mode-map (kbd "C-c o t") 'hydra-org-transclusion/body))

;;; [ org-extra-link-types ] -- Add extra link types to Org Mode.

(use-package org-extra-link-types
  :vc (:url "git://repo.or.cz/org-extra-link-types.el.git")
  :demand t)

;;; [ org-sketch ] -- Integrate Xournal(++), Draw.io into Org Mode for handwriting notetaking software with PDF annotation support.

;; FIXME: void function `org-link-edit'
;; (use-package org-sketch
;;   :load-path "yuchen-lea/org-sketch"
;;   :custom ((org-sketch-note-dir "~/Org/Sketch") ; sketch 文件存储目录
;;            ;; (org-sketch-xournal-template-dir "~/.")                    ; xournal 目标文件存储目录
;;            (org-sketch-xournal-default-template-name "template.xopp") ; 默认笔记模版名称，应该位于 org-sketch-xournal-template-dir
;;            (org-sketch-apps '("drawio" "xournal"))                    ; 设置使用的sketch应用
;;            )
;;   :commands (org-sketch-insert org-sketch-insert-new-xournal)
;;   :hook (org-mode . org-sketch-mode))

;;; [ org-krita ] -- Krita sketches in Org.

;;; NOTE: disable package because of very slow performance on opening org-mode file.
;; (use-package org-krita
;;   :vc (:url "https://github.com/lepisma/org-krita")
;;   :defer t
;;   :commands (org-krita-mode org-krita-insert-new-image)
;;   :custom (org-krita-executable (cl-case system-type
;;                                   (gnu/linux "krita")
;;                                   (darwin "/Applications/krita.app/Contents/MacOS/krita")))
;;   :hook (org-mode . org-krita-mode))

;;; [ org-excalidraw ] -- create & insert excalidraw drawings in org-mode.

(use-package org-excalidraw
  :vc (:url "https://github.com/wdavew/org-excalidraw")
  :custom (org-excalidraw-directory "~/Org/excalidraw/")
  :init (org-excalidraw-initialize)
  :commands (org-excalidraw-create-drawing))

;;; [ org-inline-anim ] -- Org-inline-anim plays animated GIF or PNG inline in Org buffers.

(use-package org-inline-anim
  :ensure t
  :commands (org-inline-anim-mode)
  :hook (org-mode . org-inline-anim-mode))

;; (setq org-yank-image-save-method 'attach)
;; (setq org-yank-dnd-default-attach-method nil)

;;; Auto insert link to file when drag-and-drop, otherwise, open file in Dired.
(defun my/org-mode-link-dnd-handler (uri _action)
  "Register DND processing rule in `org-mode' in `dnd-protocol-alist'.
Supports URL media resource downloading and org-attach.
Supports local file org-attach. (Supports multiple file selection at once)."
  (let* ((parsed-uri (url-generic-parse-url uri))
         (url-type (url-type parsed-uri))
         (url-path (url-filename parsed-uri))
         ;; for image, video links etc.
         (file-type (file-name-extension uri)))
    (cond
     ;; URL: https://twitter.com/stardiviner
     ((string-match-p "http\\(s\\)?\\|ftp" url-type)
      (let ((decoded-url (decode-coding-string (url-unhex-string uri) 'utf-8)))
        (if (and (or (eq major-mode 'org-mode) (derived-mode-p 'org-mode))
                 ;; URL is media file link?
                 file-type
                 (or (member file-type '("heic" "svg" "webp" "png" "gif" "tiff" "jpg" "jpeg" "xpm" "xbm" "pbm"))
                     (member file-type '("avi" "rmvb" "ogg" "ogv" "mp4" "mkv" "mov" "webm" "flv" "ts" "mpg"))
                     (member (intern file-type) image-types)))
            ;; let `org-attach' download URL and attach.
            (progn
              ;; (reference from `org-attach', `org-attach-attach-mv')
              (org-attach-dir t)
              (org-attach-attach uri nil 'url) ; -> [attach:FILE]
              ;; insert the last stored link. (reference from `org-insert-link')
              (org-insert-last-stored-link 1))
          ;; insert URL directly.
          (insert decoded-url))))
     ;; FILE: "file:///Users/stardiviner/Downloads/test.txt"
     ((or (string-equal url-type "file") (file-regular-p uri))
      (if (or (eq major-mode 'org-mode) (derived-mode-p 'org-mode))
          ;; org-attach file automatically and insert last link.
          (progn
            ;; (reference from `org-attach', `org-attach-attach-mv')
            (org-attach-dir t)
            (org-attach-attach url-path nil 'mv)
            ;; insert the last stored link. (reference from `org-insert-link')
            (org-insert-last-stored-link 1))
        ;; open path with Dired.
        (dired url-path))))))

;;; replace the `org-download-dnd' from config `org-download-enable'.
(add-to-list 'dnd-protocol-alist '("^\\(https?\\|ftp\\|file\\|nfs\\):" . my/org-mode-link-dnd-handler))

;;; Convert Org link video file into gif image file.
(defun my/org-link-convert-video-to-gif ()
  "Convert Org link video file into gif image."
  (interactive)
  (when-let* ((element-context (org-element-link-parser)) ; or `org-element-context'
              (link-p (eq (car element-context) 'link))
              (path-video (org-element-property :path element-context))
              (path-gif (string-replace (file-name-extension path-video) "gif" path-video))
              ;; -> [282 nil 304 312 314 0 nil nil nil nil nil nil ...]
              ;; [[file:gifs/test.mp4][test.mp4|]]|
              ;; ^                     ^       ^  ^
              ;; 282        nil       304    312  314
              (link-beginning (org-element-property :begin element-context))
              (link-end (org-element-property :end element-context))
              ;; alternative way
              ;; (element-properties (org-element-property :standard-properties element-context))
              ;; (element-beginning (aref element-properties 0))
              ;; (element-end (aref element-properties 4))
              )
    ;; convert video into gif
    (if (process-live-p (get-process "org-link-convert-video-to-gif"))
        (warn "Only one process running allowed, wait previous process finished then try again.")
      (make-process
       :name "org-link-convert-video-to-gif"
       :buffer "*org-link-convert-video-to-gif*"
       :command (list "ffmpeg" "-y" "-i" path-video path-gif)
       :sentinel (lambda (proc event)
                   (when (string-equal event "finished\n")
                     (let* ((element-context (org-element-link-parser)) ; or `org-element-context'
                            (link-p (eq (car element-context) 'link))
                            (path-video (org-element-property :path element-context))
                            (path-gif (string-replace (file-name-extension path-video) "gif" path-video))
                            (link-beginning (org-element-property :begin element-context))
                            (link-end (org-element-property :end element-context)))
                       ;; replace link video extension with extension "gif".
                       (replace-string-in-region (file-name-extension path-video) "gif" link-beginning link-end)
                       (when (and (file-exists-p path-gif) (yes-or-no-p "Delete Org link video file? "))
                         (delete-file path-video))
                       ;; replace Org tag "video" with "gif"
                       (save-excursion
                         (org-back-to-heading)
                         (org-set-tags (cl-substitute "gif" "video" (org-get-tags (point) 'local) :test 'string-equal)))
                       (message "Convert Org link video file to GIF image file finished."))))))))

;;; [ org-media-note ] -- Taking interactive notes when watching videos or listening to audios in org-mode.

(use-package org-media-note
  ;; :vc (:url "git@github.com:yuchen-lea/org-media-note.git" :rev :newest)
  :load-path "~/Code/Emacs/org-media-note/"
  ;; :ensure org-ref ; (optional requirements)
  :custom ((org-media-note-screenshot-save-method 'attach)
           (org-media-note-screenshot-link-type-when-save-in-attach-dir 'attach)
           (org-media-note-display-inline-images nil)
           (org-media-note-seek-method 'seconds) ; 'frames
           (org-media-note-seek-value 3))
  :commands (org-media-note-mode)
  :hook (emacs-startup .  org-media-note-mode)
  :bind (:map org-mode-map ("s-v" . org-media-note-hydra/body)))



(provide 'init-org-hyperlink)

;;; init-org-hyperlink.el ends here
