;;; init-org-agenda.el --- init for Org Agenda
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

(use-package org-agenda
  :defer t
  :commands (org-agenda)
  :custom ((org-agenda-window-setup 'current-window)
           (org-agenda-restore-windows-after-quit t)
           (org-agenda-sticky t)        ; don't kill *Org Agenda* buffer by [q].
           (org-agenda-span 'day)       ; default display one day agenda instead of default week.
           )
  :init
  (add-to-list 'display-buffer-alist '("^\\*Agenda Commands\\*" . (display-buffer-below-selected)))
  (add-to-list 'display-buffer-alist '("^\\*Org Agenda(.*)\\*" . (display-buffer-below-selected)))
  :config

  ;; `org-agenda-files'
  (autoload '-flatten "dash")
  (unless org-agenda-files ; when `customize.el' loaded `org-agenda-files', no need to set again.
    (setq org-agenda-files
          ;; `org-check-agenda-file' checking in `org-agenda'.
          ;; delete duplicated files in list.
          (delete-dups
           ;; filter Emacs temporary files like ".#filename".
           (-filter
            (lambda (f)
              (and (not (string-prefix-p ".#" (file-name-nondirectory f)))
                   (string= (file-name-extension f) "org")))
            ;; recursive in all directory and sub-directory.
            (-flatten
             (mapcar
              (lambda (path)
                (if (file-directory-p path) ; if it is a directory
                    ;; return all files recursively in directory
                    (directory-files-recursively path ".org$" nil t) ; predicate whether sub-directory is readable.
                  ;; if it is a file, return the file directly.
                  path))
              '(;; Tasks
                "~/Org/Tasks/Tasks.org"
                "~/Org/Tasks/Daily.org"
                "~/Org/Tasks/Repeated Tasks.org"
                "~/Org/Tasks/Agenda Statistics.org"
                "~/Org/Tasks/Computer Tasks.org"
                "~/Org/Tasks/Read It Later.org"
                "~/Org/Tasks/Read Books.org"
                "~/Org/Tasks/Plans.org"
                "~/Org/Tasks/Entertainment Tasks.org"
                "~/Org/Tasks/Life Tasks/Life Tasks.org"
                "~/Org/Tasks/Financial Tasks/Financial Tasks.org"
                "~/Org/Tasks/Learning Tasks/Learn Programming Tasks.org"
                "~/Org/Tasks/Learning Tasks/Learning Tasks.org"
                "~/Org/Tasks/Work Tasks/Work Tasks.org"
                "~/Org/Tasks/Work Tasks/我的自媒体/我的自媒体.org"
                "~/Org/Tasks/Family Tasks/Family Tasks.org"
                "~/Org/Tasks/House Tasks/House Tasks.org"
                "~/Org/Tasks/Travel/Travel Plan.org"

                ;; Projects
                "~/Org/Projects/Projects.org"
                "~/Org/Projects/Ideas HowTo.org"
                "~/Org/Projects/Gantt/Gantt.org"
                "~/Org/Projects/Human Projects/Human Projects.org"
                "~/Org/Projects/Hardware Projects/Hardware Projects.org"
                "~/Org/Projects/World Projects/World Projects.org"
                "~/Org/Projects/Programming Projects/Code.org"
                "~/Org/Projects/Programming Projects/Programming Projects.org"
                "~/Org/Projects/Programming Projects/Interpersonal Network/Interpersonal Network.org"
                "~/Org/Projects/Female Projects/Female Projects.org"
                "~/Org/Projects/We Media Projects/We Media Projects.org"
                "~/Org/Projects/Writing Projects/Writing Projects.org"
                "~/Org/Projects/Woodworking Projects/Woodworking Projects.org"
                "~/Org/Projects/Technology Projects/Technology Projects.org"
                "~/Org/Projects/Society Projects/Society Projects.org"
                "~/Org/Projects/Sex Projects/Sex Projects.org"
                "~/Org/Projects/Product Projects/Product Projects.org"
                "~/Org/Projects/Organization Projects/Organization Projects.org"
                "~/Org/Projects/Male Projects/Male Projects.org"
                "~/Org/Projects/Immortality Projects/Immortality Projects.org"
                "~/Org/Projects/Finance Projects/Finance Projects.org"
                "~/Org/Projects/Culture Projects/Culture Projects.org"
                "~/Org/Projects/Clothes Projects/Clothes Projects.org"
                "~/Org/Projects/Business Projects/Business Projects.org"
                "~/Org/Projects/Building Projects/Building Projects.org"
                "~/Org/Projects/Agriculture Projects/Agriculture Projects.org"

                ;; Life
                "~/Org/Accounts/local accounts.org"
                "~/Org/Accounts/accounts.org.gpg"
                "~/Org/Contacts/Contacts.org"
                "~/Org/Calendars/Anniversary.org"
                "~/Org/Myself/Myself.org"
                "~/Org/Myself/My Health Status Management/My Health Status Management.org"
                "~/Org/Wiki/Things/Things.org"
                ;; "~/Org/Beauty/Beauty.org"
                ;; "~/Org/My Music Library/My Music Library.org"

                ;; Taobao
                "~/Org/Tasks/Work Tasks/淘宝店铺经营/Taobao Shop Management.org"
                "~/Org/Tasks/Work Tasks/淘宝店铺经营/Taobao Order List/Taobao Order List.org" ; 淘宝店铺的订单管理
                "~/Org/Tasks/Work Tasks/京东店铺经营/京东店铺经营.org"

                ;; Chinese Government
                "~/Org/Wiki/Countries/China/Chinese Government/manuals/My Chinese Government Administration Manual/My Chinese Government Administration Manual.org"

                ;; dotfiles
                "~/Org/dotfiles/macOS/macOS dotfiles.org"
                "~/Org/dotfiles/Linux/Arch/Arch Linux dotfiles.org"
                "~/Org/dotfiles/Linux/Ubuntu/Ubuntu Linux dotfiles.org"
                "~/Org/dotfiles/Linux/Raspberry Pi/Raspberry Pi dotfiles.org"
                "~/Org/dotfiles/Windows/Windows dotfiles.org"
                "~/Org/dotfiles/Home/Home dotfiles.org"
                "~/Org/dotfiles/NAS/QNAP/My QNAP NAS dotfiles.org"
                "~/Org/dotfiles/NAS/Synology/My Synology NAS dotfiles.org"

                ;; Subscribed

                ;; Entertainment
                "~/Org/Wiki/Countries/China/Chinese Literature/Chinese Novel Literature/Novel Books/Novel Books.org"
                "~/Org/Wiki/ACG/Comic/Comic Books/Comic Books.org"
                "~/Org/Wiki/ACG/Anime/Anime Videos/Anime Videos.org"

                ;; ;; Porn
                ;; "~/Org/Wiki/Pornography/Porn/Porn Stars/Porn Stars Reference/Porn Stars Reference.org"
                ;; "~/Org/Wiki/Pornography/Porn/Porn Videos/Porn Videos.org"
                ;; "~/Org/Wiki/Pornography/Porn/Porn Audiobooks/Porn Audiobooks.org"
                ;; "~/Org/Wiki/Pornography/Porn/Porn Audios/Porn Audios.org"
                ;; "~/Org/Wiki/Pornography/Porn/Porn Images/Porn Images.org"
                ;; "~/Org/Wiki/Pornography/Porn/Sexy Images/Sexy Images.org"
                ;; "~/Org/Wiki/Pornography/Porn/Porn Novels/Porn Novels.org"
                ;; ;; Erotic
                ;; "~/Org/Wiki/Pornography/Eroticism/Erotic Film/Erotic Films/Erotic Films.org"
                ;; "~/Org/Wiki/Pornography/Eroticism/Erotic Images/Erotic Images.org"
                ;; "~/Org/Wiki/Pornography/Eroticism/Erotic Videos/Erotic Videos.org"
                ;; "~/Org/Wiki/Pornography/Eroticism/Erotic Comics/Erotic Comics.org"
                ;; "~/Org/Wiki/Pornography/Eroticism/Erotic Magazines/Erotic Magazines.org"
                ;; ;; Hentai
                ;; "~/Org/Wiki/Pornography/Hentai/Hentai Images/Hentai Images.org"
                ;; "~/Org/Wiki/Pornography/Hentai/Hentai Image Authors/Hentai Image Authors.org"
                ;; "~/Org/Wiki/Pornography/Hentai/Hentai GIFs/Hentai GIFs.org"
                ;; "~/Org/Wiki/Pornography/Hentai/Hentai Anime/Hentai Anime Videos/Hentai Anime Videos.org"
                ;; "~/Org/Wiki/Pornography/Hentai/Hentai Comic/Hentai Comic Books/Hentai Comic Books.org"
                ;; "~/Org/Wiki/Pornography/Hentai/Hentai 3D ACG/Hentai 3D ACG Videos/Hentai 3D ACG Videos.org"
                ))))))
    ;; save value to `custom.el' to reduce Emacs startup time.
    (customize-save-variable 'org-agenda-files org-agenda-files))
  (setq org-agenda-skip-unavailable-files t) ; auto skip unavailable org-agenda-files

  ;; Calendar/Diary integration
  ;; (setq org-agenda-include-diary t ; use "Diary-style expression entries" directly instead.
  ;;       diary-file (concat org-directory "/Calendars/Anniversary.org")
  ;;       org-agenda-diary-file (concat org-directory "/Calendars/Anniversary.org")
  ;;       ;; org-agenda-insert-diary-strategy 'date-tree
  ;;       )

  
  ;; Agenda Views
  (setq org-agenda-prefix-format
        '((agenda . " %i %-15c %?e %?-15t %s")
          (timeline . " %?s")
          (effort . " %i %e %(or (org-entry-get (point) \"Effort\") \"0:00\")")
          (clock . " %i %-15c %? e %?-15t")
          (todo . " %i %-15c")
          (search . " %i %-15c")
          (tags . " %i %-15c")))
  (setq org-agenda-scheduled-leaders '("Scheduled: " "%3d days │ "))

  ;; Agenda tags
  (setq org-agenda-remove-tags t        ; PERFORMANCE: don't display tags in Org Agenda
        org-agenda-show-inherited-tags nil
        org-agenda-use-tag-inheritance nil
        org-agenda-align-tags-to-column 'auto
        org-agenda-tags-column 'auto)

  ;; speedup Org Agenda
  (setq org-agenda-inhibit-startup t
        org-agenda-dim-blocked-tasks t)

  ;; toggle log mode in agenda buffer. Press [l] in org-agenda buffer.
  (setq org-agenda-start-with-log-mode '(closed clock)
        org-agenda-log-mode-items '(closed clock)
        ;; NOTE: this will make line longer and bad for `org-agenda-clock-colorize-block'.
        ;; display first line of log.
        ;; `org-agenda-entry-text-mode'
        org-agenda-log-mode-add-notes nil)

  ;; Note the clock report table slow down Org Agenda.
  (setq org-agenda-start-with-clockreport-mode t
        org-agenda-clock-report-header "Clock Report Table\n"
        org-agenda-clockreport-parameter-plist
        '(:scope agenda-with-archives :maxlevel 5 :block today :fileskip0 t :indent t :link nil))

  (setq org-agenda-prefer-last-repeat t)

  ;; sorting strategy
  (setq org-agenda-sorting-strategy
        '((agenda time-up deadline-up scheduled-down ts-up habit-down priority-down category-keep)
          (todo priority-down category-keep)
          (tags priority-down category-keep)
          (search category-keep))
        org-agenda-sorting-strategy-selected
        '(time-up deadline-up scheduled-down ts-up habit-down category-keep))

  ;; Time Grid
  ;; (setq org-agenda-time-grid '((daily today require-timed)
  ;;                              (800 900 1000 1100 1200
  ;;                                   1300 1400 1500 1600 1700 1800 1900 2000)
  ;;                              "......" "----------------"))

  (setq org-agenda-timegrid-use-ampm t)

  ;; specify different color for days
  (defun my-org-agenda-get-day-face-fn (date)
    "Return the face DATE should be displayed with."
    (let ((day-of-week (calendar-day-of-week date)))
      (cond
       ((or (= day-of-week 1) (= day-of-week 5))
        '(:foreground "forest green" :box (:color "dim gray" :line-width 3)))
       ((org-agenda-todayp date)
        'org-agenda-date-today)
       ((member day-of-week org-agenda-weekend-days)
        'org-agenda-date-weekend)
       (t 'org-agenda-date))))

  (setq org-agenda-day-face-function 'my-org-agenda-get-day-face-fn)

  (setq org-agenda-skip-timestamp-if-done t
        org-agenda-skip-deadline-if-done t
        org-agenda-skip-deadline-prewarning-if-scheduled t
        org-agenda-skip-scheduled-if-done t
        org-agenda-skip-scheduled-delay-if-deadline 'post-deadline
        org-agenda-skip-scheduled-repeats-after-deadline t
        org-agenda-skip-timestamp-if-deadline-is-shown t
        org-agenda-skip-additional-timestamps-same-entry nil
        org-deadline-warning-days 14
        ;; org-scheduled-delay-days 0 ; NOTE: if change default 0, will invalid `org-habit'.
        ;; ignore past expired schedule and deadline tasks
        ;; org-deadline-past-days (* 30 12) ; default 10000
        ;; org-scheduled-past-days (* 30 12) ; default 10000
        ;; XXX: org-agenda-ignore-properties '(effort appt stats category)
        org-agenda-tags-todo-honor-ignore-options t)

  ;; Org-Agenda All Todo List
  (setq org-agenda-todo-ignore-timestamp 'all
        org-agenda-todo-ignore-with-date nil
        org-agenda-todo-ignore-scheduled 'future
        org-agenda-todo-ignore-deadlines 'near)
  
  ;; entry text mode
  ;; (setq org-agenda-start-with-entry-text-mode t)
  ;; follow mode
  ;; (setq org-agenda-start-with-follow-mode t)

  ;; show context details when jump from agenda.
  (add-to-list 'org-show-context-detail '(agenda . tree))

  ;; [ Composite Agenda View ]
  ;; Usage: `(org-agenda nil "C")'
  ;;
  ;; The reason is that org does not expect to run arbitrary sexp (in your case,
  ;; the (format ...) expression); instead with the backtick and unquote, you
  ;; are passing in a propertized string to org, which org can understand and
  ;; make use of.

  (add-to-list
   'org-agenda-custom-commands
   `("c"
     ,(format "%s Today clocked tasks." (nerd-icons-faicon "nf-fa-hourglass_start" :face 'nerd-icons-red-alt))
     ((agenda ""
              ((org-agenda-ndays 1)
               (org-agenda-span-1)
               (org-agenda-use-time-grid t)
               (org-agenda-include-diary nil)
               (org-agenda-show-log (quote clockcheck))
               (org-agenda-clockreport t))))
     ((org-agenda-overriding-header "Today Clocked Tasks")))
   'append)
  
  (add-to-list
   'org-agenda-custom-commands
   `("p"
     ,(format "%s Programming tasks - BUG, ISSUE, FEATURE etc." (nerd-icons-faicon "nf-fa-file_code_o" :face 'nerd-icons-cyan-alt))
     ((tags-todo "bug" ((org-agenda-overriding-header "Bugs")))
      (tags-todo "issue" ((org-agenda-overriding-header "Issues")))
      (tags-todo "feature" ((org-agenda-overriding-header "Features")))))
   'append)
  
  (add-to-list
   'org-agenda-custom-commands
   `("w"
     ,(format "%s Work Agenda" (nerd-icons-faicon "nf-fa-black_tie" :face 'nerd-icons-purple-alt))
     ((tags-todo "work" ((org-agenda-overriding-header "Work")))))
   'append)

  ;; used to filter out fragment time tasks.
  (add-to-list
   'org-agenda-custom-commands
   `("f"
     ,(format "%s fragment time tasks" (nerd-icons-faicon "nf-fa-check_square_o" :face 'nerd-icons-blue-alt))
     ((tags-todo "fragment" ((org-agenda-overriding-header "Fragment Time Tasks")))))
   'append)

  ;; Show Org Agenda tasks with height spacing based on clock time with `org-agenda-log-mode'.
  ;; https://emacs-china.org/t/org-agenda/8679
  ;; work with org-agenda dispatcher [c] "Today Clocked Tasks" to view today's clocked tasks.
  (defun org-agenda-clock-colorize-block ()
    "Set different line spacing based on clock time duration."
    (save-excursion
      (let* ((colors (cl-case (frame-parameter nil 'background-mode)
		               (light (list "#F6B1C3" "#FFFF9D" "#BEEB9F" "#ADD5F7"))
		               (dark (list "#aa557f" "DarkGreen" "DarkSlateGray" "DarkSlateBlue"))))
             pos
             duration)
        (nconc colors colors)
        (goto-char (point-min))
        (while (setq pos (next-single-property-change (point) 'duration))
          (goto-char pos)
          (when (and (not (equal pos (point-at-eol)))
                     (setq duration (org-get-at-bol 'duration)))
            ;; larger duration bar height
            (let* ((time-least 10)
                   (time-average 20)
                   (line-height (if (< duration time-least) 1.0 (+ 0.5 (/ duration time-average))))
                   (ov (make-overlay (point-at-bol) (1+ (point-at-eol)))))
              (overlay-put ov 'face `(:background ,(car colors) :foreground "black" :extend t))
              (setq colors (cdr colors))
              (overlay-put ov 'line-height line-height)
              (overlay-put ov 'line-spacing (1- line-height))))))))
  
  ;; use `org-agenda-window-setup' `only-window' value to avoid ol 'line-height
  ;; property failed on long line in "colorized blocks on agenda" hook.
  ;; (when (< (display-mm-width) 800)
  ;;   (setq org-agenda-window-setup 'only-window))
  (add-hook 'org-agenda-finalize-hook #'org-agenda-clock-colorize-block)

  (define-key org-agenda-mode-map (kbd "M-s") 'org-search-view)
  )


;;; display icon for Org Agenda category
(use-package nerd-icons
  :ensure t
  :defer t
  :after org-agenda
  :config
  (setq org-agenda-category-icon-alist
        `(("Diary" ,(list (nerd-icons-mdicon "nf-md-notebook_check_outline" :face 'nerd-icons-yellow)) nil nil :ascent center)
          ("Todo" ,(list (nerd-icons-mdicon "nf-md-checkbox_marked_outline" :face 'nerd-icons-orange)) nil nil :ascent center)
          ("Habit" ,(list (nerd-icons-mdicon "nf-md-repeat_variant" :face 'nerd-icons-blue)) nil nil :ascent center)
          ("Star" ,(list (nerd-icons-mdicon "nf-md-star_check_outline" :face 'nerd-icons-yellow)) nil nil :ascent center)
          ("Org" ,(list (nerd-icons-icon-for-mode 'org-mode)) nil nil :ascent center)
          
          ;; <Work>
          ("Work" ,(list (nerd-icons-faicon "nf-fa-black_tie" :face 'nerd-icons-red)) nil nil :ascent center)
          ("Writing" ,(list (nerd-icons-mdicon "nf-md-lead_pencil" :face 'nerd-icons-blue-alt)) nil nil :ascent center)
          ("Print" ,(list (nerd-icons-mdicon "nf-md-printer" :face 'nerd-icons-silver)) nil nil :ascent center)
          
          ;; <Programming>
          ("Emacs" ,(list (nerd-icons-icon-for-mode 'emacs-lisp-mode)) nil nil :ascent center)
          ("Code" ,(list (nerd-icons-faicon "nf-fa-file_code_o" :face 'nerd-icons-cyan-alt)) nil nil :ascent center) ; "file-code-o"
          ("Programming" ,(list (nerd-icons-codicon "nf-cod-code" :face 'nerd-icons-cyan)) nil nil :ascent center)
          ("Bug" ,(list (nerd-icons-codicon "nf-cod-bug" :face 'nerd-icons-red-alt)) nil nil :ascent center)
          ("Issue" ,(list (nerd-icons-codicon "nf-cod-bug" :face 'nerd-icons-red)) nil nil :ascent center)
          ("Feature" ,(list (nerd-icons-codicon "nf-cod-gear" :face 'nerd-icons-blue)) nil nil :ascent center)
          ("VCS" ,(list (nerd-icons-mdicon "nf-md-git" :face 'nerd-icons-red)) nil nil :ascent center)
          ("Git" ,(list (nerd-icons-mdicon "nf-md-git" :face 'nerd-icons-red)) nil nil :ascent center)
          ("Database" ,(list (nerd-icons-mdicon "nf-md-database" :face 'nerd-icons-blue)) nil nil :ascent center)
          ("Design" ,(list (nerd-icons-mdicon "nf-md-palette_outline" :face 'nerd-icons-purple-alt)) nil nil :ascent center)
          ("Computer" ,(list (nerd-icons-mdicon "nf-md-desktop_mac" :face 'nerd-icons-silver)) nil nil :ascent center) ; desktop
          ("Laptop" ,(list (nerd-icons-mdicon "nf-md-laptop" :face 'nerd-icons-silver)) nil nil :ascent center)
          ("Hardware" ,(list (nerd-icons-mdicon "nf-md-devices" :face 'nerd-icons-silver)) nil nil :ascent center)
          ("Server" ,(list (nerd-icons-mdicon "nf-md-server" :face 'nerd-icons-blue)) nil nil :ascent center)
          ("Audio" ,(list (nerd-icons-sucicon "nf-seti-audio" :face 'nerd-icons-silver)) nil nil :ascent center)
          ("Analysis" ,(list (nerd-icons-mdicon "nf-md-file_chart" :face 'nerd-icons-blue)) nil nil :ascent center)
          ("Email" ,(list (nerd-icons-mdicon "nf-md-email_outline" :face 'nerd-icons-green)) nil nil :ascent center)
          ("Idea" ,(list (nerd-icons-mdicon "nf-md-head_lightbulb_outline" :face 'nerd-icons-yellow)) nil nil :ascent center)
          ("Project" ,(list (nerd-icons-codicon "nf-cod-project" :face 'nerd-icons-blue)) nil nil :ascent center)
          ("Agriculture" ,(list (nerd-icons-mdicon "nf-md-leaf" :face 'nerd-icons-green)) nil nil :ascent center)
          ("Industry" ,(list (nerd-icons-faicon "nf-fa-industry" :face 'nerd-icons-dorange)) nil nil :ascent center)
          ("Startup" ,(list (nerd-icons-mdicon "nf-md-codepen" :face 'nerd-icons-green)) nil nil :ascent center)
          ("Hack" ,(list (nerd-icons-faicon "nf-fa-user_secret" :face 'nerd-icons-dgreen)) nil nil :ascent center)
          ("Crack" ,(list (nerd-icons-faicon "nf-fa-user_secret" :face 'nerd-icons-dsilver)) nil nil :ascent center)
          ("Security" ,(list (nerd-icons-mdicon "nf-md-security" :face 'nerd-icons-green)) nil nil :ascent center)
          ("Anonymous"  ,(list (nerd-icons-octicon "nf-oct-logo_gist" :face 'nerd-icons-silver)) nil nil :ascent center)
          ("Config" ,(list (nerd-icons-sucicon "nf-seti-settings" :face 'nerd-icons-silver)) nil nil :ascent center)
          ("Command" ,(list (nerd-icons-codicon "nf-cod-terminal_cmd" :face 'nerd-icons-cyan-alt)) nil nil :ascent center)
          ("Document" ,(list (nerd-icons-mdicon "nf-md-file_document_outline" :face 'nerd-icons-silver)) nil nil :ascent center)
          ("Info" ,(list (nerd-icons-mdicon "nf-md-information_outline" :face 'nerd-icons-green)) nil nil :ascent center)
          
          ("Daily" ,(list (nerd-icons-mdicon "nf-md-calendar_today_outline" :face 'nerd-icons-green)) nil nil :ascent center)
          ("Learning" ,(list (nerd-icons-mdicon "nf-md-book_open_page_variant_outline" :face 'nerd-icons-silver)) nil nil :ascent center)
          ("University" ,(list (nerd-icons-faicon "nf-fa-university" :face 'nerd-icons-silver)) nil nil :ascent center)
          ("Reading" ,(list (nerd-icons-mdicon "nf-md-book_open_variant" :face 'nerd-icons-green)) nil nil :ascent center)
          ("ReadItLater" ,(list (nerd-icons-mdicon "nf-md-read" :face 'nerd-icons-green)) nil nil :ascent center)
          ("E-Commerce" ,(list (nerd-icons-mdicon "nf-md-storefront_outline" :face 'nerd-icons-orange)) nil nil :ascent center)
          ("WeMedia" ,(list (nerd-icons-mdicon "nf-md-podcast" :face 'nerd-icons-lblue)) nil nil :ascent center)
          
          ;; Operating Systems
          ("Linux" ,(list (nerd-icons-codicon "nf-cod-terminal_linux" :face 'nerd-icons-silver)) nil nil :ascent center)
          ("macOS" ,(list (nerd-icons-mdicon "nf-md-apple_finder" :face 'nerd-icons-blue)) nil nil :ascent center)
          ("Windows" ,(list (nerd-icons-mdicon "nf-md-microsoft_windows" :face 'nerd-icons-blue)) nil nil :ascent center)
          ("GNU" ,(list (nerd-icons-devicon "nf-dev-gnu" :face 'nerd-icons-dsilver)) nil nil :ascent center)
          ("Arch" ,(list (nerd-icons-flicon "nf-linux-archlinux" :face 'nerd-icons-lblue)) nil nil :ascent center)
          ("Ubuntu" ,(list (nerd-icons-flicon "nf-linux-ubuntu_inverse" :face 'nerd-icons-lorange)) nil nil :ascent center)
          ("BSD" ,(list (nerd-icons-flicon "nf-linux-freebsd" :face 'nerd-icons-dred)) nil nil :ascent center)
          ("Android" ,(list (nerd-icons-devicon "nf-dev-android" :face 'nerd-icons-lgreen)) nil nil :ascent center)
          ("Apple" ,(list (nerd-icons-mdicon "nf-md-apple" :face 'nerd-icons-silver)) nil nil :ascent center)
          ("Raspberry_Pi" ,(list (nerd-icons-devicon "nf-dev-rasberry_pi" :face 'nerd-icons-lred)) nil nil :ascent center)
          ("Open_Source" ,(list (nerd-icons-devicon "nf-dev-opensource" :face 'nerd-icons-lgreen)) nil nil :ascent center)
          
          ;; Programming Languages
          ;; ("LISP" ,(list (nerd-icons-icon-for-mode 'common-lisp-mode)) nil nil :ascent center)
          ;; ("Common Lisp" ,(list (nerd-icons-icon-for-mode 'common-lisp-mode)) nil nil :ascent center)
          ("Clojure" ,(list (nerd-icons-icon-for-mode 'clojure-mode)) nil nil :ascent center)
          ("CLJS" ,(list (nerd-icons-icon-for-mode 'clojurescript-mode)) nil nil :ascent center)
          ("Ruby" ,(list (nerd-icons-mdicon "nf-md-language_ruby" :face 'nerd-icons-lred)) nil nil :ascent center)
          ("Python" ,(list (nerd-icons-icon-for-mode 'python-mode)) nil nil :ascent center)
          ("Perl" ,(list (nerd-icons-icon-for-mode 'perl-mode)) nil nil :ascent center)
          ("Shell" ,(list (nerd-icons-icon-for-mode 'shell-mode)) nil nil :ascent center)
          ("PHP" ,(list (nerd-icons-icon-for-mode 'php-mode)) nil nil :ascent center)
          ("Haskell" ,(list (nerd-icons-icon-for-mode 'haskell-mode)) nil nil :ascent center)
          ("Erlang" ,(list (nerd-icons-icon-for-mode 'erlang-mode)) nil nil :ascent center)
          ("Prolog" ,(list (nerd-icons-icon-for-mode 'prolog-mode)) nil nil :ascent center)
          ("Assembly" ,(list (nerd-icons-icon-for-mode 'asm-mode)) nil nil :ascent center)
          ("C Language" ,(list (nerd-icons-icon-for-mode 'c-mode)) nil nil :ascent center)
          ("C++ Language" ,(list (nerd-icons-icon-for-mode 'c++-mode)) nil nil :ascent center)
          ("Go Language" ,(list (nerd-icons-icon-for-mode 'go-mode)) nil nil :ascent center)
          ("Swift" ,(list (nerd-icons-icon-for-mode 'swift-mode)) nil nil :ascent center)
          ("Rust" ,(list (nerd-icons-icon-for-mode 'rust-mode)) nil nil :ascent center)
          ("JavaScript" ,(list (nerd-icons-devicon "nf-dev-javascript_shield" :face 'nerd-icons-lyellow)) nil nil :ascent center)
          ("TypeScript" ,(list (nerd-icons-mdicon "nf-md-language_typescript" :face 'nerd-icons-blue)) nil nil :ascent center)
          ("Java" ,(list (nerd-icons-icon-for-mode 'java-mode)) nil nil :ascent center)
          ("HTML5" ,(list (nerd-icons-icon-for-mode 'html-mode)) nil nil :ascent center)
          ("HTML" ,(list (nerd-icons-icon-for-mode 'html-mode)) nil nil :ascent center)
          ("CSS3" ,(list (nerd-icons-icon-for-mode 'css-mode)) nil nil :ascent center)
          ("CSS" ,(list (nerd-icons-icon-for-mode 'css-mode)) nil nil :ascent center)
          ("SQL" ,(list (nerd-icons-icon-for-mode 'sql-mode)) nil nil :ascent center)
          ("PostgreSQL" ,(list (nerd-icons-devicon "nf-dev-postgresql" :face 'nerd-icons-blue)) nil nil :ascent center)
          ("R" ,(list (nerd-icons-mdicon "nf-md-language_r" :face 'nerd-icons-blue)) nil nil :ascent center)
          ("Julia" ,(list (nerd-icons-icon-for-mode 'julia-mode)) nil nil :ascent center)
          ("TeX" ,(list (nerd-icons-sucicon "nf-seti-tex" :face 'nerd-icons-silver)) nil nil :ascent center)
          ("LaTeX" ,(list (nerd-icons-sucicon "nf-seti-tex" :face 'nerd-icons-silver)) nil nil :ascent center)
          ("Web" ,(list (nerd-icons-mdicon "nf-md-web" :face 'nerd-icons-blue)) nil nil :ascent center)
          ("Network" ,(list (nerd-icons-mdicon "nf-md-network_pos" :face 'nerd-icons-dcyan)) nil nil :ascent center)
          ("GitHub" ,(list (nerd-icons-mdicon "nf-md-github" :face 'nerd-icons-silver)) nil nil :ascent center)
          ("Bitbucket" ,(list (nerd-icons-mdicon "nf-md-bitbucket" :face 'nerd-icons-blue)) nil nil :ascent center)
          ("Bitcoin" ,(list (nerd-icons-faicon "nf-fa-bitcoin" :face 'nerd-icons-lyellow)) nil nil :ascent center)
          ("GFW" ,(list (nerd-icons-mdicon "nf-md-close_network_outline" :face 'nerd-icons-red)) nil nil :ascent center)
          
          ;; <Life>
          ("Home" ,(list (nerd-icons-mdicon "nf-md-home" :face 'nerd-icons-silver)) nil nil :ascent center)
          ("Entertainment" ,(list (nerd-icons-mdicon "nf-md-television_play" :face 'nerd-icons-blue)) nil nil :ascent center)
	      ("Hotel" ,(list (nerd-icons-faicon "nf-fa-hotel" :face 'nerd-icons-orange)) nil nil :ascent center)
          ("Place" ,(list (nerd-icons-codicon "nf-cod-location" :face 'nerd-icons-orange)) nil nil :ascent center)
          ("Health" ,(list (nerd-icons-faicon "nf-fa-medkit" :face 'nerd-icons-silver)) nil nil :ascent center)
          ("Hospital" ,(list (nerd-icons-mdicon "nf-md-hospital_building" :face 'nerd-icons-silver)) nil nil :ascent center)
          ("Dining" ,(list (nerd-icons-mdicon "nf-md-food_variant" :face 'nerd-icons-orange)) nil nil :ascent center)
          ("Shopping" ,(list (nerd-icons-faicon "nf-fa-shopping_basket" :face 'nerd-icons-green)) nil nil :ascent center)
          ("Express" ,(list (nerd-icons-faicon "nf-fa-truck" :face 'nerd-icons-green)) nil nil :ascent center)
          ("Sport" ,(list (nerd-icons-mdicon "nf-md-basketball" :face 'nerd-icons-dorange)) nil nil :ascent center)
          ("Game" ,(list (nerd-icons-mdicon "nf-md-gamepad_variant" :face 'nerd-icons-blue)) nil nil :ascent center)
          ("Sex" ,(list (nerd-icons-mdicon "nf-md-human_male_female" :face 'nerd-icons-pink)) nil nil :ascent center)
          ("News" ,(list (nerd-icons-mdicon "nf-md-newspaper" :face 'nerd-icons-silver)) nil nil :ascent center)
          ("Car" ,(list (nerd-icons-mdicon "nf-md-car" :face 'nerd-icons-silver)) nil nil :ascent center)
          ("Bus" ,(list (nerd-icons-mdicon "nf-md-bus_stop" :face 'nerd-icons-green)) nil nil :ascent center)
          ("Contact" ,(list (nerd-icons-mdicon "nf-md-contacts_outline" :face 'nerd-icons-silver)) nil nil :ascent center)
          ("Talk" ,(list (nerd-icons-mdicon "nf-md-phone_in_talk_outline" :face 'nerd-icons-blue)) nil nil :ascent center)
          ("Video-Call" ,(list (nerd-icons-mdicon "nf-md-message_video" :face 'nerd-icons-green)) nil nil :ascent center)
          ("Call" ,(list (nerd-icons-codicon "nf-cod-call_outgoing" :face 'nerd-icons-green)) nil nil :ascent center)
          ("Music" ,(list (nerd-icons-mdicon "nf-md-music" :face 'nerd-icons-blue)) nil nil :ascent center)
          ("Airplane" ,(list (nerd-icons-mdicon "nf-md-airplane" :face 'nerd-icons-silver)) nil nil :ascent center)
          ("Travel" ,(list (nerd-icons-mdicon "nf-md-wallet_travel" :face 'nerd-icons-yellow)) nil nil :ascent center)
          ("Gift" ,(list (nerd-icons-mdicon "nf-md-gift" :face 'nerd-icons-lyellow)) nil nil :ascent center)
          ("WiFi" ,(list (nerd-icons-mdicon "nf-md-wifi" :face 'nerd-icons-silver)) nil nil :ascent center)
          ("Search" ,(list (nerd-icons-mdicon "nf-md-search_web" :face 'nerd-icons-lblue)) nil nil :ascent center)
          ("Mobile" ,(list (nerd-icons-codicon "nf-cod-device_mobile" :face 'nerd-icons-blue)) nil nil :ascent center)
          ("WeChat" ,(list (nerd-icons-mdicon "nf-md-wechat" :face 'nerd-icons-lgreen)) nil nil :ascent center)
          ("QQ" ,(list (nerd-icons-mdicon "nf-md-qqchat" :face 'nerd-icons-lblue)) nil nil :ascent center)
          ("Weibo" ,(list (nerd-icons-mdicon "nf-md-sina_weibo" :face 'nerd-icons-red)) nil nil :ascent center)
          ("Slack" ,(list (nerd-icons-mdicon "nf-md-slack" :face 'nerd-icons-red)) nil nil :ascent center)
          ("Facebook" ,(list (nerd-icons-mdicon "nf-md-facebook" :face 'nerd-icons-blue)) nil nil :ascent center)
          ("Twitter" ,(list (nerd-icons-mdicon "nf-md-twitter" :face 'nerd-icons-lcyan)) nil nil :ascent center)
          ("YouTube" ,(list (nerd-icons-mdicon "nf-md-youtube" :face 'nerd-icons-lred)) nil nil :ascent center)
          ("RSS" ,(list (nerd-icons-mdicon "nf-md-rss_box" :face 'nerd-icons-yellow)) nil nil :ascent center)
          ("Wikipedia" ,(list (nerd-icons-mdicon "nf-md-wikipedia" :face 'nerd-icons-silver)) nil nil :ascent center)
          ("Money" ,(list (nerd-icons-faicon "nf-fa-money" :face 'nerd-icons-green)) nil nil :ascent center)
          ("Accounting" ,(list (nerd-icons-mdicon "nf-md-receipt_text_check_outline" :face 'nerd-icons-green)) nil nil :ascent center)
          ("Bank" ,(list (nerd-icons-mdicon "nf-md-bank" :face 'nerd-icons-silver)) nil nil :ascent center)
          ("Person" ,(list (nerd-icons-faicon "nf-fa-user_circle" :face 'nerd-icons-lblue)) nil nil :ascent center)
          ("Beauty" ,(list (nerd-icons-faicon "nf-fa-female" :face 'nerd-icons-pink)) nil nil :ascent center)
          ("Birthday" ,(list (nerd-icons-faicon "nf-fa-birthday_cake" :face 'nerd-icons-silver)) nil nil :ascent center)
          
          ;; <Business>
          ("Calculate" ,(list (nerd-icons-mdicon "nf-md-calculator_variant" :face 'nerd-icons-yellow)) nil nil :ascent center)
          ("Chart" ,(list (nerd-icons-mdicon "nf-md-chart_bell_curve" :face 'nerd-icons-green)) nil nil :ascent center)
          
          ;; <Science>
          ("Chemistry" ,(list (nerd-icons-mdicon "nf-md-flask" :face 'nerd-icons-blue)) nil nil :ascent center)
          ("Language" ,(list (nerd-icons-faicon "nf-fa-language" :face 'nerd-icons-blue)) nil nil :ascent center)
          
          (".*" ,(list (nerd-icons-mdicon "nf-md-progress_question" :face 'nerd-icons-silver)) nil nil :ascent center)
          ;; (".*" '(space . (:width (16))))
          )))

;;; Tag changes that should be triggered by TODO state changes.
;; [C-c C-x a]
;; (setq org-todo-state-tags-triggers
;;       '(("" ("Task" . t))
;;         ('todo ("" . t))
;;         ('done ("ARCHIVE" . t))
;;         ("DONE" ("ARCHIVE" . t))
;;         ("CANCELLED" ("CANCELLED" . t))
;;         ))


;;;; [ org-review ] -- Track when you have done a review in org mode.

;; (use-package org-review
;;   :ensure t
;;   :config
;;   (add-to-list 'org-agenda-custom-commands
;;                '("R" "Review projects" tags-todo "-CANCELLED/"
;;                  ((org-agenda-overriding-header "Reviews Scheduled")
;;                   (org-agenda-skip-function 'org-review-agenda-skip)
;;                   (org-agenda-cmp-user-defined 'org-review-compare)
;;                   (org-agenda-sorting-strategy '(user-defined-down)))))
;;
;;   (add-to-list 'org-default-properties "NEXT_REVIEW")
;;   (add-to-list 'org-default-properties "LAST_REVIEW")
;;   )

;;; [ org-notify ] -- Notifications for Org-mode.

;; ---------------------------------------------------------
;; List of possible parameters:
;;
;;   :time      Time distance to deadline, when this type of notification shall
;;              start.  It's a string: an integral value (positive or negative)
;;              followed by a unit (s, m, h, d, w, M).
;;   :actions   A function or a list of functions to be called to notify the
;;              user.  Instead of a function name, you can also supply a suffix
;;              of one of the various predefined `org-notify-action-xxx'
;;              functions.
;;
;;   :actions -ding, -notify, -window, -notify/window, -message, -email,
;;
;;   :period    Optional: can be used to repeat the actions periodically.
;;              Same format as :time.
;;   :duration  Some actions use this parameter to specify the duration of the
;;              notification.  It's an integral number in seconds.
;;   :audible   Overwrite the value of `org-notify-audible' for this action.
;; ---------------------------------------------------------

(use-package org-notify
  :ensure t
  :after org-agenda
  ;; :commands (org-notify org-show-notification)
  ;; :custom (org-notify-audible nil)
  :config
  (cl-case system-type
    (gnu/linux
     (org-notify-add 'default
                     '(:time "1h" :period "30m" :duration 8
                             :actions (-ding -notify/window)
                             :audible t)))
    (darwin
     (defun org-notify-action-notify-macos (plist)
       "Pop up a notification for macOS."
       (let* ((duration (plist-get plist :duration))
              (title (plist-get plist :heading))
              (body (org-notify-body-text plist))
              (timeout (if duration (* duration 1000)))
              (urgency (plist-get plist :urgency)))
         (if (fboundp 'ns-do-applescript)
             (ns-do-applescript
              (format "display notification \"%s\" with title \"%s\""
                      (replace-regexp-in-string "\"" "#" body)
                      (replace-regexp-in-string "\"" "#" title))))))
     
     (org-notify-add 'default
                     '(:time "1h" :period "30m" :duration 8
                             :actions (-ding -notify-macos)
                             :audible t))))

  (org-notify-start (* 60 10)))

;;; [ org-super-agenda ] -- Supercharge your agenda.

;; (use-package org-super-agenda
;;   :ensure t
;;   :defer t)

;;; [ org-upcoming-modeline ] -- put upcoming org event in modeline.

;; (use-package org-upcoming-modeline
;;   :after org
;;   :ensure t
;;   :config (org-upcoming-modeline-mode 1))

;;; [ org-collector ] -- collect properties into tables.
;; (require 'org-collector)

;;; auto launch org-agenda after Emacs finished startup.
;; (add-hook 'after-init-hook (lambda () (org-agenda nil "a")))

;; (use-package org-timeline
;;   :ensure t
;;   :config (add-hook 'org-agenda-finalize-hook 'org-timeline-insert-timeline :append))



(provide 'init-org-agenda)

;;; init-org-agenda.el ends here
