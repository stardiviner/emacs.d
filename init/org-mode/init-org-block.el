;;; init-org-block.el --- init file for Org block -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Christopher M. Miles, all rights reserved.

;;; Commentary:



;;; Code:

;;; [ org-rich-yank ] -- Paste with org-mode markup and link to source.

(use-package org-rich-yank
  :ensure t
  :commands (org-rich-yank)
  :bind (:map org-mode-map ("C-M-y" . org-rich-yank)))

;;; [ org-volume ] -- Org dynamic blocks for metadata of volumes of medium such as books.

(use-package org-volume
  :vc (:url "git@github.com:akirak/org-volume.git" :rev :newest)
  :commands (org-volume-update-entry-from-dblock))



(provide 'init-org-block)

;;; init-org-block.el ends here
