;;; init-org-tag.el --- init for Org Tags
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:


;;; Tags

;;; tag inheritance
(setq org-use-tag-inheritance t)

;;; tags completion with `org-set-tags-command' [C-c C-q].
(setq org-use-fast-tag-selection nil)

;;; Offer Org Agenda buffers/files tags in tags completion `org-global-tags-completion-table'.

;;; sort org-mode tags in alphabetical order to have a unified view.
(setq org-tags-sort-function 'string-collate-lessp)

;; - `org-tag-alist' - globally defined your preferred set of tags.
;; - `org-tag-persistent-alist' - preferred set of tags that you would like to use in every file.
;;   Like global version of file tags '#+TAGS:'.

(setq org-tag-alist
      '(;; Org special tags
        (:startgroup . nil)
        ("ARCHIVE" . ?A) ("noexport" . ?E) ("private" . ?P) ("deprecated" . ?D) ("outdated" . ?O)
        (:endgroup)))

(setq org-tag-persistent-alist
      '(;; Marks
        (:startgroup . nil) ("@marks" . nil)
        ("on") ("off") ("star") ("like") ("favorite") ("suggested") ("heart") ("smile") ("brain")
        ("check") ("alert") ("important") ("flag") ("error")
        ("label") ("question") ("info") ("quote")
        ("table") ("translate" . ?t) ("language") ("idea" . ?i) ("comment") ("screenshot")
        ("trash") ("delete") ("clear") ("cancel") ("lock") ("unlock") ("key")
        ("refresh") ("repeat") ("shuffle")
        ("book" . ?h) ("bookmark" . ?b) ("note") ("cheatsheet") ("paperclip")
        ("plot") ("diagram") ("chart") ("line_chart") ("area_chart") ("pie_chart")
        ("file") ("archive_file")
        ("word") ("excel") ("powerpoint") ("access") ("slideshow")
        ("document") ("pdf") ("image") ("gif") ("video") ("audio") ("movie") ("film") ("picture") ("photo") ("music") ("game") ("steam")
        ("camera") ("vlog") ("record") ("mic") ("news")
        ("map") ("map_pin") ("location") ("street_view")
        ("search") ("download") ("upload") ("link") ("email") ("rss")
        ("settings") ("save") ("sync") ("backup") ("restore") ("security") ("forensic") ("computer_forensic")
        ("reply") ("send") ("share") ("screen_share") ("cast") ("blocklist")
        (:endgroup)
        
        ;; Task
        (:startgroup) ("task" . nil)
        ("event") ("event_available") ("event_busy") ("fragment")
        ("today") ("tomorrow") ("future") ("schedule") ("inprogress") ("timer") ("snooze") ("notification") ("alarm")
        (:endgroup)
        
        ;; Life
        (:startgroup) ("@life" . nil)
        ("forum") ("talk") ("call") ("voice_chat")
        ("user") ("contact") ("person") ("person_pin") ("group")
        ("male") ("female") ("neuter") ("venus") ("venus_double") ("venus_mars")
        ("store") ("shopping") ("express")
        ("finance") ("money") ("payment")
        ("credit_card") ("visa") ("mastercard") ("PayPal") ("stripe") ("gratipay")
        ("digital_currency") ("bitcoin") ("BTC") ("ethereum") ("ETH")
        ("home") ("weekend") ("repair") ("dining") ("kitchen")
        ("drink") ("coffee") ("bar")
        ("watch") ("hearing") ("headphone")
        ("hospital") ("medical") ("health") ("see_doctor") ("doctor") ("medical") ("healing") ("health")
        ("library") ("bank") ("ATM") ("school")
        ("hotel") ("spa") ("laundry") ("pets") ("florist") ("play") ("city") ("industry")
        ("law") ("government") ("censorship") ("complaint")        
        ("bicycle") ("car") ("traffic") ("parking") ("bus") ("subway") ("railway") ("train") ("tram") ("flight") ("ship") ("run") ("walk") ("travel")
        ("child")
        ("recycle")
        ("daoism") ("tree") ("empire")
        ("global")
        (:endgroup)
        
        ;; Work
        (:startgroup) ("work" . ?w)
        ("appointment" . ?a) ("meeting" . ?m) ("urgent" . ?u)
        ("print") ("business_trip")
        (:endgroup)
        
        ;; SEX & Porn
        (:startgroup) ("SEX" . ?X)
        ("H")
        ("date") ("pickup") ("PUA") ("uncensored")
        ("creampie") ("blowjob") ("orgasm") ("masturbation") ("dildo") ("lick_pussy") ("clit") ("handjob") ("moan")
        ("beauty") ("pretty") ("young") ("handsome") ("beautiful_boy") ("teen") ("girl")
        ("ride_face") ("ride_dick") ("massage") ("shower") ("pillow") ("public") ("naked") ("night")
        ("SM") ("extreme_exhibition") ("car")
        ("anal") ("trans") ("gay") ("couple") ("amateur") ("selfie") ("stealthie") ("live_stream") ("real") ("deepthroat") ("squirt") ("cum")
        ("homemade") ("kitchen") ("pussy") ("cosplay") ("oral") ("female") ("back") ("Asian") ("hard")
        ("dog") ("slow_motion") ("erotic") ("oil") ("blonde")
        ("close_up") ("big_cock") ("footjob") ("threesome") ("OL") ("office") ("exchange_student")
        ("scene") ("BBC") ("black_big_cock") ("group") ("foursome")
        ("woman_on_top") ("cowgirl")
        (:endgroup)
        
        ;; Devices
        (:startgroup)
        ("hardware") ("computer") ("laptop") ("mobile") ("phone") ("server") ("NAS") ("Arduino") ("DIY")
        ("router") ("fax") ("scanner") ("TV") ("CMCC") ("robot") ("usb") ("disk") ("WiFi") ("Nvidia")
        (:endgroup)
        
        (:startgroup) ("wiki" . ?k)
        ("thought") ("philosophy") ("psychology") ("literature")
        ("computer_science") ("math")
        ("strategy") ("science") ("finance") ("stock") ("business") ("economy") ("history") ("politics") ("society") ("medicine")
        (:endgroup)
        
        (:startgroup) ("programming")
        ("code" . ?C) ("source_code") ("algorithm") ("bug") ("issue") ("feature") ("vulnerability") ("patch") ("diff")
        ("Emacs" . ?e) ("Org_mode" . ?o) ("regex") ("git" . ?G) ("vscode") ("design")
        ("OpenCV") ("OpenCL") ("OpenGL") ("Qt")
        ("open_source")
        ("Linux" . ?L) ("GNU") ("Apple") ("macOS" . ?M) ("Windows" . ?W) ("FreeBSD") ("Android") ("adb") ("iOS") ("Arch_Linux") ("alpine-linux") ("Ubuntu_Linux")
        ("terminal") ("command") ("ssh") ("compile") ("testing")
        ("LISP" . ?l) ("Common_Lisp") ("Emacs_Lisp") ("Clojure" . ?c) ("ClojureScript" . ?s) ("Leiningen") ("Scheme")
        ("Java" . ?J) ("C") ("cpp") ("CMake") ("Go") ("Rust") ("Swift") ("C#") ("Kotlin")
        ("Shell" . ?S) ("AppleScript") ("PowerShell")
        ("Python" . ?p) ("Ruby" . ?r) ("PHP")
        ("Web") ("HTML") ("HTML5") ("CSS") ("CSS3") ("JavaScript" . ?j) ("TypeScript") ("npm")
        ("R") ("Julia") ("Haskell") ("OCaml") ("Lua") ("Matlab") ("Octave")
        ("Firefox") ("Chromium") ("Chrome") ("Edge") ("Safari") ("UserScript")
        ("database" . ?d) ("SQL") ("PostgreSQL") ("MySQL") ("MariaDB") ("Oracle") ("SQLite") ("DB2") ("Redis") ("MongoDB")
        ("TeX") ("LaTeX") ("Markdown") ("reStructuredText") ("AsciiDoc") ("YAML") ("TOML") ("CWL")
        ;; DevOps
        ("cloud") ("DevOps") ("Docker") ("Vagrant")
        ;; frameworks
        ("Vue") ("React")
        ;; Internet
        ("Internet") ("Google") ("Facebook") ("Twitter") ("Microsoft") ("Amazon") ("Mozilla") ("Reddit")
        ("GitHub") ("GitLab")
        ("YouTube") ("PornHub") ("Instagram") ("Dribbble") ("Dropbox") ("WhatsApp")
        ("Alibaba") ("Baidu") ("Tencent") ("weixin") ("WeChat") ("weibo") ("ByteDance") ("Bilibili") ("Zhihu")
        (:endgroup)
        
        (:startgroup) ("@license")
        ("GPL_license") ("GNU_General_Public_License_v3") ("GNU_General_Public_License_v2")
        ("GNU_Affero_General_Public_license") ("GNU_Lesser_General_Public_license")
        ("MIT_License") ("Apache_License") ("BSD_License") ("Eclipse_Public_License") ("Mozilla_Public_License")
        ("Creative_Commons_License")
        (:endgroup)
        
        (:startgroup) ("@relationship")
        ("family") ("sister") ("father") ("mother") ("relative") ("girlfriend") ("workmate") ("friend") ("good_friend")
        (:endgroup)
        
        (:startgroup) ("project")
        ("agriculture")
        (:endgroup)
        
        (:startgroup) ("company")
        ("users") ("promotion") ("accounting") ("copyright") ("registered")
        (:endgroup)
        ))

(setq org-tag-faces
      '(("noexport" :foreground "DimGray" :weight bold :underline t :strike-through t)
        ("deprecated" :foreground "DimGray" :strike-through t)
        ("LOG" :foreground "DeepSkyBlue")
        ("private" :foreground "deep pink")
        ("book" :foreground "deep pink")
        ("fragment" :foreground "LightGray" :weight bold)
        ("computer" :foreground "green")
        ("@life" :foreground "black")
        ("work" :foreground "DeepSkyBlue")
        ("SEX" :foreground "deep pink" :weight bold)
        ("programming" :foreground "lawn green" :weight bold)
        ("Linux" :foreground "yellow" :weight bold)
        ("Mac" :foreground "#444444" :weight bold)
        ("Emacs" :foreground "dodger blue" :weight bold)
        ("Org_mode" :foreground "green yellow" :weight bold)
        ("Hacker" :foreground "OrangeRed" :weight bold)
        ("LISP" :foreground "deep pink" :weight bold)
        ("Common_Lisp" :foreground "sky blue" :weight bold)
        ("Clojure" :foreground "sky blue" :weight bold)
        ("ClojureScript" :foreground "sky blue" :weight bold)
        ("Python" :foreground "yellow" :weight bold)
        ("Ruby" :foreground "red" :weight bold)
        ("Shell" :foreground "sea green")
        ("Java" :foreground "royal blue" :weight bold)
        ("C" :foreground "SaddleBrown" :weight bold)
        ("Go" :foreground "gold" :weight bold)
        ("Rust" :foreground "WhiteSmoke" :weight bold)
        ("JavaScript" :foreground "yellow" :weight bold)
        ("TypeScript" :foreground "blue" :weight bold)))


;;; auto add tag "LOG" for [C-c C-z] `org-add-note' when adding log note.
(defun my/org-add-note--auto-add-tag (&optional origin-func &rest args)
  "Auto add tag 'LOG' when `org-add-note'."
  (apply origin-func args)
  (when (eq org-log-note-purpose 'note)
    (org-back-to-heading t)
    (require 'seq)
    (org-set-tags (seq-uniq (cons "LOG" (org-get-tags nil t))))))

(advice-add 'org-store-log-note :around #'my/org-add-note--auto-add-tag)


;; ;;; `org-archive-tag', `org-archived'
;; (defconst org-deprecated-tag "deprecated"
;;   "The tag that marks a subtree as archived.
;; An archived subtree does not open during visibility cycling, and does
;; not contribute to the agenda listings.")
;;
;; (defface org-deprecated '((t :inherit shadow))
;;   "Face for headline with the deprecated tag."
;;   :group 'org-faces)

;;; [ org-tag-beautify ] -- Beautify Org tags with lot of icons.

(use-package org-tag-beautify
  :ensure t
  :after org
  :custom (org-tag-beautify-data-dir "~/Code/Emacs/org-tag-beautify/data/")
  :commands (org-tag-beautify-mode)
  :init (org-tag-beautify-mode 1)
  :config
  ;; Disable `org-use-fast-tag-selection' when have lot of tags.
  (if (length> org-tag-alist 50)
      (setq org-use-fast-tag-selection nil)
    (setq org-use-fast-tag-selection 'auto)))

;;; [ org-tag-eldoc ] -- Display tag explanation in Eldoc when point on tag.

(use-package org-tag-eldoc
  ;; :vc (:url "git://repo.or.cz/org-tag-eldoc.git" :rev :newest)
  :vc t :load-path "~/Code/Emacs/org-tag-eldoc/"
  :demand t)


(provide 'init-org-tag)

;;; init-org-tag.el ends here
