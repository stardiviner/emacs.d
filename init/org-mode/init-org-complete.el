;;; init-org-complete.el --- init for Org Completion
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; hide *Org Help* buffer.
(add-to-list 'display-buffer-alist `(,(rx "*Org Help*") . ((display-buffer-no-window) . ((allow-no-window . t)))))

;;; add Org-mode's `capf' default to `pcomplete' for `company-mode'.

(defun my/org-mode-completion-setup ()
  "Org-mode completion setup."
  (interactive)
  (cond
   ((featurep 'corfu)
    (setq-local corfu-auto t)
    (setq-local corfu-auto-prefix 2)
    (setq-local corfu-auto-delay 0.2)
    (setq-local completion-at-point-functions
                (list #'cape-file
                      (if (featurep 'yasnippet-capf)
                          #'yasnippet-capf
                        (cape-company-to-capf 'company-yasnippet))
                      #'cape-keyword
                      #'cape-abbrev
                      #'cape-elisp-block
                      ;; (cape-capf-super #'cape-dabbrev #'cape-ispell)
                      (cape-company-to-capf 'company-spell)
                      #'cape-tex
                      #'cape-sgml
                      #'cape-emoji))
    (corfu-mode 1))
   ((featurep 'company)
    (setq-local company-minimum-prefix-length 3)
    (setq-local company-idle-delay 0.3)
    (setq-local company-tooltip-idle-delay 0)
    (make-local-variable 'company-backends)
    (setq-local company-backends '(company-capf
                                   company-yasnippet
                                   ;; company-files
                                   ;; company-emoji
                                   ;; company-ispell
                                   company-spell))
    (company-mode 1)))
  
  ;; Fix org-mode input high latency issue.
  ;; Disable `lsp-mode' completion. But this will disable Org mode default completion too.
  (add-hook 'completion-at-point-functions 'pcomplete-completions-at-point nil t)

  ;; (when (featurep 'org-contacts) ; NOTE: This completion function is very heavy performance!
  ;;   (add-hook 'completion-at-point-functions 'org-contacts-org-complete-function nil))
  )

(add-hook 'org-mode-hook #'my/org-mode-completion-setup)


(provide 'init-org-complete)

;;; init-org-complete.el ends here
