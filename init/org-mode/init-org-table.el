;;; init-org-table.el --- init for Org Table
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

(add-to-list 'display-buffer-alist '("\^\\*Org Table Edit Field\\*" . (display-buffer-below-selected)))

;;; display table header when invisible.
;;; Activate `org-table-electric-header-mode' by default?
;; (setq org-table-header-line-p t)

;;; [ ftable ] -- Fill (auto-layout) tables.
;;; filling a table, i.e., adjusting the layout of the table so it can fit in n columns.

(use-package ftable
  :ensure t
  :commands (ftable-fill ftable-reformat ftable-edit-cell))

;; [ org-plot ] -- Plotting Tables in Org-mode.

(use-package org-plot
  :defer t
  :commands (org-plot/gnuplot)
  :config
  ;; `org-plot/gnuplot' advice of auto insert org gnuplot result image file.
  (advice-add 'org-plot/gnuplot :around #'org-plot/gnuplot-insert-org-image-link)

  (defun org-plot/gnuplot-insert-org-image-link (origin-func &rest args)
    "Auto insert `org-plot/gnuplot' generated plot image file link."
    (save-excursion
      (org-plot/goto-nearest-table)
      ;; Set default options.
      (setf params nil)
      (dolist (pair org-plot/gnuplot-default-options)
        (unless (plist-member params (car pair))
          (setf params (plist-put params (car pair) (cdr pair)))))
      ;; Collect options.
      (while (and (equal 0 (forward-line -1))
                  (looking-at "[[:space:]]*#\\+"))
        (setf params (org-plot/collect-options params))))
    (setf param-file (plist-get params :file))
    (apply origin-func args)
    (goto-char (org-table-end))
    (org-indent-line) (insert (format "\n"))
    (org-indent-line) (insert (format "[[%s][%s]]" (expand-file-name param-file) param-file))
    (org-indent-line) (insert (format "\n"))
    (when org-startup-with-inline-images (org-redisplay-inline-images))))

;;; Org Table translator functions.
(add-to-list 'org-default-properties "ORGTBL") ; for Org-mode Table translator functions.

;; define a keybinding for org table translator functions
(define-key org-mode-map (kbd "C-c \" i") 'orgtbl-insert-radio-table)
(define-key org-mode-map (kbd "C-c \" s") 'orgtbl-send-table)

;;; [ orgtbl-ascii-plot ] -- ascii-art bar plots in org-mode tables.

;; #+TBLFM: $4='(orgtbl-ascii-draw plot min max)
;;
;; - #+TBLFM: $4='(orgtbl-ascii-draw $2 0 10)

(use-package orgtbl-ascii-plot
  :ensure t
  :defer t
  :commands (orgtbl-ascii-plot))

;;; [ orgtbl-join ] -- join two Org-mode tables.

(use-package orgtbl-join
  :ensure t
  :defer t
  :commands (orgtbl-join orgtbl-to-joined-table org-insert-dblock:join))

;;; import .xlsx, .csv file into Org.
(defun org-table-import-xlsx-to-csv-org ()
  (interactive)
  (let* ((source-file  (file-name-sans-extension (buffer-file-name (current-buffer))))
         (xlsx-file (concat source-file ".xlsx"))
         (csv-file (concat source-file ".csv")))
    (org-odt-convert xlsx-file "csv")
    (org-table-import csv-file  nil)))

(defun org-table-import-xlsx-file-to-csv-org (file)
  "Import .xlsx, .csv `FILE' into Org."
  (interactive "f")
  (let* ((source-file  (file-name-sans-extension (buffer-file-name (current-buffer))))
         (xlsx-file (concat source-file ".xlsx"))
         (csv-file (concat source-file ".csv")))
    (org-odt-convert file "csv")
    (org-table-import csv-file  nil)))

;;; [ org-transform-tree-table ] -- Transform an org-mode outline and its properties to a table format (org-table, CSV).

(use-package org-transform-tree-table
  :ensure t
  :defer t
  :commands (org-transform-tree-table/toggle))

;;; [ mysql-to-org-mode ] -- Minor mode for emacs to output the results of mysql queries to org tables.

(use-package mysql-to-org
  :ensure t
  :defer t
  :commands (mysql-to-org-eval mysql-to-org-eval-string-at-point mysql-to-org-mode))

;;; [ valign ] -- visually align tables.

(use-package valign
  :if (not (featurep 'org-modern))
  :ensure t
  :delight valign-mode
  :hook ((markdown-mode org-mode) . valign-mode))



(provide 'init-org-table)

;;; init-org-table.el ends here
