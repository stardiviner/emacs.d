;;; init-org-capture.el --- init for Org Capture-Refile-Archive
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ Capture - Refile - Archive ]

(require 'org-capture)

(setq org-default-notes-file (concat org-directory "/Tasks/Tasks.org"))

(setq org-capture-bookmark nil)

(add-hook 'org-capture-mode-hook
          (lambda ()
            ;; If non-nil, always offer completion for all tags of all agenda files.
            (setq-local org-complete-tags-always-offer-all-agenda-tags t)))

(setq org-capture-templates
      `(("t" ,(format "%s\tCapture Task"
                      (nerd-icons-faicon "nf-fae-checklist_o" :face 'nerd-icons-green))
         entry (file ,(expand-file-name "~/Org/Tasks/Tasks.org")) ; (file "") means by default `org-default-notes-file'
         "* TODO %^{task title}
:PROPERTIES:
:TIME: %U
:END:

%i
%a

%?"
         ;; :time-prompt t
         :empty-lines 1 :jump-to-captured t
         :refile-targets (("~/Org/Tasks/Tasks.org" :maxlevel 2)))
        
        ;; Clock task
        ("T" ,(format "%s\tCapture Clocking Task"
                      (nerd-icons-faicon "nf-fa-hourglass_start" :face 'nerd-icons-red-alt))
         entry (file ,(expand-file-name "~/Org/Tasks/Tasks.org"))
         "* TODO [#A] %?
SCHEDULED: %(org-insert-time-stamp (org-read-date nil t \"+0d\"))
%a"
         :clock-in t :clock-resume t :clock-keep t :empty-lines 1
         :refile-targets (("~/Org/Tasks/Tasks.org" :maxlevel 2)))
        
        ;; Diary
        ("d" ,(format "%s\tRecord -> Diary.org"
                      (nerd-icons-codicon "nf-cod-notebook" :face 'nerd-icons-cyan))
         entry (file+olp+datetree ,(expand-file-name "~/Org/Diary/Diary.org"))
         "* %^{Diary Title}
:PROPERTIES:
:TIME: %U
:END:

[[file:%<%Y-%m-%d-%H-%M>.org][%<%Y-%m-%d %R>]]

Event: %?

%i
"
         ;; :time-prompt t
         :empty-lines 1 :jump-to-captured t
         :refile-targets (("~/Org/Diary/Diary.org" :maxlevel 2)))

        ;; Journal
        ("j"  ,(format "%s\tJournals"
                       (nerd-icons-codicon "nf-cod-calendar" :face 'nerd-icons-blue)))
        ("jj" ,(format "%s\tRecord -> Journal.org"
                       (nerd-icons-mdicon "nf-md-calendar_check" :face 'nerd-icons-green))
         entry (file+olp+datetree ,(expand-file-name "~/Org/Journal/Journal.org"))
         "**** Event: %?
:PROPERTIES:
:TIME: %U
:END:
"
         :time-prompt t
         :tree-type month :jump-to-captured t
         :refile-targets (("~/Org/Journal/Journal.org" :maxlevel 2)))
        ("jf" ,(format "%s\tRecord -> Family Journal.org"
                       (nerd-icons-mdicon "nf-md-greenhouse" :face 'nerd-icons-green))
         entry (file+olp+datetree ,(expand-file-name "~/Org/Journal/Family Journal/Family Journal.org"))
         "**** Event: %?
:PROPERTIES:
:TIME: %U
:END:
"
         :time-prompt t
         :tree-type month :jump-to-captured t
         :refile-targets (("~/Org/Journal/Family Journal/Family Journal.org" :maxlevel 2)))
        ("jm" ,(format "%s\tRecord -> My Masturbation Records.org"
                       (nerd-icons-mdicon "nf-md-calendar_multiselect" :face 'nerd-icons-pink))
         entry (file+olp+datetree ,(expand-file-name "~/Org/Myself/My Masturbation Records/My Masturbation Records.org"))
         "**** Record: %^{describe this time masturbation short summary}
:PROPERTIES:
:TIME: %U
:END:

%?"
         :time-prompt t
         :empty-lines 1 :jump-to-captured t
         :clock-in t :clock-keep t
         :refile-targets (("~/Org/Myself/My Masturbation Records/My Masturbation Records.org" :maxlevel 2)))
        
        ;; Bookmark
        ;;         ("b" ,(format "%s\tAdd bookmark -> Bookmarks.org database"
        ;;                       (nerd-icons-mdicon "nf-md-bookmark_plus_outline" :face 'nerd-icons-green))
        ;;          entry (file ,(expand-file-name "~/Org/Bookmarks/Bookmarks.org"))
        ;;          "* %^{bookmark description}  :bookmark:%^g
        ;; :PROPERTIES:
        ;; :URL: %c
        ;; :DATE: %t
        ;; :END:
        ;;
        ;; %?"
        ;;          :empty-lines 1 :jump-to-captured t
        ;;          :refile-targets (("~/Org/Bookmarks/Bookmarks.org" :maxlevel 4)))
        
        ;; org-passwords accounts.org.gpg
        ("a" ,(format "%s\tRecord account record -> accounts.org.gpg"
                      (nerd-icons-mdicon "nf-md-card_account_details_outline" :face 'nerd-icons-silver))
         entry (file ,(expand-file-name "~/Org/Accounts/accounts.org.gpg"))
         "* %^{Title}
%^{URL}p
%^{USERNAME}p
%^{PASSWORD}p
%^{EMAIL}p"
         :empty-lines-before 1 :empty-lines-after 1
         :refile-targets (("~/Org/Accounts/accounts.org.gpg" :maxlevel 2)))

        ;; Code Collections.org
        ("c" ,(format "%s\tRecord code -> Code Collections.org"
                      (nerd-icons-faicon "nf-fa-github" :face 'nerd-icons-dblue))
         entry (file ,(expand-file-name "~/Org/Codes Collection/Codes Collection.org"))
         "* %^{code repository name} %^g
:PROPERTIES:
:DATE: %U
:END:

%c
%?"
         :empty-lines 1 :jump-to-captured t
         :refile-targets (("~/Org/Codes Collection/Codes Collection.org" :maxlevel 1)))
        
        ;; Add Changelog into current file.
        ("L" ,(format "%s\tRecord Changelog -> current file"
                      (nerd-icons-mdicon "nf-md-text_box_plus" :face 'nerd-icons-orange))
         entry (file+headline (lambda () (buffer-file-name)) "Change Log")
         "* %^{Header of Changelog item}
:PROPERTIES:
:LOGGED: %U
:LINK: %a
:AUTHOR: stardiviner <numbchild@gmail.com>
:END:

%?"
         :empty-lines 1 :jump-to-captured t)

        ;; My Poems Collection.org
        ("p" ,(format "%s\tRecord -> My Poems Collection.org"
                      (nerd-icons-faicon "nf-fae-book_open_o" :face 'nerd-icons-blue))
         entry (file ,(expand-file-name "~/Org/Wiki/Countries/China/Chinese Literature/Chinese Poem Literature/manuals/My Poems Collection/My Poems Collection.org"))
         "* %^{title}
:PROPERTIES:
:DATE: %U
:END:

%?"
         :jump-to-captured t
         :refile-targets (("~/Org/Wiki/Countries/China/Chinese Literature/Chinese Poem Literature/manuals/My Poems Collection/My Poems Collection.org" :maxlevel 2)))

        ;; Beauty.org
        ("g"  ,(format "%s\tRecord -> Beauty.org"
                       (nerd-icons-faicon "nf-fa-file_picture_o" :face 'nerd-icons-pink)))
        ("gg" ,(format "%s\tRecord -> Beauty.org (name)"
                       (nerd-icons-faicon "nf-fa-file_picture_o" :face 'nerd-icons-pink))
         entry (file ,(expand-file-name "~/Org/Beauty/Beauty.org"))
         "* %^{name} %^g
:PROPERTIES:
:DIR:    %\\1
:DATE:   %U
:GENDER: %^{GENDER|Female|Male}
:URL:    %^{URL}p
:END:

%?"
         :empty-lines 1 :jump-to-captured t
         :immediate-finish t
         :refile-targets (("~/Org/Beauty/Beauty.org" :maxlevel 1)))
        ("ga" ,(format "%s\tRecord -> Beauty.org (anonymous)"
                       (nerd-icons-faicon "nf-fa-file_picture_o" :face 'nerd-icons-blue-alt))
         entry (file ,(expand-file-name "~/Org/Beauty/Beauty.org"))
         "* %<%Y-%m-%d>  %^g
:PROPERTIES:
:DIR:    images/%<%Y-%m-%d>/
:DATE:   %U
:GENDER: %^{GENDER|Female|Male}
:SCORE:  %^{SCORE}p
:END:

%?"
         :time-prompt t
         :empty-lines 1 :jump-to-captured t
         :immediate-finish t
         :refile-targets (("~/Org/Beauty/Beauty.org" :maxlevel 1)))
        ))

(add-to-list 'display-buffer-alist '("*Org Select*" . ((display-buffer-in-side-window) (window-height . 0.4) (side . bottom))))

;;; Dynamic org-capture-templates interactively entry file target selection.
(defvar my/org-capture-select-target-files-list
  (list
   ;; Porn
   "~/Org/Wiki/Pornography/Porn/Porn Stars/Porn Stars Reference/Porn Stars Reference.org"
   "~/Org/Wiki/Pornography/Porn/Porn Videos/Porn Videos.org"
   "~/Org/Wiki/Pornography/Porn/Porn Audiobooks/Porn Audiobooks.org"
   "~/Org/Wiki/Pornography/Porn/Porn Audios/Porn Audios.org"
   "~/Org/Wiki/Pornography/Porn/Porn Images/Porn Images.org"
   "~/Org/Wiki/Pornography/Porn/Sexy Images/Sexy Images.org"
   "~/Org/Wiki/Pornography/Porn/Porn GIFs/Porn GIFs.org"
   "~/Org/Wiki/Pornography/Porn/Porn Novels/Porn Novels.org"
   ;; Erotic
   "~/Org/Wiki/Pornography/Eroticism/Erotic Film/Erotic Films/Erotic Films.org"
   "~/Org/Wiki/Pornography/Eroticism/Erotic Images/Erotic Images.org"
   "~/Org/Wiki/Pornography/Eroticism/Erotic Videos/Erotic Videos.org"
   "~/Org/Wiki/Pornography/Eroticism/Erotic Comics/Erotic Comics.org"
   "~/Org/Wiki/Pornography/Eroticism/Erotic Magazines/Erotic Magazines.org"
   "~/Org/Wiki/Art/Photograph/Boudoir Photography/Boudoir Photography Collections/Boudoir Photography Collections.org"
   ;; Hentai
   "~/Org/Wiki/Pornography/Hentai/Hentai Images/Hentai Images.org"
   "~/Org/Wiki/Pornography/Hentai/Hentai Image Authors/Hentai Image Authors.org"
   "~/Org/Wiki/Pornography/Hentai/Hentai GIFs/Hentai GIFs.org"
   "~/Org/Wiki/Pornography/Hentai/Hentai Anime/Hentai Anime Videos/Hentai Anime Videos.org"
   "~/Org/Wiki/Pornography/Hentai/Hentai Comic/Hentai Comic Books/Hentai Comic Books.org"
   "~/Org/Wiki/Pornography/Hentai/Hentai 3D ACG/Hentai 3D ACG Videos/Hentai 3D ACG Videos.org"
   ;; Lesbian
   "~/Org/Wiki/LGBTQ/Homosexuality/Lesbian/Lesbian Porn/Lesbian Porn Images/Lesbian Porn Images.org"
   "~/Org/Wiki/LGBTQ/Homosexuality/Lesbian/Lesbian Porn/Lesbian Porn GIFs/Lesbian Porn GIFs.org"
   "~/Org/Wiki/LGBTQ/Homosexuality/Lesbian/Lesbian Porn/Lesbian Porn Videos/Lesbian Porn Videos.org")
  "A list of files for `org-capture' refile targets.")

(defvar my/org-capture-selected-target-file nil
  "A variable to store the `my/org-capture-select-target' selected file.")

(defun my/org-capture-select-target ()
  "Select `org-capture' target file."
  (let* ((file (expand-file-name (completing-read "Select target file: " my/org-capture-select-target-files-list)))
         ;; FIXME:
         ;; (location (or file
         ;;               (with-current-buffer (find-file-noselect file)
         ;;                 (call-interactively 'org-goto))
         ;;               (read-string "Input headline: ")))
         )
    (setq my/org-capture-selected-target-file file)
    (find-file file)
    (goto-char (point-max))))

(defun my/org-capture-set-org-attach-dir ()
  "Set `org-attach' :DIR: property value based on `org-capture' template target FILE path."
  (require 'f)
  (file-name-as-directory
   (completing-read
    "Set org-attach :DIR: property: "
    (mapcar 'file-name-base
            (f-directories
             (file-name-directory my/org-capture-selected-target-file) nil nil)))))

(add-to-list 'org-capture-templates
             ;; Porn & Hentai
             `("h" ,(format "%s\tRecord -> Porn/Hentai/Erotic collections"
                            (nerd-icons-mdicon "nf-md-folder_heart_outline" :face 'nerd-icons-dpink)
                            ;; (nerd-icons-faicon "nf-fa-face_grin_hearts" :face 'nerd-icons-pink)
                            ;; (nerd-icons-mdicon "nf-md-head_heart_outline" :face 'nerd-icons-dpink)
                            ;; (nerd-icons-mdicon "nf-md-tag_heart" :face 'nerd-icons-dpink)
                            )
               entry (function my/org-capture-select-target)
               "* %^{headline}  %^g
:PROPERTIES:
:DIR:  %(my/org-capture-set-org-attach-dir)
:TIME: %U
:END:

%?"
               :time-prompt t
               :empty-lines 1 :jump-to-captured t
               :immediate-finish t
               ;; :refile-targets ((,my/org-capture-select-target-files-list (:maxlevel . 1)))
               ) 'append)

;;; Context org-capture templates.
;; TODO:
;; (setq org-capture-templates-contexts
;;       '(("p" (in-mode . "message-mode"))))


;;;_* Refile

;; Refile targets include this file and any file contributing to the
;; agenda - up to 5 levels deep
(setq org-refile-targets '((nil . (:maxlevel . 3)) ; current buffer headlines
                           (org-agenda-files . (:maxlevel . 2)) ; agenda files headlines
                           (org-buffer-list . (:maxlevel . 2))) ; all opened Org buffer files headlines
      org-refile-use-outline-path t
      org-outline-path-complete-in-steps nil
      ;; org-refile-target-verify-function nil
      org-refile-allow-creating-parent-nodes 'confirm
      org-refile-active-region-within-subtree t
      org-refile-use-cache nil ; cache refile targets to speed up the process.
      )

;;;_* Archive

(add-to-list 'org-tag-alist '("ARCHIVE" . ?A))

;;; Auto save Org buffers after capture/refile/archive.
;; (add-hook 'org-capture-after-finalize-hook 'org-save-all-org-buffers)
;; (advice-add 'org-capture-refile :after 'org-save-all-org-buffers)
;; (advice-add 'org-refile         :after 'org-save-all-org-buffers)
;; (advice-add 'org-archive        :after 'org-save-all-org-buffers)

;;; Auto close *.org_archive files when exit Emacs.
(when (bound-and-true-p desktop-save-mode)
  (defun my/org-mode-close-archive-files ()
    "Auto close *.org_archive files when exit Emacs."
    (mapc 'kill-buffer
          (mapcar 'get-buffer
                  (seq-filter (lambda (buffer-name) (string-match-p ".*\\.org_archive$" buffer-name))
                              (mapcar 'buffer-name (buffer-list))))))
  (add-hook 'kill-emacs-hook #'my/org-mode-close-archive-files))


(provide 'init-org-capture)

;;; init-org-capture.el ends here
