;;; init-org-project-management.el --- Project Management with Org Mode.

;;; Commentary:



;;; Code:

;;; [ org-gantt ] -- Generate Gantt chart for Org Mode headlines.

(use-package org-gantt
  :vc t :load-path "~/Code/Emacs/org-gantt"
  :defer t
  :commands (org-insert-dblock:org-gantt-chart))

;;; [ elgantt ] -- A Gantt Chart (Calendar) for Org Mode.

(use-package elgantt
  :vc (:url "https://github.com/legalnonsense/elgantt")
  :defer t
  :commands (elgantt-open)
  :custom (elgantt-agenda-files (concat org-directory "Projects/Gantt/Gantt.org")))



(provide 'init-org-project-management)

;;; init-org-project-management.el ends here
