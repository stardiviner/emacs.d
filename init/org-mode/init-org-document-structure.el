;;; init-org-document-structure.el --- init for Org Document Structure
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ org-element ] -- Parser for Org Syntax

;;; [M-: (org-fold-core--clear-isearch-overlays)]

;; (setq org-element-use-cache nil) ; Non-nil when Org element parser should cache its results.
;; (setq org-element-cache-persistent nil)
;; (setq org-element--cache-self-verify nil) ; nil: to speed up org-mode buffer saving.
;; (setq org-element--cache-self-verify 'backtrace) ; 'backtrace: reproduce backtrace.
;; (setq org-element--cache-self-verify-frequency 0.03)
;; (setq org-element--cache-diagnostics nil)

(defun my/org-fold-isearch-overlays-reset ()
  "Reset and clear org-fold isearch overlays to fix folded headline failed to expand."
  (interactive)
  (org-fold-core--clear-isearch-overlays))

(require 'warnings)
(add-to-list 'warning-suppress-types 'org-element)
(add-to-list 'warning-suppress-types 'org-element-cache)
(add-to-list 'warning-suppress-types 'org-element-parser)
(add-to-list 'warning-suppress-log-types 'org-element-cache)

;;; [ org-persist ] -- Org Element Cache Persistent

;; (setq org-element-cache-persistent nil)
;;; Enable `counsel-org-clock-history' without need `org-agenda' initialization.
(setq org-clock-persist t)
(org-clock-persistence-insinuate)
;; (add-hook 'org-persist-before-write-hook (lambda (&rest _args) t) nil t) ; file local variables.

;;; [ org-cycle ] -- Visibility cycling of Org entries

;; (remove-hook 'org-cycle-hook #'org-cycle-optimize-window-after-visibility-change) ; PERFORMANCE:

;;; [ org-fold ] -- Folding of Org entries

;;; Org buffer startup displayed with folded overview state.
(setq org-startup-folded t)

;;; hide keywords in Org document buffer.
;; (setq org-hidden-keywords '(author date email subtitle title))

;;; Electric
(defun my/org-mode-electric-pair-setting ()
  (make-local-variable 'electric-pair-pairs)
  (setq-local electric-pair-pairs
              `((?\" . ?\")
                (,(nth 0 electric-quote-chars) . ,(nth 1 electric-quote-chars))
                (,(nth 2 electric-quote-chars) . ,(nth 3 electric-quote-chars))))
  ;; (add-to-list 'electric-pair-pairs '(?\* . ?\*)) ; bold text ; NOTE disable for change headline level.
  ;; (add-to-list 'electric-pair-pairs '(?\/ . ?\/)) ; italic text
  ;; (add-to-list 'electric-pair-pairs '(?\_ . ?\_)) ; underline text
  (add-to-list 'electric-pair-pairs '(?\= . ?\=)) ; verbatim
  (add-to-list 'electric-pair-pairs '(?\~ . ?\~)) ; code
  (electric-pair-local-mode 1))

(add-hook 'org-mode-hook #'my/org-mode-electric-pair-setting)

;; Inserting by replace ` with ‘, ' with ’, and replace double `` with ”, '' with ”.
;; (add-hook 'org-mode-hook #'electric-quote-local-mode)

(setq org-special-ctrl-a/e t)
(setq org-fontify-whole-heading-line t)
(setq org-fontify-done-headline t)
(setq org-hide-emphasis-markers t)

;;; List

;;; it makes sense to have list bullets change with depth.
(setq org-list-demote-modify-bullet '(("+" . "-") ("-" . "+") ("*" . "+")))

;; * Column View::

;; (setq org-columns-default-format
;;       "%25ITEM %TODO %3PRIORITY %TAGS %6effort(EFFORT){:}")

;; setup column views for effort estimates
(setq org-columns-default-format
      "%50ITEM(Task) %8TODO %1PRIORITY %14TIMESTAMP_IA(TimeStamp) %Effort(Effort){:} %CLOCKSUM"
      ;; Default column view headings
      ;; - %ITEM(Task) :: Headline (where (Task) is the column head)
      ;; - %PRIORITY :: Priority
      ;; - %TAGS :: tags
      ;; - %ITEMSTAMP_IA :: Timestamp
      ;; - %Effort(Effort){:} :: Effort
      ;; - %CLOCKSUM :: Clock Sum
      )

;; * Footnotes::                   How footnotes are defined in Org's syntax

(setq org-footnote-auto-label 'confirm
      org-footnote-auto-adjust t
      org-footnote-define-inline nil ; t: define foot inline, instead of separate section.
      org-footnote-section nil ; nil: let footnotes can be under any headline section.
      org-footnote-fill-after-inline-note-extraction t)

;;; [ Structure Templates ] --  ; expand snippet <[s]

;; (require 'org-tempo)
;; (add-to-list 'org-structure-template-alist '("?" . "..."))
;; (add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
;; `tempo-define-template'

;;; [ Entities ]

(defun org-insert-entity-with-ivy ()
  "Insert an org-entity using Ivy."
  (interactive)
  (ivy-read "Entity: "
            (cl-loop for element in (append org-entities org-entities-user)
			         when (not (stringp element))
			         collect
			         (cons
			          (format "%10s | %s | %s | %s"
				              (car element) ;name
				              (nth 1 element) ; latex
				              (nth 3 element) ; html
				              (nth 6 element)) ;utf-8
			          element))
	        :require-match t
            ;; Ivy [M-o]
	        :action '(1
		              ("u" (lambda (element) (insert (nth 6 (cdr element)))) "utf-8")
		              ("o" (lambda (element) (insert "\\" (cadr element))) "org-entity")
		              ("l" (lambda (element) (insert (nth 1 (cdr element)))) "latex")
		              ("h" (lambda (element) (insert (nth 3 (cdr element)))) "html"))))

;;; [ Table Of Contents (TOC) ]

;;; [ org-make-toc ] -- Automatic tables of contents for Org files.

;; (use-package org-make-toc
;;   :ensure t
;;   :defer t
;;   :commands (org-make-toc))

;;; [ orgtbl-aggregate ] -- create an aggregated Org table from another one.

(use-package orgtbl-aggregate
  :ensure t
  :commands (org-insert-dblock org-insert-dblock:aggregate)
  :config
  ;; add `orgtbl-aggregate' dynamic blocks into list.
  (org-dynamic-block-define "columnview" 'org-insert-dblock:columnview)
  (org-dynamic-block-define "aggregate" 'org-insert-dblock:aggregate)
  (org-dynamic-block-define "invoice" 'org-insert-dblock:invoice)
  (org-dynamic-block-define "join" 'org-insert-dblock:join)
  (org-dynamic-block-define "org-gantt" 'org-insert-dblock:org-gantt)
  (org-dynamic-block-define "propview" 'org-insert-dblock:propview)
  (org-dynamic-block-define "transpose" 'org-insert-dblock:transpose))

;;; [ org-eldoc ] -- display org header and src block info using eldoc.

(require 'org-eldoc)
(org-eldoc-load)

;;; [ org-lint ] -- Org-mode linter.

(use-package org-lint
  :commands (org-lint)
  :init (add-to-list 'display-buffer-alist '("^\\*Org Lint\\*" . (display-buffer-below-selected))))

;; auto re-format elfeed entry org-capture buffer.
(defun my/org-mode-auto-format ()
  "A helper command to auto format org-mode buffer."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (replace-string "　" " ")
    ;; using regex replace using \( \)\{2,\}  which means 2 or more consecutive spaces and replace that with 1 space.
    ;; (replace-regexp "\\( \\)\\{2,\\}" " ")
    ))

;; (add-hook 'org-mode-hook
;;           (lambda () (add-hook 'before-save-hook #'my/org-mode-auto-format nil t)))

;;; Add `org-lint' into `flycheck-checkers' list for `org-mode'.
;;; https://github.com/flycheck/flycheck/issues/1757

(use-package flycheck
  :ensure t
  :config
  (defconst flycheck-org-lint-form
    (flycheck-prepare-emacs-lisp-form
      (require 'org)
      (require 'org-attach)
      (let ((source (car command-line-args-left))
            (process-default-directory default-directory))
        (with-temp-buffer
          (insert-file-contents source 'visit)
          (setq buffer-file-name source)
          (setq default-directory process-default-directory)
          (delay-mode-hooks (org-mode))
          (setq delayed-mode-hooks nil)
          (dolist (err (org-lint))
            (let ((inf (cl-second err)))
              (princ (elt inf 0))
              (princ ": ")
              (princ (elt inf 2))
              (terpri)))))))

  (defconst flycheck-org-lint-variables
    '(org-directory
      org-id-locations
      org-id-locations-file
      org-attach-id-dir
      org-attach-use-inheritance
      org-attach-id-to-path-function-list)
    "Variables inherited by the org-lint subprocess.")

  (defun flycheck-org-lint-variables-form ()
    (require 'org-attach)  ; Needed to make variables available
    `(progn
       ,@(seq-map (lambda (opt) `(setq-default ,opt ',(symbol-value opt)))
                  (seq-filter #'boundp flycheck-org-lint-variables))))

  (flycheck-define-checker org-lint
    "Org buffer checker using `org-lint'."
    :command ("emacs" (eval flycheck-emacs-args)
              "--eval" (eval (concat "(add-to-list 'load-path \""
                                     (file-name-directory (locate-library "org"))
                                     "\")"))
              "--eval" (eval (flycheck-sexp-to-string
                              (flycheck-org-lint-variables-form)))
              "--eval" (eval flycheck-org-lint-form)
              "--" source)
    :error-patterns
    ((error line-start line ": " (message) line-end))
    :modes (org-mode))

  (add-to-list 'flycheck-checkers 'org-lint)

  ;; (unless global-flycheck-mode
  ;;   (add-hook 'org-mode-hook #'flycheck-mode))
  ;; (add-hook 'org-mode-hook (lambda () (flycheck-select-checker 'org-lint)))
  )


(provide 'init-org-document-structure)

;;; init-org-document-structure.el ends here
