;;; init-org-babel.el --- init for Org Babel
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

(setq org-confirm-babel-evaluate nil)
(setq org-babel-hash-show-time t) ; header argument: :cache yes.
(setq org-src-tab-acts-natively nil)

;;; add org-babel header-args property into default properties list.
(add-to-list 'org-default-properties "header-args")
(add-to-list 'org-default-properties "header-args:sh")
(add-to-list 'org-default-properties "header-args:shell")
(add-to-list 'org-default-properties "header-args:python")
(add-to-list 'org-default-properties "header-args:clojure")

;;; source block default header arguments
(setq-default org-babel-default-header-args
              '((:session . "none")
                (:noweb . "no") (:hlines . "no")
                (:tangle . "no")        ; (:comments . "link")
                (:cache . "yes")
                (:results . "replace")
                (:mkdirp . "yes")       ; auto create specified path directory
                ;; for exporting
                (:eval . "never-export")
                ;; (:exports . "both") conflict with (:eval "never-export")
                ))

;;; don't evaluate babel when exporting. For helper functions like
;;; `my:org-convert-region-to-html' etc.
(setq org-export-use-babel nil)


;; babel src block editing
(setq org-src-window-setup 'split-window-right)

;;; use `plain' to make popup source block buffer controlled by `display-buffer'.
;; (setq org-src-window-setup 'plain)
;; (add-to-list 'display-buffer-alist
;;              '("^\\*Org Src.*\\*"
;;                (display-buffer-reuse-window display-buffer-in-side-window)
;;                (reusable-frames . visible)
;;                (side . right)
;;                (slot . 1)
;;                (window-sides-vertical . t)
;;                (window-width . 0.5)
;;                (window-height . 0.15)))

;; 0: fix `diff' babel syntax highlighting invalid issue.
(setq org-edit-src-content-indentation 0)

;;; Manage org-babel popup buffers with `display-buffer-alist'.
(add-to-list 'display-buffer-alist '("^\\*Org- Src.*\\*" . (display-buffer-below-selected)))
(add-to-list 'display-buffer-alist '("^\\*Org-Babel Results\\*" . (display-buffer-below-selected)))
(add-to-list 'display-buffer-alist '("^\\*Org-Babel Preview.*\\*" . (display-buffer-below-selected)))
(add-to-list 'display-buffer-alist '("^\\*Org-Babel Error Output\\*" . (display-buffer-below-selected)))

;;; Alist mapping languages to their major mode.

(add-to-list 'org-src-lang-modes '("text" . fundamental))
(add-to-list 'org-src-lang-modes '("unix" . conf-unix))
(add-to-list 'org-src-lang-modes '("widnows" . conf-windows))
(add-to-list 'org-src-lang-modes '("javaprop" . conf-javaprop))
(add-to-list 'org-src-lang-modes '("xdefaults" . conf-xdefaults))
(add-to-list 'org-src-lang-modes '("toml" . conf-toml))
(add-to-list 'org-src-lang-modes '("ppd" . conf-ppd))

;;; [ inline source block: src_{lang}[switchs,flags]{body} ]

;; (setq org-babel-exp-inline-code-template "src_%lang[%switches%flags]{%body}")
(setq org-babel-default-inline-header-args
      '((:session . "none")
        (:results . "replace")
        ;; (:exports . "both")
        (:hlines . "yes")
        (:eval . "never-export")))

;;; [ noweb ]
;; Raise errors when noweb references don't resolve.
(setq org-babel-noweb-error-all-langs t)

;;; default loading babel language.
(setq org-babel-load-languages '((org . t)))

;;; [ ob-shell ]

(use-package ob-shell
  :config
  (org-babel-shell-initialize)
  (add-to-list 'org-babel-load-languages '(shell . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  (add-to-list 'org-babel-tangle-lang-exts '("sh" . "sh"))
  (add-to-list 'org-babel-default-header-args:sh '(:results . "output"))
  (add-to-list 'org-babel-default-header-args:sh '(:noweb . "yes"))
  (add-to-list 'org-babel-default-header-args:shell '(:results . "output"))
  (add-to-list 'org-babel-default-header-args:shell '(:noweb . "yes"))
  ;; add ":async" header argument as safe for `org-lint'.
  (add-to-list 'org-babel-header-arg-names 'async)
  ;; NOTE: org-babel does not font-lock for zsh language.
  (add-to-list 'org-src-lang-modes '("zsh" . sh))
  (add-to-list 'org-src-lang-modes '("fish" . sh)))

;;; [ Tangle ]

(add-to-list 'org-babel-tangle-lang-exts '("latex" . "tex"))
(add-to-list 'org-babel-tangle-lang-exts '("awk" . "awk"))

;;; [ org-tempo ]

(require 'org-tempo)

;; (add-to-list 'org-tempo-keywords-alist '("s" . "src"))

(tempo-define-template "source block sh" ; <sh
                       '("#+begin_src sh" n
                         p n
                         "#+end_src")
                       "<sh" 'org-tempo-tags)
(tempo-define-template "source block shell" ; <shell
                       '("#+begin_src shell" n
                         p n
                         "#+end_src")
                       "<shell" 'org-tempo-tags)
(tempo-define-template "source block sh no-eval" ; <shn
                       '("#+begin_src sh :eval no" n
                         p n
                         "#+end_src")
                       "<shn" 'org-tempo-tags)
(tempo-define-template "source block sh session async" ; <sha
                       '("#+begin_src sh :session \"*ob-shell-async*\" :async yes" n
                         p n
                         "#+end_src")
                       "<sha" 'org-tempo-tags)

(tempo-define-template "source block el" ; <el
                       '("#+begin_src emacs-lisp" n
                         p n
                         "#+end_src")
                       "<el" 'org-tempo-tags)
(tempo-define-template "source block elisp" ; <elisp
                       '("#+begin_src emacs-lisp" n
                         p n
                         "#+end_src")
                       "<elisp" 'org-tempo-tags)

(tempo-define-template "source block lisp" ; <lisp
                       '("#+begin_src lisp" n
                         p n
                         "#+end_src")
                       "<lisp" 'org-tempo-tags)

(tempo-define-template "source block scheme" ; <scheme
                       '("#+begin_src scheme" n
                         p n
                         "#+end_src")
                       "<scheme" 'org-tempo-tags)

(tempo-define-template "source block clj" ; <clj
                       '("#+begin_src clojure" n
                         p n
                         "#+end_src")
                       "<clj" 'org-tempo-tags)
(tempo-define-template "source block clojure" ; <clojure
                       '("#+begin_src clojure" n
                         p n
                         "#+end_src")
                       "<clojure" 'org-tempo-tags)
(tempo-define-template "source block clojurescript" ; <cljs
                       '("#+begin_src clojurescript" n
                         p n
                         "#+end_src")
                       "<cljs" 'org-tempo-tags)

(tempo-define-template "source block py" ; <py
                       '("#+begin_src python" n
                         p n
                         "#+end_src")
                       "<py" 'org-tempo-tags)
(tempo-define-template "source block python" ; <python
                       '("#+begin_src python" n
                         p n
                         "#+end_src")
                       "<python" 'org-tempo-tags)

(tempo-define-template "source block ruby" ; <ruby
                       '("#+begin_src ruby" n
                         p n
                         "#+end_src")
                       "<ruby" 'org-tempo-tags)

(tempo-define-template "source block java" ; <java
                       '("#+begin_src java" n
                         p n
                         "#+end_src")
                       "<java" 'org-tempo-tags)

(tempo-define-template "source block C" ; <C
                       '("#+begin_src C" n
                         p n
                         "#+end_src")
                       "<C" 'org-tempo-tags)
(tempo-define-template "source block cpp" ; <cpp
                       '("#+begin_src C++" n
                         p n
                         "#+end_src")
                       "<cpp" 'org-tempo-tags)
(tempo-define-template "source block go" ; <go
                       '("#+begin_src go" n
                         p n
                         "#+end_src")
                       "<go" 'org-tempo-tags)
(tempo-define-template "source block rust" ; <rust
                       '("#+begin_src rust" n
                         p n
                         "#+end_src")
                       "<rust" 'org-tempo-tags)
(tempo-define-template "source block swift" ; <swift
                       '("#+begin_src swift" n
                         p n
                         "#+end_src")
                       "<swift" 'org-tempo-tags)

;;======================== set different background colors for babel languages ========================

;;; darker org source block background
;; (autoload 'color-darken-name "color")
;; (set-face-attribute 'org-block nil
;;                     :background (color-darken-name (face-attribute 'default :background) 3))

;; (defun my/org-mode-source-block-faces-reset (&optional theme)
;;   "Reset `org-src-block-faces' difference language source blocks on custom theme switching."
;;   (cl-case (frame-parameter nil 'background-mode)
;;     (light
;;      (setq org-src-block-faces
;;            `(("emacs-lisp" (:background "slateblue" :extend t))
;;              ("shell" (:background "darkgreen" :extend t))
;;              ("sh" (:background "darkgreen" :extend t))
;;              ("bash" (:background "darkgreen" :extend t))
;;              ("zsh" (:background "darkgreen" :extend t))
;;              ("python" (:background "gold" :extend t))
;;              ("lisp" (:background "lightseagreen" :extend t))
;;              ("scheme" (:background "palegreen" :extend t))
;;              ("clojure" (:background "lightpink" :extend t))
;;              ("clojurescript" (:background "hotpink" :extend t))
;;              ("javascript" (:background "darkorange" :extend t))
;;              ("html" (:background "tan" :extend t))
;;              ("css" (:background "aqua" :extend t))
;;              ("java" (:background "khaki" :extend t))
;;              ("C" (:background "cadetblue" :extend t))
;;              ("C++" (:background "cornflowerblue" :extend t))
;;              ("rust" (:background "lightblue" :extend t))
;;              ("go" (:background "deepskyblue" :extend t))
;;              ("applescript" (:background "gainsboro" :extend t))
;;              ("ruby" (:background "orangered" :extend t))
;;              ("R" (:background "cadetblue" :extend t))
;;              ("julia" (:background "palevioletred" :extend t))
;;              ("lua" (:background "royalblue" :extend t))
;;              ("sql" (:background "bisque" :extend t))
;;              ("redis" (:background "deeppink" :extend t)))))
;;     (dark
;;      (setq org-src-block-faces
;;            `(("emacs-lisp" (:background "slateblue" :extend t))
;;              ("shell" (:background "darkgreen" :extend t))
;;              ("sh" (:background "darkgreen" :extend t))
;;              ("bash" (:background "darkgreen" :extend t))
;;              ("zsh" (:background "darkgreen" :extend t))
;;              ("python" (:background "gold" :extend t))
;;              ("lisp" (:background "lightseagreen" :extend t))
;;              ("scheme" (:background "palegreen" :extend t))
;;              ("clojure" (:background "lightpink" :extend t))
;;              ("clojurescript" (:background "hotpink" :extend t))
;;              ("javascript" (:background "darkorange" :extend t))
;;              ("html" (:background "tan" :extend t))
;;              ("css" (:background "aqua" :extend t))
;;              ("java" (:background "khaki" :extend t))
;;              ("C" (:background "cadetblue" :extend t))
;;              ("C++" (:background "cornflowerblue" :extend t))
;;              ("rust" (:background "lightblue" :extend t))
;;              ("go" (:background "deepskyblue" :extend t))
;;              ("applescript" (:background "gainsboro" :extend t))
;;              ("ruby" (:background "orangered" :extend t))
;;              ("R" (:background "cadetblue" :extend t))
;;              ("julia" (:background "palevioletred" :extend t))
;;              ("lua" (:background "royalblue" :extend t))
;;              ("sql" (:background "bisque" :extend t))
;;              ("redis" (:background "deeppink" :extend t)))))))

;; (add-hook 'load-theme-after-hook #'my/org-mode-source-block-faces-reset)

;;; [ org-tanglesync ] -- sync external changes back into an org-mode source block.


;;; [ Library of Babel ]

;;; automatically ingest "Library of Babel".
(with-eval-after-load 'org
  (org-babel-lob-ingest
   (concat user-emacs-directory "Org-mode/Library of Babel/Library of Babel.org")))

;;; [ Literate dotfiles management with Org-mode ]

;; Tangle Org files when we save them
;; (defun tangle-on-save-org-mode-file()
;;   (when (string= (message "%s" major-mode) "org-mode")
;;     (org-babel-tangle)))
;; (add-hook 'after-save-hook 'tangle-on-save-org-mode-file)

;;; [ coderef ]
;;; prepend comment char ahead of `org-coderef-label'.
;; auto prefix with comment char when create code ref in src block with `org-store-link'.
(defun org-src-coderef-format-prepend-comment (result)
  "Auto prefix with comment char before `org-coderef-label' `RESULT'."
  ;; notice `org-src-coderef-format' is invoked twice. got two different `comment-start' "#" and ";".
  ;; only prepend comment character before coderef when in opened source block buffer.
  (if (and (or (org-src-edit-buffer-p) org-src-mode)
           ;; add buffer-local variable state to avoid duplicated comment prepend.
           (not (bound-and-true-p org-coderef-label-format-commented-p)))
      (progn
        (setq-local org-coderef-label-format-commented-p t)
        (setq-local org-coderef-label-format
                    (concat (string-trim comment-start) " " org-coderef-label-format)))
    org-coderef-label-format))

(advice-add 'org-src-coderef-format :filter-return 'org-src-coderef-format-prepend-comment)

;;; ding around source block execution.
(add-hook 'org-src-mode-hook #'sound-tick)
(add-hook 'org-babel-after-execute-hook #'sound-voice-complete)
(add-hook 'org-babel-post-tangle-hook #'sound-voice-complete)
(add-hook 'org-babel-pre-tangle-hook #'sound-tick)

;;; Tangling with append to file instead of default overwrite.
(defun my/org-babel-tangle-append ()
  "Append source code block at point to its tangle file.
The command works like `org-babel-tangle' with prefix arg
but `delete-file' is ignored."
  (interactive)
  (cl-letf (((symbol-function 'delete-file) #'ignore))
    (org-babel-tangle '(4))))
(define-key org-babel-map (kbd "M-t") 'my/org-babel-tangle-append)

(add-hook 'org-babel-after-execute-hook 'org-toggle-inline-images)

;;; NOTE: This advice caused long suspend on babel source block suspend.
;;; auto re-display inline images when source block generate graphics image file link result.
;; (defun my/org-redisplay-inline-images (&optional arg info params)
;;   (org-redisplay-inline-images))
;; (advice-add 'org-babel-execute-src-block :after #'my/org-redisplay-inline-images)

;;; interactively insert noweb of completing named src blocks. [C-c C-v M-q]
(defun my/org-babel-insert-noweb-of-named-src-block (&optional template)
  (interactive)
  (let ((src-block (completing-read "Enter src block name[or TAB or ENTER]: "
                                    (org-babel-src-block-names))))
    (unless (string-equal "" src-block)
	  (insert (format "<<%s>>" src-block)))))

(define-key org-babel-map (kbd "M-q") 'my/org-babel-insert-noweb-of-named-src-block)

(defun my/org-babel-insert-call-of-named-src-block (&optional template)
  "Interactively insert a named src block call with `TEMPLATE'."
  (interactive)
  (let ((template (or template "#+call: %s()\n"))
	    (src-block (completing-read "Enter src block name[or TAB or ENTER]: "
                                    (org-babel-src-block-names))))
    (unless (string-equal "" src-block)
	  (insert (format template src-block)))))

(define-key org-babel-map (kbd "M-c") 'my/org-babel-insert-call-of-named-src-block)

;;; [ helm-lib-babel ] -- inserting a #+CALL: reference to an source block name.

;;; interactively insert #+CALL of completing named src blocks. [C-c C-v M-i]
(use-package helm-lib-babel
  :if (featurep 'helm)
  :ensure t
  :defer t
  :init (defalias 'my/org-babel-insert-helm-lib 'helm-lib-babel-insert)
  :bind (:map org-babel-map ("M-i" . my/org-babel-insert-helm-lib)))

(use-package context-transient
  :ensure t
  :config
  (context-transient-define org-babel-transient
    :doc "Org mode Babel transient"
    :context (org-in-src-block-p)
    :menu
    [["Header Arguments"
      ("j" "insert header argument" org-babel-insert-header-arg)
      ("hc" "check misspelled header arguments" org-babel-check-src-block)
      ;; :session header argument
      ("ss" "switch to session" org-babel-switch-to-session)
      ("sl" "load src block into session" org-babel-load-in-session)
      ("sc" "add :session for Clojure"
       (lambda ()
         (interactive)
         (org-babel-insert-header-arg
          "session"
          (completing-read "Select Clojure CIDER session: "
                           (cider-sessions) nil nil cider-session-name))))
      ("sj" "add :session for JavaScript" org-babel-insert-header-arg:js)]
     ["Navigation"
      ("z" "switch to session with code" org-babel-switch-to-session-with-code)
      ("u" "goto beginning of src block" org-babel-goto-src-block-head)
      ;; #+results
      ("rg" "go to #+RESULTS:" org-babel-goto-named-result)
      ("ro" "open #+RESULTS:" org-babel-open-src-block-result)
      ("rk" "remove #+RESULTS:" org-babel-remove-result-one-or-many)
      ;; named source blocks
      ("g" "goto named src block" org-babel-goto-named-src-block)
      ("n" "next src block" org-babel-next-src-block)
      ("p" "previous src block" org-babel-previous-src-block)]
     ["Misc"
      ("d" "demarcate src block" org-babel-demarcate-block)
      ("v" "expand src block" org-babel-expand-src-block)
      ("M-h" "mark src block" org-babel-mark-block)
      ("M-j" "ob-jupyter hydra" jupyter-org-hydra/body)]
     ["Execute"
      ("e" "execute src block" org-babel-execute-maybe)
      ("S" "execute src blocks under subtree" org-babel-execute-subtree)
      ("x" "do key sequence in edit buffer" org-babel-do-key-sequence-in-edit-buffer)]
     ["noweb reference"
      ("w" "insert noweb" my/org-babel-insert-noweb-of-named-src-block)]
     ["#+CALL"
      ("cc" "insert call" my/org-babel-insert-call-of-named-src-block)
      ("ch" "helm-lib-babel" helm-lib-babel-insert)]
     ["tangle"
      ("t" "Tangle" org-babel-tangle)
      ("T" "tangle to file" org-babel-tangle-file)
      ("a" "Append to tangle file." my/org-babel-tangle-append)]
     ["Library of Babel"
      ("li" "ingest Library of Babel" org-babel-lob-ingest)]]))

;;; [ org-babel-eval-in-repl ] -- eval org-babel block code with eval-in-repl.el

;; (use-package org-babel-eval-in-repl
;;   :ensure t
;;   :defer t
;;   :bind (:map org-mode-map
;;               ("C-<return>" . ober-eval-in-repl)
;;               ("C-c C-<return>" . ober-eval-block-in-repl))
;;   :config (with-eval-after-load "eval-in-repl"
;;             (setq eir-jump-after-eval nil)))

;;; [ org-radiobutton ] -- Get the checked item from a check list to be used for
;;; Org-mode Literate Programming variable.

;; (use-package org-radiobutton
;;   :ensure t
;;   :defer t
;;   :init (global-org-radiobutton-mode))

;; load all languages AT LAST.
(org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)

;;; Make Org Babel source block popup buffer read file local variables.

(require 'files-x)
(require 'files)
(add-hook 'org-src-mode-hook #'hack-local-variables)



(provide 'init-org-babel)

;;; init-org-babel.el ends here
