;;; init-org-attach.el --- init for Org attach
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ org-attach ] -- Manage file attachments to org-mode tasks.

;; store link auto with `org-store-link' using `file:' link type or `attachment:' link type.
;; use `file:' to avoid `attachment:' problems especially on exporters.

(use-package org-attach
  :demand t
  :custom ((org-attach-method 'mv)
           (org-attach-store-link-p 'file) ; default is `'attached'
           (org-attach-dir-relative t)  ; use relative path to attach dir
           (org-attach-auto-tag nil)    ; don't auto add tag "ATTACH"
           (org-attach-archive-delete 'query)
           (org-attach-preferred-new-method 'ask))
  :init (add-to-list 'display-buffer-alist '("^\\*Org Attach\\*" . (display-buffer-below-selected)))
  :config (add-hook 'org-attach-after-change-hook #'sound-tick)
  ;;======================== Insert link of files under the attach directory. =======================
  (defun org-attach-insert-file-links ()
    "Insert link of files under the attach directory."
    (interactive)
    ;; WARN: `org-attach' move the point around caused insert link in headline instead of original point.
    ;; Temporary solution: use `with-temp-buffer' + `kill-new' + `yank'.
    (let ((attach-dir (org-attach-dir)))
      (if attach-dir
          (let* ((files (directory-files attach-dir))
                 (selected-files (mapcar #'substring-no-properties
                                         (let ((crm-separator "[ \t]*;[ \t]*"))
                                           (completing-read-multiple "Select files (seprator \";\"): " files))))
                 (selected-files-path (mapcar (lambda (file)
                                                (file-relative-name
                                                 (expand-file-name file attach-dir)
                                                 default-directory))
                                              selected-files))
                 (files-number (length files)))
            (with-temp-buffer
              (mapcar
               (lambda (path)
                 (push (list (concat "file:" path) (file-name-nondirectory path)) org-stored-links)
                 ;; reference `org-insert-last-stored-link'
                 (org-insert-all-links files-number "" "\n"))
               selected-files-path)
              (kill-new (buffer-substring-no-properties (point-min) (point-max))))
            (message "Press [Ctrl-y] `yank' to paste links."))
        (message "Org attach directory :DIR: not set!"))))

  (add-to-list 'org-attach-commands
               '((?L) org-attach-insert-file-links
                 "Insert link of files under the attach directory."))
  )

;;; Add an advice after `org-attach' -> `org-attach-attach' command to record the new attached file into headline logbook.
(defun my/org-attach-attach-advice (file &optional visit-dir method)
  "An advice function on function `org-attach-attach'."
  ;; Auto record attachment file to log.
  ;; pass the attachment file into to Emacs kill-ring.
  (kill-new (format "Attach file '%s'." (file-name-nondirectory (substring-no-properties file))))
  ;; then paste from kill-ring.
  ;; `org-log-buffer-setup-hook'
  (add-hook 'org-log-buffer-setup-hook #'yank) ; `org-add-note' "*Org Note*"
  ;; call command `org-add-note'.
  (call-interactively 'org-add-note)
  (remove-hook 'org-log-buffer-setup-hook #'yank))

;; (advice-add 'org-attach-attach :after #'my/org-attach-attach-advice)

;;; [ org-screenshot ] -- Take and manage screenshots in Org-mode files.

;; (use-package org-screenshot
;;   :load-path "~/Code/Emacs/org-mode/contrib/lisp/"
;;   :pin manual
;;   :defer t
;;   :commands (org-screenshot-take)
;;   :init (setq org-screenshot-image-directory "data/images")
;;   (add-hook 'org-mode-hook
;;             #'(lambda () (local-set-key (kbd "C-c o M-s") 'org-screenshot-take))))

;;; [ org-attach-screenshot ] -- screenshots integrated with org attachment dirs.

;; (use-package org-attach-screenshot
;;   :ensure t
;;   :commands (org-attach-screenshot)
;;   :bind (:map Org-prefix ("M-s" . org-attach-screenshot))
;;   :init
;;   (setq org-attach-screenshot-command-line "import %f") ; "scrot -c -d 5 -s %f"
;;   :config (setq org-attach-screenshot-relative-links t))

;;; [ org-attach-embedded-images ] --

;; This module provides command `org-attach-embedded-images-in-subtree' to save such
;; images as attachments and insert org links to them. Each image is named with its sha1
;; sum.
;;
;; (require 'org-attach-embedded-images)

;;; [ org-download ] -- drag and drop online images to org-mode.

(use-package org-download
  :ensure t
  :preface
  (unless (boundp 'org-download-prefix)
    (define-prefix-command 'org-download-prefix))
  (define-key Org-prefix (kbd "d") 'org-download-prefix)
  :commands (org-download-image
             org-download-screenshot
             org-download-yank org-download-clipboard
             org-download-delete org-download-edit)
  :bind (:map org-download-prefix
              ("i" . org-download-image)
              ("s" . org-download-screenshot)
              ("c" . org-download-clipboard)
              ("y" . org-download-yank)
              ("d" . org-download-delete)
              ("e" . org-download-edit))
  :custom (;; `org-download-screenshot-method'
           (org-download-method 'attach) ; 'attach, 'directory,
           ;; if you don't want the #+DOWNLOADED: annotation in your Org document
           (org-download-annotate-function (lambda (_) ""))
           (org-download-backend t) ; url-retrieve (t), wget, curl.
           (org-download-image-dir "data/images"))
  ;; :hook ((org-mode dired-mode) . org-download-enable)
  )

;;; [ org-web-tools ] -- retrieving web page content and processing it into Org-mode content.

(use-package org-web-tools
  :ensure t
  :defer t
  :commands (org-web-tools-insert-link-for-url ; insert Org link
             org-web-tools-insert-web-page-as-entry ; insert web page as Org entry
             org-web-tools-read-url-as-org ; display web page as Org in new buffer
             org-web-tools-convert-links-to-page-entries ; convert all links in current Org entry to Org headings
             ))

;;; [ org-board ] -- Org mode's web archiver for single web page or website.

(use-package org-board
  :ensure t
  :defer t
  :preface
  (unless (boundp 'org-board-prefix)
    (define-prefix-command 'org-board-prefix))
  (define-key Org-prefix (kbd "C-a") 'org-board-prefix)
  :commands (org-board-new
             org-board-archive org-board-archive-dry-run org-board-cancel
             org-board-open)
  :custom ((org-board-default-browser 'eww) ; press [&] in eww to open in external browser
           (org-board-make-relative t)) ; org-attach use relative path
  :bind (:map org-board-prefix
              ("n" . org-board-new)
              ("a" . org-board-archive)
              ("o" . org-board-open)
              ("r" . org-board-archive-dry-run)
              ("k" . org-board-cancel)
              ("d" . org-board-delete-all)
              ("f" . org-board-diff)
              ("3" . org-board-diff3))
  :init (add-to-list 'display-buffer-alist '("org-board-wget-call" . (display-buffer-below-selected))))


(provide 'init-org-attach)

;;; init-org-attach.el ends here
