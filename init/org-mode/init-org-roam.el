;;; init-org-roam.el --- init for org-roam -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Christopher M. Miles, all rights reserved.

;;; Commentary:



;;; Code:

;;; [ org-roam ] -- Rudimentary Roam replica with Org Mode.

(use-package org-roam
  :ensure t
  :defer t
  :custom ((org-roam-directory (expand-file-name "~/Org/org-roam"))
           (org-roam-mute-cache-build t))
  :preface
  (unless (boundp 'org-roam-prefix)
    (define-prefix-command 'org-roam-prefix))
  (global-set-key (kbd "C-c o m") 'org-roam-prefix)
  :bind (:map org-roam-mode-map
              (("C-c n l" . org-roam)
               ("C-c n f" . org-roam-find-file)
               ("C-c n g" . org-roam-show-graph))
              :map org-mode-map
              (("C-c n i" . org-roam-insert))))

;; [ org-roam-ui ] -- User Interface for Org-roam.

(use-package org-roam-ui
  :ensure t
  :commands (org-roam-ui-mode))

;;; [ org-roam-timestamps ] -- Keep track of modification times for org-roam.

(use-package org-roam-timestamps
  :ensure t
  :delight org-roam-timestamps-mode
  :hook (org-roam-mode . org-roam-timestamps-mode))

;; [ org-roam-bibtex ] -- Connector between org-roam, BibTeX-completion, and org-ref.

(use-package org-roam-bibtex
  :ensure t
  :delight org-roam-bibtex-mode
  :hook (org-roam-mode . org-roam-bibtex-mode)
  :bind (:map org-mode-map (("C-c n a" . orb-note-actions))))

;; [ consult-org-roam ] -- Consult integration for org-roam.

(use-package consult-org-roam
  :ensure t
  :delight consult-org-roam-mode
  :hook (org-roam-mode . consult-org-roam-mode))



(provide 'init-org-roam)

;;; init-org-roam.el ends here
