;;; init-package-manager.el --- init for System Package Manager & Configuration Tools -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Christopher M. Miles, all rights reserved.

;;; Commentary:



;;; Code:

;;; [ Functional Package Manager & Configuration Tools ]

(when (executable-find "nix")
  (load "init-Nix"))

(when (executable-find "guix")
  (load "init-Guix"))


;;; [ helm-system-packages ] -- Helm UI wrapper for system package managers.

(use-package helm-system-packages
  :if (featurep 'helm)
  :ensure t
  :defer t
  :commands (helm-system-packages))



(provide 'init-package-manager)

;;; init-package-manager.el ends here
