;;; init-Nix.el --- init for Nix. -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Christopher M. Miles, all rights reserved.

;;; Commentary:



;;; Code:

;;; [ nix-mode ] -- Major mode for editing .nix files.

(use-package nix-mode
  :ensure t
  :defer t)

;;; [ nix-modeline ] -- Info about in-progress Nix evaluations on your modeline.

;;; [ nix-env-install ] -- Install packages using nix-env.

;;; [ nix-buffer ] -- Set up buffer environments with nix.

;;; [ nix-sandbox ] -- Utility functions to work with nix-shell sandboxes.

;;; [ nixpkgs-fmt ] -- Reformat Nix using nixpkgs-fmt.



(provide 'init-Nix)

;;; init-Nix.el ends here
