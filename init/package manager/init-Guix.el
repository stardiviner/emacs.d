;;; init-Guix.el --- init for GNU Guix. -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Christopher M. Miles, all rights reserved.

;;; Commentary:



;;; Code:

;;; [ guix ] -- Interface to GNU Guix.

(use-package guix
  :ensure t
  :defer t)



(provide 'init-Guix)

;;; init-Guix.el ends here
