;;; init-markup-Markdown.el --- init Emacs for Markdown
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ markdown-mode ]

(use-package markdown-mode
  :ensure t
  :defer t
  :custom ((markdown-hide-urls t)
           (markdown-marginalize-headers t)
           (markdown-asymmetric-header t)
           (markdown-enable-wiki-links t)
           (markdown-wiki-link-search-subdirectories t)
           (markdown-wiki-link-search-parent-directories t)
           (markdown-wiki-link-fontify-missing t)
           (markdown-fontify-whole-heading-line t)
           (markdown-header-scaling t)
           (markdown-enable-highlighting-syntax t)
           (markdown-gfm-additional-languages t)
           (markdown-fontify-code-blocks-natively t)
           (markdown-code-block-braces t)
           (markdown-enable-math t)
           (markdown-special-ctrl-a/e t)
           (markdown-hide-markup nil)))

;;; [ flymd ] -- On the fly markdown preview.

(use-package flymd
  :ensure t
  :commands (flymd-flyit))

;;; [ grip-mode ] -- Instant Github-flavored Markdown/Org preview using grip (Github README Instant Preview).

;; (use-package grip-mode
;;   :ensure t
;;   :bind (:map markdown-mode-command-map ("g" . grip-mode)))

;;; [ maple-preview ] -- Markdown, Org Mode or HTML realtime preview on Emacs.

;; (use-package maple-preview
;;   :vc (:url "https://github.com/honmaple/emacs-maple-preview")
;;   :commands (maple-preview-mode)
;;   :init (setq maple-preview:port 8083))


(provide 'init-markup-Markdown)

;;; init-markup-Markdown.el ends here
