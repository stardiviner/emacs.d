;;; init-markup.el --- init for Files Handling
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ pandoc ] -- pandoc interface.

(use-package pandoc
  :ensure t
  :defer t
  :custom (pandoc-turn-on-advice-eww t)
  :commands (pandoc-open-eww))

;;; [ pandoc-mode ] -- pandoc-mode is an Emacs mode for interacting with Pandoc.

(use-package pandoc-mode ; [C-c /] prefix
  :ensure t
  :hook (pandoc-mode . pandoc-load-default-settings)
  (markdown-mode . pandoc-mode)
  :commands (pandoc-mode conditionally-turn-on-pandoc))


(provide 'init-markup)

;;; init-markup.el ends here
