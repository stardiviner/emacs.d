;;; init-vagrant.el --- init for Vagrant
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ vagrant ] -- Manage a vagrant box from Emacs.

(use-package vagrant
  :ensure t
  :defer t
  :custom ((vagrant-project-directory "~/Code/Vagrant/Arch")
           (vagrant-up-options ""))
  :commands (vagrant-up
             vagrant-ssh
             vagrant-status vagrant-resume vagrant-reload vagrant-halt
             vagrant-destroy vagrant-provision vagrant-edit))

;;; [ vagrant-tramp ] -- Vagrant method for TRAMP.

(use-package vagrant-tramp
  :ensure t
  :defer t
  :commands (vagrant-tramp-shell vagrant-tramp-term))

;;; [ magrant ] -- A Vagrant porcelain in Emacs.

(use-package magrant
  :ensure t
  :defer t
  :commands (magrant)
  :init (add-to-list 'display-buffer-alist '("^\\*vagrant-boxes\\*" . (display-buffer-below-selected)))
  (add-to-list 'display-buffer-alist '("^\\*vagrant-machines\\*" . (display-buffer-below-selected))))

;;; For Org Babel source block. use Ruby syntax highlight for `Vagrantfile'.
(add-to-list 'org-src-lang-modes '("vagrant" . ruby))


(provide 'init-vagrant)

;;; init-vagrant.el ends here
