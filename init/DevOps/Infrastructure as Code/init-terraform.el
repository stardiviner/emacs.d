;;; init-terraform.el --- init for Terraform

;;; Commentary:



;;; Code:

;;;  [ terraform-mode ] -- Major mode for terraform configuration file

(use-package terraform-mode
  :ensure t)

;;; [ company-terraform ] -- A company backend for terraform

(when (not (featurep 'corfu))
  (use-package company-terraform
    :ensure t
    :config
    (defun my/terraform-company-setup ()
      (my-company-add-backend-locally 'company-terraform))
    (add-hook 'terraform-mode-hook #'my/terraform-company-setup)))

;;; [ lsp-mode ] -- Language Server Protocol for Terraform

(use-package lsp-mode
  :ensure t
  :after terraform-mode
  :config
  (add-to-list 'lsp-language-id-configuration '(terraform-mode . "terraform"))

  (lsp-register-client
   (make-lsp-client :new-connection (lsp-stdio-connection `(,(executable-find "terraform-lsp") "-enable-log-file"))
                    :major-modes '(terraform-mode)
                    :server-id 'terraform-ls))

  (add-hook 'terraform-mode-hook #'lsp))



(provide 'init-terraform)

;;; init-terraform.el ends here
