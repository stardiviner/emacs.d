;;; init-DevOps.el --- init for DevOps
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ ssh-deploy ] -- deployment via Tramp, global or per directory.

;; (use-package ssh-deploy
;;   :ensure t
;;   :hook ((after-save . ssh-deploy-after-save)
;;          (find-file . ssh-deploy-find-file))
;;   :config (ssh-deploy-hydra "C-c C-z")
;;   (ssh-deploy-line-mode 1))

;;; [ Virtualization ]

(load "init-container")
(load "init-vagrant")

;;; [ Infrastructure as Code ]

(load "init-puppet")
(load "init-ansible")
(load "init-terraform")

;;; [ Deployment ]

;; (use-package copy-file-on-save
;;   :ensure t
;;   :defer t
;;   :init (global-copy-file-on-save-mode))

;;; [ AWS ]

;; (load "init-AWS")

;;; [ Heroku ]

;; (use-package heroku
;;   :ensure t
;;   :defer t
;;   :commands (heroku-run))


(provide 'init-DevOps)

;;; init-DevOps.el ends here
