;;; init-Singularity.el --- init file for Singularity -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Christopher M. Miles, all rights reserved.

;;; Commentary:



;;; Code:

;;; [ singularity-mode ] -- A Singularity recipe mode for Emacs.

(require 'singularity-mode)



(provide 'init-Singularity)

;;; init-Singularity.el ends here
