;;; init-kubernetes.el --- init file for Kubernetes -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Christopher M. Miles, all rights reserved.

;;; Commentary:



;;; Code:

;;; [ k8s-mode ] -- Kubernetes mode for Kubernetes config files in Emacs.

(use-package k8s-mode
  :ensure t
  :defer t
  :mode (".*/\\.kube/config\\'" . k8s-mode)
  :hook (k8s-mode . yas-minor-mode))

;;; [ kubernetes ] -- Emacs porcelain for Kubernetes. A magit-style interface to the Kubernetes command-line client.

(use-package kubernetes
  :ensure t
  :defer t
  :commands (kubernetes-overview kubernetes-display-pods kubernetes-display-configmaps)
  :init (add-to-list 'display-buffer-alist '("^\\*kubernetes overview\\*" . (display-buffer-below-selected))))

;;; [ kubel ] -- extension for controlling Kubernetes with limited permissions.

(use-package kubel
  :ensure t
  :commands (kubel)
  :init (add-to-list 'display-buffer-alist '("^\\*kubel.*\\*" . (display-buffer-below-selected))))

;;; [ kele ] -- 🥤 (Kubernetes Enablement Layer for Emacs)

;; (use-package kele
;;   :ensure t
;;   :config (kele-mode 1))



(provide 'init-kubernetes)

;;; init-kubernetes.el ends here
