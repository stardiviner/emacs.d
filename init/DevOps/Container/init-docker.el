;;; init-docker.el --- init for Docker
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ docker-api ] -- Emacs interface to the Docker API.

(use-package docker-api
  :ensure t
  :defer t)

;;; [ docker ] -- Emacs interface to Docker.

(use-package docker
  :ensure t
  :defer t
  :commands (docker
             docker-containers docker-images docker-volumes docker-networks
             docker-container-eshell docker-container-shell
             docker-container-dired docker-container-find-file)
  :bind (:map container-prefix
              ("c" . docker-containers)
              ("i" . docker-images)
              ("v" . docker-volumes)
              ("n" . docker-networks)
              ("C-e" . docker-container-eshell)
              ("C-s" . docker-container-shell)
              ("C-d" . docker-container-dired)
              ("C-f" . docker-container-find-file))
  :custom (docker-containers-show-all t)
  :init 
  (add-to-list 'display-buffer-alist '("\\*docker-images\\*" . (display-buffer-below-selected)))
  (add-to-list 'display-buffer-alist '("\\*docker-containers\\*" . (display-buffer-below-selected)))
  (add-to-list 'display-buffer-alist '("\\*docker-machines\\*" . (display-buffer-below-selected)))
  (add-to-list 'display-buffer-alist '("\\*docker-volumes\\*" . (display-buffer-below-selected)))
  (add-to-list 'display-buffer-alist '("\\*docker-networks\\*" . (display-buffer-below-selected)))
  :config
  (defun docker-container-insert-name ()
    "A helper function to insert container ID or name."
    (interactive)
    (insert (funcall-interactively 'docker-container-read-name)))
  (define-key container-prefix (kbd "M-i") #'docker-container-insert-name)
  (require 'ob-keys)
  (define-key org-babel-map (kbd "C-M-d") 'docker-container-insert-name))

;;; [ dockerfile-mode ] -- Major mode for editing `Dockerfile'.

(use-package dockerfile-mode
  :ensure t
  :defer t
  :hook (dockerfile-mode . lsp)
  :config
  (with-eval-after-load 'company-keywords
    (add-to-list 'company-keywords-alist
                 '(dockerfile-mode "FROM"
                                   "ADD" "COPY"
                                   "RUN" "CMD" "ENTRYPOINT"
                                   "VOLUME" "ENV" "EXPOSE"  "LABEL" "ARG"
                                   "STOPSIGNAL" "USER"  "WORKDIR"
                                   "ONBUILD" "HEALTHCHECK" "SHELL"))))

;;; [ docker-compose-mode ] -- Major mode for editing `docker-compose.yml'.

(use-package docker-compose-mode
  :ensure t
  :defer t
  :mode ("docker-compose[^/]*\\.yml\\'" . docker-compose-mode))


(provide 'init-docker)

;;; init-docker.el ends here
