;;; init-container.el --- init for Container
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:


(unless (boundp 'container-prefix)
  (define-prefix-command 'container-prefix))
(define-key prog-tools-prefix (kbd "c") 'container-prefix)

(load "init-docker")
(load "init-kubernetes")
;; (load "init-LXC")
(load "init-Singularity")

;;; [ systemd-nspawn ] -- Spawn a command or OS in a light-weight container. Adds support for systemd-nspawn containers with Emacs' TRAMP system.

(use-package tramp-nspawn
  :if (executable-find "systemd-nspawn")
  :ensure t
  :hook (after-init . tramp-nspawn-setup))


(provide 'init-container)

;;; init-container.el ends here
