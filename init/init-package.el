;;; init-package.el --- init package.el
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ package.el ] -- Emacs Lisp Package Archive (ELPA)

(require 'package)      ; makes sure the ~/.confog/emacs/elpa/ directory was added to the load path.
(setq package-enable-at-startup nil)
(setq package-menu-async t)
(setq package-user-dir (expand-file-name "elpa" user-emacs-directory))

(setq package-install-upgrade-built-in t)

;;; ELPA Archives
;; (setq-default package-archives
;;               '(("gnu" . "https://elpa.gnu.org/packages/") ; https://elpa.gnu.org/
;;                 ("gnu-devel" . "https://elpa.gnu.org/devel/")
;;                 ;; ("nongnu" . "https://elpa.nongnu.org/nongnu/") ; https://elpa.nongnu.org/
;; 		        ("melpa" . "http://melpa.org/packages/")
;; 		        ;; ("melpa-stable" . "http://stable.melpa.org/packages/")
;; 		        ;; ("marmalade" . "http://marmalade-repo.org/packages/")
;; 		        ))

;;; 清华镜像源
;;; https://mirrors.tuna.tsinghua.edu.cn/elpa/
(setq-default package-archives
              '(("gnu"   . "http://mirrors.tuna.tsinghua.edu.cn/elpa/gnu/")
                ;; ("gnu-devel"   . "http://mirrors.tuna.tsinghua.edu.cn/elpa/gnu-devel/")
                ;; ("nongnu"   . "http://mirrors.tuna.tsinghua.edu.cn/elpa/nongnu/")
                ;; ("nongnu-devel"   . "http://mirrors.tuna.tsinghua.edu.cn/elpa/nongnu-devel/")
                ("melpa" . "http://mirrors.tuna.tsinghua.edu.cn/elpa/melpa/")
                ;; ("stable-melpa" . "http://mirrors.tuna.tsinghua.edu.cn/elpa/stable-melpa/")
                ))

;;; NonGNU ELPA
(add-to-list 'package-archives '("nongnu" . "https://elpa.nongnu.org/nongnu/"))

;;; Use priority if you don't want this archive overridden with other larger archives:
(setq package-archive-priorities
      '(("gnu"          . 5)
        ("gnu-devel"    . 6)
        ("nongnu"       . 7)
        ("nongnu-devel" . 8)
        ("stable-melpa" . 9)
        ("melpa"        . 10)
        ("jcs-elpa"     . 0)))

(package-initialize)

(let* ((elpa-archives-dir (expand-file-name "elpa/archives/" user-emacs-directory))
       (elpa-gnu-archives-dir (concat elpa-archives-dir "gnu"))
       (elpa-melpa-archives-dir (concat elpa-archives-dir "melpa"))
       (elpa-org-archives-dir (concat elpa-archives-dir "org")))
  (unless (and (file-exists-p elpa-gnu-archives-dir)
               (file-exists-p elpa-melpa-archives-dir)
               (file-exists-p elpa-org-archives-dir)
	           package-archive-contents)
    (package-refresh-contents)))

(add-to-list 'display-buffer-alist '("^\\*package-build-result\\*" . (display-buffer-below-selected)))

(setq package-install-upgrade-built-in t) ; Non-nil means that built-in packages like `eglot' can be upgraded via a package archive.

;;; [ package-vc ] -- Manage packages from VC checkouts.

(require 'package-vc)

;;; Load `use-package' ahead before `package-initialize' for (use-package org :pin manual ...).
;;; [ use-package ] -- A configuration macro for simplifying your .emacs.

;;; load `use-package' from Emacs built-in.
(eval-when-compile (require 'use-package))
(require 'use-package)
(use-package use-package
  :ensure t
  ;; don't use other ELPA Archive installation override Emacs built-in.
  :pin manual)
(use-package bind-key :ensure t :pin manual) ; `:bind' keyword support from Emacs built-in.
(use-package delight :ensure t :pin manual) ; `:delight' keyword support requires package "delight" installed.

(setq use-package-always-ensure nil)

(when init-file-debug ; from emacs command-line option `--debug-init' from `startup.el'.
  (setq use-package-verbose t ; 'debug: any evaluation errors report to `*use-package*` buffer.
        use-package-expand-minimally nil
        use-package-compute-statistics t
        debug-on-error t))

;;; [ Org mode ]

(if (file-exists-p "~/Code/Emacs/org-mode/")
    (progn
      ;; disable Emacs built-in Org Mode
      (cl-case system-type
        (gnu/linux
         (delete (format "/usr/local/share/emacs/%s/lisp/org" emacs-version) load-path)
         (delete "/usr/share/emacs/site-lisp/org/" load-path))
        (darwin
         (delete "/Applications/Emacs.app/Contents/Resources/lisp/org/" load-path)
         ;; /opt/homebrew/Cellar/emacs-plus@30/30.0.50/share/emacs/30.0.50/lisp/org/org.el
         (delete (format "/opt/homebrew/opt/emacs-plus@%s/share/emacs/%s/lisp/org/" emacs-major-version emacs-version) load-path)
         (delete (format "/opt/homebrew/Cellar/emacs-plus@%s/%s/share/emacs/%s/lisp/org/" emacs-major-version emacs-version emacs-version) load-path)))
      (use-package org
        :load-path "~/Code/Emacs/org-mode/lisp/"
        :pin manual
        :demand t
        :mode (("\\.org\\'" . org-mode))
        ;; :preface
        ;; ;; emphasis markup 记号前后允许中文
        ;; (setq org-emphasis-regexp-components
        ;;       (list (concat " \t('\"{"            "[:nonascii:]")
        ;;             (concat "- \t.,:!?;'\")}\\["  "[:nonascii:]")
        ;;             " \t\r\n,\"'"
        ;;             "."
        ;;             1))
        ;; disable all extra org-mode modules to speed-up Org-mode file opening.
        :custom (org-modules nil)
        ;; load org before org-mode init files settings.
        :init (require 'org)
        ;; add source code version Org-mode Info into Emacs.
        (if (file-exists-p "~/Code/Emacs/org-mode/doc/org")
            (with-eval-after-load 'info
              (add-to-list 'Info-directory-list "~/Code/Emacs/org-mode/doc/")
              (info-initialize)))
        (use-package org-contrib
          ;; :pin manual
          :load-path "~/Code/Emacs/org-contrib/lisp/"
          :no-require t)))
  (use-package org
    :pin org
    :ensure t
    :ensure org-plus-contrib
    :mode (("\\.org\\'" . org-mode))
    ;; :preface
    ;; ;; emphasis markup 记号前后允许中文
    ;; (setq org-emphasis-regexp-components
    ;;       (list (concat " \t('\"{"            "[:nonascii:]")
    ;;             (concat "- \t.,:!?;'\")}\\["  "[:nonascii:]")
    ;;             " \t\r\n,\"'"
    ;;             "."
    ;;             1))
    ;; disable all extra org-mode modules to speed-up Org-mode file opening.
    :custom (org-modules nil)))

;;; [ package-lint ] -- A linting library for elisp package authors.

(use-package package-lint
  :ensure t
  :defer t
  :init (add-to-list 'display-buffer-alist '("^\\*Package-Lint\\*\\'" . (display-buffer-below-selected))))

;;; [ flycheck-package ] -- A Flycheck checker for elisp package authors.

(use-package flycheck-package
  :ensure t
  :defer t
  :after flycheck
  :init (flycheck-package-setup))



(provide 'init-package)

;;; init-package.el ends here
