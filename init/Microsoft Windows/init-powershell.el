;;; init-powershell.el --- init for PowerShell. -*- lexical-binding: t; -*-

;;; Commentary:



;;; Code:

(use-package powershell
  :ensure t
  :commands (powershell))

(use-package ob-powershell
  :ensure t
  :config
  (add-to-list 'org-babel-load-languages '(powershell . t))
  (add-to-list 'org-src-lang-modes '("powershell" . powershell))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)
  (add-to-list 'org-babel-tangle-lang-exts '("powershell" . "ps1")))



(provide 'init-powershell)

;;; init-powershell.el ends here
