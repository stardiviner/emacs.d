;;; init-macOS.el --- init for Mac OS X
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; For MacOS
;; (setq mac-option-modifier 'hyper) ; sets the Option key as Hyper
;; (setq mac-option-modifier 'super) ; sets the Option key as Super
;; (setq mac-command-modifier 'meta) ; sets the Command key as Meta
;; (setq mac-control-modifier 'meta) ; sets the Control key as Meta
;; (setq mac-function-modifier 'ns-function-modifier)

;;; Each SYMBOL is control, meta, alt, super, hyper or none.
;;; If none, the key is ignored by Emacs and retains its standard meaning.
;;; Use Command key as Meta (M-)
(setq mac-command-modifier 'meta)
;;; Use Ctrl key as Control (C-)
(setq mac-control-modifier 'control)
;;; Use Option key as Super (S-) & Hyper (H-)
(setq mac-option-modifier 'super) ; left Option key as S- ~
(setq mac-right-command-modifier 'hyper) ; right Command key as Hyper H-
;;; Use Right Option key as Alt (A-)
(setq mac-right-option-modifier 'alt) ; right Option key as Hyper A-
;;; Use Function Fn key as ?
;; (setq mac-function-modifier 'none) ; function Fn key as ?

(global-set-key [kp-delete] 'delete-char) ; sets fn-delete to be right-delete

;;; fix macOS /usr/local/bin/ path not in Emacs default path.
(setenv "PATH" (concat (getenv "PATH") ":/usr/local/bin"))
(setq exec-path (append exec-path '("/usr/local/bin")))

;;; [ exec-path-from-shell ] -- Make Emacs use the $PATH set up by the user's shell.

;;; NOTE: /Applications/Emacs.app/Contents/Info.plist was injected "PATH" when installed from Homebrew. So no need for `exec-path-from-shell' now.
;; (use-package exec-path-from-shell
;;   :ensure t
;;   :if (memq window-system '(mac ns x))
;;   ;; :custom (exec-path-from-shell-arguments nil)
;;   :init (exec-path-from-shell-initialize)
;;   :config
;;   (when (not (and (executable-find "conda") (executable-find "rustup") (executable-find "getmail")))
;;     (user-error "[WARNING] package `exec-path-from-shell' is not loading shell PATH correctly.")))

;;; [ osx-lib ] -- Basic function for Apple/OSX.

(use-package osx-lib
  :ensure t
  :defer t
  :commands (osx-lib-notify2 osx-lib-say))

;;; [ play-sound-osx ] -- Implement `play-sound-internal' for macOS.
;;; https://github.com/leoliu/play-sound-osx

(unless (and (fboundp 'play-sound-internal)
             (subrp (symbol-function 'play-sound-internal)))
  (defun play-sound-internal (sound)
    "Internal function for `play-sound' (which see)."
    (or (eq (car-safe sound) 'sound)
        (signal 'wrong-type-argument (list sound)))

    (cl-destructuring-bind (&key file data volume device)
        (cdr sound)

      (and (or data device)
           (error "DATA and DEVICE arg not supported"))

      (apply #'start-process "afplay" nil
             "afplay" (append (and volume (list "-v" volume))
                              (list (expand-file-name file data-directory)))))))

;;; [ AppleScript ]

(load "init-prog-lang-applescript")

;;; `dwim-shell-commands-open-externally' Emacs open in Xcode at line number.

(use-package dwim-shell-command
  :ensure t
  :commands (dwim-shell-commands-open-externally))

;;; [ xcode-mode ] -- A minor mode for emacs to perform Xcode like actions.

(use-package xcode-mode
  :ensure t
  :defer t)

;;; [ xcode-project ] -- A package for reading Xcode project files.

(use-package xcode-project
  :ensure t
  :defer t)

;;; [ company-xcode ]

(use-package company
  :ensure t
  :defer t
  :config
  (defun my-xcode-setup ()
    (require 'company-xcode)
    (my-company-add-backend-locally 'company-xcode)))

;;; [ launchctl ] -- Interface to launchctl on Mac OS X.

(use-package launchctl
  :ensure t
  :commands (launchctl)
  :init (add-to-list 'display-buffer-alist '("^\\*Launchctl\\*" . (display-buffer-below-selected))))

;;; [ counsel-osx-app ]

(use-package counsel-osx-app
  :ensure t
  :defer t
  :commands (counsel-osx-app)
  :bind ("H-x" . counsel-osx-app))

;;; [ sysctl ] -- Manage sysctl through Emacs.

(use-package sysctl
  :ensure t
  :defer t
  :commands (sysctl))

;; [ org-mac-link ] -- Insert/open org-mode links to items selected in various Mac Apps.

(use-package org-mac-link
  :if (eq system-type 'darwin)
  :ensure t
  :commands (org-mac-grab-link)
  :config (add-hook 'org-mode-hook (lambda () (define-key org-mode-map (kbd "C-c l") 'org-mac-grab-link))))

;; [ grab-mac-link ] -- Grab link from Mac Apps and insert it into Emacs.

(use-package grab-mac-link
  :if (and (eq system-type 'darwin) (not (featurep 'org-mac-link)))
  :ensure t
  :defer t
  :commands (grab-mac-link grab-mac-link-dwim)
  :config (add-hook 'org-mode-hook (lambda () (define-key org-mode-map (kbd "C-c l") 'grab-mac-link))))

;;; [ org-mac-iCal ] -- Imports events from iCal.app to the Emacs diary.

;; (use-package org-mac-iCal
;;   :demand t
;;   :custom ((org-agenda-include-diary t))
;;   :commands (org-mac-iCal)
;;   ;; auto sync macOS iCal with Emacs diary.
;;   :hook (after-init . org-mac-iCal)
;;   :config
;;   ;; FIXME: This hook caused org-agenda big blank line at first line.
;;   (add-hook 'org-agenda-cleanup-fancy-diary-hook
;; 	        (lambda ()
;; 	          (goto-char (point-min))
;; 	          (save-excursion
;; 	            (while (re-search-forward "^[a-z]" nil t)
;; 		          (goto-char (match-beginning 0))
;; 		          (insert "0:00-24:00 ")))
;; 	          (while (re-search-forward "^ [a-z]" nil t)
;; 	            (goto-char (match-beginning 0))
;; 	            (save-excursion
;; 		          (re-search-backward "^[0-9]+:[0-9]+-[0-9]+:[0-9]+ " nil t))
;; 	            (insert (match-string 0))))))

;;; [ org-mac-protocol ] -- create links and remember notes from a variety of OS X applications.

;; (use-package org-mac-protocol
;;   :vc (:url "https://github.com/claviclaws/org-mac-protocol"))

;;; [ siri-shortcuts ] -- A set of Emacs commands and functions for interacting with Siri Shortcuts.

(use-package siri-shortcuts
  :if (>= (string-to-number (shell-command-to-string "sw_vers -productVersion")) 12)
  :ensure t
  :commands (siri-shortcuts-run
             siri-shortcuts-gallery-open siri-shortcuts-gallery-search
             siri-shortcuts-open-app
             siri-shortcuts-edit siri-shortcuts-create))

(defun macOS-shortcuts-ocr ()
  "OCR selected area by invoke Shortcuts."
  (interactive)
  (shell-command "shortcuts run \"OCR Selected Area\"")
  (do-applescript "tell application id \"org.gnu.Emacs\" to activate"))

(defun macOS-shortcuts-translate-select-text ()
  "Translate selected text then copy result to clipboard by invoke Shortcuts."
  (interactive)
  (shell-command "shortcuts run \"Translate Selection\"")
  (do-applescript "tell application id \"org.gnu.Emacs\" to activate"))

;;; [ macOS text-to-speech command "say" ]

(defun macOS-command-say (text)
  "Invoke macOS text-to-speech command `say' to read text content."
  (async-shell-command (format "say '%s'" (replace-regexp-in-string "[']" "" text))))


;;; Take screenshot / video recording with macOS App "CleanShot X".
(when (file-exists-p "/Applications/CleanShot X.app/")
  (defun macOS-CleanShot-X (command-name)
    "Select a command of macOS App 'CleanShot X' to execute."
    (interactive (list (completing-read "macOS App CleanShot X: "
                                        '("all-in-one" "capture-area" "capture-previous-area" "capture-fullscreen" "capture-window"
                                          "self-timer" "scrolling-capture"
                                          "pin" "record-screen" "capture-text" "open-annotate" "open-from-clipboard" "add-quick-access-overlay"
                                          "open-history" "restore-recently-closed"))))
    (let* ((parameter-key (cond
                           ((member command-name '("pin" "open-annotate" "add-quick-access-overlay" "capture-text"))
                            "filepath")))
           (parameter-value (cond
                             ((member command-name '("pin" "open-annotate" "add-quick-access-overlay" "capture-text"))
                              (when (and (eq major-mode 'org-mode)
                                         (eq (car (org-element-context)) 'link))
                                (expand-file-name (org-element-property :path (org-element-context)))))))
           (url-scheme-api (if (and parameter-key parameter-value)
                               (format "cleanshot://%s?%s=%s"
                                       command-name
                                       parameter-key (url-encode-url parameter-value))
                             (format "cleanshot://%s" command-name)))
           (shell-command (format "open \"%s\"" url-scheme-api)))
      (async-shell-command shell-command))))

;;; [ raycast-mode ] -- A minor mode for Emacs for developers building Raycast extensions.

(use-package raycast-mode
  :ensure t)


(provide 'init-macOS)

;;; init-macOS.el ends here
