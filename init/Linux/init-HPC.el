;;; init-HPC.el --- init file for High Performance Computing (HPC) -*- lexical-binding: t; -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2020-2021 Christopher M. Miles, all rights reserved.

;;; Commentary:



;;; Code:

;;; [ Slurm ] -- Interaction with the SLURM job scheduling system.

(use-package slurm-mode
  :ensure t
  :defer t
  :requires (slurm-mode slurm-script-mode)
  :commands (slurm slurm-script-mode)
  ;; enable `slurm-script-mode' for Org babel source blocks.
  :init (with-eval-after-load 'org (add-to-list 'org-src-lang-modes '("slurm-script" . slurm-script))))



(provide 'init-HPC)

;;; init-HPC.el ends here
