;;; init-chef.el --- init for chef.

;;; Commentary:



;;; Code:

(with-eval-after-load 'org-capture
  (add-to-list 'org-capture-templates
               `("R" ,(format "%s\tRecord to My Cooking Recipes Reference.org" (nerd-icons-mdicon "nf-md-chef_hat" :face 'nerd-icons-yellow))
                 entry (file ,(concat org-directory "/Wiki/Cooking/Recipe/data/Manuals/My Recipes Reference/My Recipes Reference.org"))
                 "* %^{Recipe Name}"
                 :empty-lines 1
                 :jump-to-captured t)
               :append))


;;; [ org-chef ] -- A package for making a cookbook and managing recipes with Org-mode.

;; (use-package org-chef
;;   :ensure t
;;   :defer t
;;   :init
;;   (with-eval-after-load 'org-capture
;;     (add-to-list 'org-capture-templates
;;                  `("F" ,(format "%s\tRecord to My Cooking Recipes Reference.org" (nerd-icons-mdicon "nf-md-food_variant" :face 'nerd-icons-lgreen))
;;                    entry
;;                    (file "~/Org/Wiki/Chef/Recipes/Data/Manuals/My Recipes Reference/My Recipes Reference.org")
;;                    "%(org-chef-get-recipe-from-url)"
;;                    :empty-lines 1)
;;                  :append)))



(provide 'init-chef)

;;; init-chef.el ends here
