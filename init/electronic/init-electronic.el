;;; init-electronic.el --- init for Electronic
;;; -*- coding: utf-8 -*-

;;; Commentary:



;;; Code:

;;; [ SPICE ]
(load "init-spice")



(provide 'init-electronic)

;;; init-electronic.el ends here
