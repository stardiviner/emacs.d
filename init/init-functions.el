;;; init-functions.el --- my functions collections

;;; Commentary:

;;; Convensions:
;;
;; - prefix `my/' for private functions.
;; - prefix `my-' for public global functions.
;; - use `my-func' as prefix for every function.

;;; Code:

;;; Group hooks into one new hook.

(defmacro hook-modes (modes &rest body)
  "Hook BODY on mode hooks.

(defvar progish-modes
  '(prog-mode css-mode))

(hook-modes progish-modes
  (highlight-symbol-mode)
  (highlight-symbol-nav-mode))

instead of:

(hook-modes progish-modes 'highlight-symbol-mode)

It is same as:

(defun my-non-special-mode-setup ()
  ...)
(dolist (hook '(prog-mode-hook text-mode-hook css-mode-hook ...))
  (add-hook hook 'my-non-special-mode-setup))
"
  (declare (indent 1))
  `(--each ,modes
     (add-hook (intern (format "%s-hook" it))
               (lambda () ,@body))))

;;; keybindings

;;; Usage: (local-set-minor-mode-key '<minor-mode> (kbd "key-to-hide") nil).
(defun local-set-minor-mode-key (mode key def)
  "Overrides a minor MODE keybinding KEY (definition DEF) for the local buffer.
by creating or altering keymaps stored in buffer-local
`minor-mode-overriding-map-alist'."
  (let* ((oldmap (cdr (assoc mode minor-mode-map-alist)))
         (newmap (or (cdr (assoc mode minor-mode-overriding-map-alist))
                     (let ((map (make-sparse-keymap)))
                       (set-keymap-parent map oldmap)
                       (push `(,mode . ,map) minor-mode-overriding-map-alist)
                       map))))
    (define-key newmap key def)))

;;; keybinding lookup
(defun bunch-of-keybinds (key)
  "Look up where is the KEY in key-maps."
  (interactive)
  (list
   (minor-mode-key-binding key)
   (local-key-binding key)
   (global-key-binding key)
   (overlay-key-binding key)))

(defun overlay-key-binding (key)
  "Look up KEY in which key-map."
  (mapcar (lambda (keymap) (lookup-key keymap key))
          (cl-remove-if-not
           #'keymapp
           (mapcar (lambda (overlay)
                     (overlay-get overlay 'keymap))
                   (overlays-at (point))))))

;;; Keys can be bound in 4 ways. By order of precedence, they are:
;;;
;;;     at point (overlays or text-propeties),
;;;     in minor-modes,
;;;     in buffers (where major-mode or buffer-local keybinds go),
;;;     and globally.
;;;
;;; The following function queries each one of these possibilities, and returns or prints the result.
;;
;; (defun locate-key-binding (key)
;;   "Determine in which keymap KEY is defined."
;;   (interactive "kPress key: ")
;;   (let ((ret
;;          (list
;;           (key-binding-at-point key)
;;           (minor-mode-key-binding key)
;;           (local-key-binding key)
;;           (global-key-binding key))))
;;     (when (called-interactively-p 'any)
;;       (message "At Point: %s\nMinor-mode: %s\nLocal: %s\nGlobal: %s"
;;                (or (nth 0 ret) "")
;;                (or (mapconcat (lambda (x) (format "%s: %s" (car x) (cdr x)))
;;                               (nth 1 ret) "\n             ")
;;                    "")
;;                (or (nth 2 ret) "")
;;                (or (nth 3 ret) "")))
;;     ret))

;;; improved version function:
(defun key-binding-at-point (key)
  "Lookup the KEY at point in which key-map."
  (mapcar (lambda (keymap) (lookup-key keymap key))
          (cl-remove-if-not
           #'keymapp
           (append
            (mapcar (lambda (overlay)
                      (overlay-get overlay 'keymap))
                    (overlays-at (point)))
            (get-text-property (point) 'keymap)
            (get-text-property (point) 'local-map)))))

;;; [ Buffer ]

(defun my-func/open-and-switch-to-buffer (the-command the-buffer-name &optional whether-switch-to-buffer)
  "Open a `COMMAND', and switch to that `BUFFER' depend on `OPTION'.

Usage:

 (define-key Org-prefix (kbd 'o')
   (lambda ()
     (interactive)
     (my-func/open-and-switch-to-buffer 'org-agenda-list \"*Org Agenda*\" t)))
"
  (interactive)
  (if (get-buffer the-buffer-name)
      (switch-to-buffer the-buffer-name)
    (funcall the-command)
    (bury-buffer)
    (when whether-switch-to-buffer
      (switch-to-buffer the-buffer-name))))

;;; [ Screen ]

;; (display-mm-width)

;;; a helper function to detect HiDPI screen resolution.
(defun screen-get-monitor-attributes-by-name (name)
  "Get the display monitor attributes by NAME."
  (car (delete nil
               (mapcar (lambda (monitor)    ; support multiple display screens
                         (when (string-equal name (cdr (assq 'name monitor)))
                           monitor))
                       (display-monitor-attributes-list)))))

;; (defun get-text-scale ()
;;   (expt text-scale-mode-step text-scale-mode-amount))

;;; - `window-text-pixel-size'
(defun screen-hidpi-p ()
  "A helper function to detect HiDPI screen resolution."
  (let* ((attrs (screen-get-monitor-attributes-by-name "eDP1"))
         (resolution (last (assq 'geometry attrs) 2))
         (size (cdr (assoc 'mm-size attrs)))
         (display-width (display-pixel-width))
         (display-height (display-pixel-height)))
    (if (>= (or display-width (cadr resolution)) 1600) t nil)))

(defvar high-resolution-p (>= (car (window-fringes)) 16))

;;; [ Network ]

(defun my/internet-network-available-p ()
  "Detect Internet network available?"
  (if (network-lookup-address-info "baidu.com") t nil))

;;; read in encrypted JSON file key-value pairs.

(when-let ((accounts-file (expand-file-name "secrets/accounts.json.gpg" user-emacs-directory)))
  (setq my/account-file (concat user-emacs-directory "secrets/accounts.json.gpg")))

(autoload 'json-read-file "json" nil nil)

(defun my/json-read-value (file key)
  "Read in JSON `FILE' and get the value of symbol `KEY'."
  (cdr (assoc key
              (json-read-file
               (if (file-exists-p my/account-file)
                   my/account-file
                 (concat user-emacs-directory "accounts.json.gpg"))))))

;;; [ Invoke Python ]

;;; Helper functions to run Python code in Emacs.
(defun stardiviner/python-run-script-file (file)
  "Run Python script FILE through Python interpreter command."
  (shell-command-to-string
   (format "%s %s" (or (executable-find "python3") python-interpreter) file)))

(defun stardiviner/python-run-code-to-string (&rest lines)
  "Run Python code LINES through Python interpreter command."
  (shell-command-to-string
   (format "%s %s"
           (or (executable-find "python3") python-interpreter)
           (concat "-c "
                   ;; solve double quote character issue.
                   "\"" (string-replace "\"" "\\\"" (string-join lines "\n")) "\""))))

;; (stardiviner/python-run-code-to-string
;;  "import numpy as np"
;;  "print(np.arange(6))"
;;  "print(\"blah blah\")"
;;  "print('{}'.format(3))")

(defun stardiviner/python-run-code-in-repl (&rest lines)
  "Run Python code LINES through Python REPL process return last eval result."
  (let ((python-interpreter (or (executable-find "python3") python-interpreter)))
    (run-python)
    ;; `python-shell-send-string', `python-shell-internal-send-string', `python-shell-send-string-no-output'
    (dolist (line lines
                  result)
      (setf result (python-shell-send-string-no-output line)))))

;; (stardiviner/python-run-code-in-repl
;;  "import numpy as np"
;;  "print(np.arange(6))"
;;  "print(\"blah blah\")"
;;  "print('{}'.format(3))")

;;; [ Sounds ]

(autoload 'org-clock-play-sound "org-clock")

(defun sound-typing (&rest args)
  (org-clock-play-sound
   (concat user-emacs-directory
           "resources/audio/Ingress/SFX/sfx_typing.wav")))
(defun sound-tick (&rest args)
  (org-clock-play-sound
   (concat user-emacs-directory
           "resources/audio/Hacking Game/hesfx-tick.wav")))
(defun sound-tick2 (&rest args)
  (org-clock-play-sound
   (concat user-emacs-directory
           "resources/audio/Hacking Game/hesfx_untold_tick2.wav")))
(defun sound-success (&rest args)
  (org-clock-play-sound
   (concat user-emacs-directory
           "resources/audio/Ingress/SFX/sfx_ui_success.wav")))
(defun sound-newmessage (&rest args)
  (org-clock-play-sound
   (concat user-emacs-directory
           "resources/audio/Hacking Game/hesfx-newmessage.wav")))

(defun sound-voice-hacking (&rest args)
  (org-clock-play-sound
   (concat user-emacs-directory
           "resources/audio/Ingress/Speech/speech_hacking.wav")))
(defun sound-voice-deployed (&rest args)
  (org-clock-play-sound
   (concat user-emacs-directory
           "resources/audio/Ingress/Speech/speech_deployed.wav")))
(defun sound-voice-connecting (&rest args)
  (org-clock-play-sound
   (concat user-emacs-directory
           "resources/audio/Hacking Game/voice-connecting.wav")))
(defun sound-voice-loading (&rest args)
  (org-clock-play-sound
   (concat user-emacs-directory
           "resources/audio/Hacking Game/voice-loading.wav")))
(defun sound-voice-downloading (&rest args)
  (org-clock-play-sound
   (concat user-emacs-directory
           "resources/audio/Hacking Game/voice-downloading.wav")))
(defun sound-voice-closing (&rest args)
  (org-clock-play-sound
   (concat user-emacs-directory
           "resources/audio/Hacking Game/voice-closing.wav")))
(defun sound-voice-incoming-transmission (&rest args)
  (org-clock-play-sound
   (concat user-emacs-directory
           "resources/audio/Hacking Game/voice-incoming-transmission.wav")))
(defun sound-voice-complete (&rest args)
  (org-clock-play-sound
   (concat user-emacs-directory
           "resources/audio/Hacking Game/voice-complete.wav")))
(defun sound-voice-please-confirm (&rest args)
  (org-clock-play-sound
   (concat user-emacs-directory
           "resources/audio/Hacking Game/voice-please-confirm.wav")))
(defun sound-voice-please-hold (&rest args)
  (org-clock-play-sound
   (concat user-emacs-directory
           "resources/audio/Hacking Game/voice-pleasehold.wav")))
(defun sound-voice-accepted (&rest args)
  (org-clock-play-sound
   (concat user-emacs-directory
           "resources/audio/Hacking Game/voice-accepted.wav")))
(defun sound-voice-welcome (&rest args)
  (org-clock-play-sound
   (concat user-emacs-directory
           "resources/audio/Hacking Game/voice-welcome.wav")))
(defun sound-voice-secure (&rest args)
  (org-clock-play-sound
   (concat user-emacs-directory
           "resources/audio/Hacking Game/voice-secure.wav")))



(defun my/baidu-baike-convert-footnotes-to-org-mode ()
  "Convert Baidu Baike footnote [N] into Org mode format footnote [fn:N]."
  (interactive)
  (query-replace-regexp "\\[\\([0-9]+\\)\\]" "[fn:\\1]"))



(provide 'init-functions)

;;; init-functions.el ends here
