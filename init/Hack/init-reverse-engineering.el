;;; init-reverse-engineering.el --- init for Reverse Engineering

;;; Commentary:



;;; Code:

;;; [ rmsbolt ] -- An implementation of the godbolt compiler-explorer for Emacs.

(use-package rmsbolt
  :ensure t
  :commands (rmsbolt-mode rmsbolt-compile))



(provide 'init-reverse-engineering)

;;; init-reverse-engineering.el ends here
