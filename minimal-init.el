;;; minimal-init.el --- minimal init file for testing.

;;; Commentary:



;;; Code:

;;; [ Emacs init load-path ]

(add-to-list 'load-path "/usr/share/emacs/site-lisp/") ; for compiled version Emacs load system installed Emacs related packages.

(let* ((my-init-path (expand-file-name "init" user-emacs-directory))
       (default-directory my-init-path))
  (add-to-list 'load-path my-init-path)
  ;; recursively load init files.
  (normal-top-level-add-subdirs-to-load-path))

(setq load-prefer-newer t)

;;; [ package.el ] -- Emacs Lisp Package Archive (ELPA)
(require 'package)

(setq package-enable-at-startup nil)
(setq package-menu-async t)
(setq package-user-dir (expand-file-name "elpa" user-emacs-directory))


;;; 清华镜像源
(setq-default package-archives
              '(("gnu"   . "http://mirrors.tuna.tsinghua.edu.cn/elpa/gnu/")
                ("melpa" . "http://mirrors.tuna.tsinghua.edu.cn/elpa/melpa/")
                ("org"   . "http://mirrors.tuna.tsinghua.edu.cn/elpa/org/")))

(package-initialize)

;;; Load `use-package' ahead before `package-initialize' for (use-package org :pin manual ...).
;;; [ use-package ]
(eval-when-compile (require 'use-package))
(require 'bind-key)                     ; if you use any `:bind' variant
(use-package delight                    ; if you use `:delight'
  :ensure t)
(setq use-package-verbose t ; 'debug: any evaluation errors report to `*use-package*` buffer.
      use-package-always-ensure nil)

(when (eq system-type 'darwin)
  ;; For MacOS
  ;; (setq mac-option-modifier 'hyper) ; sets the Option key as Hyper
  ;; (setq mac-option-modifier 'super) ; sets the Option key as Super
  ;; (setq mac-command-modifier 'meta) ; sets the Command key as Meta
  ;; (setq mac-control-modifier 'meta) ; sets the Control key as Meta
  ;; (setq mac-function-modifier 'ns-function-modifier)

  ;; Each SYMBOL is control, meta, alt, super, hyper or none.
  ;; Use Alt key as Hyper
  (setq mac-option-modifier 'super) ; S- ~
  ;; Use Command key as Meta
  (setq mac-command-modifier 'meta)
  (setq mac-right-option-modifier 'hyper)
  (global-set-key [kp-delete] 'delete-char) ; sets fn-delete to be right-delete

  ;; fix macOS /usr/local/bin/ path not in Emacs default path.
  (setenv "PATH" (concat (getenv "PATH") ":/usr/local/bin"))
  (setq exec-path (append exec-path '("/usr/local/bin")))

  ;; [ exec-path-from-shell ] -- Make Emacs use the $PATH set up by the user's shell.
  ;; No need in new Emacs version after 29.0
  ;; (use-package exec-path-from-shell
  ;;   :ensure t
  ;;   :if (memq window-system '(mac ns x))
  ;;   :init (exec-path-from-shell-initialize))
  )

;;; [ Org Mode (source code) ]
(if (file-exists-p "~/Code/Emacs/org-mode/")
    (progn
      ;; disable Emacs built-in Org Mode
      (cl-case system-type
        (gnu/linux
         (delete (format "/usr/local/share/emacs/%s/lisp/org" emacs-version) load-path)
         (delete "/usr/share/emacs/site-lisp/org/" load-path))
        (darwin
         (delete "/Applications/Emacs.app/Contents/Resources/lisp/org/" load-path)
         (delete (format "/opt/homebrew/opt/emacs-plus@29/share/emacs/%s/lisp/org/" emacs-version) load-path)))
      (use-package org
        :pin manual
        :load-path "~/Code/Emacs/org-mode/lisp/"
        :defer t
        :mode (("\\.org\\'" . org-mode))
        ;; :preface
        ;; ;; emphasis markup 记号前后允许中文
        ;; (setq org-emphasis-regexp-components
        ;;       (list (concat " \t('\"{"            "[:nonascii:]")
        ;;             (concat "- \t.,:!?;'\")}\\["  "[:nonascii:]")
        ;;             " \t\r\n,\"'"
        ;;             "."
        ;;             1))
        ;; disable all extra org-mode modules to speed-up Org-mode file opening.
        :custom (org-modules nil)
        ;; load org before org-mode init files settings.
        :init (require 'org)
        ;; add source code version Org-mode Info into Emacs.
        (if (file-exists-p "~/Code/Emacs/org-mode/doc/org")
            (with-eval-after-load 'info
              (add-to-list 'Info-directory-list "~/Code/Emacs/org-mode/doc/")
              (info-initialize)))
        (use-package org-contrib
          :pin manual
          :load-path "~/Code/Emacs/org-contrib/lisp/"
          :no-require t)))
  (use-package org
    :pin org
    :ensure t
    :ensure org-plus-contrib
    :mode (("\\.org\\'" . org-mode))
    ;; :preface
    ;; ;; emphasis markup 记号前后允许中文
    ;; (setq org-emphasis-regexp-components
    ;;       (list (concat " \t('\"{"            "[:nonascii:]")
    ;;             (concat "- \t.,:!?;'\")}\\["  "[:nonascii:]")
    ;;             " \t\r\n,\"'"
    ;;             "."
    ;;             1))
    ;; disable all extra org-mode modules to speed-up Org-mode file opening.
    :custom (org-modules nil)))

;;=============================== helpful packages ==============================
;;; add your customizations from here

(use-package ace-window
  :ensure t
  :bind ("C-x C-j" . ace-window))

;;=========================== minimal config required for debugging===============

(setq org-src-fontify-natively t)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (shell . t)))



(provide 'minimal-init)

;;; minimal-init.el ends here
