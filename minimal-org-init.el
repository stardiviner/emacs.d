;;; minimal-org-init.el --- minimal Org mode init file for testing.

;;; Commentary:

;;; $ emacs -Q -l ~/.config/emacs/minimal-org-init.el

;;; Code:

;;; [ package.el ] -- Emacs Lisp Package Archive (ELPA)
(require 'package)

(setq package-enable-at-startup nil)
(setq package-menu-async t)
(setq package-user-dir (expand-file-name "elpa" user-emacs-directory))

(package-initialize)

;;; Load `use-package' ahead before `package-initialize' for (use-package org :pin manual ...).
;;; [ use-package ] -- A configuration macro for simplifying your .emacs.

;;; load `use-package' from Emacs built-in.
(eval-when-compile (require 'use-package))
(require 'use-package)

(setq use-package-verbose t ; 'debug: any evaluation errors report to `*use-package*` buffer.
      use-package-always-ensure nil)

;; [ macOS ]
(when (eq system-type 'darwin)
  ;; For MacOS
  ;; (setq mac-option-modifier 'hyper) ; sets the Option key as Hyper
  ;; (setq mac-option-modifier 'super) ; sets the Option key as Super
  ;; (setq mac-command-modifier 'meta) ; sets the Command key as Meta
  ;; (setq mac-control-modifier 'meta) ; sets the Control key as Meta
  ;; (setq mac-function-modifier 'ns-function-modifier)

  ;; Each SYMBOL is control, meta, alt, super, hyper or none.
  ;; Use Alt key as Hyper
  (setq mac-option-modifier 'super) ; S- ~
  ;; Use Command key as Meta
  (setq mac-command-modifier 'meta)
  (setq mac-right-option-modifier 'hyper)
  (global-set-key [kp-delete] 'delete-char) ; sets fn-delete to be right-delete

  ;; fix macOS /usr/local/bin/ path not in Emacs default path.
  (setenv "PATH" (concat (getenv "PATH") ":/usr/local/bin"))
  (setq exec-path (append exec-path '("/usr/local/bin")))

  ;; [ exec-path-from-shell ] -- Make Emacs use the $PATH set up by the user's shell.
  (use-package exec-path-from-shell
    :ensure t
    :if (memq window-system '(mac ns x))
    :init (exec-path-from-shell-initialize))
  )

;;; [ Org Mode (source code) ]
(if (not (file-exists-p "~/Code/Emacs/org-mode/lisp/"))
    (use-package org
      :pin org
      :ensure t
      :ensure org-plus-contrib
      :mode (("\\.org\\'" . org-mode))
      ;; disable all extra org-mode modules to speed-up Org-mode file opening.
      :custom (org-modules nil))
  
  ;; disable Emacs built-in Org Mode
  (delete (format "/usr/local/share/emacs/%s/lisp/org" emacs-version) load-path)
  (delete "/usr/share/emacs/site-lisp/org/" load-path)
  (use-package org
    :pin manual
    :load-path "~/Code/Emacs/org-mode/lisp/"
    :defer t
    :mode (("\\.org\\'" . org-mode))
    ;; disable all extra org-mode modules to speed-up Org-mode file opening.
    :custom (org-modules nil)
    ;; load org before org-mode init files settings.
    :init (require 'org)
    ;; add source code version Org-mode Info into Emacs.
    (if (file-exists-p "~/Code/Emacs/org-mode/doc/org")
        (with-eval-after-load 'info
          (add-to-list 'Info-directory-list "~/Code/Emacs/org-mode/doc/")
          (info-initialize)))
    (use-package org-contrib
      :pin manual
      :load-path "~/Code/Emacs/org-contrib/lisp/"
      :no-require t)))

;;=============================== helpful packages ==============================
;;; add your customizations from here

(use-package ace-window
  :ensure t
  :bind ("C-x C-j" . ace-window))

;;=========================== minimal config required for debugging===============

(setq org-src-fontify-natively t)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (shell . t)))




(provide 'minimal-org-init)

;;; minimal-org-init.el ends here
